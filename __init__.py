import subprocess
from phdlib.settings import *

# log revision_id for results class
try:
    revision_id = subprocess.check_output(['git', 'rev-parse', 'HEAD']).decode(
        'utf-8').strip('\n')
except:
    revision_id = 'Not defined'
