import logging

import pandas as pd
from phdlib.data import manager, monthly
from phdlib.metrics.portfolio import CriteriaPortfolio
from phdlib.misc import logger_settings

__author__ = 'mmess'

logger = logging.getLogger(__name__)


class FactorAnalysis():
    """ Wrapper Class

    Around the data independent portfolio.CriteriaPortfolio and the projects
    specific data API.

    Parameters
    ----------
    criteria: pd.DataFrame
        index should be dates, columns asset ids, values criteria values
    simple_returns: pd.DataFrame
        simple returns (same columns + index as criteria)
    market_value: pd.DataFrame
        market values (same columns + index as criteria)
    start_date: str/pd.Timestamp
        when analysis should start
    end_date: str/pd.Timestamp
        when analysis should end
    ascending: bool
        if criteria is from lowest(long) to highest(short),
        if vice versa set to False
    size: str
        which size 'large', 'mid', 'small' or 'all' (None->'all')
    trade_freq: int
        in which frequency should the rebalancing/trading take place
        (orients itself on the data dates)
    min_max_mkt_cap: pd.DataFrame
        with date as index and columns "min" and "max" of mkt_cap values
    buckets: str
        either 
    """


    def __init__(self, criteria, simple_returns=None, market_value=None,
                 start_date='19650101', end_date='20141231',
                 ascending=True, size=None, buckets='ff_style',
                 trade_freq=1, weighting='equal', alt_criteria=None,
                 rets='adj_rets', min_max_mkt_cap=None, ):

        self.criteria = criteria
        self._returns = simple_returns
        self._market_value = market_value
        # start/end date min max depending on criteria or input range
        self.start_date = max(pd.Timestamp(start_date), criteria.index.min())
        self.end_date = min(pd.Timestamp(end_date), criteria.index.max())
        self.size = size
        self._portfolio = None
        self.ascending = ascending
        self.buckets = buckets
        self.trade_freq = trade_freq
        self.weighting = weighting
        self.alt_criteria = alt_criteria
        self.rets = rets
        self._min_max_mkt_cap = min_max_mkt_cap

    @property
    def returns(self):
        if self._returns is None:
            logger.info('Loading return data...')
            tmp = manager.get_monthly_data(start_date=self.start_date,
                                           end_date=self.end_date,
                                           size=self.size,
                                           demean_ret=False,
                                           columns=['PERMNO', 'adj_ret',
                                                    'ret'],
                                           drop_na=True,
                                           ret_str=self.rets)

            tmp = tmp.pivot(index='date', columns='PERMNO', values='ret')

            # align the two according to the criteria
            self._returns, criteria = tmp.align(self.criteria, join='right')

        return self._returns

    @property
    def market_cap(self):
        if self._market_value is None:
            logger.info('Loading market_cap data...')
            # get market cap from CRSP raw data directly
            # TODO add panel feature for HDF store
            tmp = monthly.get_crsp_monthly(['mkt_cap'])['mkt_cap']
            # align the two according to the criteria
            self._market_value, criteria = tmp.align(self.criteria,
                                                     join='right')

        return self._market_value

    @property
    def min_max_mkt_cap(self):
        if self._min_max_mkt_cap is None:
            stocks = manager.get_monthly_data(start_date=self.start_date,
                                              end_date=self.end_date,
                                              size='all', columns=('mkt_cap',),
                                              keep_columns=('mkt_cap',),
                                              normalize=False, fc_lead=0)
            df = stocks.pivot(index='date', columns='PERMNO', values='mkt_cap')
            min_max = pd.concat([df.min(axis=1),df.max(axis=1)], axis=1)
            min_max.columns = ['min', 'max']
            self._min_max_mkt_cap = min_max

        return self._min_max_mkt_cap

    @property
    def portfolio(self):
        if self._portfolio is None:
            self._portfolio = CriteriaPortfolio(self.criteria,
                                                self.returns,
                                                self.market_cap,
                                                return_shift=1,
                                                ascending=self.ascending,
                                                buckets=self.buckets,
                                                rebalance_freq=self.trade_freq,
                                                weighting=self.weighting,
                                                alt_criteria=self.alt_criteria,
                                                min_max_mkt_cap=self.min_max_mkt_cap,
                                                )
        return self._portfolio


if __name__ == '__main__':
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
