import unittest
from unittest import TestCase

import numpy as np
import pandas as pd
from phdlib.finance.asset_analysis import ReturnAnalysis, SRTest_LW
from phdlib.data.fama_french import Factor, Sector


class TestPortfolioPerformance(TestCase):

    @classmethod
    def setUpClass(cls):

        periods = 1000
        dates = pd.date_range('19860825', periods=periods, freq='d')

        cls.simple_ret = pd.Series(np.exp(np.random.randn(periods)/100)-1,
                                   index=dates)
        cls.factor_ret = pd.DataFrame(np.exp(np.random.randn(periods,3)/100),
                                      columns=['a','b','c'], index=dates)

        cls.sector_ret = Sector().get(schema='Sec10')
        cls.five_factor = Factor().get(model='five-factor')

        cls.PP = ReturnAnalysis(simple_rets=cls.simple_ret,
                                bm_simple_rets=cls.factor_ret['a'])
        cls.SecRA = ReturnAnalysis(simple_rets=cls.sector_ret['NoDur'],
                                   bm_simple_rets=cls.five_factor[
                                       'Mkt-RF'])

    def test_factor_regression(self):

        # with self.assertRaises(ValueError):
        #     self.PP.factor_regression(self.factor_ret[2:])
        res = self.SecRA.factor_regression(self.five_factor)

        print(res.summary())

    def test_bm_regression(self):
        res = self.PP.bm_regression()
        print(res.summary2())
        print(res.rsquared)


    def test_correlation(self):
        print(self.PP.corr)


    def test_SR_difference(self):
        res = self.PP.test_diff_sharpe_ratio()
        self.assertEqual(type(res), np.float)


    def test_sharpe_ratio_test(self):
        res = ReturnAnalysis.sharpe_ratio_test(2,2,0.1,100)
        self.assertEqual(type(res), np.float)


    def test_performance_stats(self):

        print(self.PP.performance_stats())

class TestSharpeRatioTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.sector_ret = Sector().get(schema='Sec10')
        cls.five_factor = Factor().get(model='five-factor')

    def setUp(self):
        self.sr = SRTest_LW(self.sector_ret.iloc[:, 0],
                            self.sector_ret.iloc[:, 1])

    def test_calc_moments(self):
        self.assertTrue(len(self.sr.moments)==4)

    def test_y_hat(self):
        self.assertTrue(self.sr.y_hat().shape[1] == 4)

    def test_alpha_hat(self):
        res = self.sr.alpha_hat(self.sr.y_hat())
        self.assertTrue(isinstance(res, np.float))
        self.assertTrue(res > 0)

    def test_psi(self):
        self.sr.psi(self.sr.y_hat())

    def test_standard_error_hat(self):
        self.sr.standard_error_hat()

    def test_p_value(self):
        self.assertTrue(self.sr.p_value() >= 0)
        self.assertTrue(self.sr.p_value() <= 1)


if __name__ == '__main__':
    unittest.main()