import logging

import pandas as pd
from pandas.tseries import offsets as off

logger = logging.getLogger(__name__)

class ReturnIndex:
    _freq_map = {'M': off.MonthEnd, 'MS': off.MonthBegin, 'd': off.Day,
                 'B': off.BusinessDay, 'w': off.Week}

    def __init__(self, simple_returns, base=100):
        """
        
        Parameters
        ----------
        simple_returns: pd.DataFrame
            with simple returns as values
        base: int/float
            which base for the Return index (default 100)
        """
        self.returns = simple_returns
        self.base = base
        self._return_index = None

    @property
    def index_level(self):
        if self._return_index is None:
            self._return_index = self.calc_return_idx(self.returns, self.base)

        return self._return_index

    def median(self, column_index=False, even_point='lower'):
        """ Returns the median portfolio, as of the latest RI level.

        Parameters
        ----------
        column_index: bool
            if it should return the column index (True) or the RI series
        even_point: str
            'lower' or 'higher' - in case of equal obs median is the average.
             Chose either "lower" ("higher") for the closest lower (higher)
            point of 50% percentile.

        Returns
        -------
        str/pd.Series
        """

        last_values = self.index_level.iloc[-1, :]
        if even_point == 'lower':
            median_col = last_values[
                last_values.rank(pct=True) <= 0.5].idxmax()
        else:
            median_col = last_values[
                last_values.rank(pct=True) >= 0.5].idxmin()

        if column_index is False:
            return self.index_level[median_col]
        else:
            return median_col


    @staticmethod
    def calc_return_idx(simple_returns, base=100, freq=None):
        """ Calculates a return index based on simple returns
        
        Parameters
        ----------
        simple_returns: pd.DataFrame
            with dates as index and asset_ids as columns, 
            simple returns as values
        base: float
            starting value of Return Index
        freq: str
            a valid pandas frequency string

        Returns
        -------
        pd.DataFrame
            return index values, normalized with base
        """

        RI = (simple_returns + 1).cumprod() * base
        RI.dropna(inplace=True)
        if freq is None:
            freq = pd.infer_freq(simple_returns.index)
            if freq is None:
                freq = 'M'
        # reset the index to 100
        start_date = RI.index[0] - ReturnIndex._freq_map[freq]()
        start_row = pd.DataFrame(columns=simple_returns.columns,
                                 index=[start_date])
        start_row.loc[start_date, :] = base
        # add the 100 to return index
        return pd.concat([start_row, RI])


def main():
    pass


if __name__ == '__main__':
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
    main()
