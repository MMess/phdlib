"""
Cross-sectional simulation
"""
import logging

import numpy as np
import pandas as pd
import numpy.random as npr
import arch

from phdlib.metrics.simulation_tools import (SimConstantCorrMat,
                                             reshuffle_correlation_mat)

logger = logging.getLogger(__name__)


def ar_process(constant, coef, volatility, periods=10,
               burn_in_periods=500):
    """ AR (1) process simulation: mu_t = constant + coef mu_t-1 + epsilon_t

    Parameters
    ----------
    constant: float
    coef: float
    volatility: float
    periods: int
    burn_in_periods:int

    Returns
    -------

    """

    total_periods = burn_in_periods + periods
    innovations = np.random.randn(total_periods) * volatility
    mu_ = constant / (1 - coef)

    for burn_t in range(burn_in_periods):
        mu_ = constant + coef * mu_ + innovations[burn_t]

    mu_over_time = np.zeros([periods])
    mu_over_time[0] = mu_
    for t in range(1, periods):
        mu_over_time[t] = constant + coef * mu_over_time[t - 1] + innovations[
            burn_in_periods + t]

    return mu_over_time


class GarchVola:

    def __init__(self, omega=None, alpha=None, beta=None, burn=100,
                 nobs=500):
        """ Simulates monthly garch volatility based on daily random draws

        Garch(1,1) with normal estimate based on daily US FF market
        excess returns from 1962-2014
        omega	    0.007925908
        alpha    	0.092745656
        beta	    0.901928455

        """
        if nobs > 6000:
            raise NotImplementedError('Pandas error with dates larger than')

        if omega is None:
            omega = 0.007925908
        if alpha is None:
            alpha = 0.092745656
        if beta is None:
            beta = 0.901928455

        self.nobs = nobs
        self.omega = omega
        self.alpha = alpha
        self.beta = beta
        self.burn = burn

        # specify monthly frequency
        self.dates = pd.date_range(start='16991231', periods=nobs, freq='m')
        # get underlying daily frequency for the range
        self.days = pd.date_range(start=self.dates[0], end=self.dates[-1],
                                  freq='b')

    def simulate(self, scale='monthly'):
        """ Simulates the Garch series

        Parameters
        ----------
        annualize: bool
        scale: str
            {'daily','monthly','annualized'} if vola should be scaled to
            reflect frequency

        Returns
        -------
        sigma: numpy.array
            with length=nobs
        """

        garch = arch.univariate.GARCH()
        parameter = np.array([self.omega, self.alpha, self.beta])
        rets, sigma2 = garch.simulate(parameters=parameter,
                                      nobs=len(self.days),
                                      rng=npr.standard_normal,
                                      burn=100)
        sigma = sigma2**0.5
        sigma = pd.Series(sigma/100, index=self.days)
        sigma = sigma.resample('M').last()

        if scale == 'monthly':
            sigma = sigma * (255 / 12) ** 0.5
        elif scale == 'annualized':
            sigma = sigma * 255 ** 0.5

        return sigma.values


class CrossSectionSimulator:

    # students T - degrees of freedom based on market data from 1963-2014
    dof = 7.14463717005582

    def __init__(self, num_stocks=4000, periods=50 * 12, num_fc=100,
                 market_volatility=None, num_priced_factors=6,
                 num_unpriced_factors=6, fc_corr_pairs=None,
                 fc_return_fun='standard', market_distribution='normal',
                 risk_premia_bounds=(0.015, 0.03), market_risk_premia=0.055,
                 freq_adj=12, seed=1234, r_sqrd=0.00415,
                 **ret_fun_kwargs):
        """ Cross section simulations

        As described in Messmer and Audrino (2017)

        Parameters
        ----------
        num_stocks: int
            how many stocks
        periods: int
            how many periods
        num_fc: int
            total number of FC
        market_volatility: np.array
            market_volatility values
        num_priced_factors: int
            number of priced factors
        num_unpriced_factors: int
            number of unpriced factors
        fc_return_fun: str or user-defined fun
            {'standard', 'model_3')}
            standard: risk premia/factor exposure are linear function of FC
            model_3: referes to Daniel and Titmann (1997) model 3
        fc_corr_pairs: tuple of tuples
            where ((corr, index_1, index_2, ...index_n)) where corr
            determines correlation of group and index_i indicates affected FC
        risk_premia_bounds: tuple
            upper and lower bound for uniform draws of factor risk premia
        market_risk_premia: float
            market risk premia in annualized terms
        market_distribution: str
            {'Normal', 'StudentsT'}, provided market vola is assumed to be
            estimated with same distributional assumption
        freq_adj: int
            how freq should be adjusted
        seed: int
            numpy seed number to allow for re-construction of results
        r_sqrd: float
            period specific r-sqrd
        ret_fun_kwargs:
        """

        self.periods = periods
        self.freq_adj = freq_adj
        self.num_stocks = num_stocks
        self.num_fc = num_fc
        self.num_priced_factors = num_priced_factors
        self.num_unpriced_factors = num_unpriced_factors
        self.num_factors = num_priced_factors + num_priced_factors

        self.fc_return_fun = fc_return_fun
        self.risk_premia_bounds = risk_premia_bounds
        self.market_risk_premia = market_risk_premia
        # set - numpy seed
        npr.seed(seed)
        # fixed for all simulations based on same instance
        # (constant seed fixed generally)
        self.risk_premia = self.get_risk_premia()

        self.market_distribution = market_distribution
        self.market_volatility = market_volatility
        self.fc_corr_matrix = None
        if fc_corr_pairs is None:
            fc_corr_pairs = ((0.9, 1, 12), (0.9, 2, 6), (0.9, 7, 13),
                             (0.4, 3, 14), (0.4, 4, 8), (0.4, 9, 15))
        self.fc_corr_groups = fc_corr_pairs
        # regression r squared - how much does the risk-premia signal explain
        self.r_sqrd = r_sqrd
        # equal distribution of vola value to idio and remaining factors
        vola_level = self.get_vola_value()/self.num_factors
        self.idio_vola = vola_level
        # vector of factor volas (first element will be replaced
        # by market vola in simulation part)
        self.factor_vola_vector = vola_level * np.ones(self.num_factors)
        self._ret_fun_kwargs = ret_fun_kwargs

        logger.info(self.main_log_msg())

    def main_log_msg(self):
        """ Creates run log msg
        """

        if callable(self.fc_return_fun):
            fc_return_fun = 'user defined'
        else:
            fc_return_fun = self.fc_return_fun

        msg = (
            "Initialized cross-section simulation with the following settings:"
            f"\n\t Num of stocks: {self.num_stocks}"
            f"\n\t Num of periods {self.periods}"
            f"\n\t Num of FC: {self.num_fc} "
            f"(priced factors: {self.num_priced_factors},"
            f" unpriced factors: {self.num_unpriced_factors})"
            f"\n\t Market factor distribution: {self.market_distribution}"
            f"\n\t FC return function: {fc_return_fun}"
        )

        return msg

    @property
    def signal_to_noise_ratio(self):
        """ Returns signal-to-noise ratio based on r-squared:

        Returns
        -------
        SNR: float
        """

        return self.r_sqrd / (1-self.r_sqrd)

    def simulate(self):

        # constant zero factor correlations
        factors_corr_matrix = np.diag(np.ones(self.num_factors))

        # simulate correlation matrix of firm characteristics
        self.fc_corr_matrix = self.get_fc_corr_matrix()

        data = list()

        for period in range(self.periods):

            # assign market volatility of specific period (rest constant over)
            self.factor_vola_vector[0] = self.market_volatility[period]
            vola_matrix = np.diag(self.factor_vola_vector)
            # factor cov matrix = diagonal(vola)*corr_matrix*diagonal(vola)
            factor_risk_cov = vola_matrix @  factors_corr_matrix @ vola_matrix

            factor_innovations = npr.multivariate_normal(
                self.risk_premia[period,:], factor_risk_cov)

            # if not normal market innovations, random draw market factor
            if self.market_distribution == 'StudentsT':
                factor_innovations[0] = npr.standard_t(self.dof) * \
                                        self.factor_vola_vector[0]

            # draw independent stock innovations
            idio_innovations = self.idio_vola * npr.standard_normal(
                self.num_stocks)

            # observed zero mean firm characteristics
            fc_mu = np.zeros(self.num_fc)
            fc_innovations = npr.multivariate_normal(fc_mu,
                                                     self.fc_corr_matrix,
                                                     self.num_stocks)
            # take factor loading of firm chars
            simple_returns = self.get_returns(fc_innovations,
                                              factor_innovations,
                                              idio_innovations)

            period_data = pd.DataFrame(fc_innovations)
            period_data['rets'] = simple_returns
            period_data['date'] = period
            data.append(period_data)

        data = pd.concat(data)

        return data

    def get_risk_premia(self):
        """ Prepares the risk premia

        Returns
        -------
        risk_premia: np.array
            dim: Periods x NumFactors contains risk premia
        """
        npr.seed()

        lb = self.risk_premia_bounds[0]
        ub = self.risk_premia_bounds[1]

        if isinstance(self.market_risk_premia, float):
            mkt_rp = np.array(self.market_risk_premia)
            mkt_rp = np.tile(mkt_rp, (self.periods, 1))
        else:
            mkt_rp = self.market_risk_premia

        # TODO model AR(1) model here

        priced_rp = npr.uniform(lb, ub, self.num_priced_factors - 1)
        unpriced_rp = np.zeros(self.num_unpriced_factors)

        # matrix form to incorporate time-varying risk-premia
        priced_rp = np.tile(priced_rp, (self.periods, 1))
        unpriced_rp = np.tile(unpriced_rp, (self.periods, 1))

        risk_premia = np.concatenate((mkt_rp, priced_rp, unpriced_rp), axis=1)

        return risk_premia

    def get_fc_corr_matrix(self):
        """ Simulates and constructs FC correlation matrix

        Returns
        -------
        sccm: np.array
            simulate constant correlation matrix
        """
        group_corr, group_size, swap_pairs = list(), list(), list()
        fc_counter = 0
        for corr_group in self.fc_corr_groups:
            # save correlation of group
            group_corr.append(corr_group[0])
            # determine group sizes
            group_size.append(len(corr_group[1:]))

            for fc_idx in corr_group[1:]:
                swap_pairs.append((self.num_fc - fc_counter -1, fc_idx))
                fc_counter += 1

        # TODO check swap pair values for uniqueness otherwise misbehaves
        group_size = group_size + (self.num_fc - fc_counter) * [1]
        group_corr = group_corr + (self.num_fc - fc_counter) * [0.5]
        num_groups = len(group_size)
        # simulate correlation matrix
        # flip order of groups to make reshuffle feasible
        sccm = SimConstantCorrMat(num_groups=num_groups,
                                  group_size=group_size[::-1],
                                  base_correlations=group_corr[::-1],
                                  epsilon=0.07,
                                  delta_noise=0.03,
                                  noise_space_dim=50).sim_corr_mat

        # reshuffle correlation matrix to match supplied group fc indices
        sccm = reshuffle_correlation_mat(sccm, swap_pairs)

        return sccm

    def get_vola_value(self):
        """ Determines excess volatility based on signal to noise ratio

        Corrects for risk-premia signal volatility and market vola. For
        details, see Messmer and Audrino (2017) appendix

        Returns
        -------
        volatility: float
        """

        # signal variance
        signal_var = (self.risk_premia **2).sum()

        mean_mkt_vola = self.market_volatility.mean()

        # variance of signal scaled - minus variance of market (first factor)
        variance = signal_var / self.signal_to_noise_ratio - mean_mkt_vola ** 2

        return variance ** 0.5

    def get_returns(self, fc_innovations, factor_innovation,
                    idio_innovations):
        """ Simulate stock returns

        Parameters
        ----------
        fc_innovations: np.array
        factor_innovation: np.array
        idio_innovations: np.array

        Returns
        -------
        rets: np.array
            simple returns of size (num_stocks x 1)
        """
        # if user defined return function, requires three input arguments
        if callable(self.fc_return_fun):
            rets = self.fc_return_fun(fc_innovations, factor_innovation,
                                      idio_innovations, **self._ret_fun_kwargs)

        if self.fc_return_fun == 'standard':
            # "standard" return function
            fc_exposure = fc_innovations[:, :self.num_factors]
            # simple_returns =  FC_exposure(t) * f(t) + epsilon(t)
            rets = fc_exposure @ factor_innovation + idio_innovations

        elif self.fc_return_fun == 'model3':
            # use Daniel and Titman (1997) model 3
            rets = self.model_3(fc_innovations, factor_innovation,
                                idio_innovations)

        else:

            raise NotImplementedError(f'Return function {self.fc_return_fun}'
                                      f' does not exists')

        return rets

    def model_3(self, fc_innovation, factor_innovation, idio_innovation,
                corr=0.9):
        """ Kent Daniel and Titmann (1997) Model 3

        Parameters
        ----------
        fc_innovation
        factor_innovation
        idio_innovation
        corr

        Returns
        -------

        """
        num_stocks = len(fc_innovation)
        num_factors = len(factor_innovation)
        fc_exposure = fc_innovation[:, num_factors].copy()

        mu = np.array([0, 0])
        cov_mat = np.array([[1, corr], [corr, 1]])

        # correlated pair consisting of an observed fc and latent factor
        corr_fx_exposure = npr.multivariate_normal(mu, cov_mat, num_stocks)
        fc_exposure[:,0] = corr_fx_exposure[:,1]

        # passed by reference so overwrite outer instance value
        fc_innovation[:, [0]] = corr_fx_exposure[:, 0]

        # subtract risk-premia from factor 0
        factor_innovation[0] = factor_innovation[0] - self.risk_premia[0]

        rets = fc_exposure @ factor_innovation + idio_innovation + \
               corr_fx_exposure[:, 0] * self.risk_premia[0]

        return rets


def example():
    volas = GarchVola(nobs=600).simulate()
    cs = CrossSectionSimulator(num_stocks=40, market_volatility=volas)
    df = cs.simulate()
    print(df)

def main():
    pass


if __name__ == '__main__':
    logging.root.addHandler(logging.StreamHandler())
    logger.setLevel('INFO')
    example()
