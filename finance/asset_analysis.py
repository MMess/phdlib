import numpy as np
import pandas as pd
from statsmodels import api as sm
from statsmodels.tsa.ar_model import AR
from scipy.stats import norm


class ReturnAnalysis():
    """ Return Index Analysis: standard return index data analysis

    Parameters
    ----------
    simple_rets: pd.Series
        the ReturnIndex simple return series (index are dates)
    bm_simple_rets: pd.Series
        simple return of benchmark (same index)
    rf_rate: pd.Series
        risk-free rate (same return frequency should be provided)

    """

    def __init__(self, simple_rets, bm_simple_rets=None, rf_rate=None,
                 annu_factor=None):

        simple_rets.dropna(inplace=True)
        if bm_simple_rets is not None:
            simple_rets, bm_simple_rets = simple_rets.align(bm_simple_rets,
                                                            join='left')
        self.simple_returns = simple_rets
        self.bm_returns = bm_simple_rets
        if annu_factor is None:
            self.annu_factor = 1
        else:
            self.annu_factor= annu_factor

    def factor_regression(self, factor_returns, bm=False, cov='HAC',
                          lags=1):

        if bm is False:
            rets = self.simple_returns
        else:
            rets = self.bm_returns

        # align the data
        if factor_returns is None:
            factor_returns = pd.DataFrame(index=rets.index, columns=['Alpha'])

        y_ret, x_rets = rets.align(factor_returns, join='inner')
        x_rets['Alpha'] = 1

        if x_rets.isnull().any().any():
            raise ValueError('Data missing in factor_returns')

        # regress portfolio return on provided factors
        model = sm.OLS(y_ret, x_rets)
        results = model.fit(cov_type=cov, cov_kwds={'maxlags':lags})
        return results

    def bm_regression(self):
        """ run regression with constant to determine beta and alpha w.r.t BM
        Returns
        -------

        """
        if self.bm_returns is None:
            raise ValueError('Benchmark return not defined')

        y = pd.DataFrame(self.bm_returns)
        y['Alpha'] = 1
        y.rename(columns={self.bm_returns.name:'Beta'}, inplace=True)
        model = sm.OLS(self.simple_returns, y)
        results = model.fit()
        return results

    def performance_stats(self, which=['mean', 'var', 'std', 'SR'],
                          annual_factor=12):
        """ Calculates performance stats of the portfolio based on simple
        returns using arithmetic averages

        Returns
        -------
        """
        #TODO add multiasset-analysis (new class)
        assets = [('Portfolio', self.simple_returns)]
        if self.bm_returns is not None:
            assets.append(('BM', self.bm_returns))

        df = pd.DataFrame(index=which, columns=[x[0] for x in assets])

        for name, ret in assets:
            for measure in which:
                if measure in ['mean', 'SR']:
                    df.loc['mean', name] = ret.mean() * annual_factor
                elif measure in ['std', 'SR']:
                    df.loc['std', name] = ret.std() * np.sqrt(annual_factor)
                elif measure == 'var':
                    df.loc['var', name] = ret.var() * annual_factor

        if 'SR' in which:
            df.loc['SR', :] = (df.loc['mean', :] / (
                df.loc['std', :] * np.sqrt(annual_factor))).tolist()

        return df

    @property
    def mean(self):
        return self.simple_returns.mean() * self.annu_factor

    @property
    def mean_bm(self):
        return self.bm_returns.mean() * self.annu_factor

    @property
    def std(self):
        return self.simple_returns.std() * np.sqrt(self.annu_factor)

    @property
    def std_bm(self):
        return self.bm_returns.std() * np.sqrt(self.annu_factor)

    @property
    def var(self):
        return self.simple_returns.var() * self.annu_factor

    @property
    def var_bm(self):
        return self.bm_returns.var() * self.annu_factor

    @property
    def SR(self):
        return self.mean / (self.std * np.sqrt(self.annu_factor))

    @property
    def SR_bm(self):
        return self.mean_bm / (self.std_bm * np.sqrt(self.annu_factor))

    @property
    def corr(self):
        return np.corrcoef(self.simple_returns, self.bm_returns)[0,1]

    @property
    def T(self):
        return len(self.simple_returns)

    def test_diff_sharpe_ratio(self, test='LedoitWolf'):
        """ Applies Sharpe ratio comparison of portfolio returns and bm returns

        Returns
        -------

        """
        if test == 'LedoitWolf':
            return SRTest_LW(self.simple_returns, self.bm_returns)
        elif test == 'JobsonKorkieMemmel':
            return SRTest_JKM(self.simple_returns, self.bm_returns)


class SRTest_JKM:
    def __init__(self, simple_rets_1, simple_rets_2):
        """ Sharpe Ratio Test: Jobson and Korkie with Memmel Correction


        Jobson, J.D., Korkie, B.M., 1981. Performance hypothesis testing with
          the Sharpe and Treynor measures. Journal of Finance 36, 889–908.
        Memmel, C., 2003. Performance hypothesis testing with the Sharpe
          ratio. Finance Letters 1, 21–23.

        Parameters
        ----------
        simple_rets_1: pd.Series
            containing simple excess returns of asset 1
        simple_rets_2: pd.Series
            containing simple excess returns of asset 2
        """

        self.simple_rets_1 = simple_rets_1
        self.simple_rets_2 = simple_rets_2
        self.T = len(simple_rets_1)

    @property
    def sharpe_ratios(self):
        """ Sharpe ratios of the two assets
        """
        SR_1 = self.simple_rets_1.mean() / self.simple_rets_1.std()
        SR_2 = self.simple_rets_2.mean() / self.simple_rets_2.std()
        return (SR_1, SR_2)

    @property
    def corr(self):
        return np.corrcoef(self.simple_rets_1, self.simple_rets_2)[0,1]

    @property
    def t_value(self):
        SR_1, SR_2 = self.sharpe_ratios
        return self.test(SR_1=SR_1, SR_2=SR_2, rho=self.corr,
                                      T=self.T)

    @staticmethod
    def test(SR_1, SR_2, rho, T):
        """ Testing the difference of two sharpe ratios

        For details see Jobson and Korkie (1981) and Memmel (2003)

        Parameters
        ----------
        SR_1: float
            sharpe ratio of asset 1
        SR_2: float
            sharpe ratio of asset 2
        rho: float:
            correlation coefficient of the two assets
        T: int
            number of observations

        Returns
        -------
        float:
            approximately normally distributed RV
        """

        z = - (SR_1 - SR_2) /(1 / T * (2 * (1 - rho) + 0.5 *
                    (SR_1**2  + SR_2**2 - SR_1 * SR_2 * (1 + rho ** 2))))**0.5

        return z


class SRTest_LW:

    def __init__(self, simple_rets_1, simple_rets_2):
        """ Test difference in sharpe ratio according to Ledoit and Wolf

        Technical details can be found in Ledoit and Wolf (2008)

        Parameters
        ----------
        simple_rets_1: pd.Series
        simple_rets_2: pd.Series
        """

        self.simple_rets_1 = simple_rets_1
        self.simple_rets_2 = simple_rets_2
        self.moments = self.calc_moments()
        self.T = len(simple_rets_1)


    def y_hat(self):
        """ Sample
        """

        col1 = (self.simple_rets_1 - self.moments[0])
        col2 = (self.simple_rets_2 - self.moments[1])
        col3 = (self.simple_rets_1**2 - self.moments[2])
        col4 = (self.simple_rets_2**2 - self.moments[3])

        data = pd.concat([col1, col2, col3, col4], axis=1)

        return data.values

    def calc_moments(self):
        """ Uncentered moments of simple returns series
        
        Returns
        -------
        moments: list
            with [mu_1, mu_2, gamma_1, gamma_2]
        """
        moments = list()
        moments.append(self.simple_rets_1.mean())
        moments.append(self.simple_rets_2.mean())
        moments.append((self.simple_rets_1**2).mean())
        moments.append((self.simple_rets_2**2).mean())

        return np.array(moments)

    def delta_fun(self, a, b, c, d):
        """ See f(a,b,c,d)

        Parameters
        ----------
        a: float
            mean return of asset 1
        b: float
            mean return of asset 2
        c: float
            second momement of asset 1
        d: float
            second moment of asset 2

        Returns
        -------
        float:

        """

        return a / (c - a ** 2) ** 0.5 - b / (d - b ** 2) ** 0.5

    def gradient_delta_fun(self, a, b, c, d):
        """ Returns the gradient of `meth`: self.delta_fun()

        Returns
        -------
        gradient: np.array
        """

        gradient = [c / (c - a ** 2) ** 1.5, -d / (d - b ** 2) ** 1.5,
                    -0.5 * a / (c - a ** 2) ** 1.5,
                    0.5 * b / (d - b ** 2) ** 1.5]
        
        return np.array(gradient)

    def p_value(self):
        """ Two sided p-value
        """

        return 2 *norm.cdf(-abs(self.t_value()))

    def t_value(self):
        """ T-value of the difference
        """
        return self.delta_fun(*self.moments)/ self.standard_error_hat()


    def standard_error_hat(self):
        """
        Returns
        -------
        std_err: float
            standard error estimate
        """

        gradient = self.gradient_delta_fun(*self.moments)
        covariance = self.covariance()
        var = gradient.transpose() @ covariance @ gradient / self.T

        return (var)**0.5


    def covariance(self):

        cov = self.psi(self.y_hat())

        return cov

    @staticmethod
    def alpha_hat(y_hat):

        # loop through column
        numerator = 0
        denominator = 0
        for col in range(y_hat.shape[1]):
            res = AR(y_hat[:, col]).fit(maxlag=1)
            rho_hat = res.params[1]
            sigma_hat = res.sigma2 ** 0.5
            numerator += 4 * rho_hat ** 2 * sigma_hat ** 4 / (1 - rho_hat) ** 8
            denominator += sigma_hat ** 4 / (1 - rho_hat) ** 4

        return numerator/denominator

    @staticmethod
    def parzen_kernel(x):

        if x < 0.5:
            weight = 1 - 6 * x ** 2 + 6 * x ** 3
        elif x < 1:
            weight = 2 * (1 - x) ** 3
        else:
            weight = 0
        return weight

    @staticmethod
    def psi(y_hat):
        """

        Parameters
        ----------
        y_hat

        Returns
        -------

        """
        T = len(y_hat)
        cols = y_hat.shape[1]
        gammas_sum = np.zeros((cols,cols))
        sstar = 2.6614 * (SRTest_LW.alpha_hat(y_hat)*T)**0.2

        j = 0
        while j < sstar:
            gamma = SRTest_LW.gamma(y_hat, j)
            kernel_weight = SRTest_LW.parzen_kernel(j / sstar)
            gammas_sum += kernel_weight * (gamma + gamma.transpose())
            j += 1

        return T / (T - 4) * gammas_sum

    @staticmethod
    def gamma(y_hat, current_lag):
        """

        Parameters
        ----------
        y_hat

        Returns
        -------

        """
        T = len(y_hat)
        cols = y_hat.shape[1]
        gamma_hat = np.zeros((cols, cols))
        for t in range(current_lag, T):
            gamma_hat += np.asmatrix(y_hat[t, :]).transpose() @ np.asmatrix(
                y_hat[t - current_lag, :])

        return gamma_hat / T


def main():
    pass


if __name__ == '__main__':

    SRTest_LW(1, 2)
    main()





