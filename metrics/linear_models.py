import numpy as np
from sklearn.linear_model import LassoLarsIC, LassoLarsCV
from sklearn import datasets
from sklearn.linear_model import lasso_path
import statsmodels.api as sm
from statsmodels.stats import sandwich_covariance
from statsmodels.sandbox.stats.multicomp import multipletests
import pandas as pd
import numpy.matlib as mat
import logging
from scipy.stats import norm

logger = logging.getLogger(__name__)

from phdlib.metrics.tools import KFoldCV


def fwr_selection(t_values=None, p_values=None, method='fdr_by', level=0.05):
    """ Calculates family wise error rate adjustments - multiple testing

    Parameters
    ----------
    t_values: numpy-array like
        t-values of coefficients (if specified, no p-values can be provided)
    p_values: numpy-array like
        p-values of coefficients (if specified, no t-values can be provided)
    method: str
        {'fdr_by'} which adjustment method
    level: float
        significant level

    Returns
    -------
    tuple: selected, adj_p_value

    """
    if t_values is not None and p_values is not None:
        raise ValueError('Both p- and t-values specified, use one of the two')

    if t_values is not None:
        p_values =  1 - norm.cdf(abs(t_values.values.astype(float)))

    selected, adj_p_value, _, _n = multipletests(p_values, alpha=level,
                                                 method=method)

    if isinstance(p_values, pd.Series):
        selected = pd.Series(selected, index=p_values.index)
        adj_p_value = pd.Series(adj_p_value, index=p_values.index)

    if isinstance(t_values, pd.Series):
        selected = pd.Series(selected, index=t_values.index)
        adj_p_value = pd.Series(adj_p_value, index=t_values.index)

    return selected, adj_p_value

def panel_ols_regression(y, X, cov_type ='nw-groupsum', return_type='results'):
    """ Runs panel OLS

    Parameters
    ----------
    y: pandas.Series
        the dependent variable
    X: pandas.DataFrame
        the independent variables, including a column 'date' which has datetime
        values, on which the panel defines its time dimension

    Returns
    -------
    statsmodels.results object instance
        returns the results of the panel fit
    """

    s = X['date']

    # HACK to fix issue with time delta
    # TODO test if redundant with newer pandas version
    # break down date in time periods (overflow issue with pandas time.delta)
    delta = (pd.Series(s.values.astype('uint64') - s.values.min().astype(
        'uint64'))) / 86400000000000
    time = delta.rank(method='dense').astype(int).values

    X_ = X.drop('date', axis=1)
    model = sm.OLS(y, X_)
    results = model.fit()

    if cov_type == 'nw-groupsum':
        results = results.get_robustcov_results(cov_type='nw-groupsum',
                                                maxlags=1, time=time)
    else:
        results = results.get_robustcov_results(cov_type='nw-panel',
                                                maxlags=1, time=time)
    if return_type=='results':
        return results

    elif return_type == 'para_tvalue':
        coef = pd.Series(results.params, index=X_.columns)
        t_values = pd.Series(results.tvalues, index=X_.columns)
        return coef, t_values


def fama_macbeth(y, X, lags=12):
    """ Fama Macbeth (1970) regression

    Parameters
    ----------
    y: pd.DataFrame
        with multiIndex ("date" and some ID)
    X: pd.DataFrame
        with multiIndex ("date" and some ID)
    lags: int
        how many NW lags should be used

    Returns
    -------
    tuple: para, t_values
    """
    # TODO weighted fama_macbeth

    coef = pd.DataFrame(columns=X.columns,
                        index=X.index.get_level_values('date').unique())

    for date in coef.index:

    # for date, X_t in X.groupby('date'): #super RAM intensive this way
        y_t = y.loc[date]
        model = sm.OLS(y_t, X.loc[date,:])
        ols_results = model.fit()
        coef.loc[date, :] = ols_results.params

    para , stdd, t_values = _calc_t_stat(coef, nw_lags_beta=lags)

    return para, t_values


def lasso_estimation(y, X, criterion='bic', normalize=True,
                     k_fold=None, weights=None):
    """ Lasso estimator

    Parameters
    ----------
    y: pd.Series/np.array
        dependent variable of interest
    X: pd.DataFrame/np.array
        independent variables of interest
    criterion: str
        'bic', 'aic'. 
    normalize: bool
        if data is normalized or not
    k_fold: int
        how many k in 

    Returns
    -------

    """

    if weights is not None:
        scale = (weights*len(weights))**0.5
        y = y.multiply(scale)
        X = X.multiply(scale, axis=0)


    if criterion in ['bic', 'aic']:
        model = LassoLarsIC(criterion=criterion, normalize=normalize,
                            fit_intercept=False)
    elif criterion == 'cv':
        cv_splits = KFoldCV(training_input=X, training_output=y,
                            k=k_fold, index_name='date',
                            return_type='index_bool')
        model = LassoLarsCV(normalize=normalize, cv=cv_splits)
    else:
        raise ValueError(f'Criterion {criterion} not known check input')

    if isinstance(X, pd.DataFrame):
        columns = X.columns
        X = X.values
    else:
        columns = np.arange(X.shape[1])

    if isinstance(y, pd.Series):
        y = y.values

    model.fit(X, y)

    coef = pd.DataFrame(model.coef_, index=columns, columns=['coeff'])

    return coef


def market_cap_weight(mkt_cap, period_level='date', asset_level='PERMNO',
                      equal_period_weight=True):
    """ Calculates mkt cap weights, where each period gets equally weighted

    Parameters
    ----------
    mkt_cap: pd.Series/pd.DataFrame
        with market capitalization as values and a multiindex con

    Returns
    -------
    weights: pd.DataFrame
    """
    weights = list()

    grouped = mkt_cap.groupby(level=period_level)
    T = len(grouped)
    for date, group_df in grouped:
        weights.append(group_df.div(group_df.sum() * T))

    weights = pd.concat(weights)

    return weights


def ada_lasso_estimation(y, X, criterion='bic', normalize=True,
                         first_stage='Lasso', k_fold=None, weights=None):
    """ Adaptive Lasso estimator

    Parameters
    ----------
    y: pd.Series/np.array
        dependent variable of interest
    X: pd.DataFrame/np.array
        independent variables of interest
    criterion: str
        'bic', 'aic'. etc
    normalize: bool
        if data is normalized or not
    first_stage: str
        which method to obtain first beta estimations, either 'OLS' or
        'Lasso'
    weights: pd.DataFrame/np.array
        same length as y and X

    Returns
    -------
    coef: pd.DataFrame
        adaptive Lasso estimates
    coef_1st: pd.DataFrame
        first stage estimates (either OLS or Lasso)
    """
    # first stage of adaptive lasso-------------------------------------------
    if first_stage == 'Lasso':
        coef_1st = lasso_estimation(y, X, criterion=criterion,
                                    normalize=normalize, k_fold=k_fold,
                                    weights=weights)
    elif first_stage == 'OLS':
        model = sm.OLS(y, X)
        results = model.fit()
        coef_1st = pd.DataFrame(results.params, index=X.columns,
                                columns=['coeff'])

    # second stage adaptive lasso ---------------------------------------------
    # The adaptive lasso becomes a lasso problem after rescaling the design
    # matrix after the first stage, see Buehlmann and van de Geer (2011)
    if coef_1st.transpose().abs().sum(axis=1)[0] == 0:
        logger.warning('All coeffecients are zero, no AdaLasso estimation'
                       'returning twice first stage estimation results')
        return coef_1st,coef_1st

    X_tilde = X * mat.repmat(coef_1st.transpose().abs(), X.shape[0], 1)

    coef_tilde = lasso_estimation(y, X_tilde, normalize=False,
                                  criterion=criterion, k_fold=k_fold,
                                  weights=weights)
    coef = coef_1st.abs() * coef_tilde

    return coef, coef_1st


def _calc_t_stat(beta, nw_lags_beta, use_correction=True):
    """ Calculates t-stats for FM coefficients

    Parameters
    ----------
    beta: pd.DataFrame
        with column the coefficient and values the estimates, index time
    nw_lags_beta: int
        how many lags should be considered considered

    Returns
    -------

    """
    N = len(beta)
    B = (beta - beta.mean(0)).astype(float)

    S = sandwich_covariance.S_hac_simple(B, nlags=nw_lags_beta) / N
    # S = sandwich_covariance.cov_hac_simple(B, nlags=nw_lags_beta)/N
    # C = np.zeros((B.shape[1], B.shape[1]))
    #
    # if nw_lags_beta is not None:
    #     for i in range(nw_lags_beta + 1):
    #
    #         cov = np.dot(B[i:].T, B[:(N - i)]) / N
    #         weight = i  / (nw_lags_beta + 1)
    #         C += 2 * (1 - weight) * cov

    if use_correction:
        nobs, k_params = beta.shape
        # print(nobs/(nobs - k_params))
        S *= nobs / float(nobs - 1)

    mean_beta = beta.mean(0)
    std_beta = np.diag(S)**0.5 / np.sqrt(N)
    t_stat = mean_beta / std_beta

    return mean_beta, std_beta, t_stat


def rolling_regressions(y, X, result_dict, normalize=False):
    """ Wrapper function around regression

    Parameters
    ----------
    y: pandas.DataFrame
        responds vector
    X: pandas.DataFrame
        Design matrix
    result_dict: dict
        including as keys desired results, values list
    normalize: bool
        indicating whether design matrix should be normalized

    Returns
    -------

    """

    if 'AdaptiveLasso' in result_dict.keys():
        res_ada, res = ada_lasso_estimation(y, X.drop('date', axis=1),
                                            normalize=normalize)
        result_dict['AdaptiveLasso'].append(res_ada)

    if 'Lasso' in result_dict.keys():
        if 'AdaptiveLasso' not in result_dict.keys():
            res = lasso_estimation(y, X.drop('date', axis=1),
                                   normalize=normalize)
        result_dict['Lasso'].append(res)

    if 'OLS_DK_coef' in result_dict.keys() or \
                    'OLS_DK_tValues' in result_dict.keys():
        res_ols = panel_ols_regression(y, X)

        if 'OLS_DK_coef' in result_dict.keys():
            result_dict['OLS_coef'].append(pd.DataFrame(res_ols.params).T)

        if 'OLS_DK_tValues' in result_dict.keys():
            result_dict['OLS_DK_tValues'].append(
                pd.DataFrame(res_ols.tvalues).T)

    return result_dict

def compare_t_values():

    df = pd.read_csv('D:/panel_small.csv')
    df.date = pd.to_datetime(df.date)

    # new_df = list()
    # for date, df_sub in df.groupby('date'):
    #
    #     new_df.append(df_sub.iloc[:2500])
    #
    # df = pd.concat(new_df)

    pols_dc = panel_ols_regression(df['ret'],
                                   X=df.drop(['PERMNO','ret'],axis=1),
                                   cov_type='nw-groupsum')

    pols_nw = panel_ols_regression(df['ret'],
                                   X=df.drop(['PERMNO','ret'],axis=1),
                                   cov_type='nw-panel')

    df.set_index(['date', 'PERMNO'],inplace=True)

    fm = fama_macbeth(y=df['ret'], X=df.drop('ret', axis=1), lags=12)
    print(fm)

    return (pols_dc.params, pols_nw.params, fm[0]), \
           (pols_dc.tvalues, pols_nw.tvalues, fm[1]), df




def adaptive_lasso_small_panel():

    df = pd.read_csv('D:/panel.csv')
    df.date = pd.to_datetime(df.date)

    res = ada_lasso_estimation(y=df['ret'],
                               X=df.drop(['PERMNO','ret', 'date'],axis=1),
                               normalize=False,    )

    print(res)

    return res


def main():
    pass
    # Example usage of data
    # diabetes = datasets.load_diabetes()
    # import phdlib.misc.data_generator as dg
    # X = dg.DataGenerator.get_panel(rows=20)
    # X = X.to_frame()
    # Y = X['a']*2 + X['b']*-0.8 + X['c']*0.4 + np.random.randn(len(X['c']))
    # para, t_value = fama_macbeth(Y, X)
    # adaptive_lasso_small_panel()

    # X = diabetes.data
    # y = X['a'] * X['b']
    # model = sm.OLS(y, X)
    # model = model.fit()
    # print(model)
    # print(X)
    # # res = lasso_estimation(X, y)
    # res = ada_lasso_estimation(y, X)
    #
    # return model


if __name__ == '__main__':
    df = adaptive_lasso_small_panel()
