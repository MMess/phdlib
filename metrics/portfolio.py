import logging

import numpy as np
import pandas as pd

logger = logging.getLogger(__name__)

class DataChecker:
    pass


class CriteriaPortfolio:

    _percentiles = {'decile': 0.1, 'quartile': 0.25, 'quantile': 0.2,
                    '1/3': 1 / 3, '1/2': 0.5, 'ff_style':[0, 0.3, 0.7]}

    def __init__(self, criteria, simple_returns=None, mkt_cap=None,
                 ascending=True, buckets='decile', weighting='equal',
                 return_shift=1, rebalance_freq=None, alt_criteria=None,
                 min_max_mkt_cap=None):
        """ Class for criteria based portfolios

        Parameters
        ----------
        criteria: pd.DataFrame
            comprising as column index the universe of stocks
        simple_returns: pd.DataFrame
            same index and columns as criteria frame, where value are simple
             returns
        mkt_cap: pd.DataFrame
            same index and columns as criteria frame, with market values in
        ascending: bool
            if criteria is sorted from lowest to highest (the lower the better)
        buckets: str
            what kind of portfolio buckets, 'decile', 'quartile' etc.
        weighting: str
            how the portfolio weights should be determined 'equal' or 'mkt_cap'
        return_shift: int
            how many period returns should be shifted to match criteria info set
            using criteria at t correspond to return t->t+1. Hence, return_shift
            is set to 1 (the default).
        alt_criteria: pd.DataFrame
            if provided, quartiles are calculated based on the alternative
             criteria and splits points are used for criteria
        rebalance_freq: int
            how often (measured in periods) should be rebalanced

        Returns
        -------
        dict
            with keys the portfolio numbers ('top',1,2,3,.., 'bottom')

        """

        self.criteria = criteria
        self.simple_returns = simple_returns
        self.ascending = ascending
        self.mkt_cap = mkt_cap
        self.buckets = buckets
        self._rebalance_freq = rebalance_freq
        self._weights = None
        self._pf_names = None
        self._split_points = None
        self._weighting = weighting
        self.return_shift = return_shift
        self.alt_criteria= alt_criteria
        self._turn_over = None
        self._trades = None
        self.min_max_mkt_cap = min_max_mkt_cap

    @property
    def portfolio_names(self):
        """ Names the portfolios according to their rank

        highest is called 'top', lowest is called 'bottom', in between numbers
        starting from 1 to N-1 are the names for brackets in between

        Returns
        -------

        """
        if self._pf_names is None:
            if isinstance(self._percentiles[self.buckets], list):
                self._pf_names = np.arange(0, len(self._percentiles[
                    self.buckets])).astype(int).astype(str)
            else:
                self._pf_names = np.arange(0, 1 / self._percentiles[
                    self.buckets]).astype(int).astype(str)
            self._pf_names[0] = 'Top'
            self._pf_names[-1] = 'Bottom'
            if len(self._pf_names) == 3:
                self._pf_names[1] = 'Middle'
        return self._pf_names

    @property
    def split_points(self):
        if self._split_points is None:
            if isinstance(self._percentiles[self.buckets], list):
                self._split_points = np.asarray(
                    self._percentiles[self.buckets])
            else:
                self._split_points = np.arange(0, 1,
                                           self._percentiles[self.buckets])

        return self._split_points

    @property
    def weights(self):
        """ Returns pf weights of the portfolio sorting criteria

        Returns
        -------
        dict
            with the portfolio returns of the desired  with weights as values,
        """

        if self._weights is not None:
            return self._weights

        weights = dict()
        # Aux element of 1 have all bucket edges
        splits = self.split_points.tolist() + [1]
        if self.alt_criteria is not None:
            qts = self.alt_criteria.quantile(splits, axis=1).transpose()
        for idx, pf_name in enumerate(self.portfolio_names):

            if self.alt_criteria is None:
                # get all stocks below and above desired percentile
                included = (splits[idx] < self._rank_matrix()) & \
                           (self._rank_matrix() <= splits[idx + 1])

            else:
                tmp_a = self.criteria.apply(lambda x: x > qts[splits[idx]])
                tmp_b = self.criteria.apply(
                    lambda x: x <= qts[splits[idx + 1]])
                included = tmp_a & tmp_b

            # which weighting scheme to choose
            if self.weighting == 'equal':
                tmp_weights = pd.DataFrame(columns=self.criteria.columns,
                                           index=self.criteria.index)
                eq_weights = pd.concat(
                    [included.sum(axis=1)] * tmp_weights.shape[1],axis=1)
                eq_weights.index = tmp_weights.index
                eq_weights.columns = tmp_weights.columns
                tmp_weights = tmp_weights.where(included == False,
                                                 1 / eq_weights)

            elif self.weighting == 'mkt_cap':
                tmp_cap = self.mkt_cap[included]
                # normalize selected stocks to one based on mtk_cap
                tmp_weights = tmp_cap.div(tmp_cap.sum(axis=1), axis=0)

            # if rebalance frequency is
            if self._rebalance_freq is not None:
                tmp_weights = self._create_rebalance_weights(tmp_weights)
                if self.simple_returns is not None:
                    tmp_weights =  self._dynamic_weights(tmp_weights,
                                                         self.simple_returns)

            weights[pf_name] = tmp_weights

        self._weights = weights
        return self._weights

    def _create_rebalance_weights(self, weights_raw):
        df = weights_raw.copy()
        df.loc[df.index.isin(df.index[::self._rebalance_freq]) == False,
            :] = np.nan

        return df

    @staticmethod
    def _dynamic_weights(weights_raw, simple_returns):
        """ Calculates dynamic weights of a portfolio where infrequent

            rebalancing takes place

        Parameters
        ----------
        weights_raw: pd.DataFrame
            a df with some rows of only zeros (non-rebalancing periods)
        simple_returns: pd.DataFrame

        Returns
        -------
        pd.DataFrame
            dynmaic weights
        """

        weights = weights_raw.copy()
        # sum up all non-zero weights, such that rebalancing periods can be
        # determined 1,1,1,2,2,2,3,3,3,4,4,4 with quartely rebalancing f
        weights['period'] = (weights.isnull() == False).any(axis=1).cumsum()
        # add multi index with periods
        weights.set_index('period', append=True, inplace=True)
        sub_groups = list()
        for period, group in weights.groupby(level='period'):
            group, rets = group.align(simple_returns, join='left')

            # first period set to zero since weights are given
            rets.iloc[0, :] = 0
            # cumulative returns multiplied with initial weights
            tmp_weights = (rets + 1).cumprod().multiply(group.iloc[0,:])
            # normalize the weights again such that it is accounted for
            tmp_weights = tmp_weights.div(tmp_weights.sum(axis=1), axis=0)
            sub_groups.append(tmp_weights)

        weights = pd.concat(sub_groups)
        weights.index = weights.index.droplevel('period')

        return weights

    @property
    def weighting(self):
        return self._weighting

    @weighting.setter
    def weighting(self, weighting):
        if self._weights is None or self._weighting == weighting:
            return
        else:
            self._weighting = weighting
            logger.info('Setting weights and trades back to None')
            self._weights = None
            self._trades = None

    @property
    def rebalance_freq(self):
        return self._rebalance_freq

    @rebalance_freq.setter
    def rebalance_freq(self, rebalance_freq):
        if isinstance(rebalance_freq,
                      int) is False and rebalance_freq is not None:
            raise TypeError('Rebalance_freq is of wrong type, either'
                             'None or int should be used')

        if rebalance_freq != self._rebalance_freq:
            self._rebalance_freq = rebalance_freq
            logger.info('Setting weights and trades back to None due to ')
            self._weights = None
            self._trades = None

        return

    def pf_returns(self):
        """ Calculates the long bucket portfolio returns

        Returns
        -------
        pd.DataFrame
            with column names as
        """
        pf_rets = list()
        for name in self.portfolio_names:
            tmp_weights = self.weights[name].shift(periods=self.return_shift)
            tmp_rets = (tmp_weights*self.simple_returns).sum(axis=1)
            tmp_rets.name = name
            pf_rets.append(tmp_rets)

        return pd.concat(pf_rets, axis=1)

    @property
    def factor_return(self):
        """ Calculates the factor returns

        Returns
        -------
        pd.Series
            a return series of the factor (long-short) portfolio
        """
        if self.simple_returns is None:
            raise ValueError('No returns defined')

        # shift returns by one period such that weights are aligned correctly
        top = self.weights['Top'].shift(periods=self.return_shift)
        bottom = self.weights['Bottom'].shift(periods=self.return_shift)
        # returns long (Top assets) - short (Bottom assets) factor returns
        return (top * self.simple_returns).add(-bottom * self.simple_returns,
                                               fill_value=0).sum(axis=1)

    def adj_factor_return(self, how='simple', costs_bp=50,
                          ignore_init_costs=True):
        """ Turnover adjusted factor return

        Parameters
        ----------
        costs_bp: float
            trading costs in BP
        ignore_init_costs: bool
            if first period buy costs should be ignored

        Returns
        -------
        pd.Series
            a time series of adjusted factor returns
        """
        costs = self.trading_costs(portfolio_name='factor',
                                   costs_bp=costs_bp, how=how).copy()
        if ignore_init_costs:
            costs.iloc[0] = 0
        return (1 + self.factor_return) * (1 - costs) - 1

    def _rank_matrix(self):
        """Ranks matrix accordingly, criteria NAs are ignored and NAs returned

        Returns
        -------

        """
        return self.criteria.rank(pct=True, axis=1, ascending=self.ascending)

    @property
    def trades(self):
        """ Gets all trades of each portfolio

        Returns
        -------

        """
        if self._trades is None:

            if self._rebalance_freq is not None:
                print(self._rebalance_freq)
            trades = dict()

            if self.simple_returns is None:
                raise ValueError('"simple_returns" need to be provided')

            # loop through each portfolio
            for name in self.portfolio_names:
                trades[name] = self.calc_trades(self.weights[name],
                                                self.simple_returns,
                                                return_shift=self.return_shift)

            # factor trades add long and short trades
            trades['factor'] = trades['Top'].add(trades['Bottom'],
                                                   fill_value=0)
            self._trades = trades

        return self._trades

    @property
    def turnover(self):
        """ Calculates portfolio turnover

        Returns
        -------
        dict: a dict of turnover for each portfolio_name + 'factor' portfolio
        """
        # loop through each portfolio
        turnover = dict()
        trades = self.trades
        for name in self.portfolio_names:
            turnover[name] = abs(trades[name]).sum( axis=1)

        turnover['factor'] = abs(trades['factor']).sum(axis=1)

        return turnover

    @staticmethod
    def calc_trades(target_weights, simple_returns, return_shift=1):
        """ Calculates turnover based on target_weights and returns

        Parameters
        ----------
        target_weights: pd.DataFrame
            with target weights as values
        simple_returns: pd.DataFrame
            with simple returns as values
        return_shift: int
            periods for return shift (typically 1)

        Returns
        -------
        pd.Series
            with absolute turnover (buy and sells)

        """
        # get unscaled weights of portfolio
        unscaled_w = (simple_returns + 1) * target_weights.shift(
            periods=return_shift)
        # normalize weights before rebalancing
        pre_reb_weights = unscaled_w.div(
            unscaled_w.sum(axis=1), axis=0)
        pre_reb_weights.iloc[0, :] = 0
        # show old trades
        trades = target_weights.fillna(0) - pre_reb_weights.fillna(0)

        return trades

    def trading_costs(self, portfolio_name='factor', costs_bp=50,
                      how='simple'):
        """ Calculates the turnover costs of a given portfolio/factor

        Parameters
        ----------
        portfolio_name: str
            a valid portfolio name (for factor use 'factor')
        costs_bp: float
            costs of a trade in basis points
        how: str
            how trading costs should be calculated, "simple" same costs all
            trades. "time_size" size and time dependent, see below for details.

        Returns
        -------
        pd.Series
        """
        if how=='simple':
            costs = costs_bp / 1e4
            return self.turnover[portfolio_name] * costs

        elif how == 'time_size':
            trades = self.trades[portfolio_name]
            if self.min_max_mkt_cap is None:
                raise NotImplementedError('Specify mkt cap min max')

            # # normalize ranks with respect to
            # rel_rank = (self.mkt_cap.rank(
            #     axis=1, ascending=False).add(self.start_rank-1, axis=0)).divide(
            #     , axis=0)
            max_ = self.min_max_mkt_cap['max']
            min_ = self.min_max_mkt_cap['min']
            norm_me = self.mkt_cap.sub(min_, axis=0).div(max_ - min_, axis=0)

            costs = self.time_size_trading_cost(norm_me)

            return (abs(trades) * costs).sum(axis=1)

    @staticmethod
    def time_size_trading_cost(mkt_cap_norm, date=None,
                               base_year='19740101', trend_stop='20020101'):
        """ Calculates trading costs based in time ans size effects

        More details on the methods can be found in Brandt et el. (2009),
        Hand and Green (2011)

        Parameters
        ----------
        mkt_cap_norm: float/pd.DataFrame
            if pd.DataFrame has to have date index
        date: date-like
            if mkt_cap_rel_rank is float provide date, otherwise us df index
        base_year: date-like
        trend_stop: date-like

        Returns
        -------

        """

        trend_stop = pd.Timestamp(trend_stop)
        base_delta = (trend_stop - pd.Timestamp(base_year)).days
        if isinstance(mkt_cap_norm , pd.DataFrame):

            days_delta = (trend_stop - mkt_cap_norm.index).days.values
            # take max(0,delta)
            days_delta[days_delta < 0] = 0
            time_effect = (1 + 3 * pd.Series(days_delta / base_delta,
                                             index=mkt_cap_norm.index))

            size_effect = 0.006 - 0.0025 * mkt_cap_norm

            # trading costs for each stock at specific time
            return size_effect.mul(time_effect, axis=0)

        else:
            if date is None:
                raise ValueError('"date" needs to specified with float')
            days_delta = (trend_stop - pd.Timestamp(date)).days
            time_effect = (1 + 3 * max(0, days_delta) / base_delta)
            size_effect = 0.006 - 0.0025 * (1 - mkt_cap_norm)

            return time_effect*size_effect


def main():
    pass

if __name__ == '__main__':
    main()