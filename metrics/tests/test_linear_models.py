import logging
import unittest
import matplotlib as mpl
mpl.use('Agg')
import  numpy as np
import pandas as pd

from unittest import TestCase
from glmnet_py import glmnet, glmnetCoef, cvglmnet, cvglmnetCoef
from phdlib.settings import TMP_PATH
from phdlib.misc import logger_settings, data_generator

from phdlib.metrics.linear_models import ada_lasso_estimation,\
    market_cap_weight, lasso_estimation, panel_ols_regression, fama_macbeth

logger = logging.getLogger(__name__)



class TestAdaptiveLasso(TestCase):

    @classmethod
    def setUpClass(cls):

        np.random.seed(1234)
        para = 50
        obs = 1000
        X = np.random.randn(obs,para)

        coef = np.random.uniform(0.051, 1.5, int(para / 2))

        y = X[:,:int(para/2)]@coef + np.random.randn(obs)

        cls.y = pd.DataFrame(y, columns=['y'])
        cls.y.index.name='period'
        cls.X = pd.DataFrame(X)
        cls.X.index.name = 'period'
        cls.true_coef = coef

        # cls.y.to_csv(TMP_PATH.joinpath('y.csv'))
        # cls.X.to_csv(TMP_PATH.joinpath('X.csv'))


    def test_sklearn_estimation(self):

        coef = ada_lasso_estimation(self.y, self.X, k_fold=5)
        # print(coef)
        # cv_fit = cvglmnet(x=self.X.values.copy(), y=self.y.values.copy(),
        #                   ptype = 'mse', nfolds =5)
        # print(cvglmnetCoef(cv_fit, s = 'lambda_min'))
        #
    def test_mkt_cap_weighting(self):
        index_ = 6
        values_ = 3
        df = pd.DataFrame({'a': np.random.randn(index_ * values_),
                           'b': np.random.randn(index_ * values_),
                           'd': np.random.randn(index_ * values_),
                           'e': np.random.randn(index_ * values_),
                           'c': range(index_ * values_),
                           'date': [x for x in range(index_)] * values_})

        sr = pd.Series(np.random.randn(index_*values_), index=df['date'])
        sr.index.name = 'date'
        df.set_index(['c','date'], append=True, inplace=True)
        weights = market_cap_weight(abs(df['a']), asset_level='c')
        X = df[['a','b','d']]
        y = df['e']
        print(y)
        ada_lasso_estimation(y,X,weights=weights)
        print(y)


class TestOLSEstimator(TestCase):

    @classmethod
    def setUpClass(cls):
        X = data_generator.DataGenerator.get_panel(rows=100)
        X = X.to_frame()
        cls.y = X['a'] * 2 + X['b'] * -0.8 + X['c'] * 0.4 + np.random.randn(
            len(X['c']))

        cls.X = X

    def test_pols(self):

        y = self.y.reset_index(drop=True)
        X = self.X.reset_index().drop(['ID'], axis=1)
        results = panel_ols_regression(y=y, X=X)
        para = pd.Series(results.params, index=self.X.columns)
        t_value = pd.Series(results.tvalues, index=self.X.columns)
        print(para/t_value)


    def test_fm(self):

        para, t_value = fama_macbeth(y=self.y, X=self.X)
        print(para / t_value)







def main():
    pass


if __name__ == '__main__':
    # logging.config.dictConfig(logger_settings.log_set_ups)
    # logger.setLevel('INFO')
    unittest.main()
    main()