__author__ = 'mmess'
"""SPECIFY FILE HERE
"""
import logging
from unittest import TestCase

import numpy as np
import pandas as pd
from phdlib.metrics.portfolio import CriteriaPortfolio as CP
from phdlib.misc.data_generator import DataGenerator
from string import ascii_lowercase

logger = logging.getLogger(__name__)


class TestPortfolioSort(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.small_df = pd.DataFrame({'a': [0, 0, 1],
                                     'b': [2, 10, 2],
                                     'c': [4, -4, 2],
                                     'e': [9, -3, 1],
                                     'f': [7, -2, 6],
                                     'd': [np.nan, 11, 3]})
        cls.small_cap = pd.DataFrame({'a': [1, 1, 1],
                                      'b': [2, 3, 2],
                                      'c': [4, 4, 2],
                                      'e': [3, 5, 6],
                                      'f': [2, 4, 6],
                                      'd': [np.nan, 11, 10]})

        tmp_data = dict()
        counter=0
        for letter in ascii_lowercase[:20]:
            counter+=1
            tmp_data[letter] = 20*[counter]

        cls.mid_df = pd.DataFrame(tmp_data)
        cls.mid_df.loc[1,'a']=np.nan

        cls.big_df = DataGenerator.get_df(40, 1000)
        cls.mkt_cap = DataGenerator.get_df(40, 1000)
        cls.tol = 0.0000000000001

    def setUp(self):
        self.pfs = CP(self.big_df, mkt_cap=self.mkt_cap,
                                     buckets='decile',
                                     simple_returns=self.big_df / 10)
        self.small_pfs = CP(self.small_df, buckets='quartile')
        self.mid_pf = CP(self.mid_df, mkt_cap=self.mid_df,
                                        weighting='mkt_cap')

    def test_pf_returns(self):
        self.assertEqual(type(self.pfs.pf_returns()), pd.DataFrame)

    def test_weights_mid(self):
        pass
        # splits = [0.0, 0.1, 0.2, 0.3, 1]
        # cutoffs = self.mid_df.quantile(0.5, axis=1)
        # print(cutoffs)
        # print(cutoffs[0.1])
        # print(self.mid_df)
        # tmp_a = self.mid_df.apply(lambda x: x > cutoffs[0.1])
        # tmp_b = self.mid_df.apply(lambda x: x <= cutoffs[0.2])
        #
        # print(tmp_a & tmp_b)

        # for idx, split in enumerate([0.1,0.2,0.3]):
        #     print
        #     print(self.mid_df < cutoffs[splits[idx]])
        #     # print(self.mid_df.apply(lambda row: row[splits[idx]>]))
        #


        # self.mid_pf.weights['Bottom']

    def test_weights(self):
        weights = self.pfs.weights
        self.assertEqual(type(weights),dict)
        self.assertTrue('Top' in weights.keys())
        self.assertTrue('Bottom' in weights.keys())
        self.assertEqual(type(weights['Top']), pd.DataFrame)
        for weighting in ['equal', 'mkt_cap']:
            self.pfs.weighting = weighting
            for key, value in weights.items():
                self.assertTrue((value.sum(axis=1) <= 1 + self.tol).all())
                self.assertTrue((value.sum(axis=1) > 1 - self.tol).all())
                self.assertTrue((value.sum(axis=1) >= -self.tol).all())
                self.assertTrue((value[value > 0].max(axis=1) <= 1).all())
                if self.pfs.buckets=='decile':
                    self.assertTrue(((value > 0).sum(axis=1) == 0.1 * len(
                        self.pfs.criteria.columns)).all())

        self.assertTrue(len(set(self.small_pfs.weights['Top'].columns)^
                            set(self.small_df.columns))==0)

        for key, value in self.small_pfs.weights.items():
            self.assertTrue(np.isnan(value.loc[0,'d']))

        mkt_pf = CP(self.small_df, mkt_cap=self.small_cap,
                                   buckets='1/2', weighting='mkt_cap')
        self.assertTrue((mkt_pf.weights['Top'].sum(axis=1) <= 1 + self.tol).all())

    def test_reb_freq_weights(self):
        self.pfs = CP(self.big_df, mkt_cap=self.mkt_cap,
                                     buckets='decile', rebalance_freq=5,
                                     simple_returns=self.big_df / 10)

        print(self.pfs.weights['Top'].isnull().sum(axis=1))

    def test_dynamic_weights(self):

        # self.pfs = CriteriaPortfolio(self.big_df, mkt_cap=self.mkt_cap,
        #                              buckets='decile', rebalance_freq=5,
        #                              simple_returns=self.big_df / 10)
        #
        # self.assertTrue(self.pfs.weights['Top'].isnull().any().any())
        # tmp = self.pfs._dynamic_weights(self.pfs.weights['Top'],
        #                                 self.pfs.simple_returns)
        #
        # self.assertTrue((abs(tmp.sum(axis=1)-1)<0.000001).all())

        test_df = pd.DataFrame({'a': [0.5, np.nan, np.nan, 0.5, np.nan],
                                'b': [0.5, np.nan, np.nan, 0.5, np.nan]})
        test_ret = pd.DataFrame({'a': [-0.1, 0.1, 0.1, -0.1, 0.1],
                                 'b': [-0.3, 0.2, 0.4, -0.3, 0.2]})
        test_df.index.name = 'date'
        test_ret.index.name = 'date'
        adj_weights = CP._dynamic_weights(test_df, test_ret)
        self.assertTrue(adj_weights.iloc[0,:].equals(test_df.iloc[0,:]))

    def test_factor_return(self):
        with self.assertRaises(ValueError):
            self.small_pfs.factor_return
        self.pfs = CP(self.big_df,
                                     simple_returns=self.big_df / 10,
                                     mkt_cap=self.mkt_cap)
        factor_return = self.pfs.factor_return
        self.pfs.weighting = 'mkt_cap'
        self.assertEqual(type(factor_return), pd.Series)
        factor_return = self.pfs.factor_return
        self.assertEqual(len(factor_return), len(self.pfs.criteria))

    def test_portfolio_name(self):
        self.assertEqual('Top', self.pfs.portfolio_names[0])
        self.assertEqual('Bottom', self.pfs.portfolio_names[-1])

    def test_split_points(self):
        print(self.pfs.split_points)

    def test_turnover(self):

        with self.assertRaises(ValueError):
            self.mid_pf.turnover

        self.assertEqual(type(self.pfs.turnover), dict)

        weights = pd.DataFrame({'a': [0.5, 0.3, 0.7], 'b': [0.5, 0.7, 0.3]})
        simple_rets = pd.DataFrame(columns=['a', 'b'], index=[0, 1, 2], data=0)
        tmp_pf = abs(CP.calc_trades(weights, simple_rets)).sum( axis=1)

        test_result = pd.Series([1.0, 0.4, 0.8])
        self.assertTrue((abs(test_result-tmp_pf)<1e-4).all())

    def test_trading_cost_time_size(self):

        self.assertTrue(
            isinstance(CP.time_size_trading_cost(1, '19820101'), float)
        )

        print(CP.time_size_trading_cost(1,
                                        '19740101') / CP.time_size_trading_cost(
            1, '20020101'))
        with self.assertRaises(ValueError):
            CP.time_size_trading_cost(1)

        tc = CP.time_size_trading_cost(self.mkt_cap.rank(pct=True, axis=1))

        self.assertTrue((tc.min(axis=1)[1:].values < tc.min(axis=1)[:-1]).all())


def main():
    pass


if __name__ == '__main__':
    main()
