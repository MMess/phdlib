import logging
import unittest
from unittest import TestCase

import numpy as np
import pandas as pd
from phdlib.metrics.deep_learn import HPGenerator, TrainTestSplit, \
    GPHyper, hyper_para_string, TensorPandas
from metrics.tools import KFoldCV
from phdlib.misc.data_generator import DataGenerator
from string import ascii_lowercase

logger = logging.getLogger(__name__)

class TestTools(TestCase):

    def test_hyper_string(self):
        which = ['learning_rate', 'dropout', 'hidden_units', 'l2_strength']
        gen = HPGenerator(which=which)
        random_hp = gen.generate()
        for idx, row in random_hp.iterrows():
            a = hyper_para_string(row)

        self.assertEqual(type(a), str)

class TestHPGenerator(TestCase):
    def test_which(self):
        which = ['learning_rate', 'dropout', 'l2_strength']
        gen = HPGenerator(which=which)
        random_hp = gen.generate()
        self.assertEqual(type(random_hp), pd.DataFrame)
        self.assertTrue(random_hp.columns.tolist(), which)
        self.assertTrue((random_hp['learning_rate']<1).all())

class TestCV(TestCase):

    def test_cv(self):

        index_ = 6
        values_ = 3
        df = pd.DataFrame({'a': np.random.randn(index_ * values_),
                           'b': np.random.randn(index_ * values_),
                           'c': range(index_ * values_),
                           'date': [x for x in range(index_)] * values_})

        sr = pd.Series(np.random.randn(index_*values_), index=df['date'])
        sr.index.name = 'date'
        df.set_index(['c','date'], append=True, inplace=True)

        for a,b in KFoldCV(df, sr):
            print(a)
            self.assertEqual(type(a[0]), pd.DataFrame)
            self.assertEqual(type(b[1]), pd.Series)


class TestTrainTestSplit(TestCase):

    def train_test_case(self):
        df = pd.DataFrame({'a': np.random.randn(10)})
        sr = pd.Series(np.random.randn(10))

        splitter = TrainTestSplit(df,sr)
        self.assertEqual(type(splitter.split()), dict)

class TestGPHyper(TestCase):

    def test_length_variation(self):

        df = pd.DataFrame({'a':np.random.randn((100))})
        df['Loss'] = 1.2*df['a'] + np.random.randn((100))

        GPHyper(df).length_sim()

class PandasTF(TestCase):
    def test_order_columns(self):

        df = pd.DataFrame({'a':[1,2,3], 'b':[2,3,4]})
        t_pd = TensorPandas(df.columns)

        df_1 = pd.DataFrame({'b': [1, 2, 3], 'a': [2, 3, 4], 'c': [1, 2, 3]})
        df_1 = t_pd.df_order(df_1[['b', 'a', 'c']])
        self.assertEqual(df_1.columns.tolist(), df.columns.tolist())

        df_2 = pd.DataFrame({'b': [1, 2, 3], 'c': [1, 2, 3]})

        with self.assertRaises(KeyError):
            t_pd.df_order(df_2)

def main():
    pass


if __name__ == '__main__':

    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
    unittest.main()