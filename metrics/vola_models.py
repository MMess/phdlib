import logging
import logging.config

import arch
import numpy as np
import pandas as pd
from sqlalchemy import create_engine

from phdlib import settings
from phdlib.misc import logger_settings

logger = logging.getLogger(__name__)


def get_market_returns(db_connection=create_engine(settings.SQL_URL)):
    """ Loads market returns from SQL database

    Parameters
    ----------
    db_connection

    Returns
    -------

    """
    mkt_returns = pd.read_sql('ff_daily', db_connection, index_col='index',
                              columns=['Market'])
    return mkt_returns.loc['19620101':]


def garch_estimation(returns, distribution='normal'):
    """ Estimates volatilities based on GARCH(1,1) model

    Parameters
    ----------
    returns: pd.DataFrame
    distribution: str
        {'normal', 'StudentsT'}

    Returns
    -------

    """
    if distribution == 'normal':
        am = arch.arch_model(returns)
    elif distribution == 'StudentsT':
        am = arch.arch_model(returns, dist='StudentsT')

    return am.fit()


def garch_parameter(save_para=True):
    """ GARCH parameter of market for normal and StudentstT distribution

    Parameters
    ----------
    save_para: bool
        if data sould be saved to csv

    Returns
    -------
    para: pd.DataFrame
    """

    mkt_returns = get_market_returns()

    logger.info('Estimating GARCH Normal parameters ...')
    garch_fit = garch_estimation(mkt_returns * 100)
    garch_normal = pd.DataFrame(garch_fit.params)
    garch_normal.rename(columns={'params': 'Normal' }, inplace=True)
    garch_normal.index.name = 'para_name'

    logger.info('Estimating GARCH StudentsT parameters ...')
    garch_fit = garch_estimation(mkt_returns * 100, distribution='StudentsT')
    garch_fit.summary()
    garch_studentT = pd.DataFrame(garch_fit.params)
    garch_studentT.rename(columns={'params': 'StudentT'}, inplace=True)
    garch_studentT.index.name = 'para_name'

    if save_para:
        garch_normal.to_csv(str(settings.GARCH_PARA))
        garch_studentT.to_csv(str(settings.GARCH_PARA_ST))
        return

    return pd.concat([garch_normal, garch_studentT], axis=1)



def simulate_garch_process(num_obs=6000):
    """ Simulates GARCH process based on estimated coefficients

    Parameters
    ----------
    num_obs: int

    Returns
    -------
    sigma: pd.DataFrame
        with days as index and vola (sigma) as values
    """
    if num_obs > 6000:
        raise NotImplementedError('Change time format pandas for longer days')
    dates = pd.date_range(start='1699-12-31', periods=num_obs, freq='m')
    days = pd.date_range(dates[0], dates[-1], freq='b')
    res = pd.read_csv(str(settings.GARCH_PARA), index_col='para_name')
    garch = arch.univariate.GARCH()
    rets, sigma2 = garch.simulate(parameters=res.params.values[1:],
                                  rng=np.random.standard_normal,
                                  nobs=len(days),
                                  burn=100)

    sigma = pd.DataFrame(np.sqrt(sigma2), index=days) / 100
    sigma = sigma.resample('M', fill_method='ffill')
    sigma.rename(columns={0:'Normal'}, inplace=True)

    return sigma * (255 / 12) ** 0.5


def get_garch_volas():
    """ Returns estimated GARCH volatilities

    Returns
    -------
    garch_vola: pd.DataFrame
    """
    mkt_returns = get_market_returns()
    garch_vola = dict()

    # scale by 100 (for estimation) rescale by 100 (for simulation)
    garch_fit = garch_estimation(mkt_returns * 100)
    garch_vola['Normal'] = garch_fit.conditional_volatility / 100
    garch_fit = garch_estimation(mkt_returns * 100, distribution='StudentsT')
    garch_vola['StudentsT'] = garch_fit.conditional_volatility / 100

    garch_vola['LTMean'] = pd.Series((mkt_returns['Market']).std(),
                                     index=mkt_returns.index)
    for key, value in garch_vola.items():
        value *= (255 / 12) ** 0.5
        print(type(value))
        garch_vola[key] = value.asfreq(freq='m', method='ffill')

    garch_vola = pd.DataFrame(garch_vola)

    return garch_vola


def main():

    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')

    # garch_vola.to_csv(str(settings.GARCH_PATH))
    return garch_parameter(save_para=False)
    # return simulate_garch_process()

if __name__ == '__main__':
    df = main()