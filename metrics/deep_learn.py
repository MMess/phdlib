import logging
import shutil
import datetime as dt
import keras as kr
import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn import gaussian_process

from phdlib.data.tools import z_score

logger = logging.getLogger(__name__)

# tensorflow fix seed problem hack, pseudo random seed
np.random.seed(int(dt.datetime.now().timestamp()))


class TrainTestSplit:
    def __init__(self, input_df, output_sr, shuffle=False, split_ratio=3 / 5):

        if shuffle:
            raise NotImplementedError

        self.input_df = input_df.copy()
        self.output_sr = output_sr.copy()
        self.split_ratio = split_ratio

    def split(self):
        """ Splits the data into training and test data

        Returns
        -------
        dict
            with keys 'input_train', 'output_train',
             'input_test','output_test' and values the corresponding chunk of
             data
        """

        split_point = np.int(np.floor(self.split_ratio * len(self.input_df)))

        return {'input_train': self.input_df.iloc[:split_point, :],
                'output_train': self.output_sr.iloc[:split_point],
                'input_test': self.input_df.iloc[split_point:, :],
                'output_test': self.output_sr.iloc[split_point:]
                }


class HP1DGrid:

    def __init__(self, which='learning_rate', size=30, **kwargs):
        """

        Parameters
        ----------
        which
        size
        kwargs
        """
        self.which = which
        self.size = size
        self.cases = pd.DataFrame(columns=[which], index=range(self.size))
        self.kwargs = {**kwargs}

    def _gen_map(self):
        return {'learning_rate': self.learning_rate}

    def get(self, ):
        self.cases.loc[:, self.which] = self._gen_map()[
            self.which](**self.kwargs)

        return self.cases

    def learning_rate(self, low_bound=0.2, upper_bound=8):
        """

        Parameters
        ----------
        low_bound: float
            lower bound (10^-lower_bound)
        upper_bound:float
            upper bound (10^-upper_bound)

        Returns
        -------
        np.array
            of len self.size
        """
        learning_grid = 10 ** (-np.arange(low_bound, upper_bound,
                                     (upper_bound-low_bound)/self.size))

        return learning_grid



class HPGenerator:
    def __init__(self, which, size=250, num_input_units=4):
        """ Random hyper-parameter generator

        Parameters
        ----------
        which: list
            a list of strings of which hyper-parameter to be generated
        size: int
            how many draws from the distribution should be drawn
        num_input_units: int
            how many input units to chose
        """
        self.size = size
        self.which = which
        self.cases = pd.DataFrame(columns=which, index=range(size))
        self.num_input = num_input_units
        import numpy as np
        self.np = np

    def _gen_map(self):
        return {'learning_rate': self.learning_rate,
                'optimizer': self.optimizer,
                'dropout': self.dropout,
                'hidden_units': self.hidden_units,
                'layers': self.layers,
                'l2_strength': self.l2_strength,
                'l1_strength': self.l1_strength,
                'keras_optimizer': self.keras_optimizer,
                'patience': self.patience}

    def generate(self):
        for hyper_parameter in self.which:
            self.cases.loc[:, hyper_parameter] = self._gen_map()[
                hyper_parameter]()

        return self.cases

    def learning_rate(self):
        """ Random draws of learning rates

        where x is drawn from uniform [-5,-1], and it returns 10^x

        Returns
        -------

        """
        return 10 ** (np.random.uniform(-6, -2, size=self.size))

    def optimizer(self):
        pass

    def dropout(self):
        return np.random.uniform(0.0, 0.75, size=self.size)

    def hidden_units(self):
        upper_bound = np.log(np.max([20, self.num_input * 1.5]))
        lower_bound = np.log(np.max([3, self.num_input / 2]))
        return np.round(np.exp(self.np.random.uniform(lower_bound, upper_bound,
                                                      self.size)))

    def layers(self):
        return np.round(np.random.uniform(1.5, 14.5, self.size))

    def activation_function(self):
        pass

    def l2_strength(self):
        return 10 ** (np.random.uniform(-5, -0, size=self.size))

    def l1_strength(self):
        return 10 ** (np.random.uniform(-5, -0, size=self.size))

    def keras_optimizer(self, optimizer_set=None):
        if optimizer_set is None:
            # SGD + Momentum numerical not very stable, hence removed from
            # default list
            optimizer_set = ['RMSprop', 'Nadam', 'Adagrad',
                             'Adadelta', 'Adam', 'Adamax', 'Nesterov']

        return np.random.choice(optimizer_set, size=self.size)

    def patience(self):
        return np.floor(np.random.uniform(20, 41, size=self.size))


class DNNBuilder:
    def __init__(self, input_cols, output_dim, hidden_units, layers,
                 activation='relu', dropout=None, l_1=0, l_2=0,
                 input_dropout=True):
        """ Builds a generic model architecture based on hyper-parameters
        
        Parameters
        ----------
        input_cols: int
            how many input columns
        output_dim: int
            how many output columns (in case of regression it is 1,
            for classification problems it can be any int)
        hidden_units: int
            how many hidden units per layer
        layers: int
            how many layers, depth of the network
        activation: str
            a valid keras activation function
        dropout: float
            dropout probability applied to each unit
        input_dropout: bool
            if input layer subject to dropout
        """

        self.input_cols = int(input_cols)
        self.output_dim = int(output_dim)
        self.hidden_units = int(hidden_units)
        self.layers = int(layers)
        self.activation = activation
        if dropout is None:
            self.dropout = dropout
        self.l_1 = l_1
        self.l_2 = l_2
        self.dropout = float(dropout)
        self._model = None
        self.input_dropout = input_dropout

    @property
    def model(self):
        if self._model is None:
            self.construct()

        return self._model

    def construct(self):

        if self._model is not None:
            raise AttributeError('Model already constructed')
        # core model
        model = kr.models.Sequential()

        # Input Layer
        if self.dropout is not None and self.input_dropout:
            # input level dropout
            model.add(
                kr.layers.Dropout(self.dropout,
                                  input_shape=(self.input_cols,)))
            input_shape = dict()
        else:
            input_shape = dict(input_shape=(self.input_cols,))

        # Hidden layer
        for _ in range(self.layers):

            model.add(kr.layers.Dense(units=self.hidden_units,
                                      name='Hidden' + str(_ + 1),
                                      activation=self.activation,
                                      kernel_regularizer=kr.regularizers.l1_l2(
                                          l1=self.l_1, l2=self.l_2),
                                      **input_shape)
                      )

            if self.dropout is not None:
                model.add(kr.layers.Dropout(self.dropout))

        model.add(kr.layers.Dense(units=self.output_dim, name='Output'))

        self._model = model
        return self._model


class DNNTrainer:
    _opt_collection = {'SGD': {'opt': kr.optimizers.SGD,
                               'para': dict(momentum=0.0, nesterov=False)},
                       'Momentum': {'opt': kr.optimizers.SGD,
                                    'para': dict(momentum=0.9,
                                                 nesterov=False)},
                       'Nesterov': {'opt': kr.optimizers.SGD,
                                    'para': dict(momentum=0.9, nesterov=True)},
                       'RMSprop': {'opt': kr.optimizers.RMSprop},
                       'Adagrad': {'opt': kr.optimizers.Adagrad},
                       'Adadelta': {'opt': kr.optimizers.Adadelta},
                       'Adam': {'opt': kr.optimizers.Adam},
                       'Adamax': {'opt': kr.optimizers.Adamax},
                       'Nadam': {'opt': kr.optimizers.Nadam},
                       }

    def __init__(self, model,
                 optimizer='SGD', loss='mean_squared_error',
                 learning_rate=1e-1,
                 early_stopping=True,
                 batch_size=2 ** 7,
                 model_dir=None,
                 patience=30):

        self.model = model
        self.loss = loss
        self.optimizer = optimizer
        self.learning_rate = learning_rate
        self.early_stopping = early_stopping
        self.patience = patience
        self._tensorboard = False
        if model_dir is not None:
            self._tensorboard = True
        self.model_dir = model_dir

    def compile(self):
        """ Compiles the model (adds loss function and optimizer
        
        Returns
        -------

        """

        opt_dict = self._opt_collection[self.optimizer]

        if 'para' in opt_dict.keys():
            optimizer = opt_dict['opt'](lr=self.learning_rate,
                                        **opt_dict['para'])
        else:
            optimizer = opt_dict['opt'](lr=self.learning_rate)

        self.model.compile(loss=self.loss, optimizer=optimizer)

    def callbacks(self):

        callbacks = list()
        if self.early_stopping is not None or self.early_stopping is not False:
            callbacks.append(kr.callbacks.EarlyStopping(monitor='val_loss',
                                                        min_delta=1e-6,
                                                        patience=self.patience,
                                                        verbose=0,
                                                        mode='auto'))
        if self._tensorboard:
            callbacks.append(kr.callbacks.TensorBoard(self.model_dir,
                                                      histogram_freq=1,
                                                      write_images=False,
                                                    #  write_grads=True,
                                                      ))

        return callbacks

    def fit(self, X, y, validation_data, verbose=1):
        """ Wrapper function calls model.fit()
        
        Returns
        -------

        """
        self.compile()
        self.model.fit(x=X, y=y, validation_data=validation_data,
                       callbacks=self.callbacks(), epochs=200, verbose=verbose)


class GPHyper:
    def __init__(self, df, multiple_min=1e2):
        """

        Parameters
        ----------
        df: pd.DataFrame
            a pandas data frame with column 'Loss' and other hyper-parameter
             names

        """
        self.df = df.copy()
        self.df.replace([np.inf, -np.inf, 0], np.nan, inplace=True)
        self.df.dropna(inplace=True)
        self.df = self.df.loc[
                  self.df['Loss'] <= np.max(
                      [100, self.df['Loss'].min() * multiple_min]), :]
        self.df.reset_index(drop=True, inplace=True)

    def length_sim(self, frac=4 / 5, num_sim=80):

        length_summary = dict()

        for col in self.df:

            if col == 'Loss':
                continue
            tmp_res = []
            for _ in range(num_sim):
                print(frac)
                sample_data = self.df.sample(frac=frac)

                init_length = np.random.uniform(0.2, 1)
                gp = gaussian_process.GaussianProcessRegressor(
                    gaussian_process.kernels.RBF(length_scale=init_length))
                fit = gp.fit(y=sample_data['Loss'],
                             X=z_score(sample_data.drop('Loss', axis=1),
                                       axis=0))
                print(gp.kernel_)
                tmp_res.append(fit.kernel_)

            length_summary[col] = tmp_res

        return length_summary

    def box_plot(self):
        pass


class DNN:
    def __init__(self):
        """
        """
        self.tf_pd_data = None
        self.model = None

    def training(self, train_, valid_, hyper_para, model_dir=None,
                 verbose=0):
        """
        
        Parameters
        ----------
        train_
        valid_
        hyper_para

        Returns
        -------

        """
        if 'patience' not in hyper_para.keys():
            hyper_para['patience'] = 20
        # TODO re-train old model?
        kr.backend.clear_session()
        self.tf_pd_data = TensorPandas(train_[0].columns)
        self.model = DNNBuilder(input_cols=len(train_[0].columns),
                                output_dim=1,
                                hidden_units=hyper_para['hidden_units'],
                                layers=hyper_para['layers'],
                                dropout=hyper_para['dropout'],
                                l_2=hyper_para['l2_strength'],
                                ).model

        if model_dir is not None:
            model_dir = str(model_dir)

        DNNTrainer(self.model,
                   learning_rate=hyper_para['learning_rate'],
                   optimizer=hyper_para['opt'],
                   patience=hyper_para['patience'],
                   model_dir=model_dir,
                   ).fit(
            *self.tf_pd_data.xy_values(train_),
            validation_data=self.tf_pd_data.xy_values(valid_),
            verbose=verbose)

        return self

    def predict(self, X):
        y_hat = self.model.predict(self.tf_pd_data.x_values(X))

        return pd.Series(data=y_hat[:, 0], index=X.index, name='y_hat')


def hyper_para_string(row):
    """ Returns a str which can be used for tensorboard
    
    Parameters
    ----------
    row: dict/row
        where key is the hyper-parameter name and value the value

    Returns
    -------
    str: concat str of single parts
    """

    hp_str = list()
    for key, value in row.items():
        if isinstance(value, str):
            hp_str.append('{}-{}'.format(key, value))
        elif value - int(value) > 1e-9:
            hp_str.append('{}-{:.8f}'.format(key, value))
        else:
            hp_str.append('{}-{}'.format(key, int(value)))

    return '_'.join(hp_str)


class LinearModel():
    def __init__(self, train_data, valid_data=None,
                 predict_data=None, model_dir=None):
        """ Tensorflow linear model prediction
        
        Parameters
        ----------
        train_data
        valid_data
        predict_data
        model_dir: pathlike
        """

        self.train_data = train_data
        self.valid_data = valid_data
        self.predict_data = predict_data
        self.model = None
        if model_dir is not None:
            model_dir = str(model_dir)
        self.model_dir = model_dir

    def train(self):
        """ Builds and trains the model 
        
        Returns
        -------

        """

        X_train = self.train_data[0]
        y_train = self.train_data[1]

        input_columns = list()
        for col in X_train:
            input_columns.append(tf.contrib.layers.real_valued_column(col))

        self.model = tf.contrib.learn.LinearRegressor(
            feature_columns=input_columns, model_dir=self.model_dir
        )
        # train model
        self.model.fit(
            input_fn=lambda: self.input_fn(X_train, y_train),
            steps=500)

    def evaluate(self, ):
        if self.model is None:
            self.train()

        res = self.model.evaluate(
            input_fn=lambda: self.input_fn(self.valid_data[0],
                                           self.valid_data[1], ), steps=1)
        return res['loss']

    def predict(self, X=None):
        if self.model is None:
            self.train()

        if X is None:
            X = self.predict_data

        res = self.model.predict(
            input_fn=lambda: self.input_fn(X), as_iterable=False)
        return np.array(res)

    def delete_dir(self):

        if self.model_dir is not None:
            tf.summary.FileWriterCache.clear()
            shutil.rmtree(self.model_dir)
        else:
            raise NotImplementedError


    @staticmethod
    def input_fn(df, y=None):

        df_batch = df
        y_batch = y

        feature_cols = dict()

        for col in df:
            feature_cols[col] = tf.constant(value=df_batch[col].values)

        if y is not None:
            labels = tf.constant(value=y_batch.values)
            return feature_cols, labels
        else:
            return feature_cols


class TensorPandas:
    def __init__(self, columns):
        self.columns = columns.tolist()

    def input_columns(self, input_df):

        df = input_df[self.columns].copy()

        input_cols = list()
        for col in df:
            input_cols.append(tf.contrib.layers.real_valued_column(col))

        return input_cols

    def input_fn(self, input_df, y):

        # prepare input (ensure initial columns are all present)
        df = input_df[self.columns].copy()
        feature_cols = dict()
        for col in df:
            feature_cols[col] = tf.constant(value=df[col].values)

        # target variable
        labels = tf.constant(value=y.values)

        return feature_cols, labels

    def df_order(self, df):
        """ Order columns as originally defined (all columns need to be
         present)
        
        Parameters
        ----------
        df: pd.DataFrame
            containing the same columns as originally defined

        Returns
        -------

        """
        return df[self.columns]

    def xy_values(self, data, return_type='tuple'):
        """
        
        Parameters
        ----------
        data: tuple
            a tuple of lenth two, where data[0] is X and data[1] is y
        return_type: str
            {'tuple', 'dict}

        Returns
        -------
        dict/tuple:
            a dict with keys "x" and "y" and values data[0].values,
             data[1].values 
        """
        if return_type == 'dict':
            return dict(x=data[0][self.columns].values, y=data[1].values)
        elif return_type == 'tuple':
            return data[0][self.columns].values, data[1].values

    def x_values(self, data):
        """
        
        Parameters
        ----------
        data: pd.DataFrame
            containing all columns as in self.columns

        Returns
        -------
        numpy.array
            values of data
        """
        return data[self.columns].values


def main():
    pass


if __name__ == '__main__':
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
    main()
