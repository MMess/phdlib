import pandas as pd
from sklearn.model_selection import KFold

class KFoldCV:

    def __init__(self, training_input, training_output, k=5,
                 index_name='date', return_type='df'):
        """ Returns K- Fold Cross-Validation, allows multi-indexing with panel

        Parameters
        ----------
        training_input: pd.DataFrame
            with containing the input data, same index as training_output
        training_output: pd.Series
            containing the target data, same index as training_input
        k: int
            k-fold cross-validation
        index_name: str
            the number of the index along sample is split, could be a multi-index
            in case of panel structure, such that CV is applied along period
            rather than the independent of it.
        return_type: str
            if dataframe ('df') or numpy.array ('array)
        """

        self.count = 0
        self.max_count = k
        self.input = training_input
        self.output = training_output
        self.index_name = index_name
        # since KFold can only deal with integer values, we need mapping
        self.value_map = pd.Series(training_input.index.get_level_values(
            index_name).unique().copy())
        split_values = self.value_map.index.values
        self.index_folds = KFold(n_splits=k, shuffle=True).split(
            split_values)
        self.return_type =return_type

    def __iter__(self):
        return self

    def __next__(self):
        """ iterator returns each time a different pair of train and valid df
        
        Returns
        -------
        ((a,b),(c,d)) where a,b are training-input and training-output pd.DF
            (c,d) are validation-input and validation-output pd.DF
        """
        train_index, valid_index = next(self.index_folds)

        # map integers back to original index values
        train_index = self.value_map[train_index].values
        valid_index = self.value_map[valid_index].values

        # multi-index selection
        train_tmp = self.input.index.get_level_values(self.index_name).isin(
            train_index)
        valid_tmp = self.input.index.get_level_values(self.index_name).isin(
            valid_index)
        if self.return_type == 'df':
            return (self.input.loc[train_tmp, :],
                    self.output.loc[train_tmp]), \
                   (self.input.loc[valid_tmp, :], self.output.loc[valid_tmp])
        elif self.return_type == 'array':
            return (self.input.loc[train_tmp, :].values,
                    self.output.loc[train_tmp].values), \
                   (self.input.loc[valid_tmp, :].values,
                    self.output.loc[valid_tmp].values)
        elif self.return_type=='index_bool':
            return train_tmp, valid_tmp


def main():
    pass


if __name__ == '__main__':
    main()
