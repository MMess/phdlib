import numpy as np
import scipy.linalg


def reshuffle_correlation_mat(correlation_mat, index_pairs):
    """The function reshuffles a correlation matrix according to index pairs

    Parameters
    ----------
    correlation_mat: numpy.array/matrix
        The correlation matrix which should be reshuffled

    index_pairs: list of tuples/list [index pairs: (int, int)/[int, int)]
       The list contains the index-pairs to be swapped,


    Returns
    --------
    reshuffled_correlation_mat
    """
    swapped_corr_mat = correlation_mat.copy()
    for swap_pair in index_pairs:
        aux_row = swapped_corr_mat[swap_pair[0], :].copy()
        aux_col = swapped_corr_mat[:, swap_pair[0]].copy()
        swapped_corr_mat[swap_pair[0], :] = swapped_corr_mat[swap_pair[1], :]
        swapped_corr_mat[:, swap_pair[0]] = swapped_corr_mat[:, swap_pair[1]]
        swapped_corr_mat[swap_pair[1], :] = aux_row
        swapped_corr_mat[:, swap_pair[1]] = aux_col
        # set diagonals to one
        swapped_corr_mat[swap_pair[0], swap_pair[0]] = 1
        swapped_corr_mat[swap_pair[1], swap_pair[1]] = 1
        # set corr of 1 0 to correlation(1,0)
        swapped_corr_mat[swap_pair[0], swap_pair[1]] = aux_row[swap_pair[1]]
        swapped_corr_mat[swap_pair[1], swap_pair[0]] = aux_row[swap_pair[1]]

    return swapped_corr_mat


def simulate_unit_vectors(len_vector=3, num_vectors=4):
    """The function simulates unit vectors pointing in random direction

    Parameters
    ----------
    len_vector : int,
        Unit vector drawn from R^dimension
    num_vectors: int,
        how many unit vectors

    Returns
    --------
    unit vectors: numpy.array
        a set of unit vectors concatenated in a multi-dim array (along axis=1)
    """

    normals = np.random.randn(int(len_vector), int(num_vectors))
    normalized = ((normals ** 2).sum(axis=0)) ** 0.5

    return normals / normalized[None, :]


class SimCorrelationMat:
    """The class is the base class for simulating various correlation matrices

    The algorithms are all based on Hardin, Garcia and Golan (2013) -
    A METHOD FOR GENERATING REALISTIC CORRELATION MATRICES
    """

    def __init__(self, num_groups, group_size, base_correlations, delta_noise,
                 epsilon, noise_space_dim):
        """

        Parameters
        ----------
        num_groups: int
            how many distinct groups
        group_size: list
            length of each group
        base_correlations: float/list
            within group correlation,
            if different for each group use list, else float
        delta_noise: float
            how groups are correlated with each other
        epsilon: float
            random noise of correlation
        noise_space_dim: int
        """

        # initialize corr-mat
        self.sim_corr_mat = None

        # number of total groups
        self.num_groups = num_groups

        # size of each group
        if isinstance(group_size, int):
            self.group_size = np.repeat(group_size, num_groups)
        if isinstance(group_size, list):
            self.group_size = np.array(group_size)

        # size of final correlation matrix
        self.corr_mat_size = self.group_size.sum()

        # base correlation within each group
        if isinstance(base_correlations, float):
            self.corr_base = np.repeat(base_correlations, num_groups)
        if isinstance(base_correlations, list):
            self.corr_base = np.array(base_correlations)

        # min,max (needed for algorithm)
        self._min_corr = self.corr_base.min()
        self._max_corr = self.corr_base.max()

        # initialize group mat and off group elements
        self._group_mat = []
        self._off_group_mat = []
        for group in self.group_size:
            self._group_mat.append(np.eye(int(group)))
            self._off_group_mat.append(np.ones((int(group), int(group))))

        # correlation term between groups
        if delta_noise < 0 or delta_noise >= self._min_corr:
            raise ValueError(
                'delta noise wrongly specified: {}'.format(delta_noise))
        self.delta_noise = delta_noise

        # base noise for each off-diagnol element in corrlation matrix
        if epsilon < 0 or epsilon >= 1 - self._max_corr:
            raise ValueError(f'epsilon wrongly specified: {self._max_corr}')
        self.epsilon = epsilon

        # Select N unit vectors from R^M
        self._unit_vec = np.asmatrix(simulate_unit_vectors(noise_space_dim,
                                                           self.corr_mat_size))
        self._unit_mat = self._unit_vec.T * self._unit_vec
        # ensure all elemtns are 1 (potentially small numerical deviations)
        self._unit_mat[self._unit_mat > 1] = 1

    def check_corr_mat(self):
        """ The method checks if required properties if corr are fulfilled

        raises exception if violation is detected

        Returns
        -------
        None
        """

        # symmetry
        if (self.sim_corr_mat.T == self.sim_corr_mat).all() is False:
            raise ValueError('Correlation matrix is not symetric')

        # check if all elements are below 1
        if (abs(self.sim_corr_mat) <= 1).all() is False:
            raise ValueError('Correlation matrix has elements >1 (abs))')

        # check if all diagonal elements are one
        if (np.diag(self.sim_corr_mat) == 1).all() is False:
            raise ValueError('Correlation matrix diag element !=1')

        # check if matrix is positive definite
        if (np.linalg.eigvals(self.sim_corr_mat) > 0).all() is False:
            raise ValueError('Correlation matrix not positive definite')

        return


class SimConstantCorrMat(SimCorrelationMat):
    """ The class simulates a constant base correlation matrix

    The term constant refers to a constant base correlation matrix within each
    group, off-group correlations deviate , additionally each off diagonal
    correlation term has an additional noise-term

    """

    def __init__(self, num_groups=4, group_size=1, base_correlations=0.8,
                 delta_noise=0.1, epsilon=0.05, noise_space_dim=3):
        super().__init__(num_groups, group_size, base_correlations,
                         delta_noise, epsilon, noise_space_dim)

        # construct base correlation matrix (within each group)
        for idx, mat in enumerate(self._group_mat):
            mat[mat == 0] = self.corr_base[idx]
            self._group_mat[idx] = mat
        sigma = scipy.linalg.block_diag(*self._group_mat)

        # construct off-group correlation matrix (outside of groups)
        off_group_mat = scipy.linalg.block_diag(*self._off_group_mat)
        off_group_mat[off_group_mat == 0] = self.delta_noise
        off_group_mat[off_group_mat == 1] = 0

        # construct noise matrix
        noise_mat = self.epsilon * (np.asarray(self._unit_mat))
        np.fill_diagonal(noise_mat, 0)

        # add parts to obtain final correlation matrix
        self.sim_corr_mat = sigma + off_group_mat + noise_mat

        super().check_corr_mat()


def main():
    a = SimConstantCorrMat()
    return a


if __name__ == '__main__':
    corr_mat = main()
