import logging
import time

import pandas as pd
import numpy as np

from phdlib.data import tools
from phdlib.metrics.api import panel_ols_regression, ada_lasso_estimation,\
    fama_macbeth
from phdlib.misc import functions

from phdlib import settings
from phdlib.finance.cs_simulation import CrossSectionSimulator, GarchVola
from phdlib.data.results import LocalResults

logger = logging.getLogger(__name__)


def simulation(settings, market_volatility, which=None):
    """   The function performs a cross-sectional stock market simulation

    Parameters
    ----------
    settings: dict
        including parameters as in settings file
    market_volatility: pd.Series
        with stock market volatilites as values and date as index
    which: iterable
        which methods to run

    Returns
    -------
    dict:
        with keys 'AdaptiveLasso', 'Lasso', 'OLS_DK_coef', 'OLS_DK_tValues'
        and values  pd.DataFrames with coefficient or t-stat estimates
    """
    est_results = dict()
    for method in which:
        est_results[method] = list()

    t0 = time.time()

    # loop for each simulation
    num_sim = settings.pop('num_simulations')

    # initialize cross-section simulations
    cs = CrossSectionSimulator(market_volatility=market_volatility,
                               **settings)

    logger.info(cs.main_log_msg())

    for s in range(num_sim):
        firm_char_list = list()

        logger.info('Running simulation {0} of {1} ({2:.3}%....)'.format(
            s + 1, num_sim, (s + 1) / num_sim * 100))

        # simulates a full cross-sectional panel for current simulation
        reg_data = cs.simulate()

        y = reg_data[['rets', 'date']].set_index(['date', reg_data.index])

        X_norm = tools.standardize_panel(reg_data.drop('rets', axis=1),
                                         keep_date_col=True, date_col='date')
        X_norm = X_norm.set_index(['date', X_norm.index])
        # CV5
        if 'AdaLasso_CV5' in which:
            logger.info('...CV5 adaptive lasso and lasso regressions')
            res_ada, res = ada_lasso_estimation(y=y['rets'], X=X_norm,
                                                normalize=False,
                                                criterion='cv', k_fold=5)
            est_results['AdaLasso_CV5'].append(res_ada.transpose())
            est_results['Lasso_CV5'].append(res.transpose())
        # BIC
        if 'AdaLasso_BIC' in which:
            logger.info('...BIC adaptive lasso and lasso regressions')
            X_norm.reset_index(level='date', inplace=True, drop=True)
            res_ada, res = ada_lasso_estimation(y=y['rets'], X=X_norm,
                                                normalize=False,
                                                criterion='bic')
            est_results['AdaLasso_BIC'].append(res_ada.transpose())
            est_results['Lasso_BIC'].append(res.transpose())
        # AIC
        if 'AdaLasso_AIC' in which:
            logger.info('...AIC adaptive lasso and lasso regressions')
            res_ada, res = ada_lasso_estimation(y=y['rets'], X=X_norm,
                                                normalize=False,
                                                criterion='aic')
            est_results['AdaLasso_AIC'].append(res_ada.transpose())
            est_results['Lasso_AIC'].append(res.transpose())

        if 'OLS_DK_coef' in which:
            logger.info('...OLS regression')
            res_ols = panel_ols_regression(y=reg_data['rets'],
                                           X=reg_data.drop('rets', axis=1))
            est_results['OLS_DK_coef'].append(pd.DataFrame(res_ols.params).T)
            est_results['OLS_DK_tValues'].append(
                pd.DataFrame(res_ols.tvalues).T)

        if 'FM_coef' in which:
            logger.info('...FM regression')
            reg_data['ID'] = np.arange(len(reg_data))
            reg_data.set_index(['date', 'ID'], inplace=True)
            reg_data.index.names = ['date', 'ID']
            res_fm = fama_macbeth(y=reg_data['ret'],
                                  X=reg_data.drop('ret', axis=1))

            est_results['FM_coef'].append(pd.DataFrame(res_fm[0]).T)
            est_results['FM_tValues'].append(pd.DataFrame(res_fm[1]).T)

        # display computational details---------------------------------------
        logger.info(functions.loop_tracker(s, num_sim, t0, ram_usage=True,
                                           name='Cross-Section Simulation'))

    # collect results of regressions
    df_estimation_res = {}
    for key, value in est_results.items():
        logger.info('DataType: {}, data {}'.format(type(value[0]), key))
        df_estimation_res[key] = pd.concat(value, ignore_index=True)

    logger.info('Simulation complete, returning results...')
    return df_estimation_res


def main():
    which = 'lasso_zoo'
    # which methods to run
    method_map = {'lasso_zoo': ['AdaLasso_BIC', 'AdaLasso_AIC', 'AdaLasso_CV5',
                                'Lasso_BIC', 'Lasso_AIC', 'Lasso_CV5',
                                'OLS_DK_coef', 'OLS_DK_tValues'],

                  'fm_pooled': ['OLS_DK_coef', 'OLS_DK_tValues', 'FM_coef',
                                'FM_tValues']}

    cs_settings = {'num_simulations': 100,  # default 1000/5000
                   'num_stocks': 40,  # default 4000
                   'periods': 50 * 12,  # default 50
                   'num_fc': 100,
                   'num_priced_factors': 6,
                   'num_unpriced_factors': 6,
                   'freq_adj': 12,
                   'market_distribution': 'Normal', # "Normal" , "StudentsT"
                   'R_squared': 0.00415,  # yearly R^2 ca. 5%
                   "market_risk_premia": 0.055,  # 'MarketRiskPremium': 'AR1',
                   'risk_premia_bounds': (0.015, 0.03),
                   'seed': 4720,
                   'fc_corr_pairs': ((0.9, 1, 12), (0.9, 2, 6), (0.9, 7, 13),
                                     (0.4, 3, 14), (0.4, 4, 8), (0.4, 9, 15)),
                   'fc_return_fun': 'standard'}

    # load locally estimated GARCH volas from csv files
    if cs_settings['periods'] <= 50*12:
        logger.info('Loading local volatility/GARCH(1,1) data ...')
        distribution = cs_settings['market_distribution']
        market_volatility = pd.read_csv(str(settings.GARCH_PATH),
                                        index_col='index',
                                        parse_dates=True)[distribution]
    else:
        logger.info('Simulate GARCH volatilities...')
        market_volatility = GarchVola(nobs=cs_settings['periods']).simulate()

    # run simulation and save results
    lr = LocalResults()
    results = simulation(cs_settings, market_volatility,
                         which=method_map[which])
    lr.save(data=results, parameters=cs_settings,
            results_name='simulation' + '_' + which)

    return results


if __name__ == '__main__':
    logging.root.addHandler(logging.StreamHandler())
    logger.setLevel('INFO')
    res = main()
