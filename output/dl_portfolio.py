import datetime as dt
import logging

import numpy as np
import pandas as pd
from phdlib.data.api import get_monthly_data, LocalResults, LoadLocalResult, \
    Factor, get_crsp_monthly
from phdlib.finance.api import ReturnIndex, ReturnAnalysis, FactorAnalysis
from phdlib.metrics.deep_learn import DNN, LinearModel, TensorPandas
from phdlib.metrics.tools import KFoldCV
from phdlib.metrics.api import panel_ols_regression, ada_lasso_estimation, fama_macbeth
from phdlib.misc.api import log_set_ups_runs, load_settings
from phdlib.output.lasso_zoo_regression import TimeWin

from phdlib.settings import RESULTS_PATH

try:
    import matplotlib.pyplot as plt
    from phdlib.output.return_plots import FactorGraphs
    import seaborn as sns

except:
    print('FactorGraphs/ matplotlib.pyplot import not possible')

__author__ = 'mmess'

logger = logging.getLogger(__name__)

PATTERN_DNN_PFF = 'dnn_portfolio'
PATTERN_DNN_Y_HAT = 'dnn_y_hat'
PATTERN_DNN_FC = 'y_hat_FC'
PATTERN_LINEAR_Y_HAT = 'linear_y_hat'
PATTERN_LINEAR_PFF = 'linear_portfolio'
# TODO re-factor linear and DNN in Class


def load_data(para=None):
    """ Loads monthly data
    
    Parameters
    ----------
    para: dict 
        for kwargs see `meth`:get_monthly_data

    Returns
    -------

    """
    if para is None:
        para = load_settings(PATTERN_DNN_PFF)

    df = get_monthly_data(**para['data'])
    df.dropna(inplace=True)
    df.set_index(['date', 'PERMNO'], append=True, inplace=True)
    X = df.drop(['ret'], axis=1)
    y = df['ret'] * 1000

    dates = TimeWin.get_dates(
        start_date=X.index.get_level_values('date').min(),
        end_date=X.index.get_level_values('date').max(),
        window=para['regression']['window_length'], freq='m')

    return X, y, dates, para


def load_pf_data(size='large', start_date=None, end_date=None):
    rets = get_monthly_data(start_date=start_date, end_date=end_date,
                            size=size, demean_ret=False,
                            columns=['PERMNO', 'adj_ret', 'ret'],
                            drop_na=True, ret_str='adj_ret')

    rets = rets.pivot(index='date', columns='PERMNO', values='ret')

    stocks = get_monthly_data(start_date=start_date,
                              end_date=end_date,
                              size='all', columns=('mkt_cap',),
                              keep_columns=('mkt_cap',))

    num_stocks = stocks.dropna().groupby('date').count()['mkt_cap']
    num_stocks.name = 'num_stocks'

    mkt_cap = get_crsp_monthly(['mkt_cap'])['mkt_cap']
    # align the two according to the criteria

    return rets, mkt_cap, num_stocks


def out_of_sample_linear_model(para=None, which='FM', save_results = True):
    """ Calculates out-of-sample performance of the linear model
    
    """
    model_dir = RESULTS_PATH.joinpath('Tensorflow').joinpath('Models')
    sep = '\n ==================== \n '

    if save_results:
        res_saver = LocalResults()
    if para is None:
        para = load_settings(PATTERN_DNN_PFF)
    X_all, y_all, dates, para = load_data(para=para)

    y_hat_linear = list()

    for date in dates:
        # estimate the model
        if date in dates[::para['regression']['re_estimate_freq']]:
            logger.info(sep + 'Current date: {}'.format(date) + sep)
            # prepare data
            index_loc = TimeWin.date_filter(
                X_all.index.get_level_values('date'), date,
                window_type=para['regression']['window_type'],
                window_length=para['regression']['window_length'])

            X, y = X_all.loc[index_loc, :], y_all[index_loc]
            tmp_dir = model_dir.joinpath('linear_pf_' +
                                         dt.datetime.now().strftime(
                                             '%Y-%m-%d_%H-%M'))
            if which=='TF':
                linear_model = LinearModel(train_data=(X, y), model_dir=tmp_dir)
                linear_model.train()
            elif which=='FM':
                # linear_coef = panel_ols_regression(y=y.reset_index(drop=True),
                #    X=X.reset_index().drop(['level_0', 'PERMNO'], axis=1))
                y.index = y.index.swaplevel(None, 'date')
                X.index = X.index.swaplevel(None, 'date')
                fm = fama_macbeth(y=y, X=X)
                fm[0][abs(fm[1]) <= 1.96] = 0

                # linear_coef.params[abs(linear_coef.tvalues)<=1.96]=0
                linear_coef = fm[0]
            elif which=='AdaLasso':
                linear_coef = ada_lasso_estimation(y, X,
                                                   criterion='bic')[0]['coeff']
        # linear_coef = ada_lasso_estimation(y, X, criterion='bic')[0]['coeff']

        # predict the returns (shift FC one back, since +1 for estimation)
        X_predict = X_all.groupby(level='PERMNO').shift(-1)
        index_loc = X_predict.index.get_level_values('date') == date

        # predict linear model returns
        if which=='TF':
            criteria_lin = linear_model.predict(X=X_predict.loc[index_loc, :])
            y_hat_date = pd.DataFrame([y_hat for y_hat in criteria_lin],
                                      index=X_predict.loc[index_loc, :].index,
                                      columns=['y_hat'])

        else:
            y_hat_date = X_predict.loc[index_loc, :].multiply(
                linear_coef).sum(axis=1)
            y_hat_date = y_hat_date.to_frame('y_hat')

        y_hat_linear.append(y_hat_date)

    y_hat_linear = pd.concat(y_hat_linear).reset_index()
    y_hat_linear = y_hat_linear.pivot_table(index='date',  values='y_hat',
                                            columns='PERMNO')
    if save_results:
        end = para['data']['end_date']
        para['regression']['method'] = which
        res_saver.save(data=y_hat_linear, parameters=para,
                       results_name=end + which + '_' + PATTERN_LINEAR_Y_HAT)

    return y_hat_linear


def adaptive_training(X, y, hyper_para, patience=10, threshold=0.002):
    """ Adaptive approach ot beat linear bechmark

    If none found relative best is found

    Parameters
    ----------
    X: pd.DataFrame
        input data
    y: pd.DataFrame
        output data
    hyper_para: dict
        set of hyper-parameters specifing the deep-neural network
    patience: int
        how often models and validation data can be re-trained
    threshold: float
        how much (in %) the linear model needs to be outperformed

    Returns
    -------
    model
        deep learning model
    """

    scores = pd.DataFrame(columns=['loss_ratio', 'dnn_model'],
                          index=range(patience))
    tf_pd_data = TensorPandas(X.columns)
    model_dir = RESULTS_PATH.joinpath('Tensorflow').joinpath('Models')

    saved_model = None
    last_ratio = 0
    for trial in range(patience):

        KF_data = [(train, valid) for train, valid in KFoldCV(X, y)]
        # train deep neutral network
        dnn = DNN()
        model = dnn.training(train_=KF_data[0][0], valid_=KF_data[0][1],
                     hyper_para=hyper_para, verbose=0)

        dnn_loss = dnn.model.evaluate(
            *tf_pd_data.xy_values(KF_data[0][1]))
        scores.loc[trial, 'dnn_model'] = model
        tmp_dir = model_dir.joinpath('linear_pf_' +
                                     dt.datetime.now().strftime(
                                         '%Y-%m-%d_%H-%M-%S'))
        lm = LinearModel(train_data=KF_data[0][0],
                         valid_data=KF_data[0][1],
                         model_dir=tmp_dir)
        linear_loss = lm.evaluate()
        lm.delete_dir()

        scores.loc[trial, 'loss_ratio'] = linear_loss / dnn_loss

        if scores.loc[trial, 'loss_ratio'] > 1 + threshold:
            logger.info(
                'Best model is better than linear, has ratio of {} \n {}'.format(
                    scores.loc[trial, 'loss_ratio'], scores))
            return model
        else:
            if scores.loc[trial, 'loss_ratio']> last_ratio:
                saved_model = model


    # select best model relative to linear (max value)
    best_model =  scores['loss_ratio'].idxmax()
    logger.info('Best model is worse than linear has ratio of {} \n {}'.format(
        scores.loc[best_model, 'loss_ratio'], scores ))

    return saved_model

def _fc_sensitivity(model, X, y_hat,date):
    """

    Parameters
    ----------
    model
    X
    y_hat
    date

    Returns
    -------

    """

    df = pd.DataFrame(columns=X.columns, index=[date])
    df.index.name = 'date'
    for FC in X:
        tmp_x = X.copy()
        tmp_x[FC] = 0
        df.loc[date, FC] = ((model.predict(tmp_x) - y_hat)**2).mean()

    return df


def out_of_sample_dnn(num_trials=25, para=None, approach='naive',
                      FC_sensitivity=False):
    """ runs out of sample portfolio analysis
    
    Returns
    -------

    """
    sep = '\n ==================== \n '
    save_results = True

    for n in range(num_trials):

        if save_results:
            res_saver = LocalResults()
        if para is None:
            para = load_settings(PATTERN_DNN_PFF)
        logger.info('Running out-of-sample portfolio with the following'
                    'parameters: \n {}'.format(para))
        X_all, y_all, dates, para = load_data(para=para)

        y_hat_dnn = list()
        y_hat_FC = list()

        for date in dates:
            # estimate the model
            if date in dates[::para['regression']['re_estimate_freq']]:
                logger.info(sep + 'Current date: {}'.format(date) + sep)
                # prepare data
                index_loc = TimeWin.date_filter(
                    X_all.index.get_level_values('date'), date,
                    window_type=para['regression']['window_type'],
                    window_length=para['regression']['window_length'])

                X, y = X_all.loc[index_loc, :], y_all[index_loc]
                if approach == 'naive':
                    KF_data = [(train, valid) for train, valid in
                               KFoldCV(X, y)]
                    # train deep neutral network
                    model = DNN().training(train_=KF_data[0][0],
                                           valid_=KF_data[0][1],
                                           hyper_para=para['network'],
                                           verbose=0)
                elif approach == 'adaptive':
                    model = adaptive_training(X, y, hyper_para=para['network'])

            # predict the returns (shift FC one back, since +1 for estimation)
            X_predict = X_all.groupby(level='PERMNO').shift(-1)
            index_loc = X_predict.index.get_level_values('date') == date
            # predict neural network returns
            y_hat_current = model.predict(X_predict.loc[index_loc, :])
            y_hat_dnn.append(y_hat_current)
            if FC_sensitivity:
                y_hat_FC.append(_fc_sensitivity(model=model,
                                                X=X_predict.loc[index_loc, :],
                                                y_hat=y_hat_current,
                                                date=date))

        # neural network predictions
        y_hat_dnn = pd.concat(y_hat_dnn).reset_index()
        y_hat_dnn = y_hat_dnn.pivot_table(index='date', values='y_hat',
                                          columns='PERMNO')
        end = para['data']['end_date']
        reg = para['regression']['window_type']
        if save_results:
            para['approach'] = approach
            name = '_'.join([end, reg, PATTERN_DNN_Y_HAT])
            res_saver.save(data=y_hat_dnn, parameters=para, results_name=name)

        if FC_sensitivity:
            y_hat_FC = pd.concat(y_hat_FC)
            para['approach'] = approach
            name = '_'.join([end, reg, PATTERN_DNN_FC])
            res_saver.save(data=y_hat_FC, parameters=para,  results_name=name)

    return y_hat_dnn


def oos_dnn_pf(y_hat, y_hat_parameter=None, buckets='ff_style', weighting='equal',
               save_results=True, plot=False, result_name=PATTERN_DNN_PFF,
               name='DFN', size=None, trade_freq=1, simple_returns=None,
               mkt_cap=None, num_stocks=None):
    """ Calculates factor portfolio return based on predicted stock returns
    
    Parameters
    ----------
    y_hat: pd.DataFrame
        with PERMNO as columns, dates as index, and y_hat as values
    y_hat_parameter: dict
        dictionary of input parameters, used to construct y_hat
    buckets: str
        "decile" or "ff_style"
    weighting: str
        "equal" or "mkt_cap"
    save_results: bool
        if factor returns should be saved as result

    Returns
    -------
    fa: FactorAnalysis
        containing all results w.r.t to factor construction
    """

    if save_results:
        res_saver = LocalResults()

    fa = FactorAnalysis(criteria=y_hat, simple_returns=simple_returns,
                        market_value=mkt_cap, ascending=False, weighting=weighting,
                        buckets=buckets, size=size, trade_freq=trade_freq,
                        min_max_mkt_cap=num_stocks)

    if save_results:

        parameter = y_hat_parameter.copy()
        parameter['weighting'] = weighting
        parameter['buckets'] = buckets
        logger.info('Saving factor returns ...')
        data = fa.portfolio.factor_return
        data.name = name
        result_name = '_'.join([weighting, buckets, size, result_name])
        res_saver.save(data=data, parameters=parameter,
                       results_name=result_name)

    # TODO, decide what to plot and how to save
    if plot:
        FactorGraphs(fa.portfolio, name='linear_pf').ri_turnover()

    return fa


def oos_buckets(y_hat, para, weighting, size, simple_returns=None, mkt_cap=None,
                num_stocks=None, buckets='decile'):

    fa = FactorAnalysis(criteria=y_hat, simple_returns=simple_returns,
                        market_value=mkt_cap, ascending=False,
                        weighting=weighting,
                        buckets=buckets, size=size, trade_freq=1,
                        min_max_mkt_cap=num_stocks)

    return fa




def archive_files(size='mid', pattern='dnn_portfolio_Result'):
    """ Moves files with different conditions to "archive"

    Parameters
    ----------
    size
    pattern

    Returns
    -------

    """

    conditions = dict(size=size)
    files = LoadLocalResult(file_pattern=pattern,
                            since='20170825',
                            conditions=conditions).files_true
    folder = '_'.join([size, 'dnn_portfolio_naive'])
    LoadLocalResult.move_results(files, ['deep_learn', folder])


def _forecast_combination(file_names, combination_number=75):
    predictions = list()
    for file in np.random.choice(file_names, combination_number):
        result = LoadLocalResult(file_pattern=file)
        predictions.append(result.data)

    y_hat = predictions[0] / combination_number
    for y_hat_single in predictions[1:]:
        y_hat += y_hat_single / combination_number

    return y_hat


def construct_pf_from_results(size, linear=False, approach='naive',
                              weighting='equal', combination=None,
                              buckets='ff_style', which='factor'):
    """ Loads y_hats from results, and saves DNN factor returns

    Parameters
    ----------
    size: str
        'large' or 'mid'
    linear: bool
        if linear model should be estimated
    approach: str
        'naive' or adaptive
    weighting: str
        'equal' or 'mkt_cap'
    combination: int
        if specifified it uses a forecast combination
    buckets: str
        which weighting scheme to use

    Returns
    -------


    """

    end_date = '20141231'
    conditions = dict(size=size, approach=approach, end_date=end_date)
    files = LoadLocalResult(file_pattern=PATTERN_DNN_Y_HAT,
                            since='20170730', conditions=conditions).files_true

    logger.info('Found {} files fulfilling criteria'.format(len(files)))

    return_data, mkt_cap_, num_stocks = load_pf_data(size=size,
                                                     start_date='19820101',
                                                     end_date=end_date)

    # Forecast combination approach
    if combination is not None:
        result = LoadLocalResult(file_pattern=files[0])
        para = result.parameter.copy()
        para['approach'] = '_'.join([approach, str(combination)])

        for _ in range(25):
            y_hat = _forecast_combination(files,
                                          combination_number=combination)

            _rets, _ = return_data.align(y_hat, join='right')
            _mkt_cap, _ = mkt_cap_.align(y_hat, join='right')

            if which=='factor':

                oos_dnn_pf(y_hat, para, save_results=True, buckets=buckets,
                           name='DFN', weighting=weighting, size=size,
                           result_name=para['approach'] + '_' + PATTERN_DNN_PFF,
                           simple_returns=_rets, mkt_cap=_mkt_cap,
                           num_stocks=num_stocks)
            else:

                buckets_dfn = oos_buckets(y_hat, para, buckets='decile',
                                          weighting=weighting, size=size,
                                          simple_returns=_rets,
                                          mkt_cap=_mkt_cap,
                                          num_stocks=num_stocks)

                break

    else:

        for file in files:
            logger.info('Loading y_hat from file: {}'.format(file))
            result = LoadLocalResult(file_pattern=file)
            para = result.parameter.copy()
            para['approach'] = approach
            para['dnn_y_hat_file'] = file
            y_hat = result.data
            _rets, _ = return_data.align(y_hat, join='right')
            _mkt_cap, _ = mkt_cap_.align(y_hat, join='right')
            oos_dnn_pf(y_hat, para, save_results=True, buckets=buckets,
                       name='DFN', weighting=weighting, size=size,
                       result_name=para['approach'] + '_' + PATTERN_DNN_PFF,
                       simple_returns=_rets, mkt_cap=_mkt_cap,
                       num_stocks=num_stocks)

    # archive files which have been used already to construct portfolio
    # folder = '_'.join([size, 'dnn_yhat', approach])
    # LoadLocalResult.move_results(files, ['deep_learn', folder])

    if linear is False:
        if which=='buckets':
            return buckets_dfn
        else:
            return

    if which == 'factor':
        _linear_from_results(size=size, method='FM', end_date=end_date,
                         weighting=weighting, buckets=buckets)
    else:
        buckets_linear = _linear_from_results(size=size, method='FM',
                                              end_date=end_date,
                                              weighting=weighting,
                                              buckets=buckets,
                                              which='buckets',
                                              )

        return buckets_dfn, buckets_linear


def _linear_from_results(size='large', method='FM', end_date='20141231',
                         weighting='equal', buckets='ff_style',
                         which='factor'):
    """

    Parameters
    ----------
    size
    method
    end_date
    weighting
    buckets

    Returns
    -------

    """
    logger.info('Loading linear y_hat from file...')

    conditions = dict(size=size, end_date=end_date, method=method)
    result = LoadLocalResult(file_pattern=PATTERN_LINEAR_Y_HAT,
                             conditions=conditions,
                             since='20171001', )

    if which=='factor':
        oos_dnn_pf(result.data, result.parameter, save_results=True,
                   result_name=method + '_' + PATTERN_LINEAR_PFF,
                   name='Linear', buckets=buckets,
                   weighting=weighting, size=size)
    else:
        return  oos_buckets(result.data, result.parameter, buckets=buckets,
                   weighting=weighting, size=size)



def results_dnn_factors(size='large', weighting='equal', buckets='ff_style',
                        linear=True, approach='naive', combination=None,
                        return_file_names=False, which_linear='FM'):
    """ Loads all factor portfolio files with corresponding factor returns
    
    Returns
    -------
    pd.DataFrame
        with index dates, columns portfolio_names and values simple returns
    """
    conditions = dict(size=size, weighting=weighting, buckets=buckets,
                      approach=approach, end_date='20141231')

    files = LoadLocalResult(file_pattern=PATTERN_DNN_PFF,
                            since='20171001', conditions=conditions).files_true

    rets = list()
    counter = 0
    logger.info('Reading DFN return data')
    file_names = dict()
    for file in files:
        counter += 1
        result = LoadLocalResult(file_pattern=file)

        # if result.host=='X1MM':
        #     if not isinstance(result.data, pd.DataFrame):
        #         continue
        #     if 'Linear' not in result.data:
        #         continue

        if isinstance(result.data, pd.DataFrame):
            print(result.data.columns)

        simple_returns = result.data
        name =  'DFN_' + str(counter)
        if return_file_names:
            file_names[name] = result.parameter['dnn_y_hat_file']
        simple_returns.name = name
        rets.append(simple_returns)

    if linear:
        conditions = dict(size=size, weighting=weighting, buckets=buckets,
                          method=which_linear)
        logger.info('Reading Linear return data')
        simple_returns = LoadLocalResult(file_pattern=PATTERN_LINEAR_PFF,
                                since='20170830', conditions=conditions).data
        simple_returns.columns = ['Linear']
        rets.append(simple_returns)

    simple_returns = pd.concat(rets, axis=1)

    if return_file_names is False:
        return simple_returns
    else:
        return simple_returns, file_names


def dnn_FC_sensitivity(size='large', approach='naive', window_type='expanding'):
    """ reads and combines the FC sensitivy data """

    conditions = dict(size=size, approach='naive',
                      start_date ='19700101', window_type='expanding')
    y_hat_FC_files = LoadLocalResult(file_pattern=PATTERN_DNN_FC,
                                     conditions=conditions,
                                     since='20171011').files_true

    mse_dev = list()

    for idx, file in enumerate(y_hat_FC_files):
        results = LoadLocalResult(file_pattern=file)

        df = results.data
        df['file_num'] = idx +1
        mse_dev.append(df)

    mse_dev = pd.concat(mse_dev)
    mse_dev = mse_dev.set_index(['file_num'], append=True).sort_index()

    return mse_dev



def dnn_trading_cost_factors(size='large', weighting='equal', approach='naive',
                             linear=True, buckets='ff_style',
                             combination=None, trade_freq=1):
    """ Gets factor return portfolio instances to analyze factors in detail

    Parameters
    ----------
    size: str
    weighting: str
    approach: str
    linear: str
    buckets: str

    Returns
    -------
    dict
        of criteria portfolio instances

    """

    conditions = dict(size=size, weighting=weighting, buckets=buckets,
                      approach=approach, return_file_names=True)

    _para = dict(size=size, weighting=weighting, buckets=buckets,
                 trade_freq=trade_freq, save_results=False)

    # load returns and file map
    simple_returns, file_name_map = results_dnn_factors(**conditions)
    # determine median performing portfolio
    ri = ReturnIndex(simple_returns=simple_returns.drop('Linear', axis=1))
    column_name = ri.median(column_index=True)
    # load the corresponding predictions to recreate FA instance
    y_hat_dnn = LoadLocalResult(file_pattern=file_name_map[column_name])
    # get fa object
    fa = oos_dnn_pf(y_hat_dnn.data, y_hat_dnn.parameter, name='DFN', **_para)
    # return results
    return_dict = dict(Single=fa.portfolio)

    if combination is not None:

        np.random.seed(1234)
        y_hat_comb = _forecast_combination(list(file_name_map.values()),
                                           combination_number=75)
        # construct combination forecast factor object
        return_dict['Comb'] = oos_dnn_pf(y_hat_comb, y_hat_dnn.parameter,
                                         name='Comb', **_para).portfolio

    if linear:
        # find linear benchmark model
        y_hat_linear = LoadLocalResult(file_pattern=PATTERN_LINEAR_Y_HAT,
                                       since='20170830',
                                       conditions=dict(size=size))
        # construct FA instance
        linear_fa = oos_dnn_pf(y_hat_linear.data, y_hat_linear.parameter,
                               name='Linear', **_para)
        return_dict['Linear'] = linear_fa.portfolio

    return return_dict


def asset_analysis_dnn(simple_returns=None,  which='factor_regression',
                       column='median'):

    if simple_returns is None:
        simple_returns = results_dnn_factors()

    ri = ReturnIndex(simple_returns=simple_returns.drop('Linear', axis=1))

    if column == 'Max':
        column_name = ri.index_level.iloc[-1, :].idxmax()
    elif column == 'Min':
        column_name = ri.index_level.iloc[-1, :].idxmin()
    else:
        column_name = ri.median(column_index=True)

    factors = Factor().get(model='five-factor')
    factors_mom = Factor().get(model='five+mom-factor')
    median_returns = simple_returns[column_name]
    ret_anly = ReturnAnalysis(simple_rets=median_returns,
                              bm_simple_rets=simple_returns['Linear'])

    res_values = dict()

    if 'factor_regression' in which:
        res_values['factor_regression'] = (ret_anly.factor_regression(
            factor_returns=factors), ret_anly.factor_regression(
            factor_returns=factors_mom),)

    if 'per_stat' in which:
        res_values['per_stats'] = ret_anly.performance_stats(annual_factor=12)

    if 'sr_test' in which:
        res_values['sr_test'] = ret_anly.test_diff_sharpe_ratio()

    if 'factor_regression_bm' in which:
        res_values['factor_regression_bm'] = (ret_anly.factor_regression(
            factor_returns=factors, bm=True), ret_anly.factor_regression(
            factor_returns=factors_mom, bm=True))

    return res_values


def decile_pfs():
    """

    Returns
    -------

    """
    weighting = 'mkt_cap'
    res_saver = LocalResults()
    sizes_df = list()
    for size in ['large_mid', 'large', 'mid']:
        fa = construct_pf_from_results(size=size, which='portfolio',
                                       weighting=weighting, linear=True,
                                       buckets='decile', combination=75)

        pf_dfn = fa[0].portfolio.pf_returns()
        pf_linear = fa[1].portfolio.pf_returns()


        stats = list()

        for pf, name in zip([pf_dfn, pf_linear],['DFN', 'Linear']):
            pf_stats = pd.DataFrame(columns=pf_dfn.columns, )
            pf_stats.loc['Mean', ]  = pf.mean()*12
            pf_stats.loc['Std',] = pf.std()*12**0.5
            pf_stats.loc['SR',] = (pf.mean()/pf.std())*12**0.5
            pf_stats.loc[:,'Name'] = name

            stats.append(pf_stats)

        df = pd.concat(stats, )
        df['size'] = size
        df.set_index('Name', append=True, inplace=True)
        sizes_df.append(df)

    df = pd.concat(sizes_df,)
    print(df)
    res_saver.save(df, parameters=dict(weighting=weighting),
                   results_name='decile_pf_stats')


def build_all_pf():
    for size in ['large_mid']:
        for weighting in ['mkt_cap', ]:  # , 'mkt_cap']:
            for bucket in ['decile', ]:  # , 'decile']:
                for comb in [10, 25, 50,]:
                    construct_pf_from_results(size=size,
                                              approach='naive',
                                              weighting=weighting,
                                              combination=comb,
                                              buckets=bucket,
                                              linear=False,
                                              which='factor')


def linear_comparison(predictions=False, pf_construction=False):
    """

    Returns
    -------

    """
    size = 'large'
    weighting = 'equal'
    buckets = 'ff_style'

    if predictions:
        for method in ['FM', 'TF', 'AdaLasso']:
            out_of_sample_linear_model(which=method, save_results=True)

    if pf_construction:
        for method in ['FM', 'TF', 'AdaLasso']:
            _linear_from_results(size=size, method=method)

    rets = list()
    for method in ['FM', 'TF', 'AdaLasso']:
        conditions = dict(size=size, weighting=weighting, buckets=buckets,
                           method=method)
        logger.info('Reading Linear return data')
        simple_returns = LoadLocalResult(file_pattern=PATTERN_LINEAR_PFF,
                                         since='20170930',
                                         conditions=conditions).data
        simple_returns.name = method
        rets.append(simple_returns)

    simple_returns = pd.concat(rets, axis=1)

    return simple_returns




if __name__ == '__main__':
    logging.config.dictConfig(log_set_ups_runs)
    logging.root.addHandler(logging.getLogger('run_log').handlers[0])
    logger.setLevel('INFO')
    # y_hat_ols = out_of_sample_linear_model(use_tf=False, save_results=True)

    # rets = linear_comparison(pf_construction=False)
    # # df = dnn_FC_sensitivity()
    # out_of_sample_linear_model()
    # decile_pfs()
    build_all_pf()

    # out_of_sample_dnn(100, FC_sensitivity=True)
    # # asset_analysis_median_dnn()
    # a,b =  asset_analysis_median_dnn()
    # test = asset_analysis_median_dnn(which='sr_test')
    # out_of_sample_linear_model()
    # construct_factor_pf_from_results(size='mid', linear=True)
    # oos_dnn_pf(plot=True,which=['linear'])

    # res = dnn_trading_cost_factors()

