import datetime as dt
import logging
import shutil

# from tensorflow.python.summary.writer import writer_cache
# import tensorflow.contrib.keras as kr
import keras as kr
import numpy as np
import pandas as pd
import tensorflow as tf
from phdlib.data.api import get_monthly_data, LocalResults, LoadLocalResult, \
    Sector, r2_trans
from phdlib.metrics.deep_learn import HPGenerator, DNNBuilder, DNNTrainer, \
    LinearModel, TensorPandas, hyper_para_string, HP1DGrid
from phdlib.metrics.tools import KFoldCV
from phdlib.misc.api import log_set_ups_runs, load_settings
from phdlib.settings import RESULTS_PATH

__author__ = 'mmess'

logger = logging.getLogger(__name__)

FILE_PATTERN_HYPER = 'hp_simulation'


def load_data(train=True, para=None, dates=None):

    if para is None:
        para = dict(
            size='large',
            demean_ret=True,
            fill_na=True,
            winsorize_prc=None,  # 0.01,
            drop_na=True,
            drop_columns=['cfp_ia', 'roic', 'pchemp_ia', 'ipo'],
            per_rank=True,
        #    sample_balance=4000,
        )
    if dates is None:
        dates = dict(train_start='19700101',
                     train_end='19791231',
                     valid_start='19800101',
                     valid_end='19950131', )

    if train:
        para = {**dict(start_date=dates['train_start'],
                       end_date=dates['train_end'],
                       fc_lead=1,
                       ), **para}
    else:
        para = {**dict(start_date=dates['valid_start'],
                       end_date=dates['valid_end'],
                       fc_lead=0), **para}

    df = get_monthly_data(**para)
    df.dropna(inplace=True)
    df.set_index(['date', 'PERMNO'], append=True, inplace=True)
    X = df.drop(['ret'], axis=1)
    y = df['ret'] * 1000
    # y = y.groupby(level='date').rank(pct=True)*100

    return X, y, para


def hyper_optimization(X, y, hyper_para, para, file_pattern=None):
    """ Performs a hyper-parameter optimization

    Parameters
    ----------
    X: pd.DataFrame
    y: pd.DataFrame
    hyper_para: pd.DataFrame
        with a set of hyper-parameters
    para: dict
        model parameter (size)
    file_pattern: str
        which pattern should be used to save the data

    Returns
    -------
    LocalResults
    """
    cv_runs = para['cv_depth']
    # If each model should be saved
    delete_mode_dir = True
    # If loss data should be saved
    save_performance_results = True

    model_dir = RESULTS_PATH.joinpath('TB').joinpath('keras_debug_tb') # +
    #      dt.datetime.now().strftime('%Y-%m-%d_%H-%M'))

    tf_pd_data = TensorPandas(X.columns)
    # ensure training and validation are from same period (compatibility
    # across runs)
    np.random.seed(10)
    training_data = [(train, valid) for train, valid in KFoldCV(X, y)]

    # initiate DF for result collection
    loss_df = pd.DataFrame(index=hyper_para.index,
                           columns=['dnn', 'linear', 'zero']).fillna(0)
    std_err = pd.DataFrame(index=hyper_para.index,
                           columns=['dnn_std', 'linear_std',
                                    'zero_std']).fillna(0)

    input_columns = len(X.columns)

    if save_performance_results:
        res_saver = LocalResults()
    tmp_file_name = None

    for idx, draw in hyper_para.iterrows():

        count = 0
        try:
            for train_, valid_ in training_data[:cv_runs]:
                count += 1
                logger.info('\n Current run {}/{} \n CV {}/5'.format(idx + 1,
                                                           len(hyper_para),
                                                           count))
                logger.info(draw)
                # current_model_dir = str(
                #     model_dir.joinpath(hyper_para_string(draw) + str(count)))
                current_model_dir = None

                model = DNNBuilder(input_cols=input_columns, output_dim=1,
                                   hidden_units=draw['hidden_units'],
                                   layers=draw['layers'],
                                   dropout=draw['dropout'],
                                   l_2=draw['l2_strength'],
                                   l_1=draw['l1_strength'],
                                   input_dropout=True).model

                DNNTrainer(model, learning_rate=draw['learning_rate'],
                           model_dir=current_model_dir,
                           optimizer=draw['keras_optimizer'],
                           patience=draw['patience'],
                           # loss='mean_absolute_error',
                           ).fit(*tf_pd_data.xy_values(train_),
                                 validation_data=tf_pd_data.xy_values(valid_),
                                 verbose=0)

                loss_df.loc[idx, 'dnn'] += model.evaluate(
                    *tf_pd_data.xy_values(valid_))
                std_err.loc[idx, 'dnn_std']+= model.predict(
                    x=tf_pd_data.x_values(valid_[0])).std()
                kr.backend.clear_session()
                if delete_mode_dir:
                    if current_model_dir is not None:
                        shutil.rmtree(current_model_dir)

        except tf.errors.InvalidArgumentError:
            logger.error('Error occured at run {} cv {}'.format(idx, count))
            loss_df.loc[idx, 'dnn'] = np.nan
            kr.backend.clear_session()
            shutil.rmtree(current_model_dir, ignore_errors=True)

        if save_performance_results:
            tmp_res = pd.concat([loss_df, hyper_para], axis=1)
            res_saver.save(tmp_res, parameters=para,
                           results_name=file_pattern)
            # if old tmp_files are present -> delete as new res are saved)
            if tmp_file_name is not None:
                res_saver.delete(tmp_file_name, raise_error=False)
            tmp_file_name = res_saver.file

    # train linear model and evaluate zero prediction model
    for train_, valid_ in training_data[:cv_runs]:
        linear_model = LinearModel(train_, valid_)
        loss_df['linear'] += linear_model.evaluate()
        loss_df['zero'] += np.mean(valid_[1].values ** 2)
        std_err['linear_std'] += linear_model.predict(X=valid_[0]).std()
        std_err['zero_std']=valid_[1].values.std()

    res = pd.concat([loss_df, hyper_para, std_err], axis=1)
    logger.info('Hyper-search complete, the results are:\n{}'.format(res))

    if save_performance_results:
        res_saver.save(res, parameters=para, results_name=file_pattern,
                       overwrite=True)
        if tmp_file_name is not None and res_saver.file != tmp_file_name:
            res_saver.delete(tmp_file_name, raise_error=False)

    return res


def random_search(which=None):
    """ Performs random search of deep learning

    Parameters
    ----------
    which

    Returns
    -------

    """
    para = load_settings(FILE_PATTERN_HYPER)
    num_draws = para['simulation']['num_draws']
    cv_runs = para['simulation']['cv_depth']
    # load data
    X, y, para = load_data(para=para['data'], dates=para['dates'])
    para['cv_depth'] = cv_runs

    # Draw hyper parameter
    # make random again, otherwise same parameters are run
    np.random.seed(int(dt.datetime.now().timestamp()))
    if which is None:
        which = ['learning_rate', 'l2_strength', 'l1_strength', 'dropout',
                 'hidden_units', 'layers', 'keras_optimizer', 'patience',]
    hyper_para = HPGenerator(which=which, size=num_draws,
                             num_input_units=len(X.columns)).generate()
    logger.info('The following hyper-parameter sets are used: \n {}'.format(
        hyper_para))

    hyper_optimization(X=X, y=y, hyper_para=hyper_para, para=para,
                       file_pattern=FILE_PATTERN_HYPER)


def grid_search(which=None):
    """ 2nd Step Grid search algorithm

    Returns
    -------

    """
    para = load_settings(FILE_PATTERN_HYPER)
    num_grid_points = 20
    cv_runs = para['simulation']['cv_depth']
    # load data
    X, y, para = load_data(para=para['data'], dates=para['dates'])
    para['cv_depth'] = cv_runs

    if which is None:
        which = ['learning_rate', 'l2_strength', 'l1_strength', 'dropout',
                 'hidden_units', 'layers', 'keras_optimizer', 'patience', ]
    #  load optimal parameters
    opt_para = inspect_results(size=para['size'])
    lr = HP1DGrid(which='learning_rate', size=num_grid_points,
                  low_bound=2, upper_bound=6).get()
    hyper_para = pd.DataFrame(columns=which, index=lr.index)

    for parameter in which:
        hyper_para[parameter] = opt_para.loc[0, parameter]

    hyper_para['learning_rate'] = lr['learning_rate']
    logger.info('The following learning rates are used for {}'
                'size Grid search: \n {}'.format(para['size'], hyper_para))

    hyper_optimization(X=X, y=y, hyper_para=hyper_para, para=para,
                       file_pattern='HP_grid_search')


def tensorboard_tuning(size='large'):
    """ Tensorboard tuning

    Returns
    -------

    """

    X, y, data_para = load_data()
    training_data = [(train, valid) for train, valid in KFoldCV(X, y)]
    model_dir = RESULTS_PATH.joinpath('TB').joinpath('tf_debug_tb_' +
                            dt.datetime.now().strftime('%Y-%m-%d_%H-%M'))
    which = ['learning_rate', 'l2_strength', 'l1_strength', 'dropout',
             'hidden_units', 'layers', 'keras_optimizer', 'patience']

    #  load optimal parameters
    draw = inspect_results(size=size, search='grid').loc[
        0, which].to_dict()

    current_model_dir = str(model_dir)
    tf_pd_data = TensorPandas(X.columns)
    input_columns = len(X.columns)

    train_ = training_data[0][0]
    valid_ = training_data[0][1]
    model = DNNBuilder(input_cols=input_columns, output_dim=1,
                       hidden_units=draw['hidden_units'],
                       layers=draw['layers'],
                       dropout=draw['dropout'],
                       l_2=draw['l2_strength'],
                       l_1=draw['l1_strength'],
                       input_dropout=True).model

    DNNTrainer(model, learning_rate=draw['learning_rate'],
               model_dir=current_model_dir,
               optimizer=draw['keras_optimizer'],
               patience=draw['patience'],
               # loss='mean_absolute_error',
               ).fit(*tf_pd_data.xy_values(train_),
                     validation_data=tf_pd_data.xy_values(valid_),
                     verbose=1)

    # hyper_para['hidden_units'] = 38  # 31
    # hyper_para['layers'] = 5  # 12
    # hyper_para['l2_strength'] = 0.01565639704941852  # 0.002044
    # hyper_para['dropout'] = 0.050349424919385655  # 0.138813
    # hyper_para['learning_rate'] = 0.013861455351410994  # 0.000199
    # # hyper_para['keras_optimizer'] = 'Adadelta'
    # hyper_para['l1_strength'] = 0.0010869141938006771
    #
    # optimizer = dict(Nadam=tf.train.keras.optimizers.Nadam)
    #
    # optimizer = optimizer[hyper_para['keras_optimizer']](
    #     lr=float(hyper_para['learning_rate']),
    #     l1_regularization_strength=float(hyper_para['l1_strength']),
    #     l2_regularization_strength=float(hyper_para['l2_strength'])
    # )
    #
    # tf_pandas = TensorPandas(X.columns)
    #
    # losses = {'dnn': 0, 'linear': 0, 'zero_mse': 0}
    #
    # for train_, valid_ in training_data[:1]:
    #     # build model
    #     model = tf.contrib.learn.DNNRegressor(
    #         hidden_units=[hyper_para['hidden_units']] *
    #                      hyper_para['layers'],
    #         model_dir=str(model_dir),
    #         feature_columns=tf_pandas.input_columns(X),
    #         optimizer=optimizer,
    #         dropout=hyper_para['dropout'])
    #
    #     # training_data
    #     input_train = lambda: tf_pandas.input_fn(train_[0], train_[1])
    #     input_valid = lambda: tf_pandas.input_fn(valid_[0], valid_[1])
    #     for _ in range(500):
    #         print('--------{}-----------'.format(_))
    #         model.fit(input_fn=input_train, steps=10)
    #         model.evaluate(input_fn=input_valid, steps=1)
    #         a = model.predict(input_fn=input_valid,as_iterable=False)
    #         print('Std y_hat: {} \n'.format(np.array(a).std()))
    #         print('Std y: {} \n'.format(y.std()))
    #
    # for train_, valid_ in training_data:
    #     losses['linear'] += LinearModel(train_, valid_).train()
    #     losses['zero_mse'] += np.mean(valid_[1].values ** 2)


def inspect_results(size='large', search='random', winsorize=0.01,
                    per_rank=False):
    """ Returns tables of random search

    Parameters
    ----------
    size: str
        which size to use "large", "mid", "small" - none uses all

    Returns
    -------

    """

    conditions = {
        "size": size,
        "start_date": "19700101",
        "end_date": "19811231",
        "demean_ret": True,
        "drop_columns": ["cfp_ia", "roic", "pchemp_ia", "ipo"],
        "fc_lead": 1,
        "drop_na": True,
        "winsorize_prc": winsorize,
        "fill_na": True,
        "per_rank": per_rank
    }

    if search == 'random':
        file_pattern = FILE_PATTERN_HYPER
    elif search == 'grid':
        file_pattern = 'HP_grid_search'

    # results
    files = LoadLocalResult(file_pattern=file_pattern,
                            since='20170821',
                            conditions=conditions).files_true

    results_coll = list()
    for file in files:
        print(file)
        tmp_res = LoadLocalResult(file_pattern=file)
        res = tmp_res.data
        res['Host'] = tmp_res.host
        res['start_date'] = tmp_res.parameter['start_date']
        res['end_date'] = tmp_res.parameter['end_date']
        res['run_time_end'] =  tmp_res.end_time
        res['cv_depth'] = tmp_res.parameter['cv_depth']

        results_coll.append(res)

    res = pd.concat(results_coll)
    print(len(res))
    res['R2_dnn'] = 1 - res['dnn'].divide(res['zero'])
    res['R2_lin'] = 1 - res['linear'].divide(res['zero'])
    res['Rank'] = res['R2_dnn'].rank(ascending=False)

    # add "patience" once new search is over
    cols = ['Rank', 'R2_dnn', 'R2_lin', 'learning_rate', 'l2_strength',
            'l1_strength', 'dropout', 'hidden_units', 'layers',
            'keras_optimizer', 'patience'  ]
    # res.dropna(inplace=True)
    res = res[cols].sort_values('R2_dnn', ascending=False).reset_index(
        drop=True)
    trans_fun = lambda x: r2_trans(x, 12)
    res['R2_dnn'] = res['R2_dnn'].apply(trans_fun)
    res['R2_lin'] = res['R2_lin'].apply(trans_fun)
    logger.info('Top Ten Networks for {} cap stocks are : \n {}'.format(
        size, res.head(10)))


    logger.info('In total {} show R^2 lower than 0.\n'
                'A total of {} have NA values'.format(
        (res['R2_dnn'] < 0).sum(), (res['R2_dnn'].isnull()).sum()))


    return res


def top_hyper_to_json(df, path_or_buf=None):
    """ Selects top optimization result and returns/saves as json

    Parameters
    ----------
    df: pd.DataFrame
        with hyper-parameter columns and a column named "Rank"

    Returns
    -------

    """

    which = ['learning_rate', 'l2_strength', 'l1_strength', 'dropout',
             'hidden_units', 'layers', 'keras_optimizer', 'patience']

    included_columns = list(set(which) & set(df.columns))

    return df.loc[df['Rank'] == 1, included_columns].to_json(
        path_or_buf=path_or_buf, orient='records')


if __name__=='__main__':
    logging.config.dictConfig(log_set_ups_runs)
    logging.root.addHandler(logging.getLogger('run_log').handlers[0])
    logger.setLevel('INFO')
    # tensorboard_tuning()
    # random_search()
    # grid_search()
    # res = inspect_results(search='grid', size='large_mid')
    res = top_hyper_to_json(inspect_results(search='grid', size='large_mid'))
    # tensorboard_tuning()
    # res = inspect_results()


