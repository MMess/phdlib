import logging
from phdlib.output.fm_pols_portfolios import load_portfolio

logger = logging.getLogger(__name__)


def return_index_river(save=False, size='large'):
    """ Plots the return index river chart of the portfolio

    Parameters
    ----------
    size: str
        which size to use

    Returns
    -------

    """
    weighting = 'mkt_cap'
    buckets = 'decile'
    map_weights = {'equal': 'EW', 'mkt_cap': 'VW'}

    simple_returns = load_portfolio(size, weighting=weighting,
                                                     approach=approach,
                                                     buckets=buckets,
                                                     linear=True,
                                                     return_file_names=True)

    ri = ReturnIndex(simple_returns=simple_returns.drop('Linear', axis=1))
    ri_bm = ReturnIndex(simple_returns=simple_returns[['Linear']])

    mkt_returns = Factor().get(factors=['Mkt-RF'],
                               start=simple_returns.index.min(),
                               end=simple_returns.index.max())
    mkt_factor = ReturnIndex(mkt_returns)

    sns.set(style="darkgrid")
    fig, axes = plt.subplots(nrows=2, sharex=True)
    ax1, ax2 = axes[0], axes[1]

    ReturnPlot(ri).river(ri_bm, market_index=mkt_factor,
                              min_max=True, mean=True, figure=fig, ax=ax1)
    ReturnPlot(ri).river(ri_bm, market_index=mkt_factor, use_log=True,
                              min_max=True, mean=True, figure=fig, ax=ax2)

    PF_desc = ', '.join([size.replace('_', ' + ').capitalize(), map_weights[weighting],
                           buckets.replace('_', '-')])
    ax1.set_title('Return Index Portfolios - ' + PF_desc)

    ax1.set_ylabel("Return Index")
    ax2.set_ylabel("Return Index Log Based")
    ax2.set_xlabel("date")

    if save:
        plot_name = '_'.join(['return_river', size, weighting, buckets,
                              approach])
        store_figures(fig, plot_name=plot_name, plot_path=DEEP_PLOT,
                       formats=['pdf', 'png'], factor=(1.5*1.2,1.1*1.3))

    return ax1

def main():
    pass


if __name__ == '__main__':
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
    main()