import logging
import math

import pandas as pd
from sqlalchemy import create_engine

from phdlib import settings
from phdlib.data import results, manager
from phdlib.finance.asset_analysis import ReturnAnalysis
from phdlib.output import lasso_zoo_simulation, lasso_zoo_regression
from phdlib.output.tex_tables import df_to_latex, t_value_stars
from phdlib.output.lasso_zoo_factors import summary_pf_stats,\
    summary_sharpe_ratio_tests, specification_details
from phdlib.settings import RESULTS_PATH
from phdlib.misc import logger_settings

logger = logging.getLogger(__name__)

res_path = RESULTS_PATH.joinpath('lasso_zoo')

def factor_analysis():
    file_name = '2016-10-08_1931_mkt_cap_factor_rets_large_Result.p'
    df_rets = results.load_local_result(file_name)['Results']
    ff_returns = manager.FamaFrenchFactors().get()

    df_rets, ff_returns = df_rets.align(ff_returns, join='left',
                                        axis=0)

    ret_anal = ReturnAnalysis(df_rets['AdaLasso'], bm_simple_rets=
    df_rets['OLS'])

    x = ff_returns.drop('RF', axis=1)
    factor_reg = ret_anal.bm_regression()
    print(factor_reg.summary())

    factor_reg = ret_anal.factor_regression(x, bm=True)
    print(factor_reg.summary())


def create_fc_results(file_name, size, tex_path=settings.TABLE_PATH):
    """ Function creates a tex table for the main regression

    Parameters
    ----------
    tex_path: pathlib.Path
        an existing path where the tables are stored

    Returns
    -------

    """

    res = results.LoadLocalResult(file_pattern=file_name, path=res_path)
    df = res.data*1000
    print(res.parameter)
    df.rename(columns={'OLS_t_values': 't_value', 'OLS_para': 'OLS',
                       'VWAdaLasso': 'VWAL', 'AdaLasso': 'AL'},
              inplace=True)
    df['t_value'] /= 1000
    df['absAda'] = df['AL'].abs()
    df['absT'] = df['t_value'].abs()
    df['absLasso'] = df['Lasso'].abs()
    df['absVWAL'] = df['VWAL'].abs()
    df.sort_values(by=['absVWAL', 'absAda', 'absLasso', 'absT'], inplace=True,
                   ascending=False)
    print(
        'Size: {0} total sum:{1:.2f}'.format(size, df.loc[:, 'absVWAL'].sum()))

    logger.info('The VW adaptive Lasso for {} stocks selects {} FC'.format(size,
        str((df.absVWAL != 0).sum())))
    ind = (df.absAda != 0) | (df.absVWAL != 0) | (df.absLasso != 0) | \
          (df.absT >= 1.645)
    # collect all not printed FC
    dropped = df.loc[ind == False, :].index.unique().tolist()
    # adalasso selected
    # ind = (df.absAda != 0) & (df.absVWAL != 0) & (df.absLasso != 0) & \
    #       (df.absT >= 1.645)
    adaLasso_select = df.loc[df.absVWAL != 0, :].index.unique().tolist()
    # select the FC which we want to print
    df = df.loc[ind, :]
    df.drop(['absAda', 'absT', 'absLasso', 'absVWAL'], axis=1, inplace=True)
    table_letter = ['A', 'B', 'C', 'D', 'E']

    def fun(x):
        if abs(x['t_value']) > 2.575:
            extension = '***'
        elif abs(x['t_value']) > 1.96:
            extension = '**'
        elif abs(x['t_value']) > 1.645:
            extension = '*'
        else:
            extension = ''

        ols = '{0:.2f}{2} ({1:.2f})'.format(x['OLS'], x['t_value'],
                                            extension)
        return ols

    df['OLS'] = df.apply(fun, axis=1)
    # split dataframe into two parts
    df = df[['VWAL','AL', 'OLS', 'Lasso', ]]
    max_rows = math.ceil(len(df) / 2)
    df = pd.concat([df[:max_rows].reset_index(), df[max_rows:].reset_index()],
                   axis=1)
    count_tables = 0
    # get all FC missing in Table into tex form
    drop_tex = ', '.join(['\\textit{{{}}}'.format(x) for x in dropped])
    drop_tex = drop_tex.replace('_', '\\_')
    start =pd.Timestamp(res.parameter['start_date']).strftime('%b %Y')
    end = pd.Timestamp('20141231').strftime('%b %Y')
    description = '\\textbf{{FC Selection {} Stocks:}} The table exhibits' \
                  ' the regression results of OLS, Lasso, ' \
                  'adaptive Lasso (AL), and the value-weighted adaptive Lasso' \
                  ' (VWAL) for each FC, sorted by the absolute ' \
                  'value in the order of the VWAL, AL, Lasso and ' \
                  't-values of the OLS coefficients. The numbers in the ' \
                  'brackets of the OLS column represent t-values. The ' \
                  'table displays only FC having an abs adaptive Lasso or Lasso' \
                  ' coefficient greater than zero, or an abs OLS t-value greater than ' \
                  '1.645. Accordingly, the following FC used in the selection ' \
                  'regression are not represented: {}. Note the coefficients are scaled by a factor' \
                  ' of 1000. ***, ** and * indicate significance at the 1%,' \
                  ' 5% and 10% level, respectively. Cells with blue background' \
                  ' color indicate selected status. The sample period spans' \
                  ' {} until {}.' \
                  ''.format(size.capitalize().replace('_', '\\_'), drop_tex,
                            start, end)
    color_df = pd.DataFrame(columns=df.columns, index=df.index)
    color_df['AL'] = True
    fun_lasso = lambda x: abs(float(x)) > 0
    fun_ols = lambda x: abs(float(x[-5:-1]) > 1.96)
    color_map = [(['AL', 'Lasso', 'VWAL'], fun_lasso, 'blue!10'),
                 (['OLS'], fun_ols, 'blue!10')]

    update_tex = True
    if update_tex is False:
        return adaLasso_select

    for current_df in range(0, len(df), max_rows):
        tmp_df = df[count_tables * max_rows:(count_tables + 1) * max_rows]
        count_tables += 1
        file_path = tex_path.joinpath(
            '{}_fc_results{}.tex'.format(size, count_tables))
        letter = table_letter[count_tables - 1]
        df_to_latex(tmp_df, str(file_path),
                    caption='Regression Result FC {}'.format(letter),
                    table_description=description, scaling=0.9,
                    label='_'.join([size, 'FC_result', letter]),
                    landscape=False,
                    include_index=False,
                    column_format='l|rrrr|l|rrrr',
                    color_cells=color_map)


def simulation(which=None):
    """ Creates the results of the simulation results
    :param which: dict,
        where the key is the id and the value the case name i.e. case 0 etc.
    :return:

    """
    if which is None:
        which = lasso_zoo_simulation.SIM_CASES.keys()
    num_returnFC = 6
    num_riskFC = 6
    num_uniFC = 4
    str_returnFC = 'Return FC'
    str_riskFC = 'Risk FC'
    str_uniFC = 'Unif FC with $\\rho$'
    str_summary = 'Summary FC {}-{}'.format(
        num_returnFC + num_riskFC + num_uniFC, 99)
    first_level = [str_returnFC] * num_returnFC + [str_riskFC] * num_riskFC \
                  + [str_uniFC] * num_uniFC + [str_summary] * 4

    error_ratios = dict()
    # get results
    for case in which:
        error_ratios[case] = lasso_zoo_simulation.error_ratios(
            case).transpose()

    merged_df = pd.concat(error_ratios)
    # TODO keys=list(which.values()
    unif_FC = merged_df.loc[:, 16:].transpose()
    summary = [unif_FC.mean(), unif_FC.std(), unif_FC.min(), unif_FC.max()]
    summary = pd.concat(summary, keys=['mean', 'std', 'min', 'max'], axis=1)
    merged_df = pd.concat([merged_df.loc[:, 0:15], summary], axis=1)

    sec_level = merged_df.columns.tolist()
    tuples = list(zip(*[first_level, sec_level]))
    index = pd.MultiIndex.from_tuples(tuples, names=['first', 'second'])
    merged_df.columns = index
    merged_df.index.names = ['Case', 'Method']

    # TODO rename index level 7->case0 etc.
    # copy relevant tables to table by hand

    merged_df[merged_df == 0] = ' '
    merged_df.reset_index(inplace=True)
    merged_df.loc[merged_df.index.isin(
        range(0, len(merged_df), 7)) == False, 'Case'] = ' '
    merged_df['Case'] = merged_df['Case'].apply(lambda x: x[-1])
    merged_df['Method'] = merged_df['Method'].apply(
        lambda x: x.replace('_', ' '))
    print(merged_df.to_latex(float_format=lambda x: '%1.2f' % x))

    label = 'simulation_res'
    caption = label.replace('_', ' ')
    table_name = label + '.tex'
    table_path = settings.LASSO_TABLES.joinpath(table_name)

    bold = '\\textbf{{Simulation Results}}'

    des = bold + ' The Table provides an overview of type II (for priced factor ' \
                 'FC, FC 0-5) and type I error (for unpriced factor FC, ' \
                 '6-11,  and spurious FC, 12-99) ratio behavior in percentage' \
                 ' points for the specified simulation cases. All details of ' \
                 'the cases can be found in section \\ref{sec:sensitivity}. FC ' \
                 'cases 12-15 are interesting in so far, as they are spurious ' \
                 'FC with high correlations to priced and unpriced FC. '

    merged_df.rename(columns={'Return FC': '$\\Pc$ --- Type II',
                              'Risk FC': '$\\Uc$ --- Type I',
                              'Unif FC with $\\rho$': '$\\Sc$ --- Type I',
                              }, inplace=True)

    def fun_float(x):
        try:
            float(x)
            ret = True
        except:
            ret = False
        return ret

    color_table = dict(fun=fun_float, base_color='gray!',
                       columns=['$\\Pc$ --- Type II',
                                '$\\Uc$ --- Type I',
                                '$\\Sc$ --- Type I'])
    # color_table = None
    df_to_latex(merged_df, path_table=table_path, include_index=False,
                caption=caption, label=label, table_description=des,
                centering=True, midrule=7, add_col_name=False,
                scaling=0.78, column_format='ll|cccccc|cccccc|cccc|cccc',
                color_table=color_table)


def linearity_test():

    start_date = pd.Timestamp('19700101')
    res = lasso_zoo_regression.linearity_pf_test()

    reg, pf = res['POLS_factor'], res['PF']

    reg.drop(['date'], axis=0, inplace=True)
    pf.drop(['date','PERMNO'], axis=0, inplace=True)

    coverage = pd.DataFrame(index=['Coverage', 'Corr'],
                            columns=pf.columns)

    comp_1 = pd.Series(index=reg.index)

    for model in ['Uni', 'CAPM', 'FF3', 'Carhart']:

        tmp_reg = reg.loc[:, ('full', model, 't-value')]
        a = tmp_reg > 1.96
        comp_1.loc[a] = 1
        b = tmp_reg < -1.96
        comp_1.loc[b] = 2
        comp_1.fillna(0, inplace=True)

        for bucket in ['decile', 'ff_style']:
            for weight in ['equal']:
                tmp_pf = - pf.loc[:, (model, weight, bucket)]
                comp_2 = pd.Series(index=tmp_pf.index)
                a = tmp_pf > 1.96
                b = tmp_pf < -1.96
                comp_2.loc[a] = 1
                comp_2.loc[b] = 2
                comp_2.fillna(0, inplace=True)

                cov = (comp_1 == comp_2).dropna().sum() / len(comp_1.dropna())
                coverage.loc['Coverage', (model, weight, bucket)] = cov

                corr = pd.concat([tmp_pf, tmp_reg], axis=1).astype(float)
                corr.columns = ['a', 'b']
                coverage.loc['Corr', (model, weight, bucket)] = \
                    corr.corr().loc['a', 'b']

    coverage.dropna(axis=1, inplace=True)
    coverage.rename(columns={'ff_style': 'FF Style'}, level='Cutoff',
                    inplace=True)
    coverage.columns = coverage.columns.droplevel('Weights')

    # coverage.to_latex(str(table_path),
    #                   float_format=lambda x: '%1.2f' % x)

    label = 'linear_vs_pf'
    caption = label.replace('_', ' ')
    table_name = label + '.tex'
    table_path = settings.LASSO_TABLES.joinpath(table_name)

    bold = '\\textbf{{Linear model vs. portfolio sorting}}'

    des = bold + ' The table shows a comparison of portfolio sorts and ' \
                 'a linear regression approach (pooled OLS). ' \
                 'The row "coverage" indicates the classification overlap. ' \
                 'It expresses the ratio of the number of FC, which share' \
                 ' the same classification (i.e. significant or not) over the total' \
                 ' number of FC considered. The row "corr" measures ' \
                 'the correlation between the t-value of the coefficient of ' \
                 'the regression model and the t-value of the ' \
                 '$\\alpha$ of the long-short portfolio. The model "Uni" ' \
                 'measures the differences between the hedge portfolio ' \
                 'and a univariate regression. The column ' \
                 '"FF3" measures the $\\alpha$  w.r.t to \\citet{{fama1993}} ' \
                 'three-factor model. In this case the benchmark is a' \
                 ' multivariate regression, where the FC  ' \
                 'variable is augmented by "beta", "bm" and "mve" on the right' \
                 ' hand side. ' \
                 'The other models are adjusted accordingly. ' \
                 'The data includes all stocks, from {} until {}.' \
                 ''.format(start_date.strftime(format='%Y-%m-%d'),
                           '2014-12-31')
    df_to_latex(coverage, path_table=table_path, include_index=True,
                caption=caption, label=label, table_description=des,
                centering=True)

    table_path = settings.LASSO_TABLES.joinpath('alt_linear_pf.tex')
    coverage.to_latex(str(table_path))


def create_firm_char_table(tex_path=settings.LASSO_TABLES, max_rows=40):
    """The function creates the table(s) of firm characteristics
    :param df:
    :param tex_path:
    :return:
    """
    db_connection = create_engine(settings.SQL_URL)
    df = pd.read_sql('firm_characteristics', db_connection)
    table_letter = ['A', 'B', 'C', 'D', 'E']
    description = 'The Table displays the firm characteristics used. Most ' \
                  'definitions are taken from \citet{green2014}. If not otherwise ' \
                  'stated, accounting ratios refer always to fiscal year end values.'
    df.rename(columns={'LiteratureKey': 'Reference'}, inplace=True)
    df['Reference'] = df['Reference'].apply(lambda x: '\citet{{{}}}'.format(x))

    count_tables = 0
    for current_df in range(0, len(df), max_rows):
        tmp_df = df[count_tables * max_rows:(count_tables + 1) * max_rows]
        count_tables += 1
        file_path = tex_path.joinpath(
            'firm_characteristics_{}.tex'.format(count_tables))

        letter = table_letter[count_tables - 1]
        df_to_latex(tmp_df, str(file_path),
                    caption='{}, Overview of firm characteristics:'.format(
                        letter),
                    table_description=description, scaling=0.7,
                    label='_'.join(['FC', letter]), landscape=True)


def portfolio_comparison():


    sr_test_ada = summary_sharpe_ratio_tests(sign=1)
    sr_test_ols = summary_sharpe_ratio_tests(sign=-1)
    means = summary_pf_stats(which='Mean')
    sr = summary_pf_stats(which='SR')

    file_path = settings.LASSO_TABLES.joinpath('sr_adalasso.tex')
    df_to_latex(sr_test_ada.reset_index(), path_table=file_path,
                include_index=False, column_format='l|ccccc|c')

    file_path = settings.LASSO_TABLES.joinpath('sr_ols.tex')
    df_to_latex(sr_test_ols.reset_index(), path_table=file_path,
                include_index=False, column_format='l|ccccc|c')

    file_path = settings.LASSO_TABLES.joinpath('means_pf.tex')
    df_to_latex(means.reset_index(), path_table=file_path,
                include_index=False, column_format='l|ccccc|c', midrule=2)

    file_path = settings.LASSO_TABLES.joinpath('sr_pf.tex')
    df_to_latex(sr.reset_index(), path_table=file_path,
                include_index=False, column_format='l|ccccc|c', midrule=2)

def portfolio_details():

    size_map = dict(large_mid='Large + Mid cap', large='Large cap', mid='Mid cap',
                    small='Small cap',  all='All')
    for size in ['all', 'large_mid', 'large', 'mid', 'small']:
        df = specification_details(size=size)
        df.reset_index(inplace=True)

        df.loc[df.index.isin( range(0, len(df), 32)) == False, 'FC OOS'] = ''
        df.loc[df.index.isin(range(0, len(df), 8)) == False, 'Window'] = ''
        df.loc[df.index.isin(range(0, len(df), 8)) == False, 'Type'] = ''
        df.loc[df.index.isin(range(0, len(df), 4)) == False, 'Buckets'] = ''
        df.loc[df.index.isin(range(0, len(df), 2)) == False, 'Weights'] = ''
        t_value = df.loc[:,('SR Test', 'AdaLasso-OLS')].apply(lambda x: t_value_stars(x))
        df.loc[:,('SR Test', 'AdaLasso-OLS')] =t_value

        label = size + '_pf_details'
        file_path = settings.LASSO_TABLES.joinpath(label + '.tex' )
        caption = label.replace('_', '')
        des = '\\textbf{{Long-short portfolio performance, {} stocks ' \
              'specifications}}. The table shows selected portfolio ' \
              'characteristics in annualized figures. "FC OOS" indicates ' \
              'whether the FC are ' \
              'only included in the post-publication period. The Sharpe ratio' \
              ' test column reflects t-values of the ' \
          '\\citet{{ledoit2008}} test. The sample period ranges from Jan 1972 ' \
              'until Dec 2014. Different window lengths are not forced on' \
              'the same out-of-sample start point, hence, each time window' \
              'has different return series lengths. ' \
              '***, ** and * indicate significance at the 1%, 5% and ' \
              '10% level, respectively.' \
              ''.format(size_map[size])
        df_to_latex(df, path_table=file_path,
                    column_format='llllll|cc|cc|c|cc', include_index=False,
                    add_col_name=False, scaling=0.70,
                    midrule=32,centering=True, caption=caption,
                    table_description=des, label=label)


def update_FC_selection_tables():
    tables = [
        {'file_name':
             '2017-12-11_093154_small_LassoZoo_pooled_regression_Result.h5',
         'size': 'small'},
        {'file_name':
             '2017-12-11_091128_mid_LassoZoo_pooled_regression_Result.h5',
         'size': 'mid'},
        {'file_name':
             '2017-12-11_090359_large_LassoZoo_pooled_regression_Result.h5',
         # '2017-12-21_215621_large_LassoZoo_pooled_regression_Result.h5',
         'size': 'large'},
        {'file_name':
             '2017-12-11_101455_all_LassoZoo_pooled_regression_Result.h5',
        # '2017-12-21_232726_all_LassoZoo_pooled_regression_Result.h5',
          'size': 'all'},
        {'file_name':
             '2017-12-11_091941_large_mid_LassoZoo_pooled_regression_Result.h5',
         'size': 'large_mid'}

    ]
    selected = []

    for para in tables:
        para['tex_path'] = settings.LASSO_PATH.joinpath('tables')
        create_fc_results(**para)
        # print(selected)
        # selected.append(set(create_fc_results(**para)))

    # print(set.intersection(*selected))


if __name__ == '__main__':
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
    # simulation(which=['Case_1', 'Case_4', 'Case_2', 'Case_3',])
    # portfolio_comparison()
    update_FC_selection_tables()
    # linearity_test()
    # update_FC_selection_tables()
    # simulation()
    # portfolio_details()
    # update_FC_selection_tables()
