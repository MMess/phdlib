import logging

import pandas as pd
import numpy as np
from scipy.stats import norm

from phdlib import settings
from phdlib.data.manager import get_pf_data
from phdlib.data.api import LocalResults, LoadLocalResult
from phdlib.finance.asset_analysis import ReturnAnalysis
from phdlib.finance.factor_analysis import FactorAnalysis
from phdlib.output.lasso_zoo_regression import LassoZooPrediction
from phdlib.output.tex_tables import t_value_stars, df_to_latex
from phdlib.misc.api import log_set_ups_runs, load_settings, loop_tracker
from phdlib.settings import RESULTS_PATH
from phdlib.output.fm_pols_portfolios import load_portfolio

logger = logging.getLogger(__name__)

res_path = RESULTS_PATH.joinpath('lasso_zoo')

predict_pattern = 'y_hat'
pf_pattern = 'pf'
path_rets = res_path.joinpath('portfolio_rets')


def estimation_cases():
    """ Specifies all estimation parameters, all "reasonable" combinations

    Returns
    -------
    list: all cases "cartesian product"
        (some cases are ruled out, hence no itertools product feasible)
    """

    time_windows = [10 * 12, 15 * 12, 20 * 12, ]
    window_types = ['expanding', 'rolling']
    sizes = ['all', 'large', 'mid','large_mid', 'small']# 'small', 'all',]
    if sizes is None:
        size = load_settings('lasso_predictions')['size']
        sizes = [size,]

    winsorizes = [0.01, 0.05]
    true_OOSs = [True, False]
    second_stages = ['OLS']

    start_date = '19720101'
    end_date = '20141231'

    parameters = list()
    logger.info('Case constructed for the following sizes: {}'.format(sizes))
    for time_window in time_windows:
        for window_type in window_types:
            # does not make sense to do expanding with more data 15*12
            if window_type == 'expanding' and time_window != 15 * 12:
                continue
            for size in sizes:
                for true_oos in true_OOSs:
                    for sec_stg in second_stages:
                        for winsorize in winsorizes:

                            tmp = dict(type=window_type, window=time_window,
                                       second_stage=sec_stg, true_oos=true_oos,
                                       size=size, start_date=start_date,
                                       end_date=end_date,
                                       winsorize_prc=winsorize)
                            if size == 'all':
                                tmp['sample_balance'] = 4000
                            elif size == 'small':
                                tmp['sample_balance'] = 2000
                            else:
                                tmp['sample_balance'] = None

                            parameters.append(tmp)

    return parameters


def estimate_predict():
    """ Selects FC -> estimates coefficients -> stock return predictions

    Returns
    -------

    """
    cases = estimation_cases()

    logger.info('A total of {} cases will be considered'.format(len(cases)))

    path = res_path.joinpath('predictions')
    for case in cases:
        # saving name (actually not needed, provide some human readable info)
        result_name = '_'.join([case['size'], str(case['winsorize_prc']),
                                case['type'], str(case['window']), 'OOS',
                                str(case['true_oos']), '2nd',
                                case['second_stage'], predict_pattern])
        local_res = LocalResults(path=path)

        logger.info('Running case: {}'.format(result_name))
        # Prediction class,
        ret_h = LassoZooPrediction(**case)

        case['method'] = 'AdaLasso'
        y_hat = ret_h.stock_predictions['AdaLasso']
        case['missing_dates'] = ret_h.num_missing_dates['AdaLasso']
        local_res.save(data=y_hat, parameters=case,
                       results_name=result_name + '_AdaLasso')
        case['method'] = 'OLS'
        y_hat = ret_h.stock_predictions['OLS']
        case['missing_dates'] = ret_h.num_missing_dates['OLS']
        local_res.save(data=y_hat, parameters=case,
                       results_name=result_name + '_OLS')


def portfolio_returns():
    """ Loads prediction results and calculates long_short portfolios

    Returns
    -------

    """
    since = '20171202'
    cases = estimation_cases()
    path_y_hat = res_path.joinpath('predictions')

    # load returns and market cap data for all stocks and all dates
    returns, mkt_cap = get_pf_data()
    count=0
    for case in cases:
        count += 1
        loop_tracker(count, total_iterations=len(cases), name='Portfolio Rets '
                                                              'AdaLasso + OLS')
        for method in ['OLS', 'AdaLasso']:

            current_case = case.copy()
            current_case['method'] = method
            # y_hat (y_h) 30.6 contains predictions for 31.7
            y_hat = LoadLocalResult(
                file_pattern=predict_pattern + '_' + method,
                since=since, conditions=current_case, path=path_y_hat).data

            _ret, AL_y_h = returns.align(y_hat, join='right')
            _mkt_cap, AL_y_h = mkt_cap.align(y_hat, join='right')

            for bucket in ['decile', 'ff_style']:
                for weighting in ['mkt_cap', 'equal']:
                    logger.info('Constructing portfolio: {} weighted, {} '
                                'cutoff and {}'.format(weighting, bucket,
                                                       method))
                    local_res = LocalResults(path=path_rets)
                    # create and get factor returns
                    FA = FactorAnalysis(ascending=False, simple_returns=_ret,
                                        buckets=bucket, market_value=_mkt_cap,
                                        criteria=y_hat, weighting=weighting,
                                        min_max_mkt_cap=_ret).portfolio.factor_return
                    FA.name = method
                    # save results
                    name = '_'.join([case['size'], bucket, weighting,
                                     str(case['winsorize_prc']),
                                     case['type'], str(case['window']), 'OOS',
                                     str(case['true_oos']), '2nd',
                                     case['second_stage'], pf_pattern, method])
                    current_case['weighting'] = weighting
                    current_case['buckets'] = bucket
                    local_res.save(data=FA, parameters=current_case,
                                   results_name=name)


def get_portfolio(size='large', weighting='mkt_cap', buckets='decile',
                  method='OLS', true_oos=True,  window_length=144,
                  window_type='expanding',  winsorize_prc=0.01,
                  second_stage='OLS'):
    """ Loads all factor portfolio files with corresponding factor returns

    Returns
    -------
    pd.DataFrame
        with index dates, columns portfolio_names and values simple returns
    """
    conditions = dict(size=size, method=method, true_oos=true_oos,
                      type=window_type, window=window_length,
                      weighting=weighting,
                      buckets=buckets, second_stage=second_stage)

    if winsorize_prc is not None:
        conditions['winsorize_prc'] = winsorize_prc

    simple_returns = LoadLocalResult(file_pattern=pf_pattern + '_' + method,
                                     conditions=conditions, since='20171203',
                                     path=path_rets).data

    return simple_returns


def pf_results(save=True):
    """ Loads returns from result files

    Returns
    -------

    """
    since = '20171126'
    cases = estimation_cases()
    all_results = list()
    all_info = list()

    if save:
        local_res = LocalResults(path=res_path)

    multi_columns = pd.MultiIndex.from_product((['SR', 'Mean', 'StdErr'],
                                                ['AdaLasso', 'OLS']),
                                               names=['Measure','Method', ])
    case_id = 0
    for case in cases:

        logger.info('Current case: \n {}'.format(case))
        for bucket in ['decile', 'ff_style']:
            for weighting in ['mkt_cap', 'equal']:
                # load return data
                case['method'] = 'AdaLasso'
                case['weighting'] = weighting
                case['buckets'] = bucket
                fa_rets_ada = LoadLocalResult(
                    file_pattern=pf_pattern + '_' + case['method'],
                    since=since, conditions=case, path=path_rets).data
                case['method'] = 'OLS'
                fa_rets_OLS = LoadLocalResult(
                    file_pattern=pf_pattern + '_' + case['method'],
                    since=since, conditions=case, path=path_rets).data
                # concat return data
                simple_returns = pd.concat([fa_rets_ada, fa_rets_OLS], axis=1)

                # calculate returns statistics
                res = pd.DataFrame(columns=multi_columns, index=[case_id])
                case_info = pd.DataFrame(case, index=[case_id])

                for method in ['AdaLasso', 'OLS']:
                    rets = simple_returns[method]
                    res.loc[:, ('Mean', method,)] = rets.mean() * 12
                    res.loc[:, ('StdErr', method,)] = rets.std() * 12 ** 0.5
                    res.loc[:,
                    ('SR', method,)] = 12 ** 0.5 * rets.mean() / rets.std()
                    if method is 'AdaLasso':
                        ra = ReturnAnalysis(simple_returns['AdaLasso'],
                                       simple_returns['OLS'])
                        t_value = ra.test_diff_sharpe_ratio().t_value()
                        res.loc[:, ('SR Test', 'AdaLasso-OLS')] = t_value

                all_results.append(res)
                all_info.append(case_info)

                case_id += 1

    case_info = pd.concat(all_info)
    case_info.index.name = 'case_id'
    df = pd.concat(all_results)
    df.index.name ='case_id'


    # meta comparison
    total_num = len(all_results)
    logger.info('Total specifications:{}'.format(total_num))

    num_ada = (df.loc[:, ('Mean', 'AdaLasso')] > df.loc[:, ('Mean', 'OLS')]).sum()
    logger.info('In {} out of {} cases ({}%) mean of AdaLasso > OLS'.format(
        num_ada, total_num, num_ada/total_num*100))

    num_ada = (df.loc[:, ('SR', 'AdaLasso')] > df.loc[:, ('SR', 'OLS')]).sum()
    logger.info('In {} out of {} cases ({}%) SR of AdaLasso > OLS'.format(
        num_ada, total_num, num_ada/total_num*100))

    num_ada = (df.loc[:, ('SR Test', 'AdaLasso-OLS')] > 1.96).sum()
    logger.info('In {} out of {} cases ({}%) SR test signficant'.format(
        num_ada, total_num, num_ada/total_num*100))

    if save:
        data = dict(results=df, cases=case_info)
        local_res.save(data=data, results_name='all_pf_summary',
                       parameters=dict(project='lasso_zoo'))


    print(df)
    return df, case_info


def summary_pf_stats(which='Mean'):
    res = LoadLocalResult(file_pattern='all_pf_summary', since='20171201',
                          which='latest', path=res_path)

    cases = res.data['cases']
    df = res.data['results']
    map_yes_no = dict()
    map_yes_no[True] = 'Yes'
    map_yes_no[False] = 'No'

    column_map = dict(large='Large', mid='Mid', large_mid='Large + Mid',
                      small='Small', all='All')

    # table1

    sizes = ['large', 'mid', 'small', 'large_mid', 'all']
    table = pd.DataFrame(columns=sizes, index=['Yes', 'No'])
    for size in sizes:
        for true_oos in [True, False]:

            ind =cases[(cases['size']==size) & (cases['true_oos']==true_oos)].index
            sub_df = df.loc[ind,:]
            higher_ada = (sub_df.loc[:, (which, 'AdaLasso')] >
                          sub_df.loc[:, (which, 'OLS')]).sum()

            total_num = len(sub_df)

            table.loc[map_yes_no[true_oos], size] = '{} of {} ({:2.0f}%)'.format\
                (higher_ada,    total_num,    higher_ada / total_num*100)

        ind = cases[(cases['size'] == size)].index
        sub_df = df.loc[ind, :]
        higher_ada = (sub_df.loc[:, (which, 'AdaLasso')] >
                      sub_df.loc[:, (which, 'OLS')]).sum()

        total_num = len(sub_df)

        table.loc['Total', size] = '{} of {} ({:2.0f}%)'.format\
                (higher_ada,    total_num,    higher_ada / total_num*100)

    for true_oos in [True, False]:
        ind = cases[(cases['true_oos']==true_oos)].index
        sub_df = df.loc[ind, :]
        higher_ada = (sub_df.loc[:, (which, 'AdaLasso')] >
                      sub_df.loc[:, (which, 'OLS')]).sum()

        total_num = len(sub_df)

        table.loc[map_yes_no[true_oos], 'Total'] = '{} of {} ({:2.0f}%)'.format \
            (higher_ada, total_num, higher_ada / total_num * 100)

    higher_ada = (df.loc[:, (which, 'AdaLasso')] > df.loc[:, (which, 'OLS')]).sum()
    total_num = len(df)
    table.loc['Total', 'Total'] = '{} of {} ({:2.0f}%)'.format \
            (higher_ada, total_num, higher_ada / total_num * 100)
    table.index.name = 'FC OOS'

    # print(df.loc[(cases['true_oos']==True) & (cases['size']=='small'),:])
    table.rename(columns=column_map, inplace=True)
    print(table)
    return table


def specification_details(size='large_mid'):

    res = LoadLocalResult(file_pattern='all_pf_summary', since='20171201',
                          which='latest', path=res_path)

    cases = res.data['cases']
    df = res.data['results']
    df.replace([np.inf, -np.inf], np.nan, inplace=True)
    df = df[df.columns.sort_values()]
    if size is not None:
        df= df.loc[cases['size']==size]
    for method in ['AdaLasso', 'OLS']:
        df.loc[:,('Mean', method)] =  df.loc[:,('Mean', method)]*100
        df.loc[:, ('StdErr', method)] = df.loc[:, ('StdErr', method)] * 100
    cases.drop(['start_date', 'end_date', 'sample_balance', 'method',
                'second_stage', 'size'],
               axis=1, inplace=True)

    # cases.set_ind
    new_names = ['FC OOS', 'Window', 'Type','Buckets', 'Weights', 'Win',]
    cols = ['true_oos', 'window', 'type' ,'buckets', 'weighting',
            'winsorize_prc']
    rename_cols = dict(zip(cols, new_names))
    cases.rename(columns=rename_cols, inplace=True)

    weights_map = dict(mkt_cap='VW', equal='EQ')
    cases['Weights'] = cases['Weights'].apply(lambda x: weights_map[x])
    map_yes_no = dict()
    map_yes_no[True] = 'Yes'
    map_yes_no[False] = 'No'
    print(cases.columns)
    cases['FC OOS'] = cases['FC OOS'].apply(lambda x: map_yes_no[x])
    buckets_map = dict(decile='Decile', ff_style='FF Style')
    cases['Buckets'] = cases['Buckets'].apply(lambda x: buckets_map[x])

    cases = cases.reset_index().set_index(new_names)

    df = df.merge(cases, left_index=True, right_on='case_id',
                  how='left')

    df.drop('case_id', axis=1, inplace=True)
    df.columns = pd.MultiIndex.from_tuples(df.columns,
                                           names=['Measure', 'Method'])

    df.sort_index(inplace=True)

    # df.columns = pd.MultiIndex(df.columns)

    return df



def summary_sharpe_ratio_tests(level=0.05, sign=1):
    res = LoadLocalResult(file_pattern='all_pf_summary', since='20171201',
                          which='latest', path=res_path)

    cases = res.data['cases']
    df = res.data['results']
    df.replace([np.inf, -np.inf], 0, inplace=True)
    column_map = dict(large='Large', mid='Mid', large_mid='Large + Mid',
                      small='Small', all='All')

    sizes = ['large', 'mid', 'small', 'large_mid', 'all']
    table = pd.DataFrame(columns=sizes, index=[10,5,1])

    for size in sizes:
        for level in [0.1, 0.05, 0.01]:
            cutoff = abs(norm.ppf(level / 2))
            ind = cases[(cases['size'] == size)].index
            sub_df = df.loc[ind, :]
            higher_ada = (
            sign*sub_df.loc[:, ('SR Test', 'AdaLasso-OLS')] > cutoff).sum()
            total_num = len(sub_df)
            table.loc[level*100, size] = '{} of {} ({:2.1f}%)'.format \
                (higher_ada, total_num, higher_ada / total_num * 100)

            higher_ada = (
                sign *df.loc[:, ('SR Test', 'AdaLasso-OLS')] > cutoff).sum()
            total_num = len(df)
            table.loc[level*100, 'Total'] = '{} of {} ({:2.1f}%)'.format \
                (higher_ada, total_num, higher_ada / total_num * 100)

    table.index = ['{:2.0f}%'.format(x) for x in table.index]

    table.index.name = 'Level'

    table.rename(columns=column_map, inplace=True)
    print(table)
    return table



def lasso_factor_analysis():
    """ Runs the portfolio analysis of the paper

    Returns
    -------

    """
    # create all cases
    parameters = list()
    for time_window in [10 * 12]:
        for window_type in ['rolling']:
            if window_type == 'expanding' and time_window != 15 * 12:
                continue
            for size in ['large']:
                for true_oos in [True, False]:
                    for sec_stg in ['OLS']:

                        tmp = dict(size=size, type=window_type,
                                   window=time_window, start_date='19720101',
                                   second_stage=sec_stg, true_oos=true_oos,
                                   end_date='19851231')
                        if size == 'all':
                            tmp['sample_balance'] = 4000
                        elif size == 'small':
                            tmp['sample_balance'] = 2000
                        else:
                            tmp['sample_balance'] = None

                    parameters.append(tmp)

    # parameters = [
    #     {'size': 'all', 'sample_balance': 4800, 'type': 'expanding',
    #      'window': 12 * 20, 'start_date': '19650101',
    #      'buckets': 'ff_style'}]

    predict_para = ('start_date', 'sample_balance', 'size', 'type', 'window',
                    'second_stage', 'end_date')
    factor_para = ('start_date', 'size')
    missing_para = list()
    # loop through all desired specifications
    for parameter in parameters:
        local_res = LocalResults()

        # get stock return prediction of lasso and ols
        tmp_para = dict((k, parameter[k]) for k in predict_para)
        # try:
        ret_h = LassoZooPrediction(**tmp_para)
        lasso_predictions = ret_h.stock_predictions['AdaLasso']
        ols_predictions = ret_h.stock_predictions['OLS']
        # except ValueError:
        #     missing_para.append(parameter)
        #     continue
        # get factor returns for different buckets
        for bucket in ['ff_style', 'decile']:

            tmp_para = dict((k, parameter[k]) for k in factor_para)
            lasso_factor = FactorAnalysis(criteria=lasso_predictions,
                                          ascending=False, buckets=bucket,
                                          **tmp_para)
            OLS_factor = FactorAnalysis(criteria=ols_predictions,
                                        ascending=False, buckets=bucket,
                                        **tmp_para)

            # equal and mkt_cap weighted factor returns
            for weighting in ['equal', 'mkt_cap']:
                result_name = '_'.join(
                    [weighting, 'factor_rets', parameter['size'],
                     bucket, str(parameter['window'])])
                lasso_factor.portfolio.weighting = weighting
                OLS_factor.portfolio.weighting = weighting
                adaLasso_ret = lasso_factor.portfolio.factor_return
                adaLasso_ret.name = 'AdaLasso'
                OLS_ret = OLS_factor.portfolio.factor_return
                OLS_ret.name = 'OLS'
                parameter['weighting'] = lasso_factor.portfolio.weighting
                res_data = pd.concat([adaLasso_ret, OLS_ret], axis=1)
                local_res.save(data=res_data,
                               parameters=parameter, results_name=result_name)

    if len(missing_para) > 0:
        logger.warning(
            'The following parameter set(s) have not finished because'
            'of missing coef values: \n{}'.format(missing_para))

    return lasso_factor, OLS_factor


def read_results():
    # get all results combinations
    results_list = list()
    sharpe_ratio_tests = list()
    for time_window in [12 * 20, 12 * 15, 12 * 10]:
        for bucket in ['ff_style', 'decile']:
            for window_type in ['rolling', 'expanding']:
                for size in ['all', 'large', 'mid', 'small']:
                    for weighting in ['equal', 'mkt_cap']:
                        for second_stage in ['OLS', None]:  # None]:
                            name = '_'.join([weighting, 'factor_rets',
                                             size, bucket, str(time_window)])

                            tmp = {'name': name,
                                   'size': size,
                                   'bucket': bucket,
                                   'time_window': time_window,
                                   'weighting': weighting,
                                   'window_type': window_type,
                                   'second_stage': second_stage}

                            results_list.append(tmp)

    for res_para in results_list:
        # 19740630 19650101
        if res_para['size'] in ['mid', 'small']:
            start_date = '19740630'
        else:
            start_date = '19650101'

        para = {'type': res_para['window_type'], 'start_date': start_date,
                'second_stage': res_para['second_stage']}

        try:
            if res_para['name'] == 'equal_factor_rets_small_ff_style_120':
                print('hello')
            res = results.LoadLocalResult(res_para['name'],
                                          conditions=para, since='20170101',
                                          which='latest')
        except ValueError:
            if res_para['window_type'] != 'expanding':
                print(res_para['name'])
            continue

        sr_test = sharp_ratio_res(res.data)

        if res_para:
            pass
        # try:
        #     tmp = res.parameters['second_stage']
        #     if tmp is None:
        #         second_stage = '1st_stage'
        #     else:
        #         second_stage = tmp
        # except KeyError:
        #     second_stage = 'OLS'
        #     res.update_parameters({'second_stage': 'OLS'})
        res_para.pop('name')
        res_para['SR_test'] = sr_test
        res_para['RT_end'] = res.end_time
        sharpe_ratio_tests.append(res_para)

    df = pd.DataFrame(sharpe_ratio_tests)
    df.second_stage.fillna('1st_stage', inplace=True)
    df['RT_end'] = df['RT_end'].apply(lambda x: x.strftime('%b-%d %H:%M'))
    sort_cols = ['window_type', 'size', 'weighting', 'bucket', 'second_stage']

    first_stage = df['second_stage'] == '1st_stage'
    OLS = df['second_stage'] == 'OLS'
    est_1st = 'i.e. using the respective first stage selection method also for' \
              ' the coefficient estimation.'
    est_OLS = 'i.e. using OLS for coefficient estimation'

    sr_descr = ' The column "SR test" represents ' \
               'the \\citet{ledoit2008}test. ' \
               'A negative test statistic indicates a higher SR ratio for the ' \
               'adaptive Lasso factor compared to the OLS factor'
    cases = [
        ('All combinations', est_1st + sr_descr, first_stage, None),
        # ('Selection OLS/AdaLasso; Coef Estimation OLS/AdaLasso'
        # '', (df['second_stage'] == '1st_stage'), None),
        # ('Selection OLS/AdaLasso; Coef Estimation OLS',
        #  (df['second_stage'] == 'OLS'), None),

        ('All stocks - first stage coef estimator', est_1st + sr_descr,
         (df['size'] == 'all') & first_stage, 'all_1st_stage'),
        ('Large cap stocks - first stage coef estimator', est_1st + sr_descr,
         (df['size'] == 'large') & first_stage, 'large_1st_stage'),
        ('Mid cap stocks - first stage coef estimator', est_1st + sr_descr,
         (df['size'] == 'mid') & first_stage, 'mid_1st_stage'),
        ('Small cap stocks - first stage coef estimator', est_1st + sr_descr,
         (df['size'] == 'small') & first_stage, 'small_1st_stage'),

        # ('All stocks - OLS coef estimator', est_OLS + sr_descr,
        #  (df['size'] == 'all') & OLS, 'all_OLS'),
        # ('Large cap stocks - OLS coef estimator', est_OLS + sr_descr,
        #  (df['size'] == 'large') & OLS, 'large_OLS'),
        # ('Mid cap stocks - OLS coef estimator', est_OLS + sr_descr,
        #  (df['size'] == 'mid') & OLS, 'mid_OLS'),
        # ('Small cap stocks - OLS coef estimator', est_OLS + sr_descr,
        #  (df['size'] == 'small') & OLS, 'small_OLS'),
    ]
    # for cases =
    base_cond = ((df.window_type == 'rolling') | (df.time_window == 180))
    update_tex_single = False
    t_value = 1.64
    for case, est, value, name in cases:
        sub = df.loc[base_cond & value, :].copy()

        sentences = list()
        sentences.append('Summary of \\textbf{{{}}}'.format(case) + ', ' + est)
        sentences.append(
            'In {:.2%} of the cases we observe a higher Sharpe Ratio '
            'for the Adaptive Lasso factor vs. the OLS factor'.format(
                (sub.SR_test > 0).sum() / len(sub)))
        tmp = 'The {} factor has a significantly higher Sharpe' \
              ' ratio at the 10\\% level in {:.2%} of the cases'
        sentences.append(tmp.format('Adaptive Lasso',
                                    (sub.SR_test > t_value).sum() / len(sub)))
        sentences.append(tmp.format('OLS',
                                    (sub.SR_test < -t_value).sum() / len(sub)))
        sentences.append('No significant difference is observed in'
                         ' {:.2%} of the cases'.format(
            (abs(sub.SR_test) < t_value).sum() / len(sub)))
        sentences.append(
            'A total of {} different combinations are considered.'.format(
                len(sub)))
        msg = '. \n'.join(sentences)
        print(msg)
        sub.SR_test = sub.SR_test.apply(t_value_stars)
        sub.rename(columns={'second_stage': 'Coef Estimation',
                            'time_window': 'window',
                            'window_type': 'type',
                            'bucket': 'cutoff',
                            'SR_test': 'SR test'}, inplace=True)
        sub.sort_values(['type', 'window'], inplace=True)
        print(sub)
        if update_tex_single:
            tex_file = 'oos_' + name + '.tex'
            df_to_latex(
                sub.drop(['RT_end', 'size', 'Coef Estimation'], axis=1),
                str(settings.TABLE_PATH.joinpath(tex_file)),
                label=name, caption=name, table_description=msg,
                centering=True)

    sub.reset_index(inplace=True, drop=True)
    print(sub)
    print(pd.pivot_table(sub, index=['cutoff', 'window', 'type', 'weighting'],
                         columns=['size'], values='SR test',
                         aggfunc=lambda x: ' '.join(x)).to_latex())
    # sub_stacked = sub.pivot_table(index='index', columns='size',
    #                               values='SR test')
    # print(sub_stacked)
    # print(df.loc[base_cond & (df['size']=='all') & (df['second_stage']=='OLS')])
    # tmp_df = sub.sort_values(sort_cols)
    # print(tmp_df)
    # print(tmp_df.SR_test.mean())
    # print(tmp_df.to_latex(index=False))


def sharp_ratio_res(results_df):
    # results_df =  results_df['20000630':]
    RI = (results_df + 1).cumprod() * 100
    RI.dropna(inplace=True)
    # reset the index to 100
    first_month = RI.index[0] - pd.tseries.offsets.MonthEnd()
    b = pd.DataFrame({RI.columns[0]: [100], RI.columns[1]: [100]},
                     index=[first_month])
    # add the 100 to return index
    RI = pd.concat([b, RI])
    # RI.plot()
    ra = ReturnAnalysis(RI['AdaLasso'].pct_change(), RI['OLS'].pct_change())

    return ra.test_diff_sharpe_ratio().t_value()


def compare_pf_returns():
    """ This function compares portfolio returns of two independent estimation
    mechanisms, which should yield the same results

    Returns
    -------

    """

    pols_fm = load_portfolio(method='POLS')
    ols_lasso = get_portfolio()

    ols_lasso, pols_fm =  ols_lasso.align(pols_fm, join='left')

    simple_returns = pd.concat([ols_lasso, pols_fm], axis=1)

    print('Corr: \n{}'.format(simple_returns.corr()))
    print('Means: \n{}'.format(simple_returns.mean()*12))


if __name__ == '__main__':
    logging.config.dictConfig(log_set_ups_runs)
    logging.root.addHandler(logging.getLogger('run_log').handlers[0])
    logger.setLevel('INFO')
    # a = estimation_cases()
    # estimate_predict()
    # portfolio_returns()
    # compare_pf_returns()
    # cases = estimation_cases()

    # df = pf_results()
    # summary_pf_stats()
    df = specification_details(size=None)

    # estimate_predict()
    # lasso_factor_analysis()
    # read_results()
