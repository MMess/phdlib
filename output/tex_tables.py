""" Write dataframe to latex table

very much dependent on latex packages, which I have not defined here

"""
import logging
import numpy as np
import pandas as pd
from scipy.stats import norm

logger = logging.getLogger(__name__)


# make type dependent table orientation

def get_col_type(dtype):
    """ Returns column type

    Parameters
    ----------
    dtype:

    Returns
    -------
    type: str
        {'r', 'l'}
    """
    if issubclass(dtype.type, np.number):
        return 'r'
    else:
        return 'l'

def df_to_latex(df, path_table, table=True, scaling=None, caption=None,
                table_description=None, label=None, landscape=False,
                centering=False, max_char=120, include_index=False,
                column_format=None, color_cells=None, midrule=None,
                add_col_name=True, color_table=None):
    """ Writes a dataframe to latex output

    Parameters
    ----------
    df: pandas.DataFrame
        df to be outputted to latex
    path_table: str
        path of the tex file destination
    table: bool
        if table environment should be used
    scaling: float [0:1]
        if table should be scaled
    caption: str
        caption of table
    table_description: str
        description of table
    label: str
        label which can be used for make a label reference in tex
    landscape: bool
        if table should be turned 90 degrees to landscape mode
    centering: bool
        if table should be centered
    max_char: int
        max number of char per column (if not make new line)
    include_index: bool
        if index should be part of the table
    column_format: str
        a latex table format string (for example 'llllr', if nothing inferred)
    color_cells: list of tuples
        a list of tuples where each tuple contains two values:
        (df_indicator, cell_color)
            df_indicator:  pd.DataFrame
                matrix of same shape as df, True cells are colored
            cell_color: str
                which color to use

    Returns
    -------

    """
    # TODO refactor into smaller sub modules
    buf = open(str(path_table), "w",
               encoding="utf-8")  # if in table environment

    if landscape:
        buf.write('\\begin{landscape} \n')

    if table:
        buf.write('\\begin{table}[ht!] \n')

    if centering:
        buf.write('\\centering \n')

    if scaling is not None:
        buf.write('\\scalebox{{{}}} {{ \n'.format(scaling))

    # get column format for tables
    dtypes = df.dtypes.values

    if column_format is None:
        column_format = ''.join(map(get_col_type, dtypes))
        if include_index:
            column_format = ''.join(['l|', column_format])

    # tabular part
    buf.write('\\begin{{tabular}}{{{}}}\n'.format(column_format))
    buf.write('\\toprule \\toprule \n')

    if isinstance(df.columns, pd.MultiIndex):
        #
        # if include_index:
        #     raise NotImplementedError
        col_rows = _multi_index_columns(df.columns, add_col_name=add_col_name)

        for col_row in col_rows:
            buf.write(col_row)
            buf.write(' \\\\\n')

    else:
        if include_index:
            if df.index.name is not None:
                columns = [df.index.name] + list(df.columns.tolist())
            else:
                columns = ['Index'] + list(df.columns.tolist())
        else:
            columns = df.columns.tolist()
            #  write column names (make valid tex string in map expression)
        buf.write(' & '.join(map(make_tex_string, columns)))
        buf.write(' \\\\\n')
    buf.write(' \\hline \n')

    line_count = 0

    # write rows
    for index, row in df.iterrows():
        line_count += 1

        row_strings = list()
        row_item_count = 0
        if include_index:
            row_item_count +=1
            row_strings.append([str(index)])

        # loop over each column
        # TODO make cell coloring possible
        # supply additional boolean dataframe with same index

        num_sub_rows = 1
        for col, x in row.items():
            if isinstance(x, (float, np.float64)):
                x = '{:10.2f}'.format(x)
            else:
                x = str(x)

            # check if max length of colum is exceed -> yes, break to two lines
            if len(x) > max_char:

                split_x = split_strings(x, max_char=max_char, criteria=' ')

                if num_sub_rows < len(split_x):
                    # if not first col of row
                    if row_item_count > 0:
                        # add empty strings for each add row for each col
                        for _ in range(len(split_x) - num_sub_rows):
                            row_strings.append([' '] * row_item_count)

                    else:
                        for new_sub_row in split_x:
                            row_strings.append([new_sub_row])

                    num_sub_rows = len(split_x)

                if row_item_count > 0:
                    # add the current subrows accordingly
                    for row_, new_x in zip(row_strings, split_x):
                        row_.append(new_x)

            else:
                if row_item_count==0:
                    row_strings.append([x])
                else:
                    row_strings[0].append(x)

                if num_sub_rows > 1:
                    for sub_row in range(1,num_sub_rows):
                        row_strings[sub_row].append(' ')

            if color_cells:
                for columns, fun, color in color_cells:
                    if col in columns:

                        for sub_row in row_strings:
                            _x = sub_row[row_item_count]
                            if 'na' in _x:
                                continue
                            if fun(_x):
                                _x = '\\cellcolor{{{}}}'.format(color) + _x

                                sub_row[row_item_count] = _x
                            # .format(color)
                            # blue!25
                            # if df_ind.loc[index,col].sum()==2:
                            #     x = '\\cellcolor{{{}}}'.format(color) + x
                            #     print(x)

            if color_table is not None:
                if col[0] in color_table['columns']:
                    for sub_row in row_strings:
                        _x = sub_row[row_item_count]
                        if 'na' in _x:
                            continue
                        if color_table['fun'](_x):

                            color =  color_table['base_color'] + \
                                     str(round(float(_x), 3)*50)

                            _x = '\\cellcolor{{{}}}'.format(color) + _x

                            sub_row[row_item_count] = _x

            # if x:
            #     pass
            # else:
            #     print('Not sure why...???')
            #     row_strings[0].append('{}')

            row_item_count += 1


        for sub_row in row_strings:
            buf.write(' & '.join([make_tex_string(y) for y in sub_row]))
            buf.write(' \\\\\n')

        if midrule is not None:
            if line_count<len(df):
                if line_count % midrule == 0:
                    buf.write('\\midrule \n')

    buf.write('\\bottomrule \\bottomrule \n')
    buf.write('\\end{tabular}\n')

    if scaling:
        buf.write('}\n')

    if caption is not None or table_description is not None:
        table_description = table_description.replace('%', '\%')
        buf.write('\\caption[{}:]{{{}}} \n'.format(caption, table_description))

    if label is not None:
        buf.write('\\label{{{}}} \n'.format(label))

    if table:
        buf.write('\\end{table} \n')

    if landscape:
        buf.write('\\end{landscape} \n')

    buf.close()


def t_value_stars(t_value, value=None):
    """ Adds t_value stars to value

    Parameters
    ----------
    t_value: float
        t-statistic value
    value: float
        value which t-stat is based on


    Returns
    -------

    """

    if abs(t_value) >  abs(norm.ppf(0.01/2)):
        extension = '***'
    elif abs(t_value) > abs(norm.ppf(0.05/2)):
        extension = '**'
    elif abs(t_value) > abs(norm.ppf(0.1/2)):
        extension = '*'
    else:
        extension = ''
    if value is None:
        return '{0:.2f}{1}'.format(t_value, extension)
    else:
        return '{0:5.1f}{2:3}({1:.2f})'.format(value, t_value, extension)


def _multi_index_columns(col_index, index_col=None, add_col_name=True,
                         use_multi_col=True):
    # TODO ensure order is correct

    names = col_index.names
    levels = col_index.levels
    labels = col_index.labels

    rows = list()
    for level, label in zip(levels, labels):

        tmp_row = list()

        last_label = None
        content = None
        for current_label in label:
            if current_label != last_label:

                if content is not None:
                    if count == 1:
                        tmp_row.append(content)
                    elif use_multi_col:
                        tmp_row.append(
                            '\\multicolumn{{{}}}{{c|}}{{{}}}'.format(
                                count, content))
                    else:
                        tmp_row += [content] + [' '] * (count - 1)
                count = 1
                content = str(level[current_label])
            else:
                count += 1

            last_label = current_label

        if count == 1:
            tmp_row.append(content)
        elif use_multi_col:
            tmp_row.append('\\multicolumn{{{}}}{{c}}{{{}}}'.format(
                count, content))
        else:
            tmp_row += [content] + [' '] * (count - 1)

        rows.append(tmp_row)

    row_strings = list()
    # add column index names
    if add_col_name:
        for row_name, row in zip(names, rows):
            row_strings.append(' & '.join([row_name] + row).replace('_', '-'))
    else:
        for row in rows:
            row_strings.append(' & '.join(row).replace('_', '-'))

    return row_strings


def split_strings(str_to_split, max_char=120, criteria=' '):
    """ Split strings in several lists of substrings

    Parameters
    ----------
    str_to_split: str
        string to be split
    max_char: int
        max length of each sub-string
    criteria: str
        which criteria should be used to split the string

    Returns
    -------
    list:
        list of substrings of original string
    """

    str_splits = list()

    while len(str_to_split) > max_char:
        tmp_split = str_to_split[:max_char].rsplit(' ', 1)[0]
        str_to_split = str_to_split[len(tmp_split) + 1:]
        str_splits.append(tmp_split)

    str_splits.append(str_to_split)

    return str_splits


def make_tex_string(raw_string=None):
    """ Returns a latex compatible string
    :param raw_string: string to be manipulated
    :return:
    """
    raw_string = (
        # x.replace('\\', '\\textbackslash')  # escape backslashes first
        str(raw_string).replace('_', '\\_')
            .replace('%', '\\%')
            # .replace('$', '\\$')
            .replace('#', '\\#')
            # .replace('{', '\\{')
            # .replace('}', '\\}')
            .replace('~', '\\textasciitilde')
            .replace('^', '\\textasciicircum')
            .replace('&', '\\&')
            .replace('nan', '')

    )

    return raw_string


def main():
    # logging.config.dictConfig(logger_settings.log_set_ups)
    # logger.setLevel('INFO')

    columns = pd.MultiIndex.from_product([['a_ll', 'b_s'], ['c', 'd', 's']],
                                         names=['wow', 'mar'])
    df = pd.DataFrame(columns=columns, index=[1, 2, 3])
    df_to_latex(df, path_table='D:test.tex')


if __name__ == '__main__':
    main()
    # update_FC_selection_tables()
