__author__ = 'mmess'
"""SPECIFY FILE HERE
"""
import logging

import pandas as pd
from scipy.stats import norm

from phdlib.data import simulation, results
from phdlib.data.results import LoadLocalResult
from phdlib.misc import logger_settings
from phdlib.settings import RESULTS_PATH


res_path = RESULTS_PATH.joinpath('cross_section_fm_pols')
logger = logging.getLogger(__name__)

SIM_CASES = {'Case_1': dict(Vola='Normal', Years=50),
             # 'Case_2': dict(Vola='LTMean', Years=50),
             # 'Case_3': dict(Vola='StudentsT', Years=50),
             # 'Case_4': dict(Vola='Normal', Years=20),
             'Case_2': dict(Vola='Normal', Years=350),
             # 'Case_3': dict(Vola='Normal', Years=10, NumStocks=4000, CorrFC=False,NumSimulation=5000,),
             'Case_3': dict(Vola='Normal', Years=10, NumStocks=4000, CorrFC=True, NumSimulation=5000,),
             'Case_5': dict(Vola='Normal', Years=5, CorrFC=False, NumSimulation=5000,),
             'Case_4': dict(Vola='Normal', Years=5, CorrFC=True, NumSimulation=5000,),
             # 'Case_6': dict(Vola='Normal', Years=10, NumStocks=4000,
             #                CorrFC=True),
             # 'Case_7': dict(Vola='Normal', Years=10, NumStocks=4000,
             #                NumSimulation=5000, CorrFC=False),
             # 'Case_7': dict(Vola='Normal', Years=10, ),
             }


def error_ratios(case='Case_1', level=0.05):
    """ Loads and returns error ratio results from simulation

    See Messmer and Audrino (2017) for details on the cases

    Parameters
    ----------
    case: str
        "Case_1", ...,  "Case_5" is defined

    Returns
    -------
    pd.DataFrame
        with columns 'OLS', 'AdaptiveLasso' and 'Lasso'
    """
    result = LoadLocalResult(file_pattern='simulation_fm_pooled',
                             since='20171003', conditions=SIM_CASES[case],
                             path=res_path)

    # print(result.file)

    data = result.data
    parameter = result.parameter
    # OLS = data['OLS_DK_tValues']
    # adaLasso = data['AdaptiveLasso']
    # Lasso = data['Lasso']
    set_up = simulation.simulation_parameters(parameter)

    data['OLS_DK_SE'] = data['OLS_DK_coef']/data['OLS_DK_tValues']
    data['FM_SE'] = data['FM_coef'] / data['FM_tValues']
    N = parameter['NumSimulation']


    true_coef = pd.Series(index=data['OLS_DK_SE'].columns)
    true_coef.iloc[:len(set_up['RiskPremia'])] = set_up['RiskPremia']
    true_coef.fillna(0, inplace=True)


    bias = pd.DataFrame(columns=['OLS', 'FM', ],
                         index=range(parameter['NumFC']))
    rel_bias = pd.DataFrame(columns=['OLS', 'FM', ],
                         index=range(parameter['NumFC']))
    SE = pd.DataFrame(columns=['OLS', 'FM', ],
                         index=range(parameter['NumFC']))

    ratio = pd.DataFrame(columns=['OLS', 'FM', ],
                         index=range(parameter['NumFC']))

    for col in ratio:
        if col == 'FM':
            bias.loc[:, col] = data['FM_coef'].sub(true_coef).mean()
            rel_bias.loc[:, col] = data['FM_coef'].mean() / true_coef -1
            SE.loc[:,col] = data['FM_SE'].mean()/data['FM_coef'].std() -1
            ratio.loc[:, col] = (abs(data['FM_tValues']) > \
                                 abs(norm.ppf(level / 2))).sum() / N
        elif col == 'OLS':
            bias.loc[:, col] = data['OLS_DK_coef'].sub(true_coef).mean()
            rel_bias.loc[:, col] = data['OLS_DK_coef'].mean() / true_coef -1
            SE.loc[:, col] = data['OLS_DK_SE'].mean()/data['OLS_DK_coef'].std() - 1
            ratio.loc[:, col] = (abs(data['OLS_DK_tValues']) > \
                                 abs(norm.ppf(level / 2))).sum() / N
        else:
            ratio.loc[:, col] = (data[col] != 0).sum() / N


    print('Efficiency: {}'.format(data['OLS_DK_coef'].std()/data['FM_coef'].std()))

    # adjust by 1-ratio as RetFC are the active variables H1 is True
    ratio.T.iloc[:, :set_up['NumRetFC']] = 1 - ratio.T.iloc[:,
                                                  :set_up['NumRetFC']]

    return ratio*100, rel_bias*100, SE*100


def full_results_df(case='Case_4'):
    sub_frames = error_ratios(case=case)
    names = ['Err', 'Rel Coef', 'Rel SE']

    new_frames = list()
    for frame, name in zip(sub_frames, names):
        frame = frame.transpose()
        frame['Measure'] = name
        new_frames.append(frame)

    df = pd.concat(new_frames)
    df.set_index('Measure', append=True, inplace=True)

    return df


def main():
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')

    for case in range(6,7):
        df, a,b,c, = error_ratios(case='Case_' + str(case))
        print('Case {}:\n error_ratio 0-5: {} \n 5-11: {}\n 12-15: {}'
              '\n 16-99: {}'.format(case,
                                df.loc[:6].mean(),
                                df.loc[6:12].mean(),
                                df.loc[12:15].mean(),
                                df.loc[15:].mean()))

    return df


if __name__ == '__main__':
    full_results_df()
    # df = main()
