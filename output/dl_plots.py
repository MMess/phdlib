import logging
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from phdlib.output.return_plots import ReturnPlot, FactorGraphs, store_figures
from phdlib.finance.api import ReturnIndex
from phdlib.data.api import Factor
from phdlib.output.dl_portfolio import results_dnn_factors,\
    dnn_trading_cost_factors, dnn_FC_sensitivity
from phdlib.output.dl_hyper_select import inspect_results
from phdlib.misc.api import log_set_ups
from phdlib.settings import DEEP_PLOT, RESULTS_PATH, GARCH_PATH


logger = logging.getLogger(__name__)

def training_validation_loss():
    """ Plots validation and training loss (based on tensorboard csv)
    """

    fig = plt.figure()
    ax =  plt.axes()
    csv = RESULTS_PATH.joinpath('deep_learn').joinpath('training_valid_loss_tb.csv')

    df  = pd.read_csv(csv,usecols=['Step', 'Validation', 'Training', ],
                      index_col='Step')
    sns.set(style="darkgrid")
    df.divide(df.loc[0, :]).plot(ax=ax)

    file_name = 'opt_loss_large'
    store_figures(fig, file_name, plot_path=DEEP_PLOT)


def learning_grid():
    """ Plots the grid-search w.r.t. the learning rate
    """

    fig = plt.figure()
    ax = plt.axes()

    sizes = ['large', 'mid', 'large_mid']
    colors = sns.color_palette("GnBu_d")[:len(sizes)]
    marker = ['x', 'o', 'v']
    for size, mark in zip(sizes, marker):
        table = inspect_results(size=size, search='grid')
        table.plot(x="learning_rate", y="R2_dnn", logx=True, kind='scatter',
                   label=size, ax=ax, marker=mark)

    file_name = 'grid_search'
    store_figures(fig, file_name, plot_path=DEEP_PLOT)


def return_index_river(save=False, size='large'):
    """ Plots the return index river chart of the portfolio

    Parameters
    ----------
    size: str
        which size to use

    Returns
    -------

    """
    weighting = 'mkt_cap'
    buckets = 'decile'
    approach = 'naive'
    map_weights = {'equal': 'EW', 'mkt_cap': 'VW'}

    simple_returns, file_names = results_dnn_factors(size, weighting=weighting,
                                                     approach=approach,
                                                     buckets=buckets,
                                                     linear=True,
                                                     return_file_names=True)

    print('Num of DFN PF found: {}'.format(
        len(simple_returns.drop('Linear', axis=1).columns)))
    ri = ReturnIndex(simple_returns=simple_returns.drop('Linear', axis=1))
    ri_bm = ReturnIndex(simple_returns=simple_returns[['Linear']])

    mkt_returns = Factor().get(factors=['Mkt-RF'],
                               start=simple_returns.index.min(),
                               end=simple_returns.index.max())
    mkt_factor = ReturnIndex(mkt_returns)

    sns.set(style="darkgrid")
    fig, axes = plt.subplots(nrows=2, sharex=True)
    ax1, ax2 = axes[0], axes[1]

    ReturnPlot(ri).river(ri_bm, market_index=mkt_factor,
                              min_max=True, mean=True, figure=fig, ax=ax1)
    ReturnPlot(ri).river(ri_bm, market_index=mkt_factor, use_log=True,
                              min_max=True, mean=True, figure=fig, ax=ax2)

    PF_desc = ', '.join([size.replace('_', ' + ').capitalize(), map_weights[weighting],
                           buckets.replace('_', '-')])
    ax1.set_title('Return Index Portfolios - ' + PF_desc)

    ax1.set_ylabel("Return Index")
    ax2.set_ylabel("Return Index Log Based")
    ax2.set_xlabel("date")

    if save:
        plot_name = '_'.join(['return_river', size, weighting, buckets,
                              approach])
        store_figures(fig, plot_name=plot_name, plot_path=DEEP_PLOT,
                       formats=['pdf', 'png'], factor=(1.5*1.2,1.1*1.3))

    return ax1



def training_costs_returns(save_fig=True):

    trading_costs = {'large': 45, 'mid': 110}
    size = 'large'
    approach = 'naive'
    weighting = 'mkt_cap'
    buckets = 'ff_style'

    res = dnn_trading_cost_factors(size=size, weighting=weighting,
                                   approach=approach, linear=True,
                                   buckets=buckets, combination=75)

    sns.set(style="darkgrid")
    fig = plt.figure()
    for pf in ['DFN_median', 'Linear', 'Comb']:
        pf_obj = res[pf]
        for freq in range(1,4):
            pf_obj.rebalance_freq = freq

            FactorGraphs(pf_obj, name=pf, figure=fig).ri_turnover(
                cost_bp=trading_costs[size])

    if save_fig:
        plot_name = '_'.join([size, approach, weighting, buckets, 'TO'])
        store_figures(fig, plot_name=plot_name, plot_path=DEEP_PLOT,
                      formats=['eps', 'png'], factor=(3 * 0.75, 2.2 * 0.75))


def sensitivity(save_fig=True, size='large_mid'):
    """

    Parameters
    ----------
    save_fig
    size

    Returns
    -------

    """

    approach = 'naive'
    df = dnn_FC_sensitivity(size=size, approach=approach)
    top_FC = df.mean().sort_values(ascending=False).iloc[:10].index
    df = df[top_FC].stack().reset_index().rename(columns={'level_2': 'FC', 0:'MSD'})
    df = df[['FC', 'MSD']]
    df.loc[df['MSD']>120, 'MSD'] = 120
    # Initialize the FacetGrid object
    pal = sns.cubehelix_palette(10, rot=-.25, light=.7)
    sns.set(style="darkgrid")
    g = sns.FacetGrid(df, row="FC", hue="FC", aspect=7, size=2, palette=pal)

    # Draw the densities in a few steps
    g.map(sns.kdeplot, "MSD", clip_on=False, shade=True, alpha=1, lw=1.5, bw=.2)
    g.map(sns.kdeplot, "MSD", clip_on=False, color="w", lw=2, bw=.2)
    g.map(plt.axhline, y=0, lw=2, clip_on=False)
    g.fig.subplots_adjust(hspace=-.25)

    def label(x, color, label):
        ax = plt.gca()
        ax.text(0, .2, label, fontweight="bold", color=color,
                ha="left", va="center", transform=ax.transAxes, size=20)

    g.map(label, "FC")

    # Set the subplots to overlap
    g.fig.subplots_adjust(hspace=-.05)

    # Remove axes details that don't play will with overlap
    g.set_titles("")
    g.set(yticks=[])
    g.despine(bottom=True, left=True)

    if save_fig:
        plot_name = '_'.join([size, approach, 'FC_sensitivity_density'])
        store_figures(g.fig, plot_name=plot_name, plot_path=DEEP_PLOT,
                      formats=['eps', 'png'], factor=(1.4,0.6))
    return df


def rolling_fc_sensitivity(size='large_mid', save_fig=True):
    """ Plots rolling FC sensitivity

    Returns
    -------

    """
    # load result data
    approach = 'naive'
    df = dnn_FC_sensitivity(approach=approach, size=size).dropna()
    df.index = df.index.droplevel('file_num')
    grouped = df.groupby('date')
    df = grouped.sum()/grouped.count()
    sorted = df.mean().sort_values(ascending=False).index

    df = df.transpose()
    df = df.loc[sorted]
    # print(df)
    df.columns = [x.strftime('%Y-%m') for x in df.columns]

    # create figure
    fig = plt.figure()
    ax = plt.axes()
    # cmap = sns.diverging_palette(150, 0, as_cmap=True)
    axs = sns.heatmap(df.fillna(0).divide(df.max()), ax=ax,cmap='YlGnBu',
                      yticklabels=True, xticklabels=18)
    axs.set_ylabel("FC")
    axs.set_xlabel("date")

    plt.xticks(rotation=60)
    plt.yticks(rotation=0)
    ax.set_title('MSD of {} stocks over time'.format(size))

    if save_fig:
        plot_name = '_'.join([size, 'rolling_MSD_plot'])
        logger.info('Saving {} to disk'.format(plot_name))
        store_figures(fig, plot_name=plot_name, plot_path=DEEP_PLOT,
                      formats=['eps', 'png'], factor=(2.5,1.7))


def volatility_plot():
    fig = plt.figure()
    garch_data = (pd.read_csv(str(GARCH_PATH),
                              index_col='index', parse_dates=True)) * 12 ** 0.5
    garch_data = garch_data[(garch_data.index >= '19820101') &
                                    (garch_data.index <= '20141231')]
    text = 'GARCH(1,1) Volatility and Volatility Regimes'


    ax = garch_data['Normal'].plot(kind='area')
    sns.palplot(sns.color_palette("BuGn_d"))

    ax.set_title(text.format('normally'), fontsize=13)
    ax.set_ylabel("Annualized volatility")
    ax.set_xlabel("date")
    garch_data = garch_data['Normal']

    pct = garch_data.rank(pct=True)
    regimes = pct > 0.85
    # all
    start_dates = (regimes!=regimes.shift(1)) & (regimes==True)
    end_dates = (regimes!=regimes.shift(1)) & (regimes==False)


    start_dates = start_dates[start_dates]
    end_dates = end_dates[end_dates]

    for start in start_dates.index.tolist():

        end = end_dates.loc[start:].index[0]
        ax.axvspan(pd.Timestamp(start), end, color=sns.xkcd_rgb['grey'],
                   alpha=0.2)



    store_figures(fig, plot_name='garch_vola_regimes', plot_path=DEEP_PLOT,
                      formats=['eps', 'png'], factor=(1.6,1.2))


def main():

    pass


if __name__ == '__main__':
    logging.config.dictConfig(log_set_ups)
    logger.setLevel('INFO')
    # volatility_plot()
    # rolling_fc_sensitivity()
    return_index_river(save=True, size='large_mid')
    # sensitivity()
    # rolling_fc_sensitivity(save_fig=True, size='mid')
    # learning_grid()
    # training_validation_loss()
    # training_costs_returns(save_fig=False)
    # training_costs_returns()