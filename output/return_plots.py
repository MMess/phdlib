import datetime
import logging
import shutil

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from phdlib.finance.factor_analysis import FactorAnalysis
from phdlib.data.api import Factor, get_monthly_data
from phdlib.finance.return_calc import ReturnIndex
from phdlib.metrics.portfolio import CriteriaPortfolio
from phdlib.misc import logger_settings
from phdlib.settings import PLOT_PATH

logger = logging.getLogger(__name__)
import seaborn as sns


def fix_eps(path):
    """ Fixes windows bug in font, overwrites part in the file
    
    allows to save matplotlib figure to EPS (only in windows a problem)
    
    Parameters
    ----------
    path: str
        path to the eps file

    Returns
    -------

    """

    txt = b""
    with open(path, "rb") as f:
        for line in f:
            if b"\r\rHebrew" in line:
                line = line.replace(b"\r\rHebrew", b"Hebrew")
            txt += line
    with open(path, "wb") as f:
        f.write(txt)


def store_figures(fig, plot_name, formats=['eps', 'png'], factor=1,
                  plot_path=PLOT_PATH):
    """ Stores figures the hard drive

    Parameters
    ----------
    fig: matplotlib.fig
        the figure to be stored
    plot_name:
        name of the plot
    formats: list
        in which formats you want to store the figure
    factor: float/tuple
        scaling of the figure
    plot_path: Path
        
        

    Returns
    -------

    """

    default_size = fig.get_size_inches()
    if isinstance(factor, (float, int)):
        fig.set_size_inches(
            (default_size[0] * factor, default_size[1] * factor))
    else:
        fig.set_size_inches(
            (default_size[0] * factor[0], default_size[1] * factor[1]))

    for format in formats:
        file_name = '.'.join([plot_name, format])
        file_path = plot_path.joinpath(file_name)
        archive_plots(file_name)
        logger.info('Saving {}...'.format(str(file_path)))
        fig.savefig(str(file_path), dpi=400)

    if 'eps' in formats:
        file_name = '.'.join([plot_name, 'eps'])
        logger.info('Fixing eps file...')
        fix_eps(str(plot_path.joinpath(file_name)))


def archive_plots(file_name, archive_folder='Archive'):
    """ Archives plots with date information

    Parameters
    ----------
    file_name
    archive_folder

    Returns
    -------

    """

    file_path = PLOT_PATH.joinpath(file_name)

    if file_path.exists():
        logger.info('File {} exists, copied to archive...'.format(file_path))
        now = datetime.datetime.now().strftime('%Y-%m-%d_%H%M')
        archive_file_path = PLOT_PATH.joinpath(archive_folder).joinpath(
            '_'.join([now, file_name]))
        shutil.move(str(file_path), str(archive_file_path))
        logger.info('Figure {} is saved in plot dir'.format(file_name))


class FactorGraphs():
    def __init__(self, portfolio, name='Factor', figure=None,
                 ):
        """
        
        Parameters
        ----------
        portfolio: CriteriaPortfolio
        name: str
        figure: plt.figure() 
        """
        if isinstance(portfolio, CriteriaPortfolio) is False:
            raise TypeError('Portfolio not of type CriteriaPortfolio')

        self.portfolio = portfolio
        self.name = name
        self.figure = figure

    def ri_turnover(self, roll_turnover=True, file_name=None, file_path=None,
                    cost_bp=50):

        """ Plots return index with/without costs, + trading turnover
        
        Parameters
        ----------
        roll_turnover: bool
            if 12m rolling turnover should be used
        file_name: str
            name of plot file 
        file_path: path
            path where files should be stored
        cost_bp: cost_bp
            with what cost should be calculated in basis points

        Returns
        -------

        """
        if self.figure is None:
            fig = plt.figure()
        else:
            fig = self.figure

        left, width = 0.1, 0.8
        rect1 = [left, 0.3, width, 0.6]
        rect2 = [left, 0.05, width, 0.15]
        ax1 = fig.add_axes(rect1)
        ax1 = self.ri_cost_ad(ax=ax1, cost_bp=cost_bp)

        ax1.set_ylabel('RI', color='b')
        ax2 = fig.add_axes(rect2, sharex=ax1)
        ax2 = self.roll_turnover(ax=ax2, bar=False)
        ax2.set_ylabel('12m Roll TurnOver', color='r')

        plt.show()

        if file_name is not None:
            store_figures(fig, plot_name=file_name, plot_path=file_path)

    def ri_cost_ad(self, ax=None, cost_bp=50):
        """ Plots return index with and without costs 
        
        Parameters
        ----------
        ax: matplotlib.axes

        Returns
        -------
        plot:
            with time series of RI with and without cost adjustment
        """
        pf = self.portfolio
        simple_rets = pd.concat([pf.factor_return.to_frame(self.name),
                                 pf.adj_factor_return(how='simple',
                                     costs_bp=cost_bp).to_frame(self.name +
                                                                '_CostAdj')],
                                axis=1)
        RI = ReturnIndex(simple_rets)
        return ReturnPlot(RI).base(ax=ax)

    def roll_turnover(self, ax, ignore_initial=True, bar=True, window=12):
        """Plots rolling average turnover of a strategy
        
        Parameters
        ----------
        ax: matplotlib.fig
            figure to plot into
        ignore_initial: bool
            if first period buy should be ignored as turnover (100%)
        bar: bool
            if period bars should be additionally plotted
        window: int
            length of rolling of window

        Returns
        -------

        """
        turnover = self.portfolio.turnover['factor']
        if ignore_initial:
            turnover.iloc[0] = 0
        if bar:
            ax = turnover.plot(kind='bar', ax=ax)

        roll_turn = turnover.rolling(window=window).sum()*100
        roll_turn.name = self.name

        return roll_turn.to_frame().plot(ax=ax)


class ReturnPlot():
    """ Plots all return index plots
    
    """

    def __init__(self, return_index):
        """
        
        Parameters
        ----------
        return_index: ReturnIndex
            plots this return index(s)
        """
        if isinstance(return_index, ReturnIndex) is False:
            raise TypeError('return_index not of type ReturnIndex!')

        self.return_index = return_index.index_level

    def base(self, ax=None):
        return self.return_index.plot(title='Index Level', ax=ax)

    def log(self, ax=None):
        return self.return_index.plot(logy=True, title='Log Index Level',
                                      ax=ax)

    def histogram(self, bins=60, ax=None):
        return self.return_index.pct_change().hist(bins=bins, ax=ax)

    def river(self, bm, market_index=None, min_max=True, ci=[95, 99.5],
              pf_name='DFN', ax=None, date_format='%Y',
              file_name=None, file_path=None, figure=None,
              mean=True, use_log=False):
        """ Plots shaded areas with seaborn ts.plot
        
        Currently hack due to date error with matplotlib
        
        Parameters
        ----------
        lower_bound: ReturnIndex
            minus num_of_dev standard deviations of original series
        upper_bound1: ReturnIndex
            plus num_of_dev standard deviations of original series
          

        Returns
        -------
        ax
        """
        sns.set(style="darkgrid")
        if figure is None:
            figure = plt.figure()
        if ax is None:
            ax = figure.add_axes()
        # prep data for seaborn ts plot
        dates = pd.Series(self.return_index.index.strftime(date_format))
        df_stacked = self.return_index.reset_index(
            drop=True).stack().reset_index()
        df_stacked.rename(columns={'level_0': 'date', 'level_1': 'pf',
                                   0: 'ReturnIndex'}, inplace=True)

        df_stacked['RI'] = pf_name

        if mean:
            estimator = np.mean
        else:
            def col_rand(x, axis=1):
                num_dim = x.shape[axis]
                rand_ind = np.random.choice(range(num_dim))
                if axis == 1:
                    return x[:, rand_ind]
                else:
                    return x[rand_ind, :]

            estimator = col_rand

        if use_log:
            df_stacked['ReturnIndex'] = \
                np.log((df_stacked['ReturnIndex']/100).astype(float))


        ax = sns.tsplot(df_stacked, time='date', unit='pf', condition='RI',
                        value='ReturnIndex', ci=ci, ax=ax, estimator=estimator)

        tmp = bm.index_level.reset_index(drop=True)
        if use_log:
            tmp = np.log((tmp/100).astype(float))
        ax = tmp.plot(ax=ax, color='c')

        if market_index is not None:

            # adjust market index, just in case it does not fit
            _, market_index = self.return_index.align(
                market_index.index_level, join='left', axis=0)
            tmp = market_index.reset_index(drop=True)
            if use_log:
                tmp = np.log((tmp / 100).astype(float))
            ax = tmp.plot(ax=ax, color='palegoldenrod')

        if min_max:
            min_max = self.return_index[[self.return_index.iloc[-1, :].idxmin()
                , self.return_index.iloc[-1, :].idxmax()]]
            min_max.columns = [pf_name + '_min', pf_name + '_max']

            if use_log:
                min_max = np.log((min_max/100).astype(float))
            min_max = min_max.reset_index(drop=True)
            ax = min_max.plot(ax=ax,
                              color=sns.cubehelix_palette(8, start=.5,
                                                          rot=-.75))

        # reset dates to original index
        selected_dates = dates.loc[ax.get_xticks()].fillna(dates.max())

        ax.set_xlabel("date")
        ax.set_xticklabels(selected_dates)

        if file_name is not None:
            store_figures(figure, plot_name=file_name, plot_path=file_path,
                          formats=['pdf', 'png'])

        return ax


def plot_strategies(which=('mom12m', 'mom1m')):
    """ Plots turnover of

    Parameters
    ----------
    which: iterable
        FCs

    Returns
    -------
    None
    """
    fc_data = get_monthly_data(columns=which, size='large', per_rank=True,
                               fc_lead=0, start_date='20100101')

    fig = plt.figure()

    ordering = (False, True,)

    for fc, ascending in zip(which, ordering):
        criteria = fc_data.reset_index().pivot_table(values=fc,
                                                     columns='PERMNO',
                                                     index='date')
        pf = FactorAnalysis(criteria, ascending=ascending).portfolio

        print(f'FC: {fc}. factor trading costs: {pf.roll_turnover()}')

        FactorGraphs(pf, name=fc, figure=fig).ri_turnover()

    store_figures(fig, 'test_turnover')

    return None


def main():
    simple_returns = Factor().get(
        model='five+mom-factor',
        # factors=['Mkt-RF', 'MOM', 'HML'],
        frequency='daily',
        start='20070101')
    RI = ReturnIndex(simple_returns)
    ReturnPlot(RI).base()


if __name__ == '__main__':
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
    port = plot_strategies()
