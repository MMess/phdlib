import logging
import pandas as pd
import numpy as np
from phdlib.output.fm_pols_portfolios import load_portfolio, asset_analysis
from phdlib.output.fm_pols_regressions import inspect_results
from phdlib.output.tex_tables import df_to_latex, t_value_stars
from phdlib.settings import FM_POLS_TABLES
from phdlib.output.lasso_zoo_regression import FCRegressionCore
from phdlib.output.fm_pols_simulation import full_results_df, SIM_CASES

logger = logging.getLogger(__name__)


def _get_pf_measures(simple_rets_fm, simple_rets_pols, start_date='19820101',
                     end_date='20141231', dates=None):
    """

    Parameters
    ----------
    size
    weighting
    approach
    buckets

    Returns
    -------

    """
    a_scale = 12*100
    simple_rets_fm = simple_rets_fm[(simple_rets_fm.index >= start_date) &
                                    (simple_rets_fm.index <= end_date)]

    simple_rets_pols = simple_rets_pols[(simple_rets_pols.index >= start_date) &
                                    (simple_rets_pols.index <= end_date)]

    if dates is not None:
        simple_rets_fm = simple_rets_fm.loc[dates]
        simple_rets_pols = simple_rets_pols.loc[dates]

    results = pd.DataFrame(index=['FM', 'POLS'],
                           columns=['mean', 'std', 'SR', '5ff',
                                    '5ffmom', 'SR Test'])

    which = ['factor_regression', 'sr_test', 'per_stat',
             'factor_regression_bm']

    aa = asset_analysis(simple_returns=simple_rets_fm,
                         simple_return_bm=simple_rets_pols,
                         which=which)

    results.loc['FM', 'SR Test'] = t_value_stars( aa['sr_test'].t_value())

    # Mean, Std, Sharpe Ratio, Alpha 5-FF, Alpha 5-FF Momentum
    res_map = dict(Portfolio='factor_regression',
                   BM='factor_regression_bm')
    for pf, col in zip(['FM', 'POLS'],['Portfolio', 'BM']):
        results.loc[pf, 'mean'] = aa['per_stats'].loc['mean', col] * 100
        results.loc[pf, 'std'] = aa['per_stats'].loc['std', col] * 100
        results.loc[pf, 'SR'] = aa['per_stats'].loc['SR', col] * 12 ** 0.5

        # fama french 5 factor model
        r_name = res_map[col]
        ff5 = (aa[r_name][0].params['Alpha'] * a_scale,
               aa[r_name][0].tvalues['Alpha'])
        results.loc[pf, '5ff'] = t_value_stars(value=ff5[0], t_value=ff5[1])
        # fama french 5 factor + "momentum" model
        ff5mom = (aa[r_name][1].params['Alpha'] * a_scale,
                  aa[r_name][1].tvalues['Alpha'])
        results.loc[pf, '5ffmom'] = t_value_stars(value=ff5mom[0],
                                                  t_value=ff5mom[1])

    results.rename(
        columns={'5ff': 'FF-5 $\\alpha$', '5ffmom': 'FF-5 + Mom $\\alpha$'},
        inplace=True)

    return results

def performance_measures(save=False):
    """ Writes the factor regression results to

    Returns
    -------

    """
    size = 'large'
    buckets = 'decile'
    window_type = 'expanding'
    weightings = 'equal'
    per_rank = False
    re_estimate_freq = 1
    window_length = 144
    winsorize_prc = 0.01
    start = '19820101'
    end = '20141231'
    second_stage = True

    map_weights = {'equal': 'EW', 'mkt_cap': 'VW'}
    map_buckets = {'decile': 'Decile', 'ff_style': 'FF-style'}
    map_adj = {'bonferroni': 'Bonf', 'holm':'Holm','fdr_by': 'BY', None:'-'}
    map_yes_no = dict()
    map_yes_no[True] = 'Yes'
    map_yes_no[False] = 'No'
    res_tables = list()

    for weighting in [weightings,]:
        for fw_correction in [None, 'fdr_by', 'holm', 'bonferroni' ]:
            for true_oos in [True, False]:
                if true_oos is False and fw_correction is not None:
                    continue
                for stage in [True, False]:
                    if stage is False and fw_correction is not None:
                        continue
                    para = dict(size=size, true_oos=true_oos,
                                fw_correction=fw_correction,
                                window_type=window_type, buckets=buckets,
                                window_length=window_length,
                                weighting=weighting, per_rank=per_rank,
                                re_estimate_freq=re_estimate_freq,
                                winsorize_prc=winsorize_prc,
                                second_stage=stage)
                    simple_rets_fm = load_portfolio(method='FM', **para)
                    simple_rets_pols = load_portfolio(method='POLS', **para)

                    pf_stats = _get_pf_measures(simple_rets_fm=simple_rets_fm,
                                     simple_rets_pols=simple_rets_pols, )

                    pf_stats['2nd Stage'] = None
                    pf_stats.loc['FM', '2nd Stage'] = map_yes_no[stage]
                    pf_stats['OOS FC'] = None
                    pf_stats.loc['FM', 'OOS FC'] = map_yes_no[true_oos]
                    pf_stats['Adj'] = None
                    pf_stats.loc['FM', 'Adj'] = map_adj[fw_correction]
                    print(pf_stats)
                    res_tables.append(pf_stats)

    results = pd.concat(res_tables)
    results.index.name = 'Portfolio'
    results.set_index(['OOS FC','Adj',  '2nd Stage'], append=True,
                      inplace=True)
    data_cols = results.columns
    results.reset_index(inplace=True)
    results = results[['Portfolio', 'OOS FC', 'Adj', '2nd Stage'] +
                      list(data_cols)]

    # if results['FW Err'].isnull().all():
    #     results = results.drop('FW Err', axis=1)
    # else:
    #     fwr_='FWER'

    print(results)
    label = '_'.join(['perf', size, buckets, weightings, window_type, 'pct_rank',
                      map_yes_no[per_rank],  start[:4], end[:4]])

    table_name = label + '.tex'

    table_path = FM_POLS_TABLES.joinpath(table_name)
    des = '\\textbf{{{} Cap Stocks, {} Cutoffs, {} weighted,' \
          ' Sample Period  {} until {}:}}' \
          ' The table exhibits ' \
          'performance measures for FM and POLS regressions. It contains ' \
          'value-weighted (VW) and equally-weighted (EW) long short portfolios.' \
          ' The column "OOS FC" indicates if the set of FC included at each ' \
          'point in time reflects only post-publication FC. Column "Adj" shows if' \
          ' p-values are adjusted for multiple testing, if empty no adjustment is ' \
          'performed --- "Bonf" refers to Bonferroni, "BY" to  ' \
          'Benjamini/Yekutieli. Column "2nd Stage" indicates if two regressions ' \
          'are run or not --- first, a selection regression and second, a ' \
          'coefficient estimation with the subset of selected FC. All performance measures are displayed annualized. ' \
          'The values in brackets of the $\\alpha$-columns indicate t-values, ' \
          'which are based on standard deviation estimates corrected ' \
          'for HAC, using a lag of one period. "FF 5" abbreviates the ' \
          '\\citet{{fama2014}} five-factor model, "FF 5 + mom" the momentum ' \
          'augmented version of the latter. ' \
          'The Sharpe ratio test column reflects t-values of the ' \
          '\\citet{{ledoit2008}} ' \
          'test, which tests the FM vs. the POLS estimation seperately for' \
          ' each specification.'.format(size.replace('_', ' + ').capitalize(),
                                map_buckets[buckets],
                                map_weights[weightings],
                                pd.Timestamp(start).strftime('%Y-%m'),
                                pd.Timestamp(end).strftime('%Y-%m'))

    if per_rank:
        des += ' \\textbf{{Relative ranks}} are used as input data'
    else:
        des += ' The input data are winsorized at the {} level and ' \
               'standardized w.r.t to the cross-section.'.format(
            winsorize_prc)
    if save:
        results.index +=1
        results.index.name = '#'
        df_to_latex(results, path_table=table_path,
                    caption='{} Perf {} {}'.format(size, buckets,
                                                      str(winsorize_prc) ),
                    label=label, include_index=True,
                    table_description=des, centering=True,
                    column_format='lllll|cccrrc', midrule=2, scaling=0.9)
    else:
        print(results)

    return results


def regression_meta_comparison(save=True):

    which = 'multi_reg'
    which_map = dict(single_reg='univariate', multi_reg='multivariate')
    start_date = '20031231'
    end_date = '20141231'
    sizes = ['large_mid', 'large', 'mid']
    reg_meta_view = pd.DataFrame(columns=sizes,
                                 index=['SE Ratio Mean','SE Ratio Median', 'Abs Coef Ratio',
                                        't-value Ratio', 'FC POLS|FM',
                                        'JOINT|POLS|FM'])
    for size in sizes:

        res = inspect_results(size, which=which, start_date=start_date,
                              end_date=end_date)

        reg_meta_view.loc['SE Ratio Mean', size ] = res['RatioSE'].mean()
        reg_meta_view.loc['SE Ratio Median', size] = res['RatioSE'].median()
        # reg_meta_view.loc['SE Ratio Min', size] = res['RatioSE'].min()
        # reg_meta_view.loc['SE Ratio Max', size] = res['RatioSE'].max()
        reg_meta_view.loc['Abs Coef Ratio', size] = \
            (abs(res['POLS_para'])).sum() / (abs(res['FM_para'])).sum()
        reg_meta_view.loc['t-value Ratio', size] = \
            (abs(res['POLS_t_values'])).sum() / (abs(res['FM_t_values'])).sum()

        reg_meta_view.loc['FC POLS|FM', size] = '{}|{}'.format(
            str(res['POLS_selected'].sum()), str(abs(res['FM_selected']).sum()))

        POLS_FC = set(FCRegressionCore.fm_selection(
            t_values=res['POLS_t_values'],method=None))
        FM_FC = set(FCRegressionCore.fm_selection(
            t_values=res['FM_t_values'],  method=None))

        str_FC = '|'.join([str(len(POLS_FC & FM_FC)), str(len(POLS_FC - FM_FC)),
                         str(len(FM_FC - POLS_FC))])
        reg_meta_view.loc['JOINT|POLS|FM', size] = str_FC


    label = '_'.join(['meta_fc', which, start_date[:4], end_date[:4]])

    table_name = label + '.tex'
    table_path = FM_POLS_TABLES.joinpath(table_name)
    des = '\\textbf{{Meta statistics FC selection, {} regressions, Sample Period ' \
          '{} until {}}}. This table shows a meta comparison of the FC ' \
          'selection by POLS and FM. The ratio rows always express the POLS  ' \
          'over the FM quantities. The row "SE Ratio Mean" measures the average SE Ratio' \
          ' of all FC, "SE Ratio Median" accordingly the median. "Abs Coef Ratio" exhibits the ratio of the sum ' \
          'of all absolute coef sizes. Accordingly, the "t-value Ratio" expresses ' \
          'the ratio of the sum of absolute t-values. "FC POLS|FM" counts ' \
          'the number of FC selected by each method. "JOINT|POLS|FM" indicates ' \
          'the selection commonality, by counting the number of jointly selected FC, ' \
          'the FC only selected by POLS, and last, the FC solely chosen' \
          ' by FM.' \
          ' '.format(which_map[which],
                                        pd.Timestamp(start_date).strftime(
                                            '%Y-%m'),
                                        pd.Timestamp(end_date).strftime(
                                            '%Y-%m'))
    reg_meta_view.rename(columns=dict(large_mid='large+mid'), inplace=True)
    reg_meta_view.index.name = ''


    if save:
        df_to_latex(reg_meta_view, path_table=table_path,
                    caption='Meta FC {} {} {}'.format(which,start_date[:4],
                                                      end_date[:4]),
                    label=label, include_index=True,
                    table_description=des, centering=True,
                    column_format='l|ccc')
    else:
        print(res)


def regression_FC_comparison(save=True):

    which = 'multi_reg'
    which_map = dict(single_reg='univariate', multi_reg='multivariate')
    char_map = dict(single_reg=40, multi_reg=28)
    start_date = '19700101'
    end_date = '20031231'
    sizes = [ 'large_mid', 'large', 'mid']
    res_FC = pd.DataFrame(columns=['JOINT FC', 'POLS only', 'FM only'],
                          index=sizes)
    for size in sizes:
        res = inspect_results(size, which=which, start_date=start_date,
                              end_date=end_date)

        POLS_FC = set(
            FCRegressionCore.fm_selection(t_values=res['POLS_t_values'],
                                          method=None))
        FM_FC = set(FCRegressionCore.fm_selection(t_values=res['FM_t_values'],
                                                  method=None))

        res_FC.loc[size, 'JOINT FC'] = ', '.join(list(POLS_FC & FM_FC))
        res_FC.loc[size, 'FM only'] = ', '.join(list(FM_FC - POLS_FC))
        res_FC.loc[size, 'POLS only'] = ', '.join(list(POLS_FC - FM_FC))


    label = '_'.join(['fc_selection', which, start_date[:4], end_date[:4]])

    table_name = label + '.tex'
    table_path = FM_POLS_TABLES.joinpath(table_name)
    des = '\\textbf{{FC selection, {} regressions, Sample Period ' \
          '{} until {}}}. This table gives an overview of the selected FC ' \
          'by POLS and FM. The column "JOINT FC" exhibits the FC selected by ' \
          'both POLS and FM. Accordingly, "POLS only" ("FM only") presents ' \
          'the FC included solely in the POLS (FM) selection procedure.' \
          ''.format(which_map[which], pd.Timestamp(start_date).strftime(
        '%Y-%m'), pd.Timestamp(end_date).strftime('%Y-%m'))
    res_FC.rename(index=dict(large_mid='large+mid'), inplace=True)
    res_FC.index.name = ''

    if save:
        df_to_latex(res_FC, path_table=table_path,
                    caption='Meta FC {} {} {}'.format(which, start_date[:4],
                                                      end_date[:4]),
                    label=label, include_index=True,
                    table_description=des, centering=True, midrule=1,
                    column_format='l|l|l|l',max_char=char_map[which],
                    scaling=0.95)
    else:
        print(res)


def simulation_table():

    num_returnFC = 6
    num_riskFC = 6
    num_uniFC = 4
    str_returnFC = 'Return FC'
    str_riskFC = 'Risk FC'
    str_uniFC = 'Unif FC with $\\rho$'
    str_summary = 'Summary FC {}-{}'.format(
        num_returnFC + num_riskFC + num_uniFC, 99)
    first_level = [str_returnFC] * num_returnFC + [str_riskFC] * num_riskFC \
                  + [str_uniFC] * num_uniFC + [str_summary] * 4

    error_ratios = dict()
    # get results
    for case in SIM_CASES.keys():
        error_ratios[case] = full_results_df(case)


    merged_df = pd.concat(error_ratios)
    merged_df.index.names = ['Case', 'Method', 'Measure']
    unif_FC = merged_df.iloc[:, 16:].transpose()
    summary = [unif_FC.mean(), unif_FC.std(), unif_FC.min(), unif_FC.max()]
    summary = pd.concat(summary, keys=['mean', 'std', 'min', 'max'], axis=1)
    merged_df = pd.concat([merged_df.iloc[:, :16], summary], axis=1)

    merged_df.replace([np.inf, -np.inf], np.nan, inplace=True)
    merged_df[merged_df == 0] = ' '

    merged_df.reset_index(inplace=True)
    merged_df.loc[merged_df.index.isin(
        range(0, len(merged_df), 6)) == False, 'Case'] = ' '
    merged_df.loc[merged_df.index.isin(
        range(0, len(merged_df), 2)) == False, 'Measure'] = ' '
    merged_df['Case'] = merged_df['Case'].apply(lambda x: x[-1])
    merged_df['Method'] = merged_df['Method'].apply(
        lambda x: x.replace('_', ' '))

    merged_df.rename(columns=dict(Case='#'), inplace=True)


    label = 'simulation_res'
    caption = 'fm:' + label.replace('_', ' ')
    table_name = label + '.tex'
    label = 'fm:' + label
    table_path = FM_POLS_TABLES.joinpath(table_name)


    des =' The Table shows the simulation results. Rows market with "Err" contain ' \
         'type II (columns 0-5) and type I error (6-11, 12-99) ' \
         'ratio behavior in percentage points. The rows "Rel Coef" display ' \
         'the relative deviations of the coefficient estimates, for cases in which ' \
         'the FC truly contains predictive information. The relative size of the' \
         'standard error biases are visible in the "Rel SE" rows. All details of the simulation ' \
        'specification can be found in \\citet{messmer2017}. FC 6-11 are linked ' \
         'to the risk dimension of the returns. FC cases 12-15 ' \
         'are interesting in so far, as these are FC with higher correlations ' \
         'to other FC. In case 1 T=600, case 2 T=4200, case 3 T=120, case 4 ' \
         'T=60 in case 5 T=60. Case 5 removes the correlation among FC completed. ' \
         'The number of simulations is set to 2000 for case 1 and 2, and equals ' \
         '5000 for cases 3-5.'
    merged_df.rename(columns={'Return FC': '$\\Pc$ --- Type II',
                              'Risk FC': '$\\Uc$ --- Type I',
                              'Unif FC with $\\rho$': '$\\Sc$ --- Type I',
                              }, inplace=True)


    df_to_latex(merged_df, path_table=table_path, include_index=False,
                caption=caption, label=label, table_description=des,
                centering=True, midrule=6, add_col_name=False,
                scaling=0.78, column_format='lll|cccccc|cccccc|cccc|cccc',
                landscape=True)


if __name__ == '__main__':
    # logging.config.dictConfig(logger_settings.log_set_ups)
    # logger.setLevel('INFO')
    # regression_meta_comparison()
    # regression_FC_comparison()
    # simulation_table()
    performance_measures(save=True)
    # regression_FC_comparison()
    # regression_meta_comparison()
    # performance_measures(save=True)