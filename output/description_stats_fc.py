# import matplotlib as mpl
# mpl.use('Agg')
import logging

import pandas as pd

from phdlib.misc import logger_settings
from phdlib.data.api import get_monthly_data, get_crsp_monthly, \
    weighted_corr_mat
from phdlib import settings
from phdlib.output import tex_tables
from phdlib.metrics import linear_models

logger = logging.getLogger(__name__)


def mv_weighted_average(fc_raw_data, output='mean'):
    """ Calculates a market valued weighted average, equally weighted periods

    Parameters
    ----------
    fc_raw_data: pandas.DataFrame
        Containing columns 'PERMNO', 'date' and the desired FC columns
    output: str
        what output format 'mean' (mean over all periods), 'series¨(values
        at each point in time)

    Returns
    -------

    """
    # get market caps
    mkt_cap = monthly.get_crsp_monthly(['mkt_cap'])['mkt_cap']
    mkt_cap = mkt_cap.stack().reset_index().rename(columns={'level0': 'date',
                                                            'level1': 'PERMNO',
                                                            0: 'mkt_cap'})  # merge data together
    fc_raw_data = fc_raw_data.merge(mkt_cap, left_on=['PERMNO', 'date'],
                                    right_on=['PERMNO', 'date'], how='left')
    grouped_date = fc_raw_data.groupby(by='date')

    vw_avg = dict()
    dates = list()

    # initialize each column
    for column in fc_raw_data:
        if column in ['PERMNO', 'index', 'date']:
            continue
        vw_avg[column] = list()
    #
    for date, group in grouped_date:
        logger.info('Current Date: {}'.format(date))
        for column in fc_raw_data:
            if column in ['PERMNO', 'index', 'date']:
                continue
            tmp_data = group['mkt_cap'].where(group[column].isnull() == False)
            weight = tmp_data / tmp_data.sum()
            vw_avg[column].append((weight * group[column]).sum())

        dates.append(date)

    vw_avg = pd.DataFrame(vw_avg, index=dates)

    if output == 'mean':
        return vw_avg.mean()
    elif output == 'series':
        return vw_avg
    else:
        raise NotImplementedError


def first_data_date(fc_raw_data):
    """ Returns the first date where no na_entries are found for fc
    Parameters
    ----------
    fc_raw_data

    Returns
    -------

    """
    min_dates = dict()

    for column in fc_raw_data:
        logger.info('Current Columns: {}'.format(column))
        if column in ['PERMNO', 'index', 'date']:
            continue
        tmp_data = fc_raw_data[['date', column]].dropna()

        min_dates[column] = tmp_data['date'].min().strftime('%Y-%m')

    return pd.Series(min_dates)


def top_ten_correlations(value_weighted=True):
    """ Writes to ten highest and lowest correlation to tables

    Returns
    -------

    """
    size = 'all'
    start_date = '19740630'
    df = get_monthly_data(start_date=start_date, drop_na=True, normalize=True,
                          winsorize_prc=0.05, drop_columns=['ceq', 'mkt_cap'],
                          size=size, fc_lead=0)

    if value_weighted:
        mkt_cap = get_monthly_data(start_date=start_date, size=size, fc_lead=0,
                                   keep_columns=['mkt_cap'], drop_na=True,
                                   normalize=False, columns=['mkt_cap'])

        mkt_cap = mkt_cap.dropna().set_index(['date', 'PERMNO'])
        mkt_cap, _ = mkt_cap.align(df.set_index(['date', 'PERMNO']),
                                   join='right', axis=0)

        weights = linear_models.market_cap_weight(mkt_cap=mkt_cap)['mkt_cap']

    logger.info('Calculating weighted correlations...')

    df.drop(['PERMNO', 'date', 'ret'], axis=1, inplace=True)
    if value_weighted:
        corr = weighted_corr_mat(df, weights=weights)
    else:
        corr = df.corr()

    logger.info(f'Correlation: {corr}')

    corr_st = corr.stack().reset_index()
    corr_st.rename(columns={0: 'corr'}, inplace=True)
    corr_st = corr_st[corr_st['level_0'] != corr_st['level_1']]
    corr_st['pairs'] = corr_st.apply(
        lambda row: row[['level_0', 'level_1']].tolist(), axis=1)
    corr_st['pairs'].map(lambda x: x.sort())
    corr_st['pairs'] = corr_st['pairs'].map(lambda x: ''.join(x))
    corr_st.drop_duplicates(subset='pairs', inplace=True)
    corr_st = corr_st.sort_values(by='corr').reset_index(drop=True)
    corr_st.drop(corr_st[10:-10].index, inplace=True)

    corr_st.rename(columns={'level_1': 'FC_1', 'level_0': 'FC_2'},
                   inplace=True)
    path = str(settings.LASSO_TABLES.joinpath('top_corr_{}.tex'.format(size)))
    desc = 'The table shows the ten highest and ten lowest correlations ' \
           'pairs of FC based on the normalized and pooled data. In case of' \
           ' an absolute correlation > 0.95 between two FC, we exclude the' \
           ' more recently published FC.'


    tex_tables.df_to_latex(
        corr_st[['FC_1', 'FC_2', 'corr']], path, table_description=desc,
        label='top_corr_' + size, centering=True, caption='top_corr_' + size)


def data_summary():
    """ Writes summary of raw data statistics to latex

    Returns
    -------

    """

    # get raw data without normalization and winsorizing
    start = '19650101'
    df = manager.get_monthly_data(start,
                                  normalize=False,
                                  drop_columns=['mkt_cap', 'ceq', 'roic'])
    print(df.columns)
    df_ad = manager.get_monthly_data(start, normalize=False,
                                     columns=['roic'], winsorize_prc=0.025)
    df_ad.drop(['mkt_cap', 'ceq'], axis=1, inplace=True)
    df = df.merge(df_ad, on=['date', 'PERMNO'])
    #
    df.drop(['ceq', 'mkt_cap'], axis=1, inplace=True)
    summary = df.describe(
        percentiles=[0.05, 0.5, 0.95]).transpose().reset_index()
    path = settings.TABLE_PATH.joinpath('summary_raw_fc.tex')
    summary['count'] /= len(df)
    # Calculate vw-weighted mean and add to summary
    vw_avg = pd.DataFrame({'vw_mean': mv_weighted_average(df, output='mean'),
                           'data_start': first_data_date(df)})
    summary = summary.merge(vw_avg, left_on='index',
                            right_index=True).sort_values(by='index')

    desc = 'Data description of raw data before any data adjustments ' \
           '(z-scores, winsorizing etc.) for the period 1965-01-01 to ' \
           '2014-12-31, based on {} obs. Note also, that it is the summary ' \
           'over all periods for each FC. The column vw-mean measures the ' \
           'market value weighted average. For that, we calculate vw averages ' \
           'for each period, which are then used to obtain an equally weighted' \
           ' average over all periods. Data start indicates the date on' \
           ' which the FC was calculated for at least one' \
           ' stock. Note, we display \\textit{{roic}} winsorized at 2.5% to' \
           ' control for data points which are scaled by numbers close to zero' \
           '.'.format(len(df))
    logger.info('Writing data to in file: {}...'.format(str(path)))
    summary.rename(columns={'count': 'coverage', 'index': 'FC'}, inplace=True)
    col_order = ['FC', 'coverage', 'mean', 'vw_mean', 'std', 'min',
                 '5%', '50%', '95%', 'max', 'data_start']
    print(summary.loc[summary.FC == 'roic', col_order])
    tex_tables.df_to_latex(summary[col_order], str(path), table=True,
                           scaling=0.7, table_description=desc,
                           label='raw_data_summary',
                           centering=True, caption='RawDataSummary')


def data_zero_mean_converage():
    df_drop_na = get_monthly_data(start_date='19700101', size='large',
                                  fill_na=False)
    df_fill_na = get_monthly_data(start_date='19700101', size='large',
                                  fill_na=True)

    # calculate coverage ratio
    ratio = df_drop_na.groupby('date').count().div(
        df_fill_na.groupby('date').count())




def data_density(data, group_by='date', ref_col='PERMNO', columns=None):
    grouped = data.groupby(group_by)
    if columns is None:
        density = grouped.count().div(grouped[ref_col].count(), axis=0)
    else:
        density = grouped[columns].count().div(grouped[ref_col].count(),
                                               axis=0)

    return density


def avg_NAs_data(data):
    avg_month = data.isnull().sum() / len(set(data.date.tolist()))

    avg_month.to_latex()


def number_stocks_shrcd():
    """ returns the number of securities in the CRSP database and count of
    stocks used left after filtering data with respect to exchange, shr code
    and missing data of market cap and book value

    Returns
    -------

    """
    start_date = '19650101'

    # load all stocks data
    file = settings.RAW_DATA_PATH.joinpath(settings.FILE_NAMES['CRSP_ID'])
    crsp = pd.read_csv(str(file), engine='c', compression='gzip',
                       low_memory=False, usecols=['date', 'PERMNO', 'SHRCD'])
    crsp['date'] = pd.to_datetime(crsp['date'], format='%Y%m%d')
    crsp = crsp.loc[(crsp.date > start_date) & (crsp.date <= '20141231'), :]
    all = crsp.pivot(index='date', columns='PERMNO', values='SHRCD')
    all.index = all.index.to_period('M').to_timestamp('M')

    # load only stocks which are used
    used_stocks = manager.get_monthly_data(start_date, columns=['SHRCD'])
    used = used_stocks.pivot(index='date', columns='PERMNO', values='ceq')

    total_stocks = all.count(axis=1)
    relevant_stocks = used.count(axis=1)

    stock_count = pd.concat([total_stocks, relevant_stocks, ], axis=1)

    return stock_count['1965-02-01':]


def main():
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
    top_ten_correlations()


if __name__ == '__main__':
    data_zero_mean_converage()
    main()
    # pv = number_stocks_shrcd()
