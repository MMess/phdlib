import logging
import pandas as pd
from phdlib.output.dl_hyper_select import inspect_results
from phdlib.output.dl_portfolio import asset_analysis_dnn, results_dnn_factors, \
    dnn_trading_cost_factors, dnn_FC_sensitivity
from phdlib.finance.api import ReturnAnalysis
from phdlib.misc.api import log_set_ups
from phdlib.settings import DEEP_TABLES, GARCH_PATH
from phdlib.data.api import Factor, LoadLocalResult
from phdlib.output.tex_tables import df_to_latex, t_value_stars

logger = logging.getLogger(__name__)


def hyper_parameter_search():
    """ Writes the hyper parameter search tables as tex files

    Returns
    -------

    """
    size = 'large_mid'

    # load table results
    table = inspect_results(size=size)

    below_zero = (table['R2_dnn'] < 0).sum()
    num_nan = (table['R2_dnn'].isnull()).sum()
    total = len(table)

    description = 'The Table displays the random search results for ' \
                  '\\textbf{{{} Cap Stocks:}}. ' \
                  'It shows the top ten results of the 200 trials performed, ' \
                  'sorted by $R^2$s. The $R^2$s shown are annualized based ' \
                  'on monthly observations. $R^2$ are given %-points. ' \
                  'The columns "LR", "L2", and "L1" are scaled by a factor ' \
                  'of 1000. In total {} out of {} models show an $R^2$ below zero, ' \
                  'moreover, {} out of {} models fail to provide any numerical ' \
                  'results.'.format(size.replace('_m', ' + M').capitalize(), below_zero, total,
                                    num_nan, total)

    table = table.head(10)
    table['hidden_units'] = table['hidden_units'].astype(int)
    table['layers'] = table['layers'].astype(int)
    table['Rank'] = table['Rank'].astype(int)
    table['patience'] = table['patience'].astype(int)
    table[['R2_dnn', 'R2_lin']] = table[['R2_dnn', 'R2_lin']] * 100
    table.rename(columns={'l2_strength': 'L2', 'l1_strength': 'L1',
                          'keras_optimizer': 'Opt', 'learning_rate': 'LR',
                          'R2_dnn': '$R^2$ DFN', 'R2_lin': '$R^2$ Lin',
                          'hidden_units': 'UN', 'layers': 'HL',
                          'patience': 'Patience'},
                 inplace=True)
    table[['LR', 'L2', 'L1', ]] = table[['LR', 'L2', 'L1', ]] * 1000

    table_name = 'hyper_search_' + size + '.tex'
    table_path = DEEP_TABLES.joinpath(table_name)

    df_to_latex(df=table, path_table=table_path,
                caption='Random Search {} Top Ten'.format(size),
                label='hyper_search_' + size,
                table_description=description, centering=True,
                column_format='l|cccccccccc')


def sharpe_ratio_table():
    """ Writes the sharpe ratio tests

    Returns
    -------

    """
    size = 'large'
    weighting = 'equal'

    test = asset_analysis_dnn(size=size, weighting=weighting,
                              which='sr_test')
    print(test.t_value())
    print(test.p_value())


def _get_pf_measures(size, weighting, approach, buckets, simple_returns=None,
                     start='19820101', end='20141231', dates=None):
    """

    Parameters
    ----------
    size
    weighting
    approach
    buckets

    Returns
    -------

    """
    a_scale = 12*100
    if simple_returns is None:
        simple_returns = results_dnn_factors(size=size, weighting=weighting,
                                             buckets=buckets, linear=True,
                                             approach=approach)

    simple_returns = simple_returns[(simple_returns.index >= start) &
                                    (simple_returns.index <= end)]

    if dates is not None:
        simple_returns = simple_returns.loc[dates]

    num_portfolios = len(simple_returns.drop('Linear', axis=1).columns)
    results = pd.DataFrame(index=['Median', 'Min', 'Max', 'Linear'],
                           columns=['mean', 'std', 'SR', '5ff',
                                    '5ffmom', 'SR Test'])

    which = ['factor_regression', 'sr_test', 'per_stat',
             'factor_regression_bm']

    for portfolio in ['Median', 'Min', 'Max', 'Linear']:
        res = asset_analysis_dnn(simple_returns, column=portfolio,
                                 which=which)

        if portfolio != 'Linear':
            col = 'Portfolio'
            r_name = 'factor_regression'
            results.loc[portfolio, 'SR Test'] = t_value_stars(
                res['sr_test'].t_value())
        else:
            col = 'BM'
            r_name = 'factor_regression_bm'

        # Mean, Std, Sharpe Ratio
        results.loc[portfolio, 'mean'] = res['per_stats'].loc[
                                             'mean', col] * 100
        results.loc[portfolio, 'std'] = res['per_stats'].loc['std', col] * 100
        results.loc[portfolio, 'SR'] = res['per_stats'].loc['SR', col] * 12**0.5

        # fama french 5 factor model
        ff5 = (res[r_name][0].params['Alpha']*a_scale ,
               res[r_name][0].tvalues['Alpha'])
        results.loc[portfolio, '5ff'] = t_value_stars(value=ff5[0],
                                                      t_value=ff5[1])
        # fama french 5 factor + "momentum" model
        ff5mom = (res[r_name][1].params['Alpha']*a_scale,
                  res[r_name][1].tvalues['Alpha'])
        results.loc[portfolio, '5ffmom'] = t_value_stars(value=ff5mom[0],
                                                         t_value=ff5mom[1])

    results.rename(
        columns={'5ff': 'FF-5 $\\alpha$', '5ffmom': 'FF-5 + Mom $\\alpha$'},
        inplace=True)

    return results


def trading_adj_measures():
    trading_costs = {'large': 50, 'mid': 110}
    map_weights = {'equal': 'EW', 'mkt_cap': 'VW'}
    map_buckets = {'decile': 'Decile', 'ff_style': 'FF-style'}
    size = 'large_mid'
    approach = 'naive'
    weighting = 'mkt_cap'
    buckets = 'decile'
    max_freq = 6
    frequencies = range(1, max_freq + 1)
    rows = (max_freq + 1) * 6
    para = dict(size=size, approach=approach, weighting=weighting,
                buckets=buckets)

    factors = Factor().get(model='five-factor')
    factors_mom = Factor().get(model='five+mom-factor')

    res = dnn_trading_cost_factors(linear=True, combination=75, **para)

    # unpack pf objects
    single, comb, linear = res['Single'], res['Comb'], res['Linear']

    measures = ['Mean','Std', 'SR','TO', '5ff', '5ffmom', 'SR Test']
    costs = ['Yes', 'No']
    model = ['Comb', 'Linear']
    columns = pd.MultiIndex.from_product([model, costs, measures],
                                         names=['Model', 'Costs', 'measure'])
    results = pd.DataFrame(index=frequencies, columns=columns)
    results.index.name = 'Freq'
    a_scale = 12*100

    for freq in frequencies:

        linear.rebalance_freq = freq

        for pf_obj, pf_name in zip([single, comb], ['Comb']):
            pf_obj.rebalance_freq = freq
            ret_non = ReturnAnalysis(simple_rets=pf_obj.factor_return,
                                     bm_simple_rets=linear.factor_return,
                                     annu_factor=12)

            ret_adj = ReturnAnalysis(
                simple_rets=pf_obj.adj_factor_return(
                    how='time_size'),  # , costs_bp=trading_costs[size]),
                bm_simple_rets=linear.adj_factor_return(
                    how='time_size'),  # , costs_bp=trading_costs[size])
                annu_factor=12)

            for ra, adj_name in zip([ret_non, ret_adj], ['No', 'Yes']):

                results.loc[freq, (pf_name, adj_name, 'Mean')] = ra.mean
                results.loc[freq, (pf_name, adj_name, 'Std')] = ra.std
                results.loc[freq, (pf_name, adj_name, 'SR')] = ra.SR*12**0.5
                tmp = t_value_stars(ra.test_diff_sharpe_ratio().t_value() )
                results.loc[freq, (pf_name, adj_name, 'SR Test')] = tmp

                reg = ra.factor_regression(factor_returns=factors)
                tmp = (reg.params['Alpha']*a_scale,  reg.tvalues['Alpha'])
                tmp =  t_value_stars(value=tmp[0],  t_value=tmp[1])
                results.loc[freq, (pf_name, adj_name, '5ff')] = tmp

                reg = ra.factor_regression(factor_returns=factors_mom)
                tmp = (reg.params['Alpha']*a_scale, reg.tvalues['Alpha'])
                tmp = t_value_stars(value=tmp[0], t_value=tmp[1])
                results.loc[freq, (pf_name, adj_name, '5ffmom')] = tmp

                # linear bm
                results.loc[freq, ('Linear', adj_name, 'Mean')] = ra.mean_bm
                results.loc[freq, ('Linear', adj_name, 'Std')] = ra.std_bm
                results.loc[freq, ('Linear', adj_name, 'SR')] = ra.SR_bm*12**0.5
                reg = ra.factor_regression(factor_returns=factors, bm=True)
                tmp = (reg.params['Alpha']*a_scale,  reg.tvalues['Alpha'])
                tmp = t_value_stars(value=tmp[0], t_value=tmp[1])
                results.loc[freq, ('Linear', adj_name, '5ff')] = tmp

                reg = ra.factor_regression(factor_returns=factors_mom, bm=True)
                tmp = (reg.params['Alpha']*a_scale, reg.tvalues['Alpha'])
                tmp = t_value_stars(value=tmp[0], t_value=tmp[1])
                results.loc[freq, ('Linear', adj_name, '5ffmom')] = tmp

                # average yearly turnover
                tmp = '{:3.0f}'.format(pf_obj.turnover['factor'].mean()*100)
                results.loc[freq, (pf_name, adj_name, 'TO')] = tmp
                tmp = '{:3.0f}'.format(linear.turnover['factor'].mean()*100)
                results.loc[freq, ('Linear', adj_name, 'TO')] = tmp

    results = results.stack(level=['Model', 'Costs'])
    results = results[measures]
    results.reset_index(inplace=True)
    results.loc[results.index.isin(range(0, rows, 4)) == False, 'Freq'] = ' '
    fun = lambda x: str(int(x)) if x is not ' ' else ''
    results['Freq'] = results['Freq'].apply(fun)
    results.loc[results.index.isin(range(0, rows, 2)) == False, 'Model'] = ' '
    results.loc[results.index.isin(range(1, rows, 2)) == False, 'TO'] = ' '
    results['Mean']*=100
    results['Std'] *= 100

    results.rename(
        columns={'5ff': 'FF-5 $\\alpha$', '5ffmom': 'FF-5 + Mom $\\alpha$',
                 'TO': 'TO-%'},
        inplace=True)

    label = '_'.join(['cost_adj', approach, size, buckets, weighting])
    caption = label.replace('_', ' ')
    table_name = label + '.tex'
    table_path = DEEP_TABLES.joinpath(table_name)

    bold = '\\textbf{{{} Cap Stocks, {} Cutoffs,' \
           ' {} Weighted:}}'.format(size.capitalize(),
                                    map_buckets[buckets],
                                    map_weights[weighting])

    des =  bold  + ' The table shows the performance impact once trading ' \
                   'costs are explicitly accounted for. The column "Costs" ' \
                   'compares the results with (Yes) and without (No) trading ' \
                   'costs. Trading costs are calculated as in ' \
                   '\\citet{{brandt2009}}. TO-% indicates the average of the ' \
                   'monthly portfolio turn' \
                   'over (the upper bound of a long-short portfolio is ' \
                   '400%). The column "Freq" refers to the rebalancing ' \
                   'frequency in months.' \
                   ''.format()

    print(results)
    df_to_latex(results, path_table=table_path, caption=caption, label=label,
                include_index=False, table_description=des, centering=True,
                column_format='lll|ccccrrc', midrule=4)

    return results


def performance_measures(save=False):
    """ Writes the factor regression results to

    Returns
    -------

    """
    size = 'large_mid'
    _approach = 'naive'
    buckets = 'ff_style'
    start = '19820101'
    end = '20141231'


    map_weights = {'equal': 'EW', 'mkt_cap': 'VW'}
    map_buckets = {'decile': 'Decile', 'ff_style': 'FF-style'}

    res_tables = list()

    for combination in [None, 75]:

        if combination is not None:
            approach = '_'.join([_approach, str(combination)])
            _type = 'Comb ' + str(combination)
        else:
            approach = _approach
            _type = 'single'

        for weighting in ['mkt_cap', 'equal']:
            results = _get_pf_measures(size=size, weighting=weighting,
                                       buckets=buckets, approach=approach,
                                       start=start, end=end)

            results['Weights'] = None
            results.loc['Median', 'Weights'] = map_weights[weighting]
            results['Type'] = None
            results.loc['Median', 'Type'] = _type
            print(results)
            res_tables.append(results)

    results = pd.concat(res_tables)
    results.index.name = 'Portfolio'
    results.set_index(['Weights', 'Type'], append=True, inplace=True)
    data_cols = results.columns
    results.reset_index(inplace=True)
    results = results[['Portfolio', 'Weights', 'Type'] + list(data_cols)]

    label = '_'.join([_approach, 'perf', size, buckets, start[:4], end[:4]])

    table_name = label + '.tex'

    table_path = DEEP_TABLES.joinpath(table_name)
    des = '\\textbf{{{} Cap Stocks, {} Cutoffs, Sample Period  {} until {}:}}' \
          ' The table exhibits ' \
          'performance measures for several portfolio schemas. It contains ' \
          'value-weighted (VW) and equally-weighted (EW) portfolios, both ' \
          'for single and forecast combination based predictions ("Comb 75", ' \
          'combines 75 single model predictions). \n All performance measures' \
          ' are displayed annualized. ' \
          'The values in brackets of the $\\alpha$-columns indicate t-values, ' \
          'which are based on standard deviation estimates corrected ' \
          'for HAC, using a lag of one period. "FF 5" abbreviates the ' \
          '\\citet{{fama2014}} five-factor model, "FF 5 + mom" the momentum ' \
          'augmented version of the latter. ' \
          'The Sharpe ratio test column reflects t-values of the ' \
          '\\citet{{ledoit2008}} ' \
          'test, which are measured against the linear benchmark.' \
          ' The portfolio classification into "Median",' \
          ' "Min" and "Max" portfolios are determined based on the ' \
          'the final return index values. {} DFN portfolios are' \
          ' considered.'.format(size.replace('_', ' + ').capitalize(),
                                map_buckets[buckets],
                                pd.Timestamp(start).strftime('%Y-%m'),
                                pd.Timestamp(end).strftime('%Y-%m'),
                                82)

    if save:
        df_to_latex(results, path_table=table_path,
                    caption='{} Perf {} {}'.format(_approach.capitalize(),
                                                   size, buckets),
                    label=label, include_index=False,
                    table_description=des, centering=True,
                    column_format='lll|cccrrc', midrule=4)
    else:
        print(results)

    return results


def garch_vola(cut_off=0.85):
    garch_vola = pd.read_csv(str(GARCH_PATH), index_col='index',
                             parse_dates=True)['Normal']

    garch_vola = garch_vola[(garch_vola.index >= '19820101') &
                                    (garch_vola.index <= '20141231')]
    pct = garch_vola.rank(pct=True)

    high_dates = garch_vola.index[pct > cut_off]
    low_dates = garch_vola.index[pct <= cut_off]

    return high_dates, low_dates


def vola_regimes(save=True):

    approach = 'naive_75'
    buckets = 'decile'
    weighting = 'mkt_cap'
    sizes = ['large_mid']

    # TODO integrate vola better
    high_vola, low_vola = garch_vola()
    regimes = dict(high=high_vola, low=low_vola)

    res_tables = list()
    for size in sizes:

        for key, value in regimes.items():
            results = _get_pf_measures(size=size, weighting=weighting,
                                       buckets=buckets, approach=approach,
                                       dates=value)
            results = results.loc[['Median', 'Linear']]
            results.rename(index= dict(Median='DFN'),inplace=True)
            results['Size'] = size
            results['Volatility'] = key
            res_tables.append(results)

    results = pd.concat(res_tables)
    results.index.name = 'Portfolio'
    results.set_index(['Size', 'Volatility'], append=True, inplace=True)
    data_cols = results.columns
    results.reset_index(inplace=True)
    results = results[['Size', 'Volatility', 'Portfolio',] + list(data_cols)]


    rows = len(sizes)*4
    results.loc[results.index.isin(range(0, rows, 4)) == False, 'Size'] = ' '
    results.loc[results.index.isin(range(0, rows, 2)) == False, 'Volatility'] = ' '

    print(results)

    label = '_'.join(['vola_regime', buckets, weighting, size])

    table_name = label + '.tex'

    table_path = DEEP_TABLES.joinpath(table_name)
    des = '\\textbf{{Volatility Regimes, {} Cutoffs, VW, Sample Period ' \
          '1982-01 until 2014-12:}}' \
          ' The table depicts ' \
          'performance measures conditional on the market volatility regime. ' \
          'The regime "high" are all months in which the GARCH(1,1) volatility ' \
          'estimate belongs to the 15% highest estimates, "low" accordingly ' \
          'to the lowest 85%. ' \
          'The DFN portfolio refers to the median forecast combination ' \
          'portfolio ("Comb 75"). All performance measures are displayed in ' \
          'annual terms. ' \
          'The values in brackets of the $\\alpha$-columns indicate t-values, ' \
          'which are based on standard deviation estimates corrected ' \
          'for HAC, using a lag of one period. "FF 5" abbreviates the ' \
          '\\citet{{fama2014}} five-factor model, "FF 5 + mom" the momentum ' \
          'augmented version of the latter. ' \
          'The Sharpe ratio test column reflects t-values of the ' \
          '\\citet{{ledoit2008}} ' \
          'test, which are measured against the linear benchmark.' \
              ''.format('Decile')

    if save:
        df_to_latex(results, path_table=table_path,
                    caption='Vola Regimes {} {}'.format(weighting, buckets),
                    label=label, include_index=False,
                    table_description=des, centering=True,
                    column_format='lll|cccrrc', midrule=4)
    else:
        print(results)

    return results

def fc_sensitivity():

    results = list()
    approach = 'naive'

    sizes =  ['large_mid','large', 'mid']
    size_map = dict(large_mid='l+m', large='large', mid='mid')
    for size in sizes:
        df = dnn_FC_sensitivity(approach=approach, size=size).mean()
        df = df.sort_values(ascending=False)
        df = df.head(10).to_frame('MSD')
        df = df/df.max()
        df.index.name = 'FC'
        df.reset_index(inplace=True)
        df = df.transpose()
        # df.index.name = ''
        df.columns = range(1,11)
        df['Size'] = size_map[size]
        df.set_index('Size', append=True, inplace=True)
        results.append(df)

    results = pd.concat(results)
    results.reset_index(inplace=True)
    results.loc[
        results.index.isin(range(0, len(sizes)*2, 2)) == False, 'Size'] = ' '

    label = '_'.join([approach, 'FC_sens'])

    table_name = label + '.tex'
    des = 'The table presents the MSD pooled over time and predictions' \
          ' conditioned on the respective size sample. The MSD is calculated as' \
          'in equation \\ref{eq:MSD}. Row "l+m" represents the joint sample of' \
          ' large and mid caps. The value are normalized with respect to the ' \
          'maximum MSD value in each subsample.'
    table_path = DEEP_TABLES.joinpath(table_name)
    df_to_latex(results, path_table=table_path,
                caption='{} FC Sens {}'.format(approach.capitalize(),
                                               'All'),
                label=label, include_index=False,
                table_description=des, centering=True, midrule=2,
                column_format='ll|' + 'c'*10, scaling=0.75)
    return df


def decile_portfolio():
    file_name = '2018-01-03_222358_decile_pf_stats_Result.h5'
    data = LoadLocalResult(file_pattern=file_name).data

    data = data.reset_index().rename(columns={'level_0': 'Measure',
                                              'size' : 'Size'})
    col_order = ['Size', 'Measure','Name', 'Top', '1', '2', '3', '4', '5', '6', '7', '8',
       'Bottom']

    data = data[col_order]


    cols = ['Top', '1', '2', '3', '4', '5', '6', '7', '8', 'Bottom']

    deltas = [data]
    for size in ['large_mid', 'large', 'mid']:
        ind = (data['Size'] == size) & (data['Measure'] == 'Mean')
        delta = (data.loc[ind & (data['Name'] == 'DFN'), cols]).sub(
                data.loc[ind & (data['Name'] == 'Linear'), cols].values)

        delta['Size'] = size
        delta['Measure'] = '$\Delta$ Mean'
        delta['Name'] = 'DFN - Linear'

        deltas.append(delta)

        print(delta)

    df = pd.concat(deltas)
    df = df[col_order]
    df.loc[df['Size']=='large_mid', 'Size'] = '1_large_mid'
    df = df.sort_values(['Size','Measure'])
    df.loc[df['Size'] == '1_large_mid', 'Size'] = 'l + m'
    df.reset_index(drop=True, inplace=True)
    df[cols] = df[cols]*100
    df.loc[df['Measure']=='SR',cols] = df.loc[df['Measure']=='SR',cols]/100

    df.loc[df.index.isin(range(0, len(df), 7)) == False, 'Size'] = ' '
    ind = [0,1,3,5,7,8,10,12,14,15,17,19]
    df.loc[df.index.isin(ind) == False, 'Measure'] = ' '

    print(df)


    label = 'decile_portfolios'
    table_name = label + '.tex'
    des = 'The table depicts selected decile portfolio performance statistics.' \
          ''
    table_path = DEEP_TABLES.joinpath(table_name)

    df_to_latex(df, path_table=table_path,
                caption='decile_portfolios',
                label=label, include_index=False,
                table_description=des, centering=True, midrule=7,
                column_format='lll|' + 'c'*10, scaling=0.75)


def model_averaging(save=True):
    """ Writes the factor regression results to

    Returns
    -------

    """
    # size = 'large_mid'
    _approach = 'naive'
    buckets = 'decile'
    start = '19820101'
    end = '20141231'

    map_weights = {'equal': 'EW', 'mkt_cap': 'VW'}
    map_buckets = {'decile': 'Decile', 'ff_style': 'FF-style'}

    res_tables = list()

    for size in ['large_mid','large', 'mid']:
        for combination in [10, 25, 50, 75, 100, 150]:

            if size == 'large_mid' and combination > 75:
                continue

            approach = '_'.join([_approach, str(combination)])
            _type = 'Comb ' + str(combination)

            for weighting in ['mkt_cap']:
                results = _get_pf_measures(size=size, weighting=weighting,
                                           buckets=buckets, approach=approach,
                                           start=start, end=end)

                results['Weights'] = None
                results.loc['Median', 'Weights'] = map_weights[weighting]
                results['Type'] = None
                results['Size'] = size
                results.loc['Median', 'Type'] = _type
                print(results)
                res_tables.append(results)

    results = pd.concat(res_tables)
    results  = results.reset_index()
    results = results.loc[results['index'] == 'Median']
    results['mean'] = results['mean'].astype(float)
    df = results.pivot_table(index='Type', columns='Size', values='mean')
    order = ['Comb 10', 'Comb 25', 'Comb 50', 'Comb 75', 'Comb 100', 'Comb 150']
    df = df.loc[order, :]
    df.rename(columns={'large_mid':'l+m'}, inplace=True)

    print(df)

    label = '_'.join(['model_averaging', buckets,])
    table_name = label + '.tex'

    table_path = DEEP_TABLES.joinpath(table_name)
    des = '\\textbf{{, {} Cutoffs, Sample Period  {} until {}:}}' \
          ' .'.format(map_buckets[buckets],
                      pd.Timestamp(start).strftime('%Y-%m'),
                      pd.Timestamp(end).strftime('%Y-%m') )

    if save:
        df_to_latex(df, path_table=table_path,
                    caption='{} Combination All'.format(buckets),
                    label=label, include_index=True,
                    table_description=des, centering=True,
                    column_format='l|cccrrc',)
    else:
        print(df)

    return df


if __name__ == '__main__':
    logging.config.dictConfig(log_set_ups)
    logger.setLevel('INFO')
    # hyper_parameter_search()
    model_averaging()
    # decile_portfolio()
    # trading_adj_measures()
    # vola_regimes()
    # performance_measures(save=True)
    # fc_sensitivity()

    # fc_sensitivity()
    # res = trading_adj_measures()
    # sharpe_ratio_table()>
    # hyper_parameter_search()
