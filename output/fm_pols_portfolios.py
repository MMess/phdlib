import logging
import pandas as pd

from phdlib.data.api import get_monthly_data, LocalResults, LoadLocalResult, \
         get_crsp_monthly, published_FC_set
try:
    from phdlib.data.api import Factor
except ImportError:
    print('Factor not available')

from phdlib.finance.api import ReturnIndex, ReturnAnalysis, FactorAnalysis
from phdlib.metrics.api import panel_ols_regression, fama_macbeth,\
    fwr_selection
from phdlib.misc.api import log_set_ups_runs, load_settings
from phdlib.output.lasso_zoo_regression import TimeWin
from phdlib.settings import RESULTS_PATH, SETTINGS_PATH
from scipy.stats import norm

logger = logging.getLogger(__name__)

res_path = RESULTS_PATH.joinpath('cross_section_fm_pols')
PATTERN_LINEAR_Y_HAT = 'y_hat'
PATTERN_LINEAR_PF = 'portfolio'


def load_data(data_para, reg_para):
    """ Loads monthly data

    Parameters
    ----------
    data_para: dict
        for kwargs see `meth`:get_monthly_data
    reg_para: dict
        regression parameter

    Returns
    -------

    """

    df = get_monthly_data(**data_para)
    df.dropna(inplace=True)
    df.set_index(['date', 'PERMNO'], append=True, inplace=True)
    X = df.drop(['ret'], axis=1)
    y = df['ret']


    new_para = data_para.copy()
    new_para['fc_lead']=0
    df_predict = get_monthly_data(**new_para)
    df_predict.dropna(inplace=True)
    df_predict.set_index(['date', 'PERMNO'], append=True, inplace=True)
    X_predict = df_predict.drop(['ret'], axis=1)


    dates = TimeWin.get_dates(
        start_date=X_predict.index.get_level_values('date').min(),
        end_date=X_predict.index.get_level_values('date').max(),
        window=reg_para['window_length'], freq='m')

    return X, y, dates, X_predict


def load_pf_data(size='large', start_date=None, end_date=None):
    rets = get_monthly_data(start_date=start_date, end_date=end_date,
                            size=size, demean_ret=False,
                            columns=['PERMNO', 'adj_ret', 'ret'],
                            drop_na=True, ret_str='adj_ret')

    rets = rets.pivot(index='date', columns='PERMNO', values='ret')

    stocks = get_monthly_data(start_date=start_date,
                              end_date=end_date,
                              size='all', columns=('mkt_cap',),
                              keep_columns=('mkt_cap',))

    num_stocks = stocks.dropna().groupby('date').count()['mkt_cap']
    num_stocks.name = 'num_stocks'

    mkt_cap = get_crsp_monthly(['mkt_cap'])['mkt_cap']
    # align the two according to the criteria

    return rets, mkt_cap, num_stocks


def out_of_sample_linear_model(data_para, reg_para, save_results=True,
                               true_oos=True, which='FM', fw_correction=None,
                               second_stage=True):
    """ Calculates true out-of-sample predictions of the linear model

    Parameters
    ----------
    data_para
    reg_para
    save_results
    true_oos
    which
    fw_correction

    Returns
    -------

    """
    sep = '\n ==================== \n '

    if save_results:
        res_saver = LocalResults(path=res_path)

    X_all, y_all, dates, X_predict = load_data(data_para=data_para,
                                               reg_para=reg_para)

    y_hat_linear = list()

    for date in dates:
        # estimate the model
        if date in dates[::reg_para['re_estimate_freq']]:
            logger.info(sep + 'Current date: {}'.format(date) + sep)
            # prepare data
            index_loc = TimeWin.date_filter(
                X_all.index.get_level_values('date'), date,
                window_type=reg_para['window_type'],
                window_length=reg_para['window_length'])

            if true_oos:
                columns = list(
                    set(published_FC_set(date)) & set(X_all.columns))
            else:
                columns = X_all.columns

            X, y = X_all.loc[index_loc, columns].copy(), y_all[index_loc].copy()

            if which == 'FM':
                y.index = y.index.swaplevel(None, 'date')
                X.index = X.index.swaplevel(None, 'date')
                coef, t_values = fama_macbeth(y=y, X=X)

            elif which == 'POLS':
                coef, t_values = panel_ols_regression(
                    y=y.reset_index(drop=True),
                    X=X.reset_index().drop(['level_0', 'PERMNO'], axis=1),
                    return_type='para_tvalue')
                # coef = pd.Series(reg_res.params, index=X.columns)
                # t_values = pd.Series(reg_res.tvalues, index=X.columns)

            # Family-wise error correction
            if fw_correction is None:
                inactive = abs(t_values) <= abs(norm.ppf(0.05/2))
                if inactive.all():
                    # set max absolute t-value to active
                    inactive[abs(t_values)==abs(t_values).max()] = False
                coef[inactive] = 0
            else:
                reject, p_values = fwr_selection(t_values=t_values,
                                                 method=fw_correction,
                                                 level=0.05)
                if reject.any() == False:
                    reject[abs(t_values) == abs(t_values).max()] = True
                coef[reject == 0] = 0

            # second stage regression
            if reg_para['second_stage']:
                ind = coef[coef != 0].index
                if which == 'FM':
                    coef, t_values = fama_macbeth(y=y, X=X[ind])
                elif which == 'POLS':
                    coef, t_values = panel_ols_regression(
                        y=y.reset_index(drop=True),
                        X=X[ind].reset_index().drop(['level_0', 'PERMNO'],
                                                    axis=1),
                        return_type='para_tvalue')

        # predict the returns (shift FC one back, since +1 for estimation)
        # X_predict = X_all.groupby(level='PERMNO').shift(-1)
        index_loc = X_predict.index.get_level_values('date') == date
        # predict linear model returns
        y_hat_date = X_predict.loc[index_loc, :].multiply(
            coef).sum(axis=1)
        y_hat_date = y_hat_date.to_frame('y_hat')
        y_hat_linear.append(y_hat_date)

        del index_loc, y_hat_date

    y_hat_linear = pd.concat(y_hat_linear).reset_index()
    y_hat_linear = y_hat_linear.pivot_table(index='date', values='y_hat',
                                            columns='PERMNO')

    if save_results:
        reg_para['TrueOOS'] = true_oos
        para = dict(data=data_para, regression=reg_para)
        para['regression']['method'] = which
        para['regression']['true_oos'] = true_oos
        para['regression']['fw_correction'] = fw_correction
        name = '_'.join([which, 'FW', str(fw_correction), 'OOS_FC',
                         str(true_oos), reg_para['window_type'][:4] +
                         str(reg_para['window_length']), PATTERN_LINEAR_Y_HAT])
        res_saver.save(data=y_hat_linear, parameters=para,
                       results_name=name)

    return y_hat_linear


def oos_pf(y_hat, y_hat_parameter=None, buckets='decile',
           weighting='equal', save_results=True,
           name='pf', size=None, trade_freq=1, simple_returns=None,
           mkt_cap=None, num_stocks=None):
    """ Calculates factor portfolio return based on predicted stock returns

    Parameters
    ----------
    y_hat: pd.DataFrame
        with PERMNO as columns, dates as index, and y_hat as values
    y_hat_parameter: dict
        dictionary of input parameters, used to construct y_hat
    buckets: str
        "decile" or "ff_style"
    weighting: str
        "equal" or "mkt_cap"
    save_results: bool
        if factor returns should be saved as result

    Returns
    -------
    fa: FactorAnalysis
        containing all results w.r.t to factor construction
    """

    if save_results:
        res_saver = LocalResults(path=res_path)

    fa = FactorAnalysis(criteria=y_hat, simple_returns=simple_returns,
                        market_value=mkt_cap, ascending=False,
                        weighting=weighting,
                        buckets=buckets, size=size, trade_freq=trade_freq,
                        min_max_mkt_cap=num_stocks)

    if save_results:
        parameter = y_hat_parameter.copy()
        parameter['weighting'] = weighting
        parameter['buckets'] = buckets
        logger.info('Saving factor returns ...')
        data = fa.portfolio.factor_return
        data.name = name
        result_name = '_'.join([weighting, buckets, size, name])

        res_saver.save(data=data, parameters=parameter,
                       results_name=result_name)

    return fa


def load_portfolio(size='large', weighting='mkt_cap', buckets='decile',
                   method='FM', true_oos=True, save=True, fw_correction=None,
                   window_length=144, window_type='expanding',
                   re_estimate_freq=1, per_rank=False, winsorize_prc=0.01,
                   second_stage=True):
    """ Loads all factor portfolio files with corresponding factor returns

    Returns
    -------
    pd.DataFrame
        with index dates, columns portfolio_names and values simple returns
    """
    conditions = dict(size=size, method=method, true_oos=true_oos,
                      fw_correction=fw_correction, window_type=window_type,
                      window_length=window_length, weighting=weighting,
                      buckets=buckets, re_estimate_freq=re_estimate_freq,
                      per_rank=per_rank, second_stage=second_stage)

    if winsorize_prc is not None:
        conditions['winsorize_prc'] = winsorize_prc

    simple_returns = LoadLocalResult(file_pattern=PATTERN_LINEAR_PF,
                             conditions=conditions, since='20171204',
                             path=res_path).data

    return simple_returns


def pf_from_results(size='large', method='FM', true_oos=True, save=True,
                    fw_correction=None, weighting='mkt_cap', per_rank=False,
                    buckets='decile', window_length=144, re_estimate_freq=1,
                    window_type='expanding', winsorize_prc=0.01,
                    second_stage=True):
    """

    Parameters
    ----------
    size
    method
    end_date
    weighting
    buckets

    Returns
    -------

    """
    logger.info('Loading linear y_hat from file...')

    conditions = dict(size=size, method=method, true_oos=true_oos,
                      fw_correction=fw_correction, window_type=window_type,
                      window_length=window_length, per_rank=per_rank,
                      re_estimate_freq=re_estimate_freq,
                      second_stage=second_stage)

    if winsorize_prc is not None:
        conditions['winsorize_prc'] = winsorize_prc

    name = '_'.join([method, 'FW', str(fw_correction), 'OOS_FC',
                     str(true_oos), window_type[:4] +
                     str(window_length), PATTERN_LINEAR_Y_HAT])

    result = LoadLocalResult(file_pattern=name,
                             conditions=conditions, since='20171107',
                             path=res_path)


    name = '_'.join([method,'FW', str(fw_correction), 'OOS_FC',
                     str(true_oos), window_type[:4] + str(window_length),
                     PATTERN_LINEAR_PF])

    oos_pf(result.data, result.parameter, save_results=save,
           name=name, buckets=buckets, weighting=weighting, size=size)



def asset_analysis(simple_returns, simple_return_bm=None,
                   which='factor_regression'):

    factors = Factor().get(model='five-factor')
    factors_mom = Factor().get(model='five+mom-factor')
    ret_anly = ReturnAnalysis(simple_rets=simple_returns,
                              bm_simple_rets=simple_return_bm)

    res_values = dict()

    if 'factor_regression' in which:
        res_values['factor_regression'] = (ret_anly.factor_regression(
            factor_returns=factors), ret_anly.factor_regression(
            factor_returns=factors_mom),)

    if 'factor_regression_bm' in which:
        res_values['factor_regression_bm'] = (ret_anly.factor_regression(
            factor_returns=factors, bm=True), ret_anly.factor_regression(
            factor_returns=factors_mom, bm=True))

    if 'per_stat' in which:
        res_values['per_stats'] = ret_anly.performance_stats(annual_factor=12)

    if 'sr_test' in which:
        res_values['sr_test'] = ret_anly.test_diff_sharpe_ratio()

    return res_values


def return_predictions(size=None, second_stage=None, methods=None,
                       fwer_corrections=None):

    #winsorize_prc=0.01,

    if size is None:
        size = load_settings('fm_pols_predictions')['size']
    if second_stage is None:
        second_stage = load_settings('fm_pols_predictions')['second_stage']
    if methods is None:
        methods = load_settings('fm_pols_predictions')['methods']
    if fwer_corrections is None:
        fwer_corrections = load_settings('fm_pols_predictions')['fwer_corrections']

    logger.info('Running {} regressions, second stage: {}, \n methods: {}, '
                'FWER corrections: {} '
                ''.format(size, str(second_stage), methods, fwer_corrections))

    reg_para = dict(re_estimate_freq=1, window_type='expanding',
                    window_length=144, second_stage=second_stage)

    # TODO change back to 19700101
    data_para = dict(size=size, start_date='19720101',
                     end_date='20141231', demean_ret=True,
                     drop_columns=["cfp_ia", "roic", "pchemp_ia", "ipo"],
                     fc_lead=1, drop_na=True, fill_na=True,
                     winsorize_prc=0.01,
                     per_rank=False)

    for method in methods:
        for true_oos in [True, False]:
            for fw_correction in fwer_corrections:
                out_of_sample_linear_model(data_para, reg_para,
                                           which=method,
                                           save_results=True,
                                           true_oos=true_oos,
                                           fw_correction=fw_correction, )


def return_predictions_left(size='large'):

    #winsorize_prc=0.01,

    reg_para = dict(re_estimate_freq=12, window_type='expanding',
                    window_length=144, second_stage=False)
    data_para = dict(size=size, start_date='19700101',
                     end_date='20141231', demean_ret=True,
                     drop_columns=["cfp_ia", "roic", "pchemp_ia", "ipo"],
                     fc_lead=1, drop_na=True, fill_na=True,
                     winsorize_prc=0.01,
                     per_rank=False)

    for method in ['FM']:
        for true_oos in [False, True]:
            for fw_correction in [None]:
                out_of_sample_linear_model(data_para, reg_para, which=method,
                                           save_results=True,
                                           true_oos=true_oos,
                                           fw_correction=fw_correction, )

def construct_portfolios():
    #  large and large_mid FM still pending
    sizes = [ 'large', 'mid','large_mid',]
    buckets = ['decile',  ] #'ff_style'
    weightings = ['mkt_cap', 'equal' ] #'equal'
    multiple_tests = [None, 'holm', 'bonferroni', 'fdr_by']

    for second_stage in [False]:#, False]:
        for size in sizes:
            for method in ['POLS', 'FM']:
                for true_oos in [True, False]:#, False]:
                    for fw_corr in multiple_tests:
                        for bucket in buckets:
                            for weighting in weightings:
                                pf_from_results(size=size,
                                                true_oos=true_oos,
                                                method=method,
                                                fw_correction=fw_corr,
                                                buckets=bucket,
                                                weighting=weighting,
                                                second_stage=second_stage,
                                                save=True,
                                                window_length=144,
                                                re_estimate_freq=1,
                                                winsorize_prc=0.01,
                                                per_rank=False,
                                                window_type='expanding',)


if __name__ == '__main__':
    logging.config.dictConfig(log_set_ups_runs)
    logging.root.addHandler(logging.getLogger('run_log').handlers[0])
    logger.setLevel('INFO')
    construct_portfolios()
    # return_predictions(size='mid', second_stage=False,
    #                    methods=['POLS', 'FM'],
    #                    fwer_corrections=[None, ])
    # return_predictions_left()
    # construct_portfolios()
   # return_predictions()
