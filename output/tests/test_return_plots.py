import logging
import unittest
from phdlib.output.return_plots import ReturnPlot
from finance.return_calc import ReturnIndex
from phdlib.data.fama_french import Factor

logger = logging.getLogger(__name__)

class RIndex(unittest.TestCase):
    def test_RI(self):
        simple_returns = Factor().get(model='five-factor')
        RI = ReturnIndex(simple_returns)
        ReturnPlot(RI).log_plot()

def main():
    pass


if __name__ == '__main__':
    # logging.config.dictConfig(logger_settings.log_set_ups)
    # logger.setLevel('INFO')
    unittest.main()