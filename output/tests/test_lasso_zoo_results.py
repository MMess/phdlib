import logging
import unittest
import pandas as pd
import numpy as np
from phdlib.data import manager
from phdlib.output import lasso_zoo_regression
from phdlib.misc import logger_settings

logger = logging.getLogger(__name__)

class TestLassoZooRegression(unittest.TestCase):

    def test_panel_ols(self):
        logging.config.dictConfig(logger_settings.log_set_ups)
        logger.setLevel('INFO')
        _data_drops = ['cfp_ia', 'roic', 'pchemp_ia', 'ipo']


        tmp = manager.get_monthly_data_csv(start_date='20051231',
                                           size='large',
                                           drop_columns=_data_drops,
                                           fc_lead=1,
                                           demean_ret=True,
                                           fill_na=True,
                                           winsorize_prc=0.05,
                                           drop_na=True)
        y = tmp['ret']
        X = tmp.drop(['ret', 'PERMNO'], axis=1)

        tmp_1 = manager.get_monthly_data(start_date='20051231',
                                         size='large',
                                         drop_columns=_data_drops,
                                         fc_lead=1,
                                         demean_ret=True,
                                         fill_na=True,
                                         winsorize_prc=0.05,
                                         drop_na=True)
        y_2 = tmp_1['ret']
        X_2 = tmp_1.drop(['ret', 'PERMNO'], axis=1)

        lasso_zoo_regression.FCRegressionCore.panel_ols(y, X)
        lasso_zoo_regression.FCRegressionCore.panel_ols(y_2, X_2)





def main():
    pass


if __name__ == '__main__':
    unittest.main()