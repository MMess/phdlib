import logging
import unittest

logger = logging.getLogger(__name__)

from phdlib.output.tex_tables import split_strings



class StrSplit(unittest.TestCase):

    @classmethod
    def setUpClass(cls):

        cls.max_char=70

        cls.str_to_split = "This study includes cross-sectional stock data " \
                           "from the CRSP/Compustat database from 1970-2014." \
                           " In total I use 68 published FC constructed based " \
                           "on accounting and market data. The focus of the " \
                           "analysis lies solely on large and mid cap stocks," \
                           " to prevent a potential contamination arising " \
                           "from economically unimportant small and micro-cap " \
                           "stocks, as recently documented in"

    def test_split_str(self):

        splits = split_strings(self.str_to_split, max_char=self.max_char)

        for part in splits:
            self.assertTrue(len(part)<=self.max_char)

        re_stacked = ' '.join(splits)
        self.assertEqual(re_stacked, self.str_to_split)

def main():
    pass


if __name__ == '__main__':
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
    main()