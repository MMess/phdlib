import logging
from scipy.stats import norm
import pandas as pd

from phdlib.data.api import LocalResults, LoadLocalResult
from phdlib.output.lasso_zoo_regression import FCPooledRegression,\
    FCSingleRegression
from phdlib.misc.api import log_set_ups_runs
from phdlib.settings import RESULTS_PATH

logger = logging.getLogger(__name__)


pattern_reg = 'multi_regression_fm_pols'
pattern_reg_single = 'single_regression_fm_pols'
res_path = RESULTS_PATH.joinpath('cross_section_fm_pols')


def full_sample_regression(save=True, size='large_mid'):

    res_saver = LocalResults(path=res_path)

    para = dict(size=size, sample_balance=None,
                start_date='20031231', end_date='20141231',
                demean_ret=True, fc_lead=1, drop_na=True, fill_na=True,
                drop_columns=["cfp_ia", "roic", "pchemp_ia", "ipo"],
                winsorize_prc=0.01, per_rank=False)

    reg = FCPooledRegression(which=['POLS', 'FM'], **para)
    res = reg.run()

    if save:
        para['method'] = 'multi_reg'
        name = '_'.join([para['size'], para['start_date'][:4],
                         para['end_date'][:4], pattern_reg])
        res_saver.save(data=res, parameters=para, results_name=name)


    return res


def single_regression(save=True, size='all'):

    res_saver = LocalResults(path=res_path)

    para = dict(size=size, sample_balance=None,
                start_date='19700101', end_date='20141231',
                demean_ret=True, fc_lead=1, drop_na=True, fill_na=True,
                drop_columns=["cfp_ia", "roic", "pchemp_ia", "ipo"],
                winsorize_prc=0.01, per_rank=False)

    reg = FCSingleRegression(**para)
    res = reg.run(which=['FM', 'POLS'])

    res = pd.concat(list(res.values()), axis=1)

    if save:
        para['method'] = 'uni_reg'
        name = '_'.join([para['size'], para['start_date'][:4],
                         para['end_date'][:4], pattern_reg_single])
        res_saver.save(data=res, parameters=para, results_name=name)


    print(res)


def inspect_results(size='large', level=0.05, which='multi_reg',
                    start_date='19700101', end_date='20141231'):
    if which == 'multi_reg':
        pattern = pattern_reg
    else:
        pattern = pattern_reg_single

    conditions = dict(size=size, start_date=start_date,
                      end_date=end_date, winsorize_prc=0.01,
                      per_rank=False)

    res = LoadLocalResult(file_pattern=pattern,
                          conditions=conditions, since='20171220',
                          path=res_path).data

    res.rename(columns=dict(OLS_para='POLS_para',
                            OLS_t_values='POLS_t_values'),
               inplace=True)

    res['POLS_std_err'] = res['POLS_para']/res['POLS_t_values']
    res['FM_std_err'] = res['FM_para'] / res['FM_t_values']
    res['RatioSE'] = res['POLS_std_err'] / res['FM_std_err']
    res['FM_selected'] = abs(res['FM_t_values']) > abs(norm.ppf(level/2))
    res['POLS_selected'] = abs(res['POLS_t_values']) > abs(norm.ppf(level/2))


    return res



if __name__ == '__main__':
    logging.config.dictConfig(log_set_ups_runs)
    logging.root.addHandler(logging.getLogger('run_log').handlers[0])
    logger.setLevel('INFO')
    full_sample_regression()
    full_sample_regression(size='large')
    full_sample_regression(size='mid')
    # single_regression()
    # single_regression(size='large')
    # single_regression(size='mid')
    # single_regression(size='large_mid')
    # # inspect_results()
    # full_sample_regression(size='large')
    # full_sample_regression(size='mid')
    # inspect_results()