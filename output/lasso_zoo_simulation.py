__author__ = 'mmess'
"""SPECIFY FILE HERE
"""
import logging

import pandas as pd
from scipy.stats import norm

from phdlib.data import simulation, results
from phdlib.data.results import LoadLocalResult
from phdlib.settings import RESULTS_PATH
from phdlib.misc import logger_settings

logger = logging.getLogger(__name__)

SIM_CASES = {'Case_1': dict(Vola='Normal', Years=50, FCFunction='linear',
                            MarketRiskPremium=0.055, NumSimulation=2000, ),
             'Case_2': dict(Vola='LTMean', Years=50, NumSimulation=2000,),
             'Case_3': dict(Vola='StudentsT', Years=50, NumSimulation=2000,),
             'Case_4': dict(Vola='Normal', Years=20,NumSimulation=2000,),
             'Case_5': dict(Vola='Normal', Years=350,NumSimulation=2000,),
             'Case_6': dict(Vola='Normal', Years=50, MarketRiskPremium='AR1',
                            NumSimulation=2000, ),
             'Case_7': dict(Vola='Normal', Years=50, FCFunction='Model3',
                            NumSimulation=2000, ),
             # 'Case_8': dict(LassoIC='cv_10', Vola='Normal', Years=50),
             # 'Case_9': dict(LassoIC='cv_10', Vola='Normal', Years=20),
            # 'Case_10': dict(LassoIC='bic', Vola='Normal', Years=50,
            #                 FCFunction='non_linear_1' )
             }
res_path = RESULTS_PATH.joinpath('lasso_zoo')

def error_ratios(case='Case_1', level=0.05):
    """ Loads and returns error ratio results from simulation

    See Messmer and Audrino (2017) for details on the cases

    Parameters
    ----------
    case: str
        "Case_1", ...,  "Case_9" is defined

    Returns
    -------
    pd.DataFrame
        with columns 'OLS', 'AdaptiveLasso' and 'Lasso'
    """
    logger.info('Loading: {}'.format(case))
    result = LoadLocalResult(file_pattern='simulation_lasso_zoo',
                             since='20170920',
                            conditions=SIM_CASES[case],
                             path=res_path)

    # for file in result.files_true:
    #     sub_result = LoadLocalResult(file_pattern=file,
    #                                  path=res_path)
    #     print(sub_result.parameter['Vola'])

    data = result.data
    parameter = result.parameter
    # OLS = data['OLS_DK_tValues']
    # adaLasso = data['AdaptiveLasso']
    # Lasso = data['Lasso']
    N = parameter['NumSimulation']

    ratio = pd.DataFrame(columns=['OLS',
                                   'AdaLasso_BIC', 'AdaLasso_AIC', 'AdaLasso_CV5',
                                   'Lasso_BIC', 'Lasso_AIC', 'Lasso_CV5'],
                          index = range(parameter['NumFC']))

    for col in ratio:
        if col == 'FM':
            ratio.loc[:, col] = (abs(data['FM_tValues']) > \
                                 abs(norm.ppf(level / 2))).sum() / N
        elif col == 'OLS':
            ratio.loc[:, col] = (abs(data['OLS_DK_tValues']) > \
                                 abs(norm.ppf(level / 2))).sum() / N
        else:
            ratio.loc[:, col] = (data[col] != 0).sum() / N

    # # calculate ratios of acceptance
    # ratios = list()
    # ratios.append((abs(OLS)>1.96).sum().T/N)
    # ratios.append((adaLasso!=0).sum().T/N)
    # ratios.append((Lasso!=0).sum().T/N)
    # ratio_df = pd.concat(ratios, axis=1)

    set_up = simulation.simulation_parameters(parameter)
    # adjust by 1-ratio as RetFC are the active variables H1 is True
    ratio.T.iloc[:, :set_up['NumRetFC']] = 1 - ratio.T.iloc[:,
                                                  :set_up['NumRetFC']]


    return ratio


def main():
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')

    df = error_ratios()

    return df


if __name__ == '__main__':
    df = main()
