import logging
import logging.config
import pandas as pd
import time
from statsmodels.sandbox.stats.multicomp import multipletests
from scipy.stats import norm

from phdlib.data import results, tools
from phdlib.data.api import get_monthly_data, fc_years

try:
    from phdlib.data.api import Factor
except ImportError:
    print('Factor not available')
from phdlib.metrics import linear_models, portfolio
from phdlib.misc import logger_settings, functions
from phdlib.finance.api import FactorAnalysis, ReturnAnalysis
from phdlib.settings import RESULTS_PATH

__author__ = 'mmess'

logger = logging.getLogger(__name__)
res_path = RESULTS_PATH.joinpath('lasso_zoo')


class TimeWin:
    @staticmethod
    def get_dates(start_date, end_date, window, freq):
        start_date = pd.Timestamp(start_date)
        end_date = pd.Timestamp(end_date)
        if freq == 'm':
            window = pd.tseries.offsets.MonthEnd(window)

        return pd.date_range(start_date + window, end_date, freq=freq)

    @staticmethod
    def date_filter(date_index, date, window_type='rolling',
                    window_length=120):
        """ Filters data for rolling application (assuming monthly data)
        
        Parameters
        ----------
        date_index: pd.Index/Series
            containing date like values
        date: date like
            as of which date should be calculated
        window_type: str
            'rolling' or 'expaning'
        window_length: int
            length of the window

        Returns
        -------
        index_loc:
            a pandas index selector,
        
        """
        date = pd.Timestamp(date)
        window_offset = pd.tseries.offsets.MonthEnd(window_length)
        index_loc = (date_index <= date)
        if window_type == 'rolling':
            index_loc &= (date_index > date - window_offset)

        return index_loc


class FCRegressionCore():
    # characteristics dropped because of data problems
    _data_drops = ['cfp_ia', 'roic', 'pchemp_ia', 'ipo']

    def __init__(self, start_date='19650101', end_date='20141231',
                 size=None, sample_balance=4000, drop_columns=None,
                 fc_lead=1, which=None, **kwargs):
        """

        Parameters
        ----------
        start_date: str
            a date string with format YYYYMMDD, from when to use data
        end_date: str
            a date string with format YYYYMMDD, until when to use data
        size: str
            which stocks should be used: None -> 'all', 'large' or 'mid' or
            'small' cap
        sample_balance: int
            if data should be sampled to a certain sample size
        drop_columns: list
            which columns should be dropped, list of valid str
        fc_lead: int
            how many periods (months) FC should be lead

        """

        self.start_date = start_date
        self.end_date = end_date
        self.size = size
        self.sample_balance = sample_balance
        self._data = None
        self.fc_lead = fc_lead
        # prepare columns to be dropped
        if drop_columns is None:
            drop_columns = []

        # ensure with set operation unique elements
        self.drop_columns = list(set(drop_columns + self._data_drops))
        self.which = which
        self._weights = None
        self._data_kwargs = {**kwargs}

    def _get_data(self, columns=None, prediction_data=False,
                  weights_index=False):
        """ Loads the data into memory and assigns them to y and X

        Returns
        -------

        """
        if self._data is not None:
            return

        data_defaults = dict(winsorize_prc=0.05, drop_na=True, demean_ret=True,
                             fill_na=True, per_rank=False)

        # use the defaults if nothing is specified
        for key in set(data_defaults.keys()) - set(self._data_kwargs.keys()):
            self._data_kwargs[key] = data_defaults[key]

        if prediction_data:
            fc_lead = 0
        else:
            fc_lead = self.fc_lead

        tmp = get_monthly_data(start_date=self.start_date,
                               end_date=self.end_date, size=self.size,
                               sample_balance=self.sample_balance,
                               drop_columns=self.drop_columns, columns=columns,
                               fc_lead=fc_lead, **self._data_kwargs)
        # Drop NA stocks
        tmp_obs = len(tmp)
        tmp.dropna(inplace=True)
        tmp.reset_index(inplace=True, drop=True)
        logger.info('{0:.03}% of data were dropped due to NAs. \n NumObs: {1};'
                    ' NumObs with NAs {2}; Start Date: {3}; End Date: {4};'
                    .format((1 - len(tmp) / tmp_obs), len(tmp), tmp_obs,
                            tmp['date'].min(), tmp['date'].max()))

        self._data = tmp

        if self.which is None:
            return

        elif 'VWAdaptiveLasso' in self.which:

            logger.info('Loading market_cap data...')
            mkt_cap = get_monthly_data(start_date=self.start_date,
                                       end_date=self.end_date,
                                       size=self.size, fc_lead=1,
                                       keep_columns=['mkt_cap'], drop_na=True,
                                       normalize=False, columns=['mkt_cap'])

            mkt_cap = mkt_cap.dropna().set_index(['date', 'PERMNO'])
            mkt_cap, _ = mkt_cap.align(tmp.set_index(['date', 'PERMNO']),
                                       join='right', axis=0)

            weights = linear_models.market_cap_weight(mkt_cap=mkt_cap)[
                'mkt_cap']

            if weights_index is False:
                self._weights = weights.reset_index(drop=True)
            else:
                self._weights = weights

    @staticmethod
    def panel_ols(y, X):
        """ Runs a panel regression of X on y

        Parameters
        ----------
        y: pandas.Series
            dependent variable
        X: pandas.DataFrame
            independent variables/design matrix

        Returns
        -------
        statsmodels.ols results
        """
        res = linear_models.panel_ols_regression(y, X)
        res = pd.DataFrame({'OLS_para': res.params,
                            'OLS_t_values': res.tvalues},
                           index=X.drop('date', axis=1).columns)
        return res

    @staticmethod
    def fm(y, X, lags=12):
        """ Runs a fama-macbeth regression using NW adjustment

        Parameters
        ----------
        y: pandas.Series
            dependent variable
        X: pandas.DataFrame
            independent variables/design matrix

        Returns
        -------
        statsmodels.ols results
        """
        para, t_values = linear_models.fama_macbeth(y, X, lags=lags)
        res = pd.DataFrame({'FM_para': para,
                            'FM_t_values': t_values},
                           index=X.columns)
        return res

    @staticmethod
    def lasso_selection(estimates, which='selected'):
        """ Lasso selection, keeps non-zero entries, drops zero coefficients

        Parameters
        ----------
        estimates: pandas.Series
            where index are coefficient names, and values the estimates
        which: str
            which to return, selected or dropped coefficients

        Returns
        -------
        list
            a list of either 'selected
        """
        coef_hat = estimates.copy()
        if which == 'selected':
            return coef_hat[coef_hat != 0].index.tolist()
        elif which == 'dropped':
            return coef_hat[coef_hat == 0].index.tolist()
        elif which == 'both':
            return coef_hat[coef_hat != 0].index.tolist(), \
                   coef_hat[coef_hat == 0].index.tolist()

    @staticmethod
    def ols_selection(estimates, t_values, level=0.05, which='selected'):
        """

        Parameters
        ----------
        estimates: pd.Series
            coefficient estimates, same index as t_stats!
        t_values: pd.Series
            t_values estimates, same index as estimates!
        which: str
            which to return, selected or dropped coefficients
        level: num
            which significance level to choose

        Returns
        -------
        pd.Series

        """
        inactive = abs(t_values) <= abs(norm.ppf(level / 2))

        if which == 'selected':
            return estimates[inactive == False].index.tolist()
        elif which == 'dropped':
            return estimates[inactive].index.tolist()
        elif which == 'both':
            return estimates[inactive == False].index.tolist(), \
                   estimates[inactive].index.tolist()

    @staticmethod
    def fm_selection(estimates=None, t_values=None, level=0.05,
                     which='selected', method='fdr_by'):
        """

        Parameters
        ----------
        fwr

        Returns
        -------

        """

        p_value = 1 - norm.cdf(abs(t_values.values.astype(float)))
        if method is not None:
            selected, adj_p_value, _, _n = multipletests(p_value, alpha=level,
                                                         method=method)
        else:
            selected = p_value < level / 2

        if which == 'selected':
            return t_values[selected].index.tolist()
        elif which == 'dropped':
            return t_values[selected == False].index.tolist()
        elif which == 'both':
            return t_values[selected].index.tolist(), \
                   t_values[selected == False].index.tolist()

    @staticmethod
    def adaptive_lasso(y, X, weights=None):
        """ Runs a panel regression of X on y

        Parameters
        ----------
        y: pandas.Series
            dependent variable
        X: pandas.DataFrame
            standardized independent variables/design matrix
        weights: pandas.Series
            weights of observation same len as X and y

        Returns
        -------
        tuple: [0] pd.Series with coef estimates of adaptive Lasso
               [1] pd.Series with coef estimates of Lasso

        """
        print('Ada lass static \n {}'.format(y))
        print('Ada lass static \n {}'.format(X))
        res_ada, res = linear_models.ada_lasso_estimation(y, X,
                                                          normalize=False,
                                                          weights=weights)
        res_ada.rename(columns={'coeff': 'AdaLasso'}, inplace=True)
        res.rename(columns={'coeff': 'Lasso'}, inplace=True)

        return res_ada, res

    @classmethod
    def _messages(cls, which=None):
        """ Logging messages

        Parameters
        ----------
        which: str
            which message

        Returns
        -------

        """

        if which == 'AdaLasso':
            msg = 'Running Adaptive lasso and Lasso regression...'
        if which == 'OLS':
            msg = 'Running OLS regression with...'

        msg += '(in {})'.format(cls.__name__)
        return msg

    def run(self):
        raise NotImplementedError('Abstract super class does not have a fit '
                                  'method callable')

    @classmethod
    def core_analysis(cls, data, which=None, weights=None):
        """ run core regression of

        Parameters
        ----------
        data: pandas.DataFrame
            containing columns 'ret', 'date', 'permno' + any arbitrary FC
        which: list
            list of which regressions methods to use, 'OLS', 'adaptiveLasso',
            'Lasso'

        Returns
        -------
        pd.DataFrame
            with corresponding results

        """
        y = data['ret']
        X = data.drop(['ret', 'PERMNO'], axis=1)
        # save results in dict
        res = list()

        if 'OLS' in which or 'POLS' in which:
            res_ols = cls.panel_ols(y, X)
            res.append(res_ols)

        if 'FM' in which:
            tmp_panel = data.set_index(['date', 'PERMNO'])
            res_fm = cls.fm(tmp_panel['ret'], tmp_panel.drop(['ret',],
                                                             axis=1))
            res.append(res_fm)

        # first ada Lasso
        if 'AdaptiveLasso' in which:
            res_ada, res_lasso = cls.adaptive_lasso(y, X.drop('date', axis=1))
            res.append(res_ada)

        if 'Lasso' in which:
            if 'AdaptiveLasso' not in which:
                res_lasso = linear_models.lasso_estimation(y, X.drop('date',
                                                                     axis=1),
                                                           normalize=False)
            res.append(res_lasso)

        if 'VWAdaptiveLasso' in which:
            print(len(X))
            print(len(weights))
            res_vw_ada, _ = cls.adaptive_lasso(y, X.drop('date', axis=1),
                                               weights=weights)
            res_vw_ada.columns = ['VWAdaLasso']
            res.append(res_vw_ada)

        # concat results
        df = pd.concat(res, axis=1, ignore_index=False)
        df.index.name = 'FC'
        return df

    @classmethod
    def single_pols_factor(cls, data, which, weights=None,
                           periods=('full', 'is', 'oos'), fc_yrs=None,
                           factor_corr=False):
        """ Runs single pooled OLS

        Parameters
        ----------
        data
        which
        weights
        periods
        fc_yrs

        Returns
        -------

        """
        y = data['ret']
        X_all = data.drop(['ret', 'PERMNO'], axis=1)

        # univariate OLS
        columns = ['Uni', 'CAPM', 'FF3', 'Carhart']
        capm = ['beta']
        ff3 = ['mve', 'bm', 'beta']
        Carhart = ['mve', 'bm', 'beta', 'mom12m']
        columns = pd.MultiIndex.from_product([periods, columns,
                                              ['Coef', 't-value']],
                                             names=['Period', 'Model', 'para'])
        results = pd.DataFrame(index=X_all.columns, columns=columns)

        for FC in X_all.columns:

            if FC in ['index', 'date']:
                continue

            for period in periods:
                logger.info('Running single regression for {} in '
                            '{} period'.format(FC, period))

                threshold = fc_yrs.loc[fc_yrs.FC == FC, 'Year'].values[0]
                if period == 'is':
                    date_ind = X_all.date < threshold
                    if date_ind.sum() == 0:
                        continue
                elif period == 'oos':
                    if pd.Timestamp(threshold) > pd.Timestamp('20141231'):
                        continue
                    date_ind = X_all.date >= threshold

                else:
                    date_ind = X_all.index

                _X = X_all.loc[date_ind, :]
                _y = y.loc[date_ind]

                cols = [FC, 'date']
                results.loc[FC, (period, 'Uni')] = \
                    cls.panel_ols(_y, _X[cols]).loc[FC].values
                if FC in capm:
                    continue

                results.loc[FC, (period, 'CAPM')] = \
                    cls.panel_ols(_y, _X[cols + capm]).loc[FC].values

                if FC in ff3:
                    continue
                results.loc[FC, (period, 'FF3')] = \
                    cls.panel_ols(_y, _X[cols + ff3]).loc[FC].values
                if FC in Carhart:
                    continue
                results.loc[FC, (period, 'Carhart')] = \
                    cls.panel_ols(_y, _X[cols + Carhart]).loc[FC].values

        return results

    @classmethod
    def single_pols(cls, data):
        """

        Parameters
        ----------
        data

        Returns
        -------

        """
        y = data['ret']
        X_all = data.drop(['ret', 'PERMNO'], axis=1)

        res = list()
        for FC in X_all:

            if FC in ['index', 'date']:
                continue
            cols = [FC, 'date']
            res_pols = cls.panel_ols(y, X_all[cols])
            res.append(res_pols)

        res = pd.concat(res)

        return res

    @classmethod
    def single_fm(cls, data):
        """ Univariate Fama-MacBeth regressions

        Parameters
        ----------
        data

        Returns
        -------

        """
        tmp_panel = data.set_index(['date', 'PERMNO'])

        res = list()
        for FC in tmp_panel:
            if FC in ['ret', 'index']:
                continue
            res_fm = cls.fm(tmp_panel['ret'], tmp_panel[[FC]], lags=12)
            res.append(res_fm)

        res = pd.concat(res)

        return res


class FCPooledRegression(FCRegressionCore):
    def __init__(self, start_date='19650101', end_date='20141231',
                 size=None, sample_balance=4000, drop_columns=None,
                 which=('AdaptiveLasso', 'Lasso', 'OLS', 'VWAdaptiveLasso'),
                 **kwargs):
        super().__init__(start_date=start_date, end_date=end_date,
                         size=size, sample_balance=sample_balance,
                         drop_columns=drop_columns, which=which,
                         **kwargs)

    def run(self):
        """ runs the regression

        Returns
        -------
        pd.DataFrame
            containing as columns the different methods and row index the
            coefficient names
        """
        # prepare and load data
        self._get_data()

        return self.core_analysis(self._data, which=self.which,
                                  weights=self._weights, )


class FCSingleRegression(FCRegressionCore):
    def __init__(self, start_date='19650101', end_date='20141231',
                 size=None, sample_balance=4000, drop_columns=None,
                 **kwargs):

        super().__init__(start_date=start_date, end_date=end_date,
                         size=size, sample_balance=sample_balance,
                         drop_columns=drop_columns, **kwargs)

    def _load_data(self):
        """ Loads data for single variable portfolio sorting

        Returns
        -------

        """

        data_defaults = dict(drop_na=True, demean_ret=True,  fill_na=True,
                             per_rank=False)

        # use the defaults if nothing is specified
        for key in set(data_defaults.keys()) - set(self._data_kwargs.keys()):
            self._data_kwargs[key] = data_defaults[key]
        tmp_win = self._data_kwargs['winsorize_prc']
        self._data_kwargs['winsorize_prc'] = None
        tmp = get_monthly_data(start_date=self.start_date,
                               end_date=self.end_date, size=self.size,
                               sample_balance=None,
                               drop_columns=self.drop_columns,
                               fc_lead=self.fc_lead,  **self._data_kwargs)
        self._data_kwargs['winsorize_prc'] = tmp_win

        ff3 = Factor().get(model='three-factor')
        carh = Factor().get(model='carhart')
        capm = Factor().get(model='CAPM')

        mkt = get_monthly_data(start_date=self.start_date,
                               end_date=self.end_date,
                               size=self.size,
                               keep_columns=['mkt_cap'],
                               normalize=False, columns=['mkt_cap'],
                               fc_lead=0)

        mkt_cap = mkt.pivot(index='date', columns='PERMNO', values='mkt_cap')

        stocks = get_monthly_data(start_date=self.start_date,
                                  end_date=self.end_date,
                                  size='all', columns=('mkt_cap',),
                                  keep_columns=('mkt_cap',))

        num_stocks = stocks.dropna().groupby('date').count()['mkt_cap']
        num_stocks.name = 'num_stocks'

        return tmp, capm, ff3, carh, mkt_cap, num_stocks

    def _long_short_alphas(self):
        """Runs for all FC the specific long-short alpha regressions

        Returns
        -------

        """
        weights, buckets = ['equal', 'mkt_cap'], ['ff_style', 'decile']
        # factor returns
        logger.info('Load data for long-short alphas')
        data, capm, ff3, carh, mkt_cap, num_stocks = self._load_data()

        factors = [None, capm, ff3, carh]
        factor_names = ['Uni', 'CAPM', 'FF3', 'Carhart']
        #
        # data = self._data.copy()
        rets = data.pivot(index='date', columns='PERMNO', values='ret')
        # TODO fix default shift on low-level and use this constant here
        X_all = data.drop(['ret'], axis=1)
        columns = pd.MultiIndex.from_product((factor_names, weights, buckets),
                                             names=(
                                                 'Model', 'Weights', 'Cutoff'))
        results = pd.DataFrame(index=X_all.columns, columns=columns)

        for FC in X_all.columns:

            if FC in ['date', 'index', 'mkt_cap', 'PERMNO']:
                continue
            logger.info('Running single factor analysis for {}'.format(FC))
            criteria = X_all.pivot(index='date', columns='PERMNO',
                                   values=FC).shift(-1)
            _rets, _ = rets.align(criteria, join='right')
            _mkt_cap, _ = mkt_cap.align(criteria, join='right')

            for bucket in buckets:
                for weight in weights:
                    pf = FactorAnalysis(criteria=criteria,
                                        simple_returns=_rets,
                                        market_value=_mkt_cap, buckets=bucket,
                                        weighting=weight,
                                        min_max_mkt_cap=num_stocks).portfolio

                    ra = ReturnAnalysis(simple_rets=pf.factor_return)
                    for model, name in zip(factors, factor_names):
                        ols_res = ra.factor_regression(factor_returns=model)

                        results.loc[FC, (name, weight, bucket)] = \
                            ols_res.tvalues['Alpha']

        return results

    def run(self, which=('PF', 'POLS_factor')):
        """ runs single pooled regressions and individual long-short factors

        Returns
        -------
        pd.DataFrame
            containing as columns the different methods and row index the
            coefficient names
        """
        # prepare and load data
        self._get_data()
        res = dict()

        if 'PF' in which:
            res['PF'] = self._long_short_alphas()

        if 'POLS_factor' in which:
            fc_ = fc_years(1)
            res['POLS_factor'] = self.single_pols_factor(self._data,
                                                        which=self.which,
                                                        weights=self._weights,
                                                        fc_yrs=fc_,
                                                        periods=('full',))
        if 'FM' in which:
            res['FM'] = self.single_fm(self._data)

        if 'POLS' in which:
            res['POLS'] = self.single_pols(self._data)

        return res


class LassoZooTimeWindow(FCRegressionCore):
    def __init__(self, start_date='19650101', end_date='20141231',
                 size=None, sample_balance=4000, drop_columns=None,
                 which=['VWAdaptiveLasso','AdaptiveLasso', 'Lasso', 'OLS'],
                 window=None,  type=None, fc_lead=1, **kwargs):
        """

        Parameters
        ----------
        start_date: str
            a date string with format YYYYMMDD, from when to use data
        end_date: str
            a date string with format YYYYMMDD, until when to use data
        size: str
            which stocks should be used: None -> 'all', 'large' or 'mid' or
            'small' cap
        sample_balance: int
            if data should be sampled to a certain sample size
        drop_columns: list
            which columns should be dropped, list of valid str
        which: list
            a list of which methods are supposed to be applied
        window: int
            number of periods which are to be considered
        type: str
            which type of time window, 'rolling' or 'expanding'

        """
        self.window = window
        self.type = type
        self.dates = None
        self._FC_date = None

        super().__init__(start_date=start_date, end_date=end_date, which=which,
                         size=size, sample_balance=sample_balance,
                         drop_columns=drop_columns, fc_lead=fc_lead, **kwargs
                         )

    @property
    def FC_date(self):
        """ Loads FC and date

        Returns
        -------
        pd.DataFrame
            with column "FC" and "Year"
        """

        if self._FC_date is None:
            self._FC_date = fc_years(1)

        return self._FC_date

    def published_FC(self, date):
        """ Returns FC published as of

        Parameters
        ----------
        date: datelike
            cutoff for FC publication, selects all FC published prior to this date
        lag: int
            how much publication lag (1=beginning of the next year)
        Returns
        -------
        list of FC published as of date
        """

        return self.FC_date.loc[self.FC_date['Year'] <= date, 'FC'].tolist()

    def _filter_data(self, date=None, true_oos=True):
        """ Filters data based on date and window length

        Parameters
        ----------
        date: datetime
            last date to be included
        true_oos: bool
            if FC should be restricted to be truly out-of-sample (McLean Pontiff)

        Returns
        -------
        pd.DataFrame
            where data is in date-range (date-window, date]
        """
        window_offset = pd.tseries.offsets.MonthEnd(self.window)
        cond = (self._data['date'] <= date)

        if self.type == 'rolling':
            cond &= (self._data['date'] > date - window_offset)

        if true_oos:
            keep_cols = ['ret', 'PERMNO', 'date']
            oos_fc = list(
                set(self.published_FC(date)) & set(self._data.columns))
            return self._data.loc[cond, oos_fc + keep_cols]
        else:
            return self._data.loc[cond, :]

    def _filter_weights(self, date):
        window_offset = pd.tseries.offsets.MonthEnd(self.window)
        dates = self._weights.index.get_level_values('date')
        cond = (dates <= date)

        if self.type == 'rolling':
            cond &= (dates> date - window_offset)

        return self._weights.loc[cond, :].reset_index(drop=True)

    def _get_dates(self):
        """ Sets the relevant date range for the regression period

        Returns
        -------
        """
        # get the relevant dates for rolling regression
        self.dates = TimeWin.get_dates(self.start_date,
                                       self.end_date, self.window, 'm')

    def run(self, name='Rolling Lasso', true_oos=True, **kwargs):
        """ runs the window regression by conditioning the information on date

        Returns
        -------
        pd.DataFrame
            containing as columns the different methods and row index the
            coefficient names
        """
        # load data
        self._get_data(weights_index=True)
        self._get_dates()

        # rolling regression
        results = list()
        counter = 0
        t0 = time.time()
        weights = None

        for date in self.dates:
            tmp_name = ' '.join([name, self.size, date.strftime('%Y-%m-%d')])
            msg = functions.loop_tracker(counter, len(self.dates), t0,
                                         tmp_name, True)
            logger.info(msg)
            # filter data such that only relevant dates are included
            tmp_data = self._filter_data(date, true_oos=true_oos)
            if 'VWAdaptiveLasso' in self.which:
                weights = self._filter_weights(date=date)

            tmp_df = self.core_analysis(tmp_data.reset_index(drop=True),
                                        which=self.which,
                                        weights=weights, **kwargs)
            tmp_df['date'] = date
            results.append(tmp_df.reset_index())
            counter += 1

        results = pd.concat(results, ignore_index=True)
        return results


class LassoZooPrediction(LassoZooTimeWindow):
    # to shorten the name :)
    LZR = FCRegressionCore

    def __init__(self, start_date='19650101', end_date='20141231',
                 size=None, sample_balance=4800, drop_columns=None,
                 which=['AdaptiveLasso', 'OLS'], window=None,
                 type=None, fc_lead=1, second_stage='OLS', true_oos=True,
                 **kwargs):

        # parameter estimates for each method
        self._coefficients = None
        # predictions for each stock
        self._stock_predictions = None
        # factor returns for each method
        self.factor_returns = None
        # which method to use for second stage regression
        self.second_stage = second_stage
        # if FC should be restricted to be true oos
        self.true_oos = true_oos
        # number of missing dates
        self.num_missing_dates = dict()
        super().__init__(start_date, end_date, size, sample_balance,
                         drop_columns, which, window, type, fc_lead, **kwargs)

    @classmethod
    def core_analysis(cls, data, which=None, second_stage=None):
        # run regressions for adaptive Lasso and OLS to get variable selection
        coef_selection = cls.LZR.core_analysis(data, which=which)
        coef_list = list()

        if 'OLS' in which:
            selected, dropped_para = cls.ols_selection(
                coef_selection['OLS_para'],
                coef_selection['OLS_t_values'],
                which='both')
            logger.info('OLS drops: {}'.format(dropped_para))
            if len(selected) == 0:
                logger.warning('No parameter selected at first stage,'
                               ' skipping 2nd stage OLS estimation')
                estimates = pd.DataFrame({'OLS_para': [None]})
            else:
                estimates = cls.LZR.core_analysis(
                    data.drop(dropped_para, axis=1), which=['OLS'])
            coef_list.append(estimates[['OLS_para']])

        if 'AdaptiveLasso' in which:
            selected, dropped_para = cls.lasso_selection(
                coef_selection['AdaLasso'],
                which='both')
            logger.info('AdaLasso drops: {}'.format(dropped_para))

            if second_stage == 'OLS':
                # in case all coefficients are dropped
                if len(selected) == 0:
                    logger.warning('No parameter selected at first stage,'
                                   ' skipping AdaLasso estimates')
                    estimates = pd.DataFrame({'AdaLasso': [None]})
                else:
                    estimates = cls.LZR.core_analysis(
                        data.drop(dropped_para, axis=1), which=['OLS'])
                    estimates.rename(columns={'OLS_para': 'AdaLasso'},
                                     inplace=True)
            else:
                estimates = coef_selection[['AdaLasso']]
            coef_list.append(estimates[['AdaLasso']])

        coefficient_estimates = pd.concat(coef_list, axis=1)
        coefficient_estimates.index.name = None
        return coefficient_estimates

    def _calc_return_predictions(self):
        """ Calculates the return prediction for each single stock

        Returns
        -------

        """
        # self.data -> stacked panel
        #
        # self.predictions -> panel, 'OLS' and 'AdaLasso' as item
        ret_predictions = dict()

        # loop through each method
        estimates = self.coefficients.items()

        # load return data again because we want to include all stocks
        # and FC should be obtained without lagged
        tmp_sample_balance = self.sample_balance
        self.sample_balance = None
        self._data = None
        self._get_data(prediction_data=True)
        self.sample_balance = tmp_sample_balance

        for key, coef in estimates:
            counter = 0
            predictions = list()
            t0 = time.time()
            for date in self.dates:
                msg = functions.loop_tracker(counter, len(self.dates), t0,
                                             'Return prediction ' + self.size,
                                             True)
                logger.info(msg)
                # get all FC of current date + fc_lead to correct for
                # data alignment with returns resulting from estimation part
                # where t rets are aligned with t-1 FC. Hence we need t+1 FC
                # from self._data to get FC data from t
                # fc_date = date + self.fc_lead * pd.tseries.offsets.MonthEnd()
                tmp_data = self._data.loc[self._data['date'] == date, :]
                # save PERMNO, return and date as this information is needed
                tmp_cols = tmp_data[['ret', 'PERMNO']]
                # shift coefficients one period forward because returns lead+1
                # also to be on the conservative side with respect to estimates
                curr_coef = coef.loc[date, :].squeeze()
                # sum up the prediction of returns coef'*variables
                tmp_data = (tmp_data.drop('date', axis=1) *
                            curr_coef.fillna(0)).fillna(0).sum(
                    axis=1).to_frame(name='ret_hat')
                tmp_data[['ret', 'PERMNO']] = tmp_cols
                tmp_data['date'] = date
                predictions.append(tmp_data)
                counter += 1

            # concat all periods
            tmp_df = pd.concat(predictions)
            # get df for prediction returns
            ret_hat = tmp_df.pivot(index='date', columns='PERMNO',
                                   values='ret_hat')

            ret_predictions[key] = ret_hat

        return ret_predictions

    @property
    def coefficients(self):
        if self._coefficients is None:
            # run window regression, based on LassoZooTimeWindow, be aware
            # that it uses a different core_analysis method that
            #  LassoZooTimeWindow
            tmp = self.run(name='Portfolio Coefficients',
                           second_stage=self.second_stage,
                           true_oos=self.true_oos)
            self._coefficients = dict()
            mapping = {'OLS': 'OLS_para', 'AdaLasso': 'AdaLasso'}
            for key, value in mapping.items():
                tmp_df = tmp.pivot(index='date', columns='index', values=value)
                # check if any row has only NA coefficients
                missing_rows = tmp_df.isnull().all(axis=1).sum()
                if missing_rows > 0:
                    logger.warning('{} has no estimates at \n {}'. \
                                   format(key, tmp_df.loc[
                                               tmp_df.isnull().all(axis=1),
                                               :]))
                    if missing_rows > 30:
                        raise ValueError('Too many missing coef estimates: \n'
                                         '{} dates empty'.format(missing_rows))
                    tmp_df = self.fill_missing_rows(tmp_df, limit=12)

                # drop 0 (a result of missing selected coef at point in time)
                if 0 in tmp_df:
                    tmp_df.drop(0, axis=1, inplace=True)

                self.num_missing_dates[key] = missing_rows

                self._coefficients[key] = tmp_df

        return self._coefficients

    @staticmethod
    def fill_missing_rows(df, limit=1, method='ffill'):
        """ Fill completed missing row with previous values

        Parameters
        ----------
        df: pd.DataFrame
            a dataframe with potential missing rows
        limit: int
            how many max rows forward fills are allowed. If more than
            limit-rows are nan, no values are filled in

        Returns
        -------

        """
        df = df.copy()
        na_rows = df.isnull().all(axis=1)
        tmp = df.loc[na_rows | na_rows.shift(-1), :].fillna(method=method,
                                                            limit=limit)
        df.loc[na_rows, :] = tmp.loc[na_rows, :]

        return df

    @property
    def stock_predictions(self):
        if self._stock_predictions is None:
            self._stock_predictions = self._calc_return_predictions()

        return self._stock_predictions


def main_pooled_regressions():
    """ runs the basic regression, full sample selection, rolling regressions

    Returns
    -------

    """
    # full sample pooled OLS, Lasso, adapti ve Lasso
    start_date = '19791231'
    end_date = '20141231'
    parameters = [
        {'size': 'large', 'sample_balance': None, 'start_date': start_date,
         'drop_columns': ['beta_sq'], 'end_date':end_date,},
        # {'size': 'mid', 'sample_balance': None, 'start_date': start_date,
        #  'drop_columns': ['beta_sq']},
        # {'size': 'large_mid', 'sample_balance': None, 'start_date': start_date,
        #  'drop_columns': ['beta_sq']},
        # {'size': 'small', 'sample_balance': 2000, 'start_date': start_date,
        #  'drop_columns': ['beta_sq']},
        # {'size': 'all', 'sample_balance': 4000, 'start_date': start_date,
        #  'drop_columns': ['beta_sq']}
    ]

    for parameter in parameters:
        tmp_res = results.LocalResults(path=res_path)
        parameter['winsorize_prc'] = 0.01
        full_panel_regression = FCPooledRegression(**parameter)
        tmp_data = full_panel_regression.run()
        logger.info('Done with {} stocks. The following res: \n {}'.format(
            parameter['size'], tmp_data))
        print(tmp_data)
        tmp_res.save(data=tmp_data, parameters=parameter,
                     results_name='_'.join(
                         [parameter['size'], 'LassoZoo_pooled_regression']))


def main_rolling_regression():
    # rolling regression OLS, Lasso, adaptive Lasso
    parameters = [
        {'size': 'large', 'sample_balance': None, 'type': 'rolling',
         'window': 12 * 15, 'drop_columns': ['beta_sq'],
         'start_date': '19740630'},
        {'size': 'mid', 'sample_balance': None, 'type': 'rolling',
         'window': 12 * 15, 'drop_columns': ['beta_sq'],
         'start_date': '19740630'},
        {'size': 'small', 'sample_balance': 3000, 'type': 'rolling',
         'start_date': '19740630', 'window': 12 * 15},
        {'size': 'large_mid', 'type': 'rolling','sample_balance': None,
         'window': 12 * 15, 'start_date': '19740630',
         'drop_columns': ['beta_sq'],},
        {'size': 'all', 'sample_balance': 4000, 'type': 'rolling',
         'window': 12 * 15, 'start_date': '19740630',},
    ]

    for parameter in parameters:
        tmp_res = results.LocalResults(path=res_path)
        rolling = LassoZooTimeWindow(**parameter)
        tmp_data = rolling.run(true_oos=False)
        tmp_res.save(data=tmp_data, parameters=parameter,
                     results_name='_'.join(
                         [parameter['size'],
                          'LassoZoo_pooled_roll_regression']))

def linearity_pf_test(load_results=True):

    if load_results:
        file_name = '2017-12-15_044833_LassoZoo_linearity_test_Result.h5'

        data  = results.LoadLocalResult(file_pattern=file_name,
                                        path=res_path).data

    else:
        start_date = pd.Timestamp('19700101')
        tmp_res = results.LocalResults(path=res_path)
        data = FCSingleRegression(start_date=start_date).run()

        tmp_res.save(data=data, parameters=dict(start_date=start_date),
                     results_name='LassoZoo_linearity_test')

    return data



def single_regression():
    # fm = LassoZooSingle(start_date='19800101',sample_balance=None,
    #                      winsorize_prc=0.01, pct_rank=False).run(which='fm')
    #
    # ols = LassoZooSingle(start_date='19800101', sample_balance=4500,
    #                      winsorize_prc=0.01, pct_rank=False).run(which=['ols'])

    # res = FCSingleRegression(start_date='19800101', sample_balance=None,
    #                          winsorize_prc=0.01, per_rank=False, ).run(
    #     which=['FM'])
    #
    start_date = pd.Timestamp('19700101')
    tmp_res = results.LocalResults(path=res_path)
    res = FCSingleRegression(start_date=start_date).run()

    tmp_res.save(data=res, parameters=dict(start_date=start_date),
                 results_name='LassoZoo_linearity_test')
    # res2 = FCRegressions(start_date='19800101', sample_balance=4500,
    #                      winsorize_prc=0.01, pct_rank=False,
    #                      which=['FM', 'OLS']).run()

    # a = res['fm']
    # b = ols['single_reg'].loc[:,('full', 'Uni')]
    # aa = a.loc[abs(a).sort_values('FM_t_values', ascending=False).index]
    # bb = b.loc[abs(b).sort_values('t-value', ascending=False).index]

    return res

    # select = LassoZooRegressions.fm_selection(res['FM_para'],
    #                                           res['FM_t_values'], )

    # return res, select


if __name__ == '__main__':
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
    main_pooled_regressions()
    # main_rolling_regression()
    # c = linearity_pf_test()
    # main_regressions()
    # fc = fc_years(1)
