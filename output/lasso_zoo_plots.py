__author__ = 'mmess'
"""SPECIFY FILE HERE
"""
import json
import logging

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

logger = logging.getLogger(__name__)

from phdlib.misc import logger_settings
from phdlib import settings
from phdlib.output import description_stats_fc, lasso_zoo_simulation, lasso_zoo_regression
from phdlib.output.return_plots import store_figures
from phdlib.data.simulation import simulation_parameters
from phdlib.data import manager, results, tools
from phdlib.misc.api import log_set_ups_runs
from phdlib.metrics import linear_models
from finance.asset_analysis import ReturnAnalysis


PLOT_PATH = settings.LASSO_PATH.joinpath('plots')
res_path = settings.RESULTS_PATH.joinpath('lasso_zoo')


def garch_plot_data():
    fig = plt.figure()
    # ax1 = plt.subplot2grid((5,1), (0,0), rowspan=2)
    # ax2 = plt.subplot2grid((5,1), (2,0), rowspan=2,sharex=ax1)
    # ax3 = plt.subplot2grid((5,1), (4,0), rowspan=1,sharex=ax1)
    garch_data = (pd.read_csv(str(settings.GARCH_PATH),
                              index_col='index')) * 12 ** 0.5
    # f, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(8, 6), sharex=True)

    text = 'GARCH(1,1) cond. volatility with {} distributed residuals'
    # ax1.set_title(text.format('normally'), fontsize=13)
    # ax2.set_title(text.format('student-t'), fontsize=13)
    # ax3.set_title('Difference of the two volatility estimates)',fontsize=13)

    ax = garch_data['Normal'].plot(kind='area')
    sns.palplot(sns.color_palette("BuGn_d"))
    # garch_data['StudentsT'].plot(kind='area', ax=ax2,
    #                              color=sns.xkcd_rgb["pale red"])
    # delta = garch_data['Normal'] - garch_data['StudentsT']
    # delta.plot(ax=ax3, color=sns.xkcd_rgb["medium green"])
    ax.set_title(text.format('normally'), fontsize=13)
    ax.set_ylabel("Annualized volatility")
    ax.set_xlabel("date")
    # ax2.set_ylabel("Annualized volatility")
    # ax2.set_xlabel("date")
    # ax3.set_ylabel("Delta")
    # ax3.set_xlabel("date")
    # store_figures(fig, 'garch_vola', factor=1.5)


def error_ratios():
    """ plots the error ratios of simulation

    Returns
    -------

    """
    case = 'Case_1'
    ratios = lasso_zoo_simulation.error_ratios(case=case)[:31]
    ind = ratios.index

    sns.set(style="darkgrid")
    num_subplots = len(ratios.columns)
    fig, axes = plt.subplots(num_subplots, 1, figsize=(8, 6),
                             sharex=True, sharey=True)

    # cases = {'OLS': ax1, 'AdaLasso_BIC': ax2, 'Lasso': ax3}

    for key, subplot in zip(ratios.columns, axes):
        sns.barplot(y=ratios[key], x=ind, palette="GnBu_d", ax=subplot)
        subplot.set_ylabel(key.replace('_', ''))
        subplot.axvline(5.5, color='k')
        subplot.axvline(11.5, color='k', linestyle='--')
        subplot.set_ylim([0,1])

    axes[3].text(2, -.15, r'$\mathcal{P}$')
    axes[3].text(8, -.15, r'$\mathcal{U}$')
    axes[3].text(17, -.15, r'$\mathcal{S}$')
    axes[2].text(10, -.125, 'Type I', style='italic',
            bbox={'facecolor': 'grey', 'alpha': 0.4, 'pad': 2.5})
    axes[2].text(1.5, -.125, 'Type II', style='italic',
             bbox={'facecolor': 'grey', 'alpha': 0.4, 'pad': 2.5})
    axes[-1].set_xlabel("FC")
    plt.xticks(rotation='vertical')
    axes[0].set_title(
        'Type I and Type II Error Rate; {}'.format(case.replace('_', ' ')))

    file_name = 'error_ratios_{}'.format(case)
    # store_figures(fig, file_name, factor=(1.5*0.8,2.5*0.8), plot_path=PLOT_PATH)


def factor_returns_ols_lasso():
    """ plots factor returns of lasso and ols

    Returns
    -------

    """
    file_name = '2017-01-02_1935_equal_factor_rets_mid_ff_style_180_Result.p'
    file_name = '2017-01-01_2144_equal_factor_rets_mid_ff_style_240_Result.p'
    a = results.load_local_result(file_name)
    print(a)
    res = a['Results'] ['1990':]
    RI = (res+ 1).cumprod() * 100
    RI.dropna(inplace=True)
    first_month = RI.index[0] - pd.tseries.offsets.MonthEnd()
    b = pd.DataFrame({RI.columns[0]: [100], RI.columns[1]: [100]},
                     index=[first_month])

    RI = pd.concat([b, RI])

    np.log(RI).plot()

    a = ReturnAnalysis(RI['AdaLasso'].pct_change(), RI['OLS'].pct_change())

    print(a.performance_stats())
    print(a.test_diff_sharpe_ratio())
    # print(a.performance_stats())
    print(a.corr)
    RI.plot()
    # print(RI)
    # print(RI)


def fc_over_time(start='1973', end='2014'):

    fig = plt.figure()
    ax = plt.axes()

    sns.set(style="darkgrid")
    dates = pd.date_range(start=start, end=end, freq='A')
    fc_years = manager.fc_years(1)

    number_fc = pd.Series(index=dates)

    for date in dates:
        number_fc[date]=(fc_years['Year']<=date).sum()


    number_fc.plot(title = 'FC over time', ax=ax )
    ax.set_xlabel("date")
    ax.set_ylabel("Num of FC")



def number_of_stocks_over_time():
    """The functions plots the number of stocks over time
    :return:
    """
    file_name = 'num_stocks_over_time'
    fig = plt.figure()
    ax = plt.axes()
    stock_count = description_stats_fc.number_stocks_shrcd()
    axs = stock_count.plot(ax=ax)
    axs.set_ylabel('Number of Stocks')
    axs.set_title('Number of CRSP Stocks over time ', fontsize=12)
    # stock_count.plot()
    means = stock_count.mean()
    a = 'Num CRSP Securities (average: {:.0f})'.format(means[0])
    b = 'Num of considered CRSP Stocks (average: {:0.0f})'.format(means[1])
    axs.legend([a,b], loc=2)

    store_figures(fig, file_name, factor=1)


def histogram_fc():
    """ Plots and saves the histogram of all FC in one plot.

    Returns
    -------

    """
    file_name= 'histogram_all_fc'
    df = manager.get_monthly_data(start_date='19650101',
                                  winsorize_prc=0.05, normalize=False)

    fun = lambda x: tools.winsorize(x, percentile=0.01, axis=0)
    df.loc[:, ['ret']] = df[['ret']].groupby('date', axis=1).apply(fun)
    fig = plt.figure()
    ax = plt.axes()
    subplots = df.drop(['date', 'PERMNO'], axis=1).hist(bins=40, ax=ax)
    for row in subplots:
        for plot in row:
            plot.get_xaxis().set_visible(False)
            plot.get_yaxis().set_visible(False)

    store_figures(fig, file_name, factor=(2.5, 2.2))

    return df


def correlation_fc(save=False):
    """ Plots  and saves the correlation heatmap of FC

    Returns
    -------
    saves plot to plot path
    """

    file_name = 'correlation_fc'
    fig = plt.figure()
    ax = plt.axes()
    df = manager.get_monthly_data(start_date='19740630',
                                  normalize=True, winsorize_prc=0.05)
    # df.drop(['mkt_cap', 'ceq'],axis=1, inplace=True)
    corr_mat = df.corr()

    if corr_mat.index.isin(['PERMNO']).any():
        corr_mat.drop(['PERMNO'], inplace=True)
        corr_mat.drop(['PERMNO'], inplace=True, axis=1)
    axs = sns.heatmap(corr_mat, vmin=-1, vmax=1, ax=ax, cmap="RdBu_r",
                      yticklabels=True, xticklabels=True) #annot=True, fmt=".1g"
    plt.xticks(rotation=90)
    plt.yticks(rotation=0)

    if save:
        store_figures(fig, file_name, factor=1.8, plot_path=PLOT_PATH)


def lasso_ada_lasso():
    reg_data = lasso_zoo_regression.clean_collect_data()
    y = reg_data['ret']
    X = reg_data.drop(['ret', 'PERMNO', 'date'])
    linear_models.lasso_path_plot(y, X, axis=1)
    linear_models.ada_lasso_path_plot(y, X, axis=1)


def rolling_lasso_plot(size, file_name, save=False):
    """ Plots rolling adaptive Lasso plots

    Returns
    -------

    """
    # load result data
    logger.info('Loading data for {} stocks rolling regression'.format(size))
    df = results.LoadLocalResult(file_pattern=file_name,
                                 path=res_path).data

    df[['VWAdaLasso']] *= 100
    df.loc[abs(df['OLS_t_values'])<=1.96,'OLS_t_values'] = 0
    df = df.pivot(index='FC', columns='date', values='VWAdaLasso')
    df = df/abs(df).max().max()
    df = df.transpose()

    # get most often selected FC
    a = ((df!=0).sum()/len(df)).sort_values(ascending=False)
    b = ', '.join('\\textit{{{0}}}({1:.1f}\\%)'.format(index, value*100)
                  for index, value in a[:5].items())
    logger.info('The following FC are selected most often through time: \n'
                '{}'.format(b))

    df = df.transpose()
    df.columns = [x.strftime('%Y-%m') for x in df.columns]

    # create figure
    fig = plt.figure()
    ax = plt.axes()
    # color map
    sns.set(style="darkgrid")
    # cmap = sns.diverging_palette(145, 260, as_cmap=True)
    # cmap = sns.color_palette("RdBu_r", 7)
    axs = sns.heatmap(df, ax=ax, cmap="RdBu_r", yticklabels=True,
                      vmin=-abs(df).max().max(), vmax=abs(df).max().max())
    axs.set_ylabel("FC")
    axs.set_xlabel("date")
    plt.xticks(rotation=60)
    plt.yticks(rotation=0)

    ax.set_title(
        'Rolling Regression OLS: {} stocks'.format(size))

    if save:
        file_name = '_'.join([size, 'stocks', 'rolling_reg_plot'])
        logger.info('Saving {} to disk'.format(file_name))
        store_figures(fig, file_name, factor=(2.3, 1.6),
                      plot_path=PLOT_PATH)

    return df

def data_density():
    df = manager.get_monthly_data_csv(start_date='19630312', normalize=False)

    density = description_stats_fc.data_density(df)
    return density


def correlation_simulation():

    file_name = 'heat_corr_case2'
    first_FC = 25
    line_color = 'k'
    # load settings file
    with open(str(settings.SIMULATION_SETTINGS), 'r') as fp:
        set_up = json.load(fp)
    corr_mat = simulation_parameters(set_up)['CorrMatFC']

    # plotting
    sns.set(style="white")
    cmap = sns.cubehelix_palette(light=1, as_cmap=True)
    fig = plt.figure()
    ax = plt.axes()
    axs = sns.heatmap(corr_mat[:first_FC, :first_FC], vmin=0, ax=ax,cmap=cmap,)
    axs.set_ylabel("FC")
    axs.set_xlabel("FC")
    axs.axvline(x=6, color=line_color)
    axs.axvline(x=12, color=line_color)
    axs.axhline(y=6, color=line_color)
    axs.axhline(y=12, color=line_color)
    axs.text(1,5, '$\\mathcal{P}$', fontsize=21)
    axs.text(7,10, '$\\mathcal{U}$', fontsize=21)
    axs.text(14,17, '$\\mathcal{S}$', fontsize=21)
    ax.set_title('Correlation Heatmap - a slice covering the first {} FC'.format(
         first_FC))

    store_figures(fig, file_name, factor=1.5, plot_path=PLOT_PATH)


def update_FC_rolling():
    tables = [
        {'file_name':
             '2017-12-19_043323_all_LassoZoo_pooled_roll_regression_Result.h5',
         'size': 'all'},
        {'file_name':
             '2017-12-19_000625_large_LassoZoo_pooled_roll_regression_Result.h5',
         'size': 'large'},
        {'file_name':
             '2017-12-19_003553_mid_LassoZoo_pooled_roll_regression_Result.h5',
         'size': 'mid'},
        {'file_name':
             '2017-12-19_015743_small_LassoZoo_pooled_roll_regression_Result.h5',
         'size': 'small'},
        {'file_name':
             '2017-12-19_024555_large_mid_LassoZoo_pooled_roll_regression_Result.h5',
         'size': 'large_mid'},
    ]

    for para in tables:

        rolling_lasso_plot(**para)



def main():
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
    # garch_plot_data()
    # error_ratios()
    update_FC_rolling()
    # fc_over_time()
    # return factor_returns_ols_lasso()
    # error_ratios()

if __name__ == '__main__':

    logging.config.dictConfig(log_set_ups_runs)
    logging.root.addHandler(logging.getLogger('run_log').handlers[0])
    logger.setLevel('INFO')
    # correlation_fc(save=True)
    update_FC_rolling()
    # main()

