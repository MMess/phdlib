__author__ = 'mmess'
"""SPECIFY FILE HERE
"""
import datetime as dt
import logging
import multiprocessing

import pandas as pd

from phdlib import settings
from phdlib.misc import logger_settings, functions

logger = logging.getLogger(__name__)


def read_csv_files(file_name, prefix='', compression='gzip', suffix='.csv.gz'):
    logger.info('Parsing data {}...'.format(file_name))
    tmp_csv_file = settings.TMP_PATH.joinpath(
            ''.join([prefix, file_name, suffix]))

    return pd.read_csv(str(tmp_csv_file), index_col='date',
                       compression=compression, engine='c')




def parallel_version():

    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')

    files = ['tang', 'age', 'divi', 'divo']
    pool = multiprocessing.Pool(processes=2)
    logger.info('Parse data...')
    result_list = pool.map(read_csv_files, files)
    result = {}
    map(result.update, result_list)
    logger.info('done.')
    return result

def normal_version():

    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')

    files = ['tang', 'age', 'divi', 'divo']
    logger.info('Parse data...')
    result = dict()
    for file in files:
        result[file] = read_csv_files(file)
    return result


if __name__ == '__main__':

    start_datetime = dt.datetime.now()
    parallel_version()
    end_datetime = dt.datetime.now()
    run_time_str = functions.run_time_string(start_datetime, end_datetime)
    print(run_time_str)
