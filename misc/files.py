import os
import fnmatch

import logging
logger = logging.getLogger(__name__)

# TODO for more advanced pattern matches use glob module


class Directory:
    """Directory provides functionality to dir and file operations
    """

    def __init__(self, directory=None, create=False):
        """
        :param directory: path of directory (string), def=Nne
        :param create: create new directory (bool), def=False
        :return: instance of Directory object
        """
        self.dir = str(directory)

        if create:
            self._create_dir()

        if not os.path.exists(self.dir):
            logging.error('%s does not exists or was not created',
                          self.dir)

    def _create_dir(self):
        """ Create
        :return:
        """
        if not os.path.exists(self.dir):
            os.makedirs(self.dir)
            logger.info('New directory %s created', self.dir)
        else:
            logger.warning('Directory %s exists already', self.dir)

        return

    def find_file(self, file_name=None, pattern_loc='start',
                  full_dir=False):
        """Returns a list of files matching the file_name* pattern

        Parameters
        ----------
        file_name: str
            string pattern to be match
        pattern_loc: str
            either 'start', 'any', 'end'. where to search for the file pattern
            if 'start' only if the first characters of the file match the
            pattern. if 'any' string is matched at any location of the file name
            if 'end' if last characters should be matched -> can be used to
            find file extensions like ".txt" or ".csv"

        Returns
        -------

        """

        if pattern_loc == 'start':
            pattern = ''.join([file_name, '*'])
        if pattern_loc == 'any':
            pattern = ''.join(['*', file_name, '*'])
        if pattern_loc == 'end':
            pattern = ''.join(['*', file_name])

        matched_files = list()
        if full_dir is False:
            for file in os.listdir(self.dir):
                if fnmatch.fnmatch(file, pattern):
                    matched_files.append(file)
        else:
            for root, directories, file_names in os.walk(self.dir):
                for directory in directories:
                    # print(os.path.join(root, directory))
                    pass

                for file in file_names:
                    if fnmatch.fnmatch(file, pattern):
                        matched_files.append(os.path.join(root, file))


        if not matched_files:
            logger.warning('No files with starting pattern "%s" found',
                           file_name)

        return matched_files


def main():
    pass


if __name__ == '__main__':
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
    main()