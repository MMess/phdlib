__author__ = 'mmess'
"""SPECIFY FILE HERE
"""
import pandas as pd
import numpy as np
import string

class DataGenerator():

    np.random.seed(1282)

    @staticmethod
    def get_df(rows=100, cols=10, freq='M'):
        """ Simulate data frame
        """
        if cols < 26:
            col_name = list(string.ascii_lowercase[:cols])
        else:
            col_name = range(cols)
        if rows > 2000:
            freq = 'Min'
        index = pd.date_range('19860825', periods=rows, freq=freq)
        df = pd.DataFrame(np.random.standard_normal((rows, cols)),
                          columns=col_name, index=index)
        df.index.name = 'date'
        df.columns.name = 'ID'
        return df

    @staticmethod
    def get_panel(rows=1000, cols=500, items=10):
        """ simulate panel data
        """

        if items < 26:
            item_names = list(string.ascii_lowercase[:items])
        else:
            item_names = range(cols)
        panel_ = dict()

        for item in item_names:
            panel_[item] = DataGenerator.get_df(rows=rows, cols=cols)

        return pd.Panel(panel_)

if __name__ == '__main__':
    df = DataGenerator.get_df(cols=40)
