"""  Time Series Momentum replication for literature seminar

"""
import pandas as pd
import numpy as np
import pandas_datareader.data as web
import statsmodels.api as sm
import matplotlib.pyplot as plt
import seaborn as sns
from sqlalchemy import create_engine

from phdlib.data import monthly

def get_data_web(data_path):
    """ The function downloads data from FF

    Parameters
    ----------
    data_path: path

    Returns
    -------
    None
    """

    store = pd.HDFStore(data_path)
    start_data = '19640101'
    # monthly data
    returns_monthly = web.DataReader("30_Industry_Portfolios", "famafrench")
    returns_monthly  = returns_monthly[0][start_data:]
    returns_monthly.index =  returns_monthly.index.to_timestamp('M').to_datetime()
    store['monthly'] = returns_monthly
    # Daily data
    returns_daily = web.DataReader("30_Industry_Portfolios_daily", "famafrench")
    store['daily'] = returns_daily[0][start_data:]
    # FF Factors + RF
    ff_factors = web.DataReader("F-F_Research_Data_5_Factors_2x3", "famafrench")
    ff_factors =  ff_factors[0][start_data:]
    ff_factors.index =  ff_factors.index.to_timestamp('M').to_datetime()
    ff_factors['RF'] = ff_factors['RF'] / 12

    ff_mom = web.DataReader("F-F_Momentum_Factor", "famafrench")
    ff_mom =  ff_mom[0][start_data:]
    ff_mom.index =  ff_mom.index.to_timestamp('M').to_datetime()
    ff_factors['UMD'] = ff_mom
    store['FF'] = ff_factors

    store.close()


def get_data(data_path):
    """ get data from hdf5 store

    Parameters
    ----------
    data_path

    Returns
    -------
    returns: pd.DataFrame

    """
    returns = {}
    data_keys = ['daily', 'monthly', 'FF']
    for key in data_keys:
        returns[key] = pd.read_hdf(data_path, key=key)
        returns[key] = returns[key] / 100

    returns['monthly'] = returns['monthly'].subtract(returns['FF'].RF, axis=0)

    return returns

def scale_monthly_returns(returns):
    """ scales monthly returns by volaitlity estimate

    Parameters
    ----------
    returns: pd.DataFrame

    Returns
    -------
    scaled_returns: pd.DataFrame
    """
    returns_daily = returns['daily']
    returns_monthly = returns['monthly']

    vola = pd.ewmstd(returns_daily, span=60, min_periods=60)
    vola = vola.resample('M', closed='right', how='last')*(261**0.5)/12**0.5

    scaled_returns = (returns_monthly/vola.shift(1))*0.20/12**0.5

    return scaled_returns


def lag_regression(returns, max_lag_length):
    """  lag regression of n lags

    Parameters
    ----------
    returns: pd.DataFrame
    max_lag_length: int

    Returns
    -------

    """
    t_stats_ret = {}
    t_stats_sign = {}

    for lag in range(max_lag_length):
        y, X = returns.align(returns.shift(lag+1).dropna(), join='inner')
        dates = X.reset_index(drop=True).stack().reset_index().level_0.values
        X = pd.DataFrame(X.stack().values)
        X['constant']=1

        # regression of r_{t-s}
        model = sm.OLS(y.stack().values,X.values)
        results = model.fit()
        results = results.get_robustcov_results(cov_type='nw-groupsum',
                                                maxlags=1,time=dates)

        t_stats_ret[lag + 1] = results.tvalues[0]

        # regression of sign(r_{t-s})
        X['sign']=1
        X.loc[X[0]<0,'sign']=-1
        model = sm.OLS(y.stack().values,X[['sign', 'constant']].values)
        results = model.fit()
        results = results.get_robustcov_results(cov_type='nw-groupsum',
                                                maxlags=1, time=dates)
        t_stats_sign[lag + 1] = results.tvalues[0]

    t_stats = pd.DataFrame([t_stats_ret, t_stats_sign]).transpose()
    t_stats.rename(columns={0:'ret', 1:'sign'}, inplace=True)
    return t_stats


def ff_regression(y, X):
    """ Fama French regression

    Parameters
    ----------
    y
    X

    Returns
    -------

    """

    factors = X.drop('RF', axis=1)
    factors['constant']=1
    model  = sm.OLS(y,factors)
    res = model.fit()
    res = res.get_robustcov_results(cov_type='HAC', maxlags=1)
    print(res.summary())

    return res


def tsmom_factor(returns,scaled_returns):
    """ Constructs time series momentum factor

    Parameters
    ----------
    returns
    scaled_returns

    Returns
    -------
    tsmom_factor_ret: pd.DataFrame
    """

    momentum  = monthly.momentum_calc(returns['monthly'],
                                      waiting_periods=0, end_period=12)

    momentum.dropna(inplace=True)
    sign =  momentum.where(momentum>=0,-1)
    sign = sign.where(momentum<0,1)
    tsmom_factor_ret=  (sign.shift(1)*scaled_returns).mean(axis=1)

    return tsmom_factor_ret


def plot_bar(t_stats, tex_path):

    # Set up the matplotlib figure
    f, (ax1, ax2) = plt.subplots(2, 1, figsize=(8, 6), sharex=True)

    # Generate some sequential data
    x = t_stats.index
    y1 = t_stats['ret'].values
    f1 = sns.barplot(x, y1, palette="BuGn_d", ax=ax1)
    ax1.set_ylabel("t-values")
    ax1.set_xlabel("Lag $r_t$")
    f1.text(42, 4, 'OLS: $\\frac{r_t}{\\sigma_{t-1}}= \\alpha +'
                   '\\beta_{h} \\frac{r_{t-h}}{\\sigma_{t-h-1}} +'
                   '\\epsilon_{t}$', fontsize=16)
    y2 = t_stats['sign'].values
    f2 = sns.barplot(x, y2, palette="RdBu_r", ax=ax2)
    ax2.set_ylabel("t-values")
    ax2.set_xlabel("Lag $r_t$")
    f2.text(40, 4, 'OLS: $\\frac{r_t}{\\sigma_{t-1}}= \\alpha +'
                   '\\beta_{h} sign(r_{t-h}) + \\epsilon_{t}$', fontsize=16)

    sns.despine(bottom=True)
    plt.setp(f.axes, yticks=[-3,-1,1,3,5])
    plt.tight_layout(h_pad=3)
    ax1.set_title('t-values of lagged returns (Driscoll Kraay robust SE)',
                  fontsize=13)

    f.savefig('\\'.join([tex_path,'t_stats_robust.jpg']), format='jpg', dpi=1000,
              orientation='landscape')


def performance_plot(tsmom, market):
    plt.style.use('ggplot')
    ax1 = plt.subplot2grid((3,1), (0,0), rowspan=2)
    ax2 = plt.subplot2grid((3,1), (2,0), sharex=ax1)
    # f, (ax1, ax2) = plt.subplots(2, 1, figsize=(8, 6), sharex=True)

    df = pd.concat([tsmom, market], axis=1)+1
    df.rename(columns={0: 'TSM'}, inplace=True)
    index = df.cumprod()
    f = index.plot(logy=True,ax=ax1, ylim=[0.5,10])
    f.set_ylabel('Index (Log-Scale)')
    f.set_title('Performance TSMOM and Mkt Index', fontsize=12)

    log_diff = -(np.log(index)).diff(axis=1)['Mkt-RF']
    log_diff.name = 'Log Diff'

    f =log_diff.plot(ax=ax2)
    f.set_ylabel('Log Diff')
    f.set_title('Performance Difference', fontsize=12)

    # sns.tsplot(df[start:end].cumprod(), df[start:end].index)


def main():
    # Inputs

    pwd = '***'
    con_str = f'mysql+pymysql://mm:{pwd}@X1MM/crsp_compustat'
    db_connection = create_engine(con_str)
    data_path = 'D:\\Database\\tmp\\sector_data.h5'
    tex_plots = 'C:\\Users\\Marcial\\Dropbox\\PhD\\Seminars\\' \
                'LiteratureSeminar_20151125\\Plots'
    start_date = '19850101'
    end_date = '20091231'
    max_lags = 60
    update_ff_data = False
    # -----------------------------------------------
    # download FF sector data from web
    if update_ff_data:
        get_data_web(data_path)
    # get sector data from disk
    returns = get_data(data_path)
    # scale returns by ew variance
    scaled_returns = scale_monthly_returns(returns)
    t_stats = lag_regression(scaled_returns[start_date:end_date],max_lags)
    plot_bar(t_stats,tex_plots)
    tsmom = tsmom_factor(returns,scaled_returns)

    # cut to desired time-window
    tsmom = tsmom[start_date:end_date]
    returns['FF'] = returns['FF'][start_date:end_date]

    # performance analytics
    res = ff_regression(tsmom, returns['FF'])

    return res


if __name__ == '__main__':
    data = main()
