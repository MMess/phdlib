__author__ = 'mmess'
"""SPECIFY FILE HERE
"""
import logging
import time
import datetime
import psutil
import json
import sys
import pandas as pd


from phdlib.settings import SETTINGS_PATH

logger = logging.getLogger(__name__)


def days_hours_minutes(td):
    """ The function returns a string of time_delta days, hours, min and sec
    :param td: timedelta
    :return:
    """
    days = td.days
    hours = td.seconds//3600
    min = (td.seconds//60)%60
    seconds = td.seconds % 60

    return '{} day(s), {} hour(s), {} minutes(s)' \
                   ' and {} second(s)'.format(days,hours,min,seconds)


def run_time_string(start_datetime, end_datetime, format='%Y-%m-%d %H:%m'):
    """ Returns a string of runtime information (start, end, runtime)
    :param start_datetime: datetime
    :param end_datetime: datetime
    :param format: string (default='%Y-%m-%d %H:%m')
    :return:
    """

    time_delta = end_datetime - start_datetime
    return 'Start DateTime: {} \n End DateTime: {} \n Run Time: {}'.format(
        start_datetime.strftime(format),
        end_datetime.strftime(format),
        days_hours_minutes(time_delta))


def loop_tracker(current, total_iterations, start_time=None,
                 name=None, ram_usage=False, format='%Y-%m-%d %H:%M'):
    """ Returns a run-time tracker message, calculates iterations etc.

    Parameters
    ----------
    current: int
        current iteration number
    total_iterations: int
        total number of iterations
    start_dt: time.time
        start_time of calculations
    ram_usage: bool
        show current RAM usage
    format: str
        a valid format string

    Returns
    -------
    str
        current status of the loop with respect to run time, etc.
    """
    current +=1
    if name is not None:
        text = '--------{}----------\n'.format(name)
    else:
        text = '------------------\n'
    pct_done = current / total_iterations
    a = 'Running... {2:.3}% completed ({0} of {1}). \n'.format(
        current, total_iterations, pct_done * 100)
    text +=a

    if name is not None:
        text += 'Running: ' + name  + ' \n'
    if start_time is not None:
        f = lambda x: datetime.timedelta(seconds=x)
        avg_time = (time.time() - start_time) / current
        remain_time = f(round((total_iterations - current) * avg_time))
        run_time = f(round(time.time() -start_time))
        end_time = (datetime.datetime.now() + remain_time).strftime(format)
        avg_time = f(round(avg_time))
        # text += '\n------------------\n'
        b = 'Avg time per run {}. Expected run time left : {}. \n' \
            'Running for {} already. Approximated end time: {}'.format(
            avg_time, remain_time,  run_time, end_time)
        text += b

    if ram_usage:
        text += '\nCurrent RAM usage -- {}% '.format(
            psutil.virtual_memory().percent)
    text += '\n------------------\n'
    return text


def load_settings(file_name):
    """ Loads a json  file
    
    Parameters
    ----------
    file_name: str
        file file name, optionally including the suffix (.json)

    Returns
    -------
    dict:
        the json files content
    """
    if file_name[-5:] != '.json':
        file_name += '.json'
    file_path = SETTINGS_PATH.joinpath(file_name)

    with open(str(file_path), 'r') as fp:
        para = json.load(fp)

    return para


def save_settings(target_dict, file_name):
    """ Saves settings as json file

    Parameters
    ----------
    target_dict
    file_name

    Returns
    -------

    """
    file_name = file_name + '.json'
    with open(str(SETTINGS_PATH.joinpath(file_name)), 'w') as fp:
        json.dump(target_dict, fp)


def print_versions():

    print(sys.version)
    print('pandas version -- {}'.format(pd.__version__))



def main():
    fm_settings = dict(size='large', second_stage=True, methods=['POLS'],
                       fwer_corrections=['bonferroni', 'holm', ])
    save_settings(fm_settings, 'fm_pols_predictions')

if __name__ == '__main__':
    main()
