__author__ = 'mmess'
"""SPECIFY FILE HERE
"""
import gzip
import logging
import shutil

from phdlib import settings
from phdlib.misc import logger_settings

logger = logging.getLogger(__name__)


def gzip_file(path_file, path_file_out):
    """ GZIPs a file based in
    :param path_file:
    :param path_file_out:
    :return:
    """

    # open file which should be compressed
    with open(str(path_file), 'rb') as f_in:
        # open file which is the compressed file
        with gzip.open(str(path_file_out),'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)


def compress_tmp_files():
    """ Compresses files stored as tmp
    :return:
    """
    file_endings = ['ep', 'dy', 'bm', 'lev', 'quick', 'currat', 'pchcurrat',
                    'quick', 'pchquick', 'salecash', 'salerec', 'saleinv',
                    'cashdebt', 'depr', 'pchdepr', 'sgr', 'sp',
                    'pchsale_pchinvt',
                    'pchsale_pchrect', 'pchgm_pchsale', 'pchsale_pchxsga',
                    'chinv', 'pchemp', 'hire', 'cfp', 'acc', 'rd', 'lgr',
                    'egr', 'rd_sale', 'rd_mve', 'agr', 'cashpr', 'gma', 'cash',
                    'pctacc', 'absacc', 'grcapex', 'tang', 'age', 'divi',
                    'divo', 'pchcapx', 'chato', 'chpm']

    # Compress each file separately
    for file in file_endings:
        logger.info('Compressing file {}...'.format(file))
        file_name = settings.TMP_PATH.joinpath(''.join([file, '.csv']))
        file_name_out = settings.TMP_PATH.joinpath(''.join([file, '.csv.gz']))
        gzip_file(file_name,file_name_out)

    logger.info('Compression completed...')


def main():

    logging.config.dictConfig(logger_settings.log_set_ups)
    logging.root.setLevel('DEBUG')
    compress_tmp_files()




if __name__ == '__main__':
    main()
