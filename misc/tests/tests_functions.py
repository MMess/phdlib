__author__ = 'mmess'
"""SPECIFY FILE HERE
"""
import logging
import time
import unittest
logger = logging.getLogger(__name__)
from phdlib.misc import functions


class Test_loop_tracking(unittest.TestCase):

    def test_base(self):
        iterations = 10
        pausing = 3
        t0 = time.time()
        for current in range(iterations):
            time.sleep(pausing)
            print(functions.loop_tracker(current, iterations, t0,
                                         name='wow', ram_usage=True))


def main():
    pass


if __name__ == '__main__':
    main()
