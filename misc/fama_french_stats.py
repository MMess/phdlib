
from pathlib import Path
import matplotlib.pyplot as plt
from phdlib.data.fama_french import Portfolio, Factor
from phdlib.finance.return_calc import ReturnIndex
from phdlib.finance.asset_analysis import ReturnAnalysis
from phdlib.output.return_plots import ReturnPlot, store_figures

def main():
    simple_returns = Portfolio(update=False).get(variable='MOM12',
                                                 frequency='monthly',
                                                 cutoff='mom_style',
                                                 start='19630731')

    mkt_factor = Factor(update=True).get(factors=['Mkt-RF', 'RF'],
                                          frequency='monthly',
                                          start='19630731',
                                          end='20180331')
    print(mkt_factor.columns)

    simple_returns['L-S'] = simple_returns['Hi PRIOR'] - simple_returns[
        'Lo PRIOR']
    simple_returns = simple_returns.join(mkt_factor, how='left')
    simple_returns.rename(columns={'Hi PRIOR':'TopDecile-RF'}, inplace=True)
    simple_returns['TopDecile-RF']-=  simple_returns['RF']
    RI = ReturnIndex(simple_returns[['TopDecile-RF', 'L-S', 'Mkt-RF']],
                     base=1)

    figure_mom_log =  plt.figure()
    ax_mom_log = plt.axes()

    print(ReturnAnalysis(simple_returns['TopDecile-RF']).factor_regression(
        simple_returns[['Mkt-RF', 'SMB', 'HML']]).summary())

    ReturnPlot(RI).log(ax_mom_log)
    figure_mom = plt.figure()
    ax_mom = plt.axes()
    ReturnPlot(RI).base(ax=ax_mom)


    path_plots = Path("C:\\Users\\T430MM\\Dropbox\\PhD\\Seminars\\" 
                "PublicDefense_20180416\\Plots")

    store_figures(figure_mom_log, plot_name='chart_mom_log', plot_path=path_plots,
                      formats=['eps', 'png'], factor=(1.6,1.2))
    store_figures(figure_mom, plot_name='chart_mom',
                  plot_path=path_plots, formats=['eps', 'png'],
                  factor=(1.6,1.2))

    return simple_returns, RI

if __name__ == '__main__':
    df, RI = main()