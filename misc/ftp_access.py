import datetime
import json
import logging
import logging.config
import os
import pathlib
from ftplib import FTP

from phdlib import settings
from phdlib.misc import logger_settings

logger = logging.getLogger(__name__)


def connect_ftp(host_name='research.gsb.columbia.edu'):
    # Conncets to an FTP server
    # TODO replace with SFTP library since connection failed
    ftp = FTP(host_name)
    credentials = json.load(open(str(settings.FTP_CRD), 'r'))
    ftp.login(user=credentials['user'], passwd=credentials['pwd'])

    return ftp


def upload_dir(local_dir=None, target_dir=None, ftp=None, folders=None,
               file_format=None):
    """
    :param local_dir:
    :param target_dir:
    :param ftp:
    :return:
    """
    ftp.mkd(target_dir)
    ftp.cwd('/' + target_dir)

    # loop through local directory
    for root, dirs, files in os.walk(str(local_dir), topdown=True):

        relative = root[len(str(local_dir)):].lstrip(os.sep)

        # check if only a subset of folders is to be copied
        if folders:
            for d in dirs:
                if d in folders:
                    logger.info('Create new dir: {}'.format(d))
                    ftp.mkd(str(pathlib.PurePosixPath(relative, d)))
        else:
            logger.info('No dir restriction found, copy all dir')
            for d in dirs:
                ftp.mkd(str(pathlib.PurePosixPath(relative, d)))

        if file_format:
            for f in files:
                if f[-len(file_format):] in file_format:
                    tmp_dir = str(pathlib.PurePosixPath(relative)).replace(
                        '\\', '/')
                    logger.info('dir {}, and file {}'.format(tmp_dir, f))
                    ftp.cwd(tmp_dir)
                    ftp.storbinary('STOR ' + f,
                                   open(os.path.join(str(local_dir),
                                                     relative, f), 'rb'))
                    ftp.cwd('/' + target_dir)
        else:
            for f in files:
                tmp_dir = str(pathlib.PurePosixPath(relative)).replace('\\',
                                                                       '/')
                logger.info('dir {}, and file {}'.format(tmp_dir, f))
                ftp.cwd(tmp_dir)
                ftp.storbinary('STOR ' + f, open(os.path.join(str(local_dir),
                                                              relative, f),
                                                 'rb'))
                ftp.cwd('/' + target_dir)


def upload_file(local_f_path, server_f_path, ftp):
    """The function updates file on the server
    :param local_f_path: the local path of the file
    :param server_f_path: target directory path (assumed to be a linux path)
    :param ftp:
    :return:
    """

    logger.info('Write file {} to {}'.format(local_f_path, server_f_path))

    ftp.cwd(str(server_f_path.as_posix()))
    ftp.storbinary('STOR ' + local_f_path.name, open(str(local_f_path), 'rb'))


def get_server_files(ftp, local_dir=settings.RESULTS_PATH,
                     server_dir=settings.UX_DATA_PATH.joinpath('Results'),
                     suffix=settings.SUFFIX_RES, prefix=None):
    """
    :param ftp: ftp instance from ftplib,
    :param local_dir: a pathlib path of the local target dir (files to copy to)
    :param server_dir: a pathlib path of the server dir (files to copy from)
    :param suffix: str, conditioning on specify file suffixes
    :param prefix: str, conditioning on specific file prefixes
    :return:
    """

    ftp.cwd(str(server_dir.as_posix()))

    dir_list = ftp.nlst()
    count=0

    for file in dir_list:
        # check for suffix and prefix conditions
        if suffix:
            if file[-len(suffix):] != suffix:
                continue
        if prefix:
            if file[:len(prefix)] != prefix:
                continue

        # set local file path
        local_file_path = local_dir.joinpath(file)

        # check if file was already copied
        if local_file_path.exists():
            logger.warning(
                '{} already exists in local dir, not copied'.format(file))
            continue

        logger.info('Write {} to local results'.format(file))
        count += 1
        # open local file
        local_file = open(str(local_file_path), 'wb')
        # copy ftp server file to local file
        ftp.retrbinary('RETR '+ file, local_file.write)

        local_file.close()

    return count


def update_code():
    """The function takes the repository directory and copies it to the ftp
    account
    """
    folders_to_upload = ['archive', 'data', 'finance', 'metrics', 'misc',
                         'output', 'tmp']
    now = datetime.datetime.now().strftime('%Y-%m-%d_%H%M')

    # set target directories
    local_code_dir = settings.XX1_CODE_PATH
    kolmi_code_dir = settings.UX_CODE_PATH # '/mydata/Code'

    # ftp connection
    ftp = connect_ftp()
    ftp.cwd(str(kolmi_code_dir.as_posix()))

    # archive old code
    logger.info('archive old repository...')
    ftp.rename('phdlib', '_'.join([now, 'phdlib']))

    # update code library
    upload_dir(local_code_dir,
               str(kolmi_code_dir.joinpath('phdlib').as_posix()),
               ftp, folders_to_upload, '.py')

    ftp.quit()


def get_result_data():
    """The function gets all the
    :return:
    """
    ftp = connect_ftp()
    number_files_copied = get_server_files(ftp)

    logger.info('Copying results from server to local complete,'
                ' a total of {} were copied'.format(number_files_copied))

    ftp.quit()


def main():

    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')

    # ftp  = FTP('kolmi.unisg.ch')
    # credentials =  json.load(open('D:\Database\kolmi_1', 'r'))
    # ftp.login(user=credentials['user'], passwd=credentials['pwd'])
    #
    # ftp.quit()
    get_server_files(connect_ftp())
    # update_code()


if __name__ == '__main__':
    main()
