__author__ = 'mmess'
"""SPECIFY FILE HERE
"""
import datetime
import copy

from phdlib.settings import LOG_PATH

today = datetime.datetime.now().strftime('%Y-%m-%d')
now = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

log_set_ups = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'detailed': {
            'class': 'logging.Formatter',
            'format': '%(asctime)s %(name)-15s %(levelname)-8s %(processName)-10s %(message)s'
        },
        'simple': {
            'class': 'logging.Formatter',
            'format': '%(name)-15s %(levelname)-8s %(processName)-10s %(message)s'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'INFO',
            'formatter': 'simple',
        },
        'console_debug': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'simple',
        },
        # 'monthly_firm_data_file': {
        #     'class': 'logging.FileHandler',
        #     'level': 'INFO',
        #     'filename':  str(LOG_PATH.joinpath(now + \
        #                                        '_firm_characteristics.log')),
        #     'mode': 'a',
        #     'formatter': 'detailed',
        # },
        # 'accounting_file': {
        #     'class': 'logging.FileHandler',
        #     'level': 'INFO',
        #     'filename':  str(LOG_PATH.joinpath(now + '_accounting_data.log')),
        #     'mode': 'a',
        #     'formatter': 'detailed',
        # },
        'tmp_file': {
            'class': 'logging.FileHandler',
            'filename': str(LOG_PATH.joinpath('tmp_msgs.log')),
            'mode': 'w',
            'level': 'DEBUG',
            'formatter': 'detailed',
        },
        'tmp_errors': {
            'class': 'logging.FileHandler',
            'filename': str(LOG_PATH.joinpath('tmp_error.log')),
            'mode': 'a',
            'level': 'ERROR',
            'formatter': 'detailed',
        },
    },
    'loggers': {
        # 'monthly_firm': {
        #     'handlers': ['monthly_firm_data_file']
        # },
        # 'yearly_acc': {
        #     'handlers': ['accounting_file']
        # 'run_log': {
        #     'handlers': ['tmp_file_run']
        # }
    },
    'root': {
        'level': 'INFO',
        'handlers': ['console_debug', 'tmp_file', 'tmp_errors']
    },
}

log_set_ups_runs = copy.deepcopy(log_set_ups)
log_set_ups_runs['handlers']['tmp_file_run'] = {
    'class': 'logging.FileHandler',
    'filename': str(LOG_PATH.joinpath(now + '_tmp.log')),
    'mode': 'w',
    'level': 'INFO',
    'formatter': 'detailed'}
log_set_ups_runs['loggers']['run_log']= {'handlers': ['tmp_file_run']}


def main():
    pass


if __name__ == '__main__':
    main()
