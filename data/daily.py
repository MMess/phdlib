"""  This file creates creates monthly statistics from the daily CRSP data

"""
import logging
import math

import numpy as np
import pandas as pd

from phdlib import settings
from phdlib.data import manager
from phdlib.misc import logger_settings, files

logger = logging.getLogger(__name__)


def write_permno_daily(path, db_con):
    """ The function writes the unique permno of daily file to the DB
    :param path:  path to data
    :param db_con: database connection engine
    :return: nothing (wr
    """
    counter = 0
    # path += 'CRSP_SHRCD_10_11\\'
    file_pattern = 'DailyCRSP_*'
    raw_dir = files.Directory(path)
    raw_files = raw_dir.find_file(file_pattern)
    data = pd.DataFrame()
    for file in raw_files:
        data_tmp = pd.read_csv(path + file, engine='c', compression='gzip',
                               low_memory=False,usecols=['PERMNO'])
        data_tmp = pd.Series(data_tmp['PERMNO'].unique())
        frame = [data, data_tmp]

        data = pd.concat(frame)
        del data_tmp

        counter += 1
        print('File Number: {}; Num elements: {}'.format(counter, len(data)))

    data = pd.Series(data[0].unique())
    data.name = 'PERMNO'
    print(str(len(data)))
    data.to_sql('permno_daily', db_con, if_exists='append',
                index=False, chunksize=100)


def get_daily_data_file_names():

    file_pattern = 'DailyCRSP_*'
    raw_dir = files.Directory(str(settings.RAW_DATA_PATH))
    return raw_dir.find_file(file_pattern)


def get_crsp_daily(data_list = None):
    """ Calculates characteristics based on daily crsp data

    Parameters
    ----------
    data_list: list
        A list of str which characteristics should be calculated

    Returns
    -------

    """

    # get daily return file names
    raw_files = get_daily_data_file_names()

    data_dict= dict()
    for fc in data_list:
        data_dict[fc] = list()

    # as memory is a constraint, need of looping over daily file is there
    for file in raw_files:
        file_path = settings.RAW_DATA_PATH.joinpath(file)
        logger.info('Reading file {} ...'.format(file_path))
        data_tmp = pd.read_csv(str(file_path), engine='c', compression='gzip',
                               low_memory=False, parse_dates=['date'],
                               usecols=['date', 'RET', 'PERMNO'], )

        data_tmp = data_tmp.pivot(index='date', columns='PERMNO',
                                  values='RET')
        data_tmp[data_tmp.isin(['B', 'C'])] = np.nan
        data_tmp = data_tmp.astype(float)

        if 'maxret' in data_list:
            logger.info('Calculating monthly max of returns...')
            data_dict['maxret'].append(
                data_tmp.resample('BM', closed='right').max())

        if 'retvol' in data_list:
            logger.info('Calculating monthly std of returns...')
            data_dict['retvol'].append(
                data_tmp.resample('BM', closed='right').std())

    # Concatenating of all monthly statistics
    for fc in data_list:
        data_dict[fc] = pd.concat(data_dict[fc])
        file_name = '.'.join([fc, 'csv'])
        data_dict[fc].to_csv(str(settings.TMP_PATH.joinpath(file_name)))


def daily_to_weekly_returns(path, db_con):
    """ Uses crsp daily return data and resamples to weekly excess return

    Parameters
    ----------
    path: str
        path of daily data
    db_con: sqlalchemy.engine
        a valid sql alchemy engine
    Returns
    -------

    """
    raw_files = get_daily_data_file_names()
    permno_list = pd.read_sql('permno_daily', db_con).PERMNO.tolist()
    cut = lambda lst, sz: [lst[n:n+sz] for n in range(0, len(lst), sz)]
    sub_set_list = cut(permno_list,math.ceil(len(permno_list)/8))
    f = lambda x: x.prod()

    risk_free_rate = pd.read_sql('ff_weekly', db_con, columns=['index','RF'],
                                 index_col='index')

    for idx, sub_set in enumerate(sub_set_list):
        logger.info('Running subset: {}/{}...'.format(idx, len(sub_set_list)))
        data = []

        for file in raw_files:
            file_path = settings.RAW_DATA_PATH.joinpath(file)
            logger.info('Reading file: {}'.format(file))
            data_tmp = pd.read_csv(str(file_path), engine='c', low_memory=False,
                                   compression='gzip', parse_dates=['date'],
                                   usecols=['date', 'RET', 'PERMNO'])
            data_tmp = data_tmp.loc[data_tmp['PERMNO'].isin(sub_set),]
            data_tmp = data_tmp.pivot(index='date', columns='PERMNO',
                                      values='RET')
            data_tmp[data_tmp.isin(['B', 'C'])] = np.nan
            data_tmp = data_tmp.astype(float)
            data.append(data_tmp)
            del data_tmp

        logger.info('Concatenating data...')
        data = pd.concat(data) + 1
        logger.info('Done concatenating')

        # re-sample returns to weekly data
        logger.info('Calculating weekly returns...')
        data = data.resample('W-FRI', closed='right', how=f) - 1

        logger.info('Aligning risk free rate...')
        data, tmp_risk_free = data.align(risk_free_rate, axis=0, join='left')

        logger.debug('tmp_risk_free: {}'.format(tmp_risk_free.head(10)))
        logger.debug('data: {}'.format(data.head(10)))
        # calculate weekly excess returns
        data = data.sub(tmp_risk_free['RF'], axis=0)

        # write weekly data to temporary files
        logger.info('Writing weekly excess returns written to csv...')
        weekly_file_name = ''.join(['WeeklyCRSP_', str(idx), '.csv'])
        data.to_csv(str(settings.RAW_DATA_PATH.joinpath(weekly_file_name)))


def add_daily_stats(path):
    """This function serves as substitute to add to monthly firm char files
    :param path:
    :return:
    """
    logger.info('Add max and std to monthly firm data')
    max_monthly = pd.read_csv(''.join([path, 'tmp_monthly_daily_max.csv']))
    std_monthly = pd.read_csv(''.join([path, 'tmp_monthly_daily_std.csv']))

    add_data= {'maxret':max_monthly, 'retvol':std_monthly }
    manager.add_monthly_firm_char(add_data)


def get_ff_daily_data(con):
    """ The function writes the FF data the database
    :param con:
    :return:
    """
    FF = web.DataReader("F-F_Research_Data_Factors_daily_TXT",
                        "famafrench")[0]
    FF.index = pd.to_datetime(FF.index, format='%Y%m%d')
    FF.rename(columns={"1 b'Mkt-RF'": 'Market', "2 b'SMB'": 'SMB',
                       "3 b'HML'": 'HML', "4 b'RF'": 'RF'},inplace=True)
    FF = FF / 100
    FF.to_sql('ff_daily', con, if_exists='append', chunksize=100)


def ff_to_weekly_data(db_con):
    """ This function creates weekly (Mo-Fr) FF returns based on daily
    :param db_con:
    :return:
    """
    ff_daily = pd.read_sql('ff_daily', db_con, index_col='index')
    f = lambda x: x.prod()

    # add risk free to get market return
    ff_daily['Market'] = ff_daily['Market'] + ff_daily['RF']
    # add 1 to obtain gross return, needed for product
    ff_daily = ff_daily + 1
    ff_weekly = ff_daily.resample('W-FRI', closed='right', how=f) - 1
    # get excess market return
    ff_weekly['Market'] = ff_weekly['Market'] - ff_weekly['RF']
    ff_weekly.to_sql('ff_weekly', db_con, if_exists='append', chunksize=100)


def main():
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')

    path = str(settings.RAW_DATA_PATH)
    # db_connection = create_engine(settings.SQL_URL)

    # write_permno_daily(path, db_connection)

    # get_ff_daily_data(db_connection)
    # ff_to_weekly_data(db_connection)

    # run daily data -> daily data to weekly data
    # daily_to_weekly_returns(path, db_connection)
    get_crsp_daily(['maxret', 'retvol'])
    # add_daily_stats(path)


if __name__ == '__main__':
    main()
