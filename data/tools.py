""" Collection of basic functions to prepare data

"""
import pandas as pd
import numpy as np
import logging

logger = logging.getLogger(__name__)


def standardize_panel(df, date_col='date', keep_date_col=False):
    """ Standardizes panel data as of each date
    
    Parameters
    ----------
    df: pd.DataFrame
        with column as date_col
    date_col: str
        name of the date column
    keep_date_col: bool
        if date column should be kept

    Returns
    -------
    pd.DataFrame
        with point in time normalized (mean=0, std=1) values
    """
    df = df.copy()
    grouped_df = df.groupby(date_col)
    std_df = []
    for group in grouped_df:

        # logger.info('group[1]: {}'.format(group[1]))
        tmp_data = z_score(group[1].drop(date_col, axis=1), axis=1)
        if keep_date_col:
            tmp_data[date_col] = group[0]
        std_df.append(tmp_data)

    return pd.concat(std_df)


def _weighted_cov(x,y, weights):
    """ Weighted coveriance calculation

    Parameters
    ----------
    x: pd.Series
    y: pd.Series
    weights: pd.Series

    Returns
    -------
    weighted_cov: float

    """

    w_mu_x = np.average(x,weights=weights)
    w_mu_y = np.average(y, weights=weights)
    return np.sum(weights*(x-w_mu_x)*(y-w_mu_y))/np.sum(weights)

def weighted_corr_mat(X, weights):
    """

    Parameters
    ----------
    X: pd.DataFrame
        with dimensions NxP
    weights: pd.Series
        with length (N) weight of each observation

    Returns
    -------
    corr_mat:
        value-weighted correlation
    """


    X = X.copy()
    weights = weights.copy()

    X = X.reset_index(drop=True)
    weights = weights.reset_index(drop=True)
    w = weights.values

    corr_mat = pd.DataFrame(index=X.columns, columns=X.columns,
                            data=np.diag(np.ones(len(X.columns))))
    columns_completed = list()
    for col_1 in X:
        columns_completed.append(col_1)
        for col_2 in X:
            if col_2 in columns_completed:
                continue
            x = X[col_1].values
            y = X[col_2].values
            weighted_cov = _weighted_cov(x=x, y=y, weights=w)
            weighted_corr = weighted_cov / np.sqrt(_weighted_cov(x, x, w) *
                                                   _weighted_cov(y, y, w))

            corr_mat.loc[col_1, col_2] = weighted_corr
            corr_mat.loc[col_2, col_1] = weighted_corr


    return corr_mat


def z_score(df, axis=1):
    """Standardize the mean and variance of the data axis

    Parameters
    ----------
    df : pandas.DataFrame
        Data to normalize
    axis : int (default=1)
        Which axis to normalize across. If 1, normalize across rows, if 0,
        normalize across columns.

    Returns
    -------
    pandas.DataFrame
        Normalized data with a mean of 0 and variance of 1 across the
        specified axis.
    """

    copy_df = df.copy()

    if axis == 0:
        z_scored = copy_df
    else:
        z_scored = copy_df.T

    z_scored = (z_scored - z_scored.mean()) / z_scored.std()

    if axis == 0:
        return z_scored
    else:
        return z_scored.T


def winsorize(df, percentile=0.05, axis=0, exclude='binary'):
    """ Winsorizes a data to desired level

    PARAMETERS
    ----------
    win_df: pandas.DataFrame,
        data to be winsorized
    percentile: float [0:1],
        which cut-off should be used
    axis: int,
        column by column (axis=0), by row (axis=1)

    Returns
    -------
    pandas.DataFrame
        winsorized data

    """
    # TODO testing of copy behaviour
    win_df = df.copy()

    #  winsorize along the rows if axis=1
    if axis == 1:
        win_df = win_df.tranpose()

    columns = list()
    for column in win_df:
        # only use non na values for data assignment
        series = win_df[column]
        values = series.dropna()
        if len(values) < 10:
            columns.append(series)
            logger.warning('Column {} has no values'.format(column))
            continue
        if len(values.unique()) < 5:
            columns.append(series)
            logger.warning('{} has only {} unique values'.format(column, len(
                values.unique())))
            continue
        # replace values which belong to the lower percentile (no NA overwrite)
        win_series = series.where(
                (series.rank(pct=True) > percentile) | (series.isnull()),
                np.percentile(values, q=percentile * 100))
        # replace values which belong to the upper percentile (no NA overwrite)
        win_series = win_series.where(
                (win_series.rank(pct=True) < 1 - percentile) | (
                    series.isnull()),
                np.percentile(values, q=100 - percentile * 100))

        # collect in row
        columns.append(win_series)

    win_df = pd.concat(columns, axis=1)

    # transpose matrix back to order it correctly
    if axis == 1:
        win_df = win_df.transpose()

    return win_df

def r2_trans(r_sqrt, scaling=1 / 12):
    """ Transform R^2 into different frequency

    Parameters
    ----------
    r_sqrt: float
        given r-squared
    scaling: float
        scaling factor by which we want to transform

    Returns
    -------
    float:
        scaled R^2
    """
    SNR = r_sqrt / (1 - r_sqrt)

    return scaling * SNR / (1 + SNR * scaling)


def main():
    pass

if __name__ == '__main__':
    main()

