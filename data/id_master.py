""" The file writes all ID information to a SQL server

The three main source files are:
    1. Pure CRSP
    2. Pure Compustat (CS)
    3. CRSP/Compustat merged link table (CCM)

Not needed for data loading, just to easily inspect data via SQL
"""

import numpy as np
import pandas as pd
from sqlalchemy import create_engine

from phdlib import settings


def hist_cusip_crsp(con=None):
    # matching cusip->ncusip
    file = settings.RAW_DATA_PATH.joinpath('CRSP_IdentifierInfo_1962-2014.gz')
    cusip = pd.read_csv(str(file),
                        engine='c', compression='gzip',
                        low_memory=False,
                        usecols=['date', 'CUSIP', 'NCUSIP'])

    cusip['date'] = pd.to_datetime(cusip['date'], format='%Y%m%d')
    cusip['NCUSIP'].fillna(cusip['CUSIP'], inplace=True)
    cusip.drop_duplicates(subset=['CUSIP', 'NCUSIP'], take_last=True,
                          inplace=True)
    cusip.to_sql('hist_cusip', con, if_exists='append',
                 index=False, chunksize=100)


def read_write_crsp(path, con=None):
    # crsp id
    crsp = pd.read_csv(path + 'CRSP_IdentifierInfo_1962-2014.gz',
                       engine='c', compression='gzip', low_memory=False)
    crsp['date'] = pd.to_datetime(crsp['date'], format='%Y%m%d')
    crsp.drop_duplicates(subset=['PERMNO'], take_last=True,
                         inplace=True)
    crsp.to_sql('crsp_id', con, if_exists='append', index=False,
                chunksize=100)


def hist_shrcd_crsp(path, con=None):
    # crsp id
    crsp = pd.read_csv(path + 'CRSP_IdentifierInfo_1962-2014.gz',
                       engine='c', compression='gzip', low_memory=False,
                       usecols=['date', 'PERMNO', 'SHRCD'])
    crsp['date'] = pd.to_datetime(crsp['date'], format='%Y%m%d')
    crsp.drop_duplicates(subset=['PERMNO', 'SHRCD'], take_last=True,
                         inplace=True)
    crsp.to_sql('hist_shrcd', con, if_exists='append', index=False,
                chunksize=100)


def hist_siccd_crsp(path, con=None):
    # crsp id
    crsp = pd.read_csv(path + 'CRSP_IdentifierInfo_1962-2014.gz',
                       engine='c', compression='gzip', low_memory=False,
                       usecols=['date', 'PERMNO', 'SICCD'])
    crsp['date'] = pd.to_datetime(crsp['date'], format='%Y%m%d')
    crsp.drop_duplicates(subset=['PERMNO', 'SICCD'], take_last=True,
                         inplace=True)
    crsp.to_sql('hist_siccd', con, if_exists='append', index=False,
                chunksize=100)


def read_write_cs(path, con):
    compustat = pd.read_csv(
        path + 'Compustat_IdentifierInfo_1950_2015.gz',
        engine='c', compression='gzip', low_memory=False)
    compustat['datadate'] = pd.to_datetime(compustat['datadate'],
                                           format='%Y%m%d')
    compustat.drop_duplicates(subset=['gvkey', 'fyr'], take_last=True,
                              inplace=True)
    compustat['cusip'] = compustat['cusip'].map(
        lambda x: str(x)[:-1])
    compustat.rename(columns={'conm': 'Name', 'cusip': 'CUSIP',
                              'datadate': 'Date', 'fyr': 'FYR'},
                     inplace=True)
    compustat.to_sql('compustat_id', con, if_exists='append',
                     index=False, chunksize=100)


def read_write_ccm(path, con):
    file_name = 'CRSP_CS_LinkTable.gz'
    data_crsp = pd.read_csv(path + file_name, engine='c',
                            compression='gzip', low_memory=False)
    # format date
    data_crsp['LINKDT'] = pd.to_datetime(data_crsp['LINKDT'],
                                         format='%Y%m%d')
    # end default for existing data null
    data_crsp.loc[data_crsp['LINKENDDT'] == 'E', ['LINKENDDT']] = np.nan
    data_crsp['LINKENDDT'] = pd.to_datetime(data_crsp['LINKENDDT'],
                                            format='%Y%m%d')
    data_crsp.to_sql('ccm_link_table', con, if_exists='append',
                     index=False, chunksize=100)


def crsp_head_info(path,con):
    """The function adds puts a detailed file
    :param path:
    :param con:
    :return:
    """
    file = settings.ID_DATA_PATH.joinpath('CRSP_headerInfo_all.gz')
    header_crsp = pd.read_csv(str(file), engine='c',
                            compression='gzip', low_memory=False)

    header_crsp.to_sql('crsp_header_info', con, if_exists='append',
                       index=False, chunksize=100)


def main():

    db_connection = create_engine(settings.SQL_URL)

    # read_write_ccm(path, db_connection)
    # read_write_crsp(path, db_connection)
    # read_write_cs(path, db_connection)
    # hist_siccd_crsp(path, db_connection)
    # hist_shrcd_crsp(path, db_connection)
    # hist_cusip_crsp(path, db_connection)
    crsp_head_info(str(settings.ID_DATA_PATH), db_connection)


if __name__ == '__main__':
    main()
