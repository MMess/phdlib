""" Cross section simularion file

"""
import datetime
import json
import logging
import logging.config
import time

import numpy as np
import numpy.random as npr
import pandas as pd

from phdlib import settings
from phdlib.data import tools
from phdlib.data.results import LocalResults
from phdlib.metrics import vola_models
from phdlib.metrics.api import panel_ols_regression, ada_lasso_estimation,\
    fama_macbeth
from phdlib.metrics import simulation_tools
from phdlib.misc import ftp_access, functions
from phdlib.misc.api import log_set_ups_runs

logger = logging.getLogger(__name__)


def simulation_parameters(set_up):
    """The function provides all simulation inputs of the specified set-up
    :param set_up:
    :return:
    """
    logger.info('Creating simulation inputs...')
    base_group_corr = 0.5
    npr.seed(  set_up['NumpySeed'])  # ensures reconstruction of simulation inputs
    sim_input = dict()
    # Number of periods
    sim_input['T'] = set_up['FreqAdjustment'] * set_up['Years']
    # Artificial date range (due to real GARCH estimation)

    if sim_input['T'] > 600:
        tmp_dict = {'start': '1699-12-31'}
    else:
        tmp_dict = {'end': '2014-12-31'}

    sim_input['DateRange'] = pd.date_range(periods=sim_input['T'],
                                           freq=set_up['Freq'],
                                           **tmp_dict)
    # idio vola for of each stock - tuning parameter for signal-to-noise ratio

    # sim_input['IdioVola'] = 1 / np.sqrt(set_up['FreqAdjustment']) \
    #                         * set_up['YearlyIdioVola']

    # Determine how many of each firm characteristics (FC) - Return/Risk/Uninf
    sim_input['NumRetFC'] = np.int(
        np.floor(set_up['RatioRetFC'] * set_up['NumFC']))
    sim_input['NumRiskFC'] = np.int(
        np.floor(set_up['RatioRiskFC'] * set_up['NumFC']))
    sim_input['NumRiskRet'] = sim_input['NumRiskFC'] + sim_input['NumRetFC']
    sim_input['NumUnInfFC'] = set_up['NumFC'] - sim_input['NumRiskRet']

    # Simulate risk premia (RP) - set market RP to pre-specified size
    low_bound_rp = set_up['BoundsRiskPremia'][0] / set_up['FreqAdjustment']
    up_bound_rp = set_up['BoundsRiskPremia'][1] / set_up['FreqAdjustment']

    if set_up['MarketRiskPremium'] == 'AR1':
        mu_market = 0.055
    else:
        mu_market = set_up['MarketRiskPremium']


    sim_input['RiskPremia'] = np.concatenate(
        (np.array([mu_market / set_up['FreqAdjustment']]),
         npr.uniform(low_bound_rp, up_bound_rp, sim_input['NumRetFC'] - 1),
         np.zeros(sim_input['NumRiskFC'])))

    # Determine idiosyncratic and factor volatility based on desired R^2
    # signal to noise ratio (SNR)
    SNR = set_up['R_squared'] / (1 - set_up['R_squared'])
    logger.debug('SNR: {}'.format(SNR))
    signal_var = (sim_input['RiskPremia'] ** 2).sum()
    if 'MeanStockGarchVola' not in set_up.keys():
        logger.warning('Mean GARCH vola not defined, set to 15%')
        set_up['MeanStockGarchVola'] = 0.15 / 12 ** 0.5

    var = signal_var / SNR - set_up['MeanStockGarchVola'] ** 2
    logger.debug('Long Term GARCH: {} - Rest Vola: {}'.format(
        set_up['MeanStockGarchVola'] ** 2, var))
    sim_input['IdioVola'] = var ** 0.5 / sim_input['NumRiskRet']
    sim_input['VolaVector'] = sim_input['IdioVola'] * np.ones(
        sim_input['NumRiskRet'])

    # Simulate correlation matrix of firm characteristics
    # initialize list to 1 because of the first element being the market factor
    # check if reshuffling is feasible
    group_corr, group_size, swap_pairs = list(), list(), list()
    corr_idx, ret_idx, risk_idx, unif_idx = set_up['NumFC'] - 1, 1, sim_input[
        'NumRetFC'], \
                                            sim_input['NumRiskRet']
    counter = [0, 0, 0]
    positions_group0 = list()
    for group in set_up['CorrGroupsFC']:
        group_corr.append(group[0])
        group_size.append(len(group[1:]))
        # prepare swap elements of groups
        for element in group[1:]:
            if element == 0:  # Return FC
                # positions_group0.append(ret_idx)
                counter[0] += 1
                swap_pairs.append((corr_idx, ret_idx))
                ret_idx += 1
            if element == 1:  # Risk FC
                counter[1] += 1
                swap_pairs.append((corr_idx, risk_idx))
                risk_idx += 1
            if element == 2:  # Uninformative FC
                counter[2] += 1
                swap_pairs.append((corr_idx, unif_idx))
                unif_idx += 1
            corr_idx -= 1

    # group1_swaps = list(zip(positions_group0, (
    #     1 + np.array(positions_group0).argsort().argsort())))
    # swap_pairs += group1_swaps

    # check if enough return observations are actually available to match corr
    num_fc = [sim_input['NumRetFC'], sim_input['NumRiskFC'],
              sim_input['NumUnInfFC']]
    for idx, count in enumerate(counter):
        if count > num_fc[idx]:
            raise ValueError(
                'Not enough rets for correlation in group {}'.format(idx))

    # add additional groups of length one to ensure correct size of corr FC
    add_groups = np.ones(set_up['NumFC'] - np.array(group_size).sum())
    group_size.reverse()
    group_corr.reverse()
    group_size = add_groups.tolist() + group_size
    group_corr = (add_groups * base_group_corr).tolist() + group_corr

    if set_up['CorrelationTypeFC'] != 'Constant':
        raise NotImplementedError('Teoplitz and dynamic correlation missing')

    # Simulate basic correlation matrix
    corr = simulation_tools.SimConstantCorrMat(num_groups=len(group_size),
                                               group_size=group_size,
                                               base_correlations=group_corr,
                                               delta_noise=set_up[
                                                   'CorrDeltaNoiseFC'],
                                               noise_space_dim=50,
                                               epsilon=set_up['CorrEpsilonFC'])

    # reshuffle the correlation matrix
    sim_input['CorrMatFC'] = simulation_tools.reshuffle_correlation_mat(
        corr.sim_corr_mat, swap_pairs)

    # load degrees of freedom parameter of t-distributed GARCH vola
    if set_up['Vola'] == 'StudentsT':
        sim_input['dof'] = \
            (pd.read_csv(str(settings.GARCH_PARA_ST),
                         index_col='para_name')).loc[
                'nu'].values[0]

    return sim_input


def ar_process(constant, coef, volatility, periods=10,
               burn_in_periods=500):
    """ AR (1) process simulation: mu_t = constant + coef mu_t-1 + epsilon_t

    Parameters
    ----------
    constant: float
    coef: float
    volatility
    periods
    burn_in_periods

    Returns
    -------

    """

    total_periods = burn_in_periods + periods
    innovations = np.random.randn(total_periods) * volatility
    mu_ = constant / (1 - coef)

    for burn_t in range(burn_in_periods):
        mu_ = constant + coef * mu_ + innovations[burn_t]

    mu_over_time = np.zeros([periods])
    mu_over_time[0] = mu_
    for t in range(1, periods):
        mu_over_time[t] = constant + coef * mu_over_time[t - 1] + innovations[
            burn_in_periods + t]

    return mu_over_time


def simulation(set_up, garch_vola=1, which=None):
    """   The function performs a cross-sectional stock market simulation

    
    Parameters
    ----------
    set_up: dict
        including parameters as in settings file
    garch_vola: pd.Series
        with stock market volatilites as values and date as index

    Returns
    -------
    dict: 
        with keys 'AdaptiveLasso', 'Lasso', 'OLS_DK_coef', 'OLS_DK_tValues'
        and values  pd.DataFrames with coefficient or t-stat estimates
    """

    # get all simulation inputs
    set_up['MeanStockGarchVola'] = garch_vola.mean()
    sim_in = simulation_parameters(set_up)

    logger.info('Creation completed...\n...simulation run with {} ret FC, '
                '{} risk FC and {} uninformative FC \nwith {} stocks and '
                '{} years. \n{} distributed errors in volatility. \n'
                '{} FC function'
                .format(sim_in['NumRetFC'], sim_in['NumRiskFC'],
                        sim_in['NumUnInfFC'],
                        set_up['NumStocks'], set_up['Years'], set_up['Vola'],
                        set_up['FCFunction']))

    corr_mat_factors = np.diag(np.ones(sim_in['NumRiskRet']))

    est_results = dict()
    for method in which:
        est_results[method]= list()

    t0 = time.time()
    # loop for each simulation
    num_sim = set_up['NumSimulation']
    for s in range(num_sim):
        firm_char_list = list()

        logger.info('Running simulation {0} of {1} ({2:.3}%....)'.format(
            s + 1, num_sim, (s + 1) / num_sim* 100))

        # time varying mu - AR 1
        if set_up['MarketRiskPremium'] == 'AR1':
            mu = 0.055 / set_up['FreqAdjustment']
            coef = 0.2
            constant = mu * (1 - coef)
            vola_psi = ((1 / 5)*set_up['MeanStockGarchVola'] ** 2  * (
                1 - coef ** 2))**0.5

            mu_t = ar_process(constant=constant, coef=coef,
                              volatility=vola_psi,
                              periods=len(sim_in['DateRange']),
                              burn_in_periods=500)
            mu_t = pd.Series(data=mu_t, index=sim_in['DateRange'])

        for t in sim_in['DateRange']:
            # factor simulation------------------------------------------------
            sim_in['VolaVector'][0] = garch_vola[t.strftime(format='%Y-%m-%d')]
            if set_up['MarketRiskPremium'] == 'AR1':
                sim_in['VolaVector'][0] *= (4 / 5) ** 0.5
            vola_matrix = np.asmatrix(np.diag(sim_in['VolaVector']))

            # factor cov matrix = diagonal(vola)*corr_matrix*diagonal(vola)
            factor_risk_cov = vola_matrix * np.asmatrix(
                corr_mat_factors) * vola_matrix

            # draw normals of factor returns
            if set_up['MarketRiskPremium'] == 'AR1':
                sim_in['RiskPremia'][0] = mu_t[t]
            factor_innovations = npr.multivariate_normal(sim_in['RiskPremia'],
                                                         factor_risk_cov)

            # replace first innovation by t-distributed error (as zero corr)
            if set_up['Vola'] == 'StudentsT':
                factor_innovations[0] = sim_in['VolaVector'][0] * \
                                        npr.standard_t(sim_in['dof'])

            # idiosyncratic simulation-----------------------------------------
            idio_innovations = sim_in['IdioVola'] * npr.standard_normal(
                set_up['NumStocks'])

            # firm characteristics simulation ---------------------------------
            if set_up['CorrFC'] :
                corr_fc = sim_in['CorrMatFC']
            else:
                corr_fc =  np.diag(np.ones(set_up['NumFC']))
            firm_char = npr.multivariate_normal(np.zeros(set_up['NumFC']),
                                                corr_fc, set_up['NumStocks'])
            # (for return generating) take only FC of risk return
            firm_char_m = np.asmatrix(
                firm_char[:, :sim_in['NumRiskRet']]).copy()

            if set_up['FCFunction'] == 'non_linear_1':
                ind_1 = range(2, sim_in['NumRiskRet'], 2)
                firm_char[:, ind_1] = abs(firm_char[:, ind_1])
                firm_char_m[:, ind_1] = np.power(firm_char[:, ind_1], 2)
                ind_2 = range(1, sim_in['NumRiskRet'], 2)
                firm_char[:, ind_2] = abs(firm_char[:, ind_2])
                firm_char_m[:, ind_2] = np.power(firm_char[:, ind_2], 0.5)

            # return creation (ret =  FC*f_t + epsilon------------------------
            if set_up['FCFunction'] != 'Model3':
                returns = firm_char_m * np.asmatrix(factor_innovations).transpose() \
                          + np.asmatrix(idio_innovations).transpose()
            else:
                # Model 3
                corr = 0.9
                mu = np.array([0, 0])
                COV_mat = np.asmatrix([[1,corr],[corr,1]])
                FC_exposure = npr.multivariate_normal(mu, COV_mat,
                                                      set_up['NumStocks'])
                FC_exposure  = np.asmatrix(FC_exposure)
                # stock market factor with zero mean
                factor_innovations[0] =  npr.randn(1)*sim_in['VolaVector'][0]
                firm_char_m[:,0] = FC_exposure[:,0]
                # add the mean component of the FC
                returns = firm_char_m * np.asmatrix(
                    factor_innovations).transpose() \
                           + np.asmatrix(idio_innovations).transpose() +\
                          FC_exposure[:,1]*sim_in['RiskPremia'][0]
                firm_char[:,[0]] = FC_exposure[:,1]

            # collect regression data
            df = pd.DataFrame(firm_char)
            df['ret'] = returns
            df['date'] = t
            firm_char_list.append(df)

        reg_data = pd.concat(firm_char_list)

        y = reg_data[['ret', 'date']].set_index(['date', reg_data.index])

        X_norm = tools.standardize_panel(reg_data.drop('ret', axis=1),
                                         keep_date_col=True)
        X_norm = X_norm.set_index(['date', X_norm.index])
        # CV5
        if 'AdaLasso_CV5' in which:
            logger.info('...CV5 adaptive lasso and lasso regressions')
            res_ada, res = ada_lasso_estimation(y=y['ret'], X=X_norm,
                                                normalize=False,
                                                criterion='cv', k_fold=5)
            est_results['AdaLasso_CV5'].append(res_ada.transpose())
            est_results['Lasso_CV5'].append(res.transpose())
        # BIC
        if 'AdaLasso_BIC' in which:
            logger.info('...BIC adaptive lasso and lasso regressions')
            X_norm.reset_index(level='date', inplace=True, drop=True)
            res_ada, res = ada_lasso_estimation(y=y['ret'], X=X_norm,
                                                normalize=False,
                                                criterion='bic')
            est_results['AdaLasso_BIC'].append(res_ada.transpose())
            est_results['Lasso_BIC'].append(res.transpose())
        # AIC
        if 'AdaLasso_AIC' in which:
            logger.info('...AIC adaptive lasso and lasso regressions')
            res_ada, res = ada_lasso_estimation(y=y['ret'], X=X_norm,
                                                normalize=False,
                                                criterion='aic')
            est_results['AdaLasso_AIC'].append(res_ada.transpose())
            est_results['Lasso_AIC'].append(res.transpose())

        if 'OLS_DK_coef' in which:
            logger.info('...OLS regression')
            res_ols = panel_ols_regression(y=reg_data['ret'],
                                           X=reg_data.drop('ret', axis=1))
            est_results['OLS_DK_coef'].append(pd.DataFrame(res_ols.params).T)
            est_results['OLS_DK_tValues'].append(pd.DataFrame(res_ols.tvalues).T)

        if 'FM_coef' in which:
            logger.info('...FM regression')
            reg_data['ID'] = np.arange(len(reg_data))
            reg_data.set_index(['date', 'ID'], inplace=True)
            reg_data.index.names = ['date', 'ID']
            res_fm = fama_macbeth(y=reg_data['ret'],
                                  X=reg_data.drop('ret', axis=1))

            est_results['FM_coef'].append(pd.DataFrame(res_fm[0]).T)
            est_results['FM_tValues'].append(pd.DataFrame(res_fm[1]).T)

        # display computational details---------------------------------------
        logger.info(functions.loop_tracker(s, num_sim, t0, ram_usage=True,
                    name='Cross-Section Simulation'))

    # collect results of regressions
    df_estimation_res = {}
    for key, value in est_results.items():
        logger.info('DataType: {}, data {}'.format(type(value[0]), key))
        df_estimation_res[key] = pd.concat(value, ignore_index=True)

    logger.info('Simulation complete, returning results...')
    return df_estimation_res


def update_settings(update_settings_ftp=False):
    """ Saves settings as json to local drive, and pushes the data to server

    Parameters
    ----------
    update_settings_ftp: bool
        if data should ne pushed to server

    Returns
    -------

    """
    set_up = {'NumSimulation': 100,  # default 1000/5000
              'NumStocks': 40,  # default 4000
              'Years': 50,  # default 50
              'Freq': 'M',
              'Vola': 'Normal',  # "LTMean"; (GARCH) -> "Normal" , "StudentsT"
              'R_squared': 0.00415,  # yearly R^2 ca. 5%
              'NumFC': 100, 'RatioRetFC': 0.06, 'RatioRiskFC': 0.06,
              'CrossCorrelation': True, 'TimeSeriesCorrelation': False,
              # "MarketRiskPremium": 0.055
              'MarketRiskPremium': 'AR1', 'BoundsRiskPremia': (0.015, 0.03),
              'FreqAdjustment': 12, 'NumpySeed': 4720,
              'CorrelationTypeFC': 'Constant',
              'CorrDeltaNoiseFC': 0.03, 'CorrEpsilonFC': 0.07,
              'CorrGroupsFC': [(0.9, 0, 2), (0.9, 0, 1), (0.9, 1, 2),
                               (0.4, 0, 2), (0.4, 0, 1), (0.4, 1, 2)],
              # [0.4, 0,1], [0.4, 0, 1],
              'CorrFC': True,
              'FCFunction': 'linear'}

    with open(str(settings.SIMULATION_SETTINGS), 'w') as fp:
        json.dump(set_up, fp)

    # update file on kolmi
    if update_settings_ftp:
        logger.info('Updating settings files on Kolmi')
        ftp = ftp_access.connect_ftp()
        data_dir = settings.SIMULATION_SETTINGS.parts[-2]
        ftp_access.upload_file(settings.SIMULATION_SETTINGS,
                               settings.UX_DATA_PATH.joinpath(data_dir),
                               ftp)
        ftp.quit()

    return set_up


def main():
    # load logger settings
    logging.config.dictConfig(log_set_ups_runs)
    logging.root.addHandler(logging.getLogger('run_log').handlers[0])
    logger.setLevel('INFO')

    # load set-up for simulation
    with open(str(settings.SIMULATION_SETTINGS), 'r') as fp:
        set_up = json.load(fp)

    if 'CorrFC' not in set_up.keys():
        set_up['CorrFC'] = True

    # load GARCH vola saved in csv files
    if set_up['Years'] <= 50:
        garch_vola = pd.read_csv(str(settings.GARCH_PATH), index_col='index',
                                 parse_dates=True)
    else:
        num_obs = set_up['Years'] * 12
        logger.info('Simulate {} GARCH volatilities...'.format(num_obs))
        garch_vola = vola_models.simulate_garch_process(num_obs=num_obs)

    if 'Which' not in set_up.keys():
        which = 'lasso_zoo'  # fm_pooled/lasso_zoo
    else:
        which = set_up['Which']

    method_map = {'lasso_zoo': ['AdaLasso_BIC', 'AdaLasso_AIC', 'AdaLasso_CV5',
                                'Lasso_BIC', 'Lasso_AIC', 'Lasso_CV5',
                                'OLS_DK_coef', 'OLS_DK_tValues'],
                  'fm_pooled': ['OLS_DK_coef', 'OLS_DK_tValues', 'FM_coef',
                                'FM_tValues']}

    # run simulation and save results
    estimation_results = LocalResults()
    results = simulation(set_up, garch_vola[set_up['Vola']],
                         which=method_map[which])
    estimation_results.save(data=results,
                            parameters=set_up,
                            results_name='simulation' + '_'  + which,)


    return results


if __name__ == '__main__':
    # update_settings(update_settings_ftp=False)
    res = main()
