""" Main data loader API to local HDF data storage, loads FC data and rets

"""
import re
import ctypes  # An included library with Python install.
import datetime
import logging
import logging.config

import numpy as np
import pandas as pd
from pandas.tseries.offsets import YearBegin

from phdlib import settings
from phdlib.data import tools, storage
from phdlib.misc import logger_settings, functions

logger = logging.getLogger(__name__)


def load_data_storage():
    """ Defines and initializes generic LargeDFStorage cls for monthly data

    Returns
    -------
    storage:
    """
    groups_map = dict(
        query=dict(columns=['rank_mkt_cap', 'EXCHCD', 'mkt_cap', 'SHRCD',
                            'ceq', 'rank_mkt_cap_lag'], ),
        crsp_raw_1=dict(columns=['SHROUT', 'PRC', 'RET', 'VOL', 'DLRET',
                                 'SICCD'], ),
        fc_crsp_1=dict(columns=['mom1m', 'mom12m', 'mom36m', 'mom6m',
                                'adj_ret', 'dl_ret', 'ret', 'chg_mom6m',
                                'chshrout', 'ipo'], ),
        fc_crsp_2=dict(columns=['dolvol', 'turn', 'sector_sic2', 'sector_sic',
                                'sector_48', 'mve',], ),
        fc_crsp_daily=dict(columns=['beta', 'beta_sq', 'idiovol', 'maxret',
                                    'retvol']),
        fc_cs_1=dict(columns=['ep', 'dy', 'bm', 'lev', 'currat', 'pchcurrat',
                              'quick', 'pchquick', 'salecash', ]),
        fc_cs_2=dict(columns=['salerec', 'saleinv', 'pchsaleinv',
                              'cashdebt', 'depr', 'pchdepr', 'sgr', 'sp',
                              'pchsale_pchinvt', 'pchsale_pchrect', ]),
        fc_cs_3=dict(columns=['pchgm_pchsale', 'pchsale_pchxsga', 'chinv',
                              'pchemp', 'hire', 'cfp', 'acc', 'rd', 'lgr',
                              'egr']),
        fc_cs_4=dict(columns=['rd_sale', 'rd_mve', 'agr', 'cashpr', 'gma',
                              'cash', 'pctacc', 'absacc', 'roic',
                              'grcapex', ]),
        fc_cs_5=dict(columns=['tang', 'invest', 'age', 'divi', 'divo',
                              'pchcapx','chato', 'chpm']),
        fc_cs_ia_1=dict(columns=['mve_ia', 'bm_ia', 'pchemp_ia', 'cfp_ia',
                                 'pchcapx_ia', 'chato_ia', 'chpm_ia'])
    )
    path = settings.DATA_STORE
    # TODO load daily stats to storage

    return storage.LargeDFStorage(path, groups_map=groups_map,
                                  start_end_index='date')


def add_crsp_raw(which=None, update=False, how=None):
    """ Adds CRSP raw data to data storage

    Parameters
    ----------
    which: iterable
        which RAW data names should be added
    update: Bool
        if data is updated (True) or added for the first time (False)
    how: str
        in case you update the data use 'left', 'right, 'outer' or 'inner'
        'right' refers to using the new data's index and 'left' vice versa

    Returns
    -------

    """
    # default selection of base columns needed
    if which is None:
        which = ['SHROUT', 'PRC', 'RET', 'VOL', 'DLRET', 'SICCD',
                 'SHRCD', 'EXCHCD']
    # load raw data
    logger.info('Loading CRSP monthly raw with {}'.format(which))
    raw_df = pd.read_csv(str(settings.CRSP_MONTHLY), engine='c',
                         compression='gzip', low_memory=False,
                         usecols=which + ['date', 'PERMNO'])
    raw_df['date'] = pd.to_datetime(raw_df['date'], format='%Y%m%d')
    raw_df['date'] = pd.Index(raw_df['date']).to_period('m').to_timestamp(
        'm')
    raw_df.set_index(['date', 'PERMNO'], inplace=True)
    raw_df = raw_df.sortlevel()

    dstore = load_data_storage()
    if update:
        logger.info('Updating {} to store...'.format(which))
        dstore.update(raw_df, how=how)
        logger.info('Updating of {} successful'.format(which))
    else:
        logger.info('Adding {} to store...'.format(which))
        dstore.add_columns(raw_df)
        logger.info('Adding of {} successful'.format(which))


def add_csv_fc(which=None, update=False, how=None):
    """ Adds csv files compustat data to data_storage

     it is assumed that a data is available in csv format in folder
     settings.TMPPATH

    Parameters
    ----------
    which: iterable
        with str describing the desired FC
    update: bool
        if update function should be used (old data will be overwritten
    how: how data should be added


    Returns
    -------

    """

    tmp_data = dict()
    for file in which:

        current_file_path = settings.TMP_PATH.joinpath(file + '.csv')
        logger.info('Loading {} to memory ...'.format(str(current_file_path)))
        tmp_df = pd.read_csv(current_file_path, index_col='date',
                             parse_dates=True)
        tmp_df = tmp_df.asfreq('M', method='ffill')
        tmp_df.columns = [int(float(x)) for x in tmp_df.columns]
        tmp_df.columns.name = 'PERMNO'
        tmp_data[file] = tmp_df

    df = pd.Panel(tmp_data).to_frame(filter_observations=False)
    df.dropna(how='all', inplace=True)

    dstore = load_data_storage()
    if update :
        logger.info('Updating {} to store...'.format(which))
        dstore.update(df, how=how)
        logger.info('Updating of {} successful'.format(which))
    else:
        logger.info('Adding {} to store...'.format(which))
        dstore.add_columns(df)
        logger.info('Adding of {} successful'.format(which))

    return df


def add_crsp_fc(which=None, update=False, how=None):
    """ Adds crsp derived data to data-storage

    Parameters
    ----------
    which: iterable
        which data should be added to data store
    update: Bool
        if data is updated (True) or added for the first time (False)

    Returns
    -------

    """

    if which is None:
        which = ['turn', 'sector_sic2', 'sector_sic', 'sector_48', 'mve',
                 'rank_mkt_cap', 'mkt_cap', 'mom1m', 'mom12m', 'mom36m',
                 'mom6m', 'mom6m', 'ret', 'chg_mom6m', 'chshrout',
                 'dolvol', 'adj_ret' ] #  'ipo','adj_ret','dl_ret',


    # calc data from CRSP raw data
    from data.monthly import get_crsp_monthly
    dict_df = get_crsp_monthly(which)
    pnl_ = pd.Panel(dict_df)

    df = pnl_.to_frame(filter_observations=False)
    # select only the FC which should actually be update (sometimes more are
    # returned, as they are need to construct the final result)
    df = df[df.columns[df.columns.isin(which)]]
    df.dropna(how='all', inplace=True)
    # return df
    dstore = load_data_storage()
    if update :
        logger.info('Updating {} to store...'.format(which))
        dstore.update(df, how=how)
        logger.info('Updating of {} successful'.format(which))
    else:
        logger.info('Adding {} to store...'.format(which))
        dstore.add_columns(df)
        logger.info('Adding of {} successful'.format(which))


def fc_years(lag=1):
    """ FC with starting date

    Parameters
    ----------
    lag: int
        how many years data should be lagged, if 0 t

    Returns
    -------
    pd.DataFrame
        with column "FC" and "Year"
    """

    df = pd.read_csv(settings.FC_OVERVIEW,
                     usecols=['Acronym', 'LiteratureKey'])

    find_years = lambda x: pd.Timestamp(re.findall(r'\d+', x)[0]) + YearBegin(
        lag)
    df['Year'] = df['LiteratureKey'].apply(find_years)

    df.rename(columns={'Acronym': 'FC'}, inplace=True)

    year = pd.Timestamp('20060101') + YearBegin(lag)
    df = df.append(pd.DataFrame(dict(Year=[year], FC=['chg_mom6m'])))

    return df[['Year', 'FC']]


def published_FC_set(date, lag=1):
    """ Returns FC published as of

    Parameters
    ----------
    date: datelike
        cutoff for FC publication, selects all FC published prior to this date
    lag: int
        how much publication lag (1=beginning of the next year)
    Returns
    -------
    list of FC published as of date
    """

    df = fc_years(lag=lag)

    return df.loc[df['Year']<=date, 'FC'].tolist()


def get_data():
    dstore = load_data_storage()
    df = dstore.get(['gma'],
                    where='(SHRCD=10 | SHRCD=11) & (EXCHCD=1)',
                    start='19650101',end='20141231')

    return df


def get_permno_names():
    """ Returns a table with PERMNO and company names (latest names)

    Returns
    -------
    pandas.DataFrame
        a df containing columns 'PERMNO' and 'conm' (company name)
    """
    df = pd.read_csv(settings.LINK_DATA, usecols=['LPERMNO', 'conm'],
                            engine='c', compression='gzip', low_memory=False)
    # db_con = create_engine(settings.SQL_URL)
    # df = pd.read_sql('ccm_link_table', db_con, columns=['LPERMNO', 'conm'])
    df.rename(columns={'LPERMNO':'PERMNO', 'conm':'Name'}, inplace=True)
    df.dropna(inplace=True)
    return df.drop_duplicates(subset='PERMNO',keep='last')


def permno_filter(crsp_panel, shr_codes=[10, 11], date=None,
                  mkt_bounds=(0, 500), mkt_by='rank', size=None):
    """ Filters PERMNO based on share_codes and mkt_cap

    Parameters
    ----------
    crsp_panel: pandas.Panel
        containing the items 'shr_code' and 'mkt_cap'
    shr_codes: list
        which share codes to keep
    date: datetime
        for which date the data should be filter
    mkt_bounds: tuple
        containing either lower and upper ranks (if mkt_by='rank')
        or lower and upper percentiles (if mkt_by='size')
    mkt_by: str
        how mkt_cap data should be filter. Either 'size', which selects
        a relative portion of mkt_cap (0,0.5) 50% of mkt cap is included
        or 'rank', with, for example(0,1000), which uses the top 1000 stocks
    size: str
        'large' (top 1000), 'mid' (1001-2000) or 'small'(2001-rest) FF 1996

    Returns
    -------
    filtered_permno: list
        returns a list containing the filtered PERMNO
    """

    # filter by share code
    shr_code = crsp_panel['shr_code']
    mkt_cap = crsp_panel['mkt_cap']
    filtered_permno = shr_code.loc[:, shr_code.loc[date, :].isin(
        shr_codes)].columns.tolist()

    # use large, mid, small size groups
    if size == 'large':
        mkt_bounds = (0, 1000)
    elif size == 'mid':
        mkt_bounds = (1000, 2000)
    elif size == 'small':
        mkt_bounds = (2000, None)

    # filter by mkt_cap
    if mkt_by == 'rank' or size is not None:
        mkt_cap = mkt_cap.loc[date, filtered_permno].sort_values(
            ascending=False)
        filtered_permno = mkt_cap[mkt_bounds[0]:mkt_bounds[1]].index.tolist()
    elif mkt_by == 'size':
        mkt_cap = mkt_cap.loc[date, filtered_permno].sort_values(
            ascending=False)
        condition_1 = mkt_cap.cumsum() / mkt_cap.sum() > mkt_bounds[0]
        condition_2 = mkt_cap.cumsum() / mkt_cap.sum() <= mkt_bounds[1]

        filtered_permno = mkt_cap[condition_1 | condition_2].index.tolist()

    return filtered_permno


def _load_data(start_date='19650101', end_date='20141231', share_code=(10, 11),
               exchanges=(1, 2, 3), drop_columns=None, size=None,
               keep_columns=set(), columns=None, ret_str='adj_ret',
               drop_mkt_cap_na=True, drop_book_value_na=True):
    """ Loads data from disk into memory, by filtering mostly on disk

    """
    # data which should ne be loaded as FC, only additional filter
    _no_data_columns = {'SHROUT', 'PRC', 'RET', 'VOL', 'DLRET', 'SICCD',
                        'sector_48', 'rank_mkt_cap','rank_mkt_cap_lag',
                        'EXCHCD', 'mkt_cap',
                        'SHRCD', 'ceq', 'dl_ret', 'sector_sic2', 'chpm',
                        'pchcapx', 'pchemp', 'chato', 'sector_sic'}

    # keep data which does not belong to the default set of FC
    if keep_columns is not None:
        _no_data_columns = _no_data_columns - set(keep_columns)

    # which columns to drop
    if drop_columns is None:
        drop_columns = _no_data_columns
    else:
        # any additional drops?
        drop_columns = set(drop_columns) | _no_data_columns

    # if adj_ret should be used or plain returns
    if ret_str == 'adj_ret':
        drop_columns.update(['ret'])

    dstore = load_data_storage()

    # if no desired columns load all column names from store
    if columns is None:
        columns = list(set(dstore.columns))
    load_columns = set(columns) - (drop_columns | set(['PERMNO']))

    aux_cols = set()
    # if filter on mkt_cap we need 'mkt_cap' data
    if drop_mkt_cap_na:
        if 'mkt_cap' not in keep_columns:
            aux_cols.update(['mkt_cap'])
        load_columns = set(load_columns) | set(['mkt_cap'])
    # if filter on book value we need 'ceq' data
    if drop_book_value_na:
        if 'ceq' not in keep_columns:
            aux_cols.update(['ceq'])
        load_columns = set(load_columns) | set(['ceq'])

    #  construct where query for data storage
    _ands = list()

    if share_code is not None:
        _ands.append(' | '.join(['SHRCD={}'.format(x) for x in share_code]))

    if exchanges is not None:
        _ands.append(' | '.join(['EXCHCD={}'.format(x) for x in exchanges]))

    if size is not None:
        if size == 'all':
            pass
        elif size == 'large':
            _ands.append('rank_mkt_cap_lag<=1000')
        elif size == 'mid':
            _ands.append('rank_mkt_cap_lag>1000 & rank_mkt_cap_lag<=2000')
        elif size == 'small':
            _ands.append('rank_mkt_cap_lag>2000')
        elif size== 'large_mid':
            _ands.append('rank_mkt_cap_lag<=2000')
        elif len(size) == 2 and (isinstance(size[0], int)):
            _ands.append('rank_mkt_cap>{} & rank_mkt_cap<={}'.format(size[0],
                                                                     size[1]))
        else:
            raise ValueError(f'"{size}" Size not known')

    # concat all ORs with in ANDs
    if len(_ands) > 0:
        where = ' & '.join(['({})'.format(x) for x in _ands])
    else:
        where = None

    logger.info(f'Loading data from {start_date} to {end_date} \n '
                f'Filter: {where} \n  Columns: {load_columns}')

    # load data from disk
    df = dstore.get(columns=load_columns, start=start_date, end=end_date,
                    where=where)

    # TODO replace with HDF query information
    if drop_mkt_cap_na:
        df.dropna(subset=['mkt_cap'], inplace=True)
    if drop_book_value_na:
        df.dropna(subset=['ceq'], inplace=True)

    if len(aux_cols) > 0:
        df.drop(list(aux_cols), axis=1, inplace=True)

    if ret_str is 'adj_ret':
        df.rename(columns={'adj_ret':'ret'}, inplace=True)

    return df


def _prepare_data(df, normalize=True, winsorize_prc=None, fc_lead=1,
                  fill_na=False, replace_inf=True, sample_balance=None,
                  demean_ret=False, drop_na=False, per_rank=False,
                  return_data=None):
    """ Prepares data ready for regression

    see parameters in :meth:`_get_monthly_data`

    """

    if 'ret' not in df:
        ind = df.columns
    else:
        ind = list(set(df.columns) - set(['ret']))

    # replace inf by 0
    if replace_inf:
        logger.info('Replacing -inf + inf by 0...')
        df.replace([np.inf, -np.inf], 0, inplace=True)

    # winsorize data if wanted
    if winsorize_prc is not None:
        logger.info('Performing winsorizing ...')
        fun = lambda x: tools.winsorize(x, percentile=winsorize_prc,axis=0)
        df.loc[:, ind] = df[ind].groupby(level='date').apply(fun)

    # fill NAs by means
    if fill_na:
        logger.info('Filling NAs by means ...')
        fun =  lambda x: x.fillna(x.mean())
        df.loc[:, ind] = df[ind].groupby(level='date').apply(fun)

    # shift FC by fc_lead-period(s) forward
    if fc_lead != 0 and fc_lead is not None:

        logger.info('Filtering returns (only PERMNOs and dates in use)')
        used_permnos = df.index.get_level_values('PERMNO').unique()
        used_dates = df.index.get_level_values('date').unique()
        full_index = pd.MultiIndex.from_product([used_dates, used_permnos],
                                                    names=['date', 'PERMNO'])
        full_panel = pd.DataFrame(index=full_index)
        # get full panel, so shift can actually performed as each date exists
        # if unbalanced shift=fc_lead shifts data forward without frequency
        if return_data is not None:
            return_data = full_panel.merge(return_data, how='left',
                                           left_index=True, right_index=True)
            logger.info('Merge returns back to FC data set')
            # needed to ensure no waste of prediction data due to size classifcation
            df = return_data.merge(df, how='outer', right_index=True,
                                   left_index=True, suffixes=('', '_drop'))
            df.drop('ret_drop', axis=1, inplace=True)
        else:
            df = full_panel.join(df, how='left')

        logger.info(f'Shifting FC by {fc_lead} forward ...')
        df.loc[:, ind] = df[ind].groupby(level='PERMNO').shift(fc_lead)

        # uncomment in future, currently not implemented with multiindex
        # df.loc[:, ind] = df[ind].groupby(level='PERMNO').shift(
        #     fc_lead, freq=pd.tseries.offsets.MonthEnd())


    #  drop all entries where at least one entry is missing
    if drop_na:
        tmp_len = len(df)
        df.dropna(inplace=True)
        logger.info('Dropping {0:.03}% of data due to NAs'.format(
            (1 - len(df) / tmp_len)))

    # draw a random sample for each period
    re_merge = False
    if sample_balance is not None:
        logger.info(f"Draw random sample of size {sample_balance}...")
        # df = df.to_panel().sample(sample_balance, axis=2,
        #                           replace=True).to_frame()
        tmp_dfs = list()
        for date, df in df.groupby(level='date'):
            tmp_dfs.append(df.sample(sample_balance, replace=True))
        df = pd.concat(tmp_dfs)

    # keep columns not affected by normalize and ranking
    if per_rank or normalize:
        if len(set(df.columns) - set(ind)) > 0:
            re_merge = True
            tmp_columns = df.loc[:, set(df.columns) - set(ind)]
            tmp_columns = tmp_columns[~tmp_columns.index.duplicated(
                keep='first')]

    #  percentage rank per period
    if per_rank:
        logger.info('Performing ranking ...')
        df = df[ind].groupby(level='date').rank(pct=True)
        normalize = False

    # normalize the FC
    if normalize:
        logger.info('Performing normalizing ...')
        fun = lambda x: tools.z_score(x, axis=0)
        df = df[ind].groupby(level='date').apply(fun)

    # add the missing columns (typically "ret") back to dataframe
    if re_merge:
        df = df.merge(tmp_columns, how='left', left_index=True,
                      right_index=True)

    # demean the returns
    if 'ret' in df and demean_ret:
        logger.info('Demeaning returns ...')
        fun = lambda x: x - x.mean()
        df.loc[:, ['ret']] = df[['ret']].groupby(level='date').apply(fun)

    return df


def get_monthly_data(start_date='19650101', end_date='20141231',
                     columns=None, drop_columns=None, keep_columns={},
                     share_codes=(10, 11),  exchanges=(1, 2, 3), size=None,
                     ret_str='adj_ret',   drop_na_columns=None,
                     normalize=True, winsorize_prc=None,
                     fc_lead=1, fill_na=False, replace_inf=True,
                     sample_balance=None, demean_ret=False, drop_na=False,
                     company_names=False, drop_index=True,
                     drop_mkt_cap_na=True, drop_book_value_na=True,
                     per_rank=False):
    """Collects and returns monthly CRSP/CS-data FC and corresponding returns

    Parameters
    ----------
    start_date: str/pd.Timestamp
        date-string YYYYMMDD (default= '19650101') when data should start
    end_date: str/pd.Timestamp
        date-string YYYYMMDD (default= '20141231') when data should end
    columns: list
        which FC columns to use (via FC names)
    drop_columns: list
        which FC should be dropped (uses all except in drop_columns)
    keep_columns: list
        which data columns should be kept, despite not belonging to the FC set
    share_codes: list
        what crsp share codes to use
    exchanges: list
        which stock exchanges should be considered.
        1: NYSE, 2: AMEX, 3: NASDAQ, 10: Boston Stock Ex,
        13: Chicago Stock Ex, 16: Pacific Stock Ex, 17: Philadelphia Stock Ex
        19: Toronto Stock Ex 20: OTC
    size: str/tuple
        'large' (top 1000), 'mid' (1001-2000) or 'small'(2001-rest) (FF 1996).
        If specific ranks are desired use a tuple (of len 2) indicating the
        rank range which should be considered
    ret_str: str
        which variable should be used for ret representation. Either 'ret' or
        'adj_ret' (adjusted returns consider delisting returns)
    drop_na_columns: list
        drop row if any of the columns has a missing value
    normalize: bool
        if data should be normalized at each point in time
    winsorize_prc: float
        between [0:1] or None if no winsorizing desired, which percentile
        level to use for winsorizing the data.
    replace_inf: bool
        if inf in data should be replaced by numpy.nan
    fill_na: bool
        True if NAs in data with mean at point in time
    fc_lead: int
        how many leads should the FC - data should have
        (all CS data come already with 6m lag). Returns of date t are aligned
        with FC data of date t-fc_lead
    demean_ret: bool
        if returns should be demeaned in each period
    sample_balance: int
        how many stocks should be selected each period (balanced panel)
    drop_na: bool
        if any NA should be carried on
    company_names: bool
        if company names should be added to df returned
    drop_book_value_na: bool
        if data where no book value is available should be dropped
    drop_mkt_cap_na: bool
        if data where no market cap is available should be dropped
    per_rank: book
        returns percentage ranks instead of real valued values

    Returns
    ----------
    data: pd.DataFrame

    """
    # TODO sector=None,
    logger.info('Using HDF Store data retrieval')
    # load all needed data into memory
    df = _load_data(start_date=start_date, end_date=end_date,
                    share_code=share_codes, exchanges=exchanges,
                    drop_columns=drop_columns, keep_columns=keep_columns,
                    size=size, columns=columns,
                    ret_str=ret_str, drop_mkt_cap_na=drop_mkt_cap_na,
                    drop_book_value_na=drop_book_value_na)

    if fc_lead != 0 and 'ret' in df:
        logger.info('Load return data to ensure data consistency with lags')
        return_data = _load_data(start_date=start_date, end_date=end_date,
                                 share_code=share_codes, exchanges=exchanges,
                                 columns=['PERMNO', 'adj_ret', 'ret'],
                                 drop_book_value_na=False, ret_str=ret_str,
                                 drop_mkt_cap_na=False, size='all', )
    else:
        return_data = None

    # clean and prepare data as desired
    df = _prepare_data(df, normalize=normalize, winsorize_prc=winsorize_prc,
                       fc_lead=fc_lead, fill_na=fill_na,
                       replace_inf=replace_inf, sample_balance=sample_balance,
                       demean_ret=demean_ret, drop_na=drop_na,
                       per_rank=per_rank, return_data=return_data)

    if company_names:
        cnm_df = get_permno_names()
        df = df.reset_index()
        df = df.merge(cnm_df, on='PERMNO', how='left')
        df.set_index(['date', 'PERMNO'], inplace=True)

    if drop_index:
        df.reset_index(inplace=True)

    # TODO add panel feature for data

    return df

def get_pf_data(start_date='19650101', end_date='20141231', size='all'):
    """ Get portfolio data (returns and market cap

    Parameters
    ----------
    start_date: datelike
    end_date: datelike
    size: str

    Returns
    -------
    tuple: (rets, mkt_cap)
        rets: pd.DataFrame with date index, PERMNO columns, returns as values
        mkt_cap: pd.DataFrame with date index, PERMNO columns, MV as values

    """

    df = get_monthly_data(start_date=start_date, end_date=end_date,
                          size=size, demean_ret=False,
                          columns=['PERMNO', 'adj_ret', 'ret', 'mkt_cap'],
                          fc_lead=0, drop_na=True, ret_str='adj_ret',
                          normalize=False, keep_columns=('mkt_cap',))

    rets = df.pivot(index='date', columns='PERMNO', values='ret')

    # stocks = get_monthly_data(start_date=start_date,
    #                           end_date=end_date,
    #                           size='all', columns=('mkt_cap',),
    #                           keep_columns=('mkt_cap',))

    # num_stocks = stocks.dropna().groupby('date').count()['mkt_cap']
    # num_stocks.name = 'num_stocks'
    mkt_cap =  df.pivot(index='date', columns='PERMNO', values='mkt_cap')
    # mkt_cap = get_crsp_monthly(['mkt_cap'])['mkt_cap']
    # align the two according to the criteria

    return rets, mkt_cap

def get_monthly_data_csv(start_date='19650101', end_date='20141231', sector=None,
                         columns=None, drop_columns=None, drop_na_columns=None,
                         share_codes=[10, 11],
                         normalize=True, winsorize_prc=None, fc_lead=1,
                         fill_na=False, replace_inf=True, mkt_bounds=None,
                         mkt_by=None, size=None, company_names=False,
                         sample_balance=None, demean_ret=False, drop_na=False):
    """Collects and returns monthly CRSP/CS-data FC and corresponding returns

    Parameters
    ----------
    start_date: str/pd.Timestamp
        date-string YYYYMMDD (default= '19650101') when data should start
    end_date: str/pd.Timestamp
        date-string YYYYMMDD (default= '20141231') when data should end
    columns: list
        which FC columns to use (via FC names)
    drop_columns: list
        which FC should be dropped (uses all except in drop_columns)
    drop_na_columns: list
        drop row if any of the columns has a mising value
    share_codes: list
        what crsp share codes to use
    normalize: bool
        if data should be normalized at each point in time
    winsorize_prc: float
        between [0:1] or None if no winsorizing desired, which percentile
        level to use for winsorizing the data.
    replace_inf: bool
        if inf in data should be replaced by numpy.nan
    fill_na: bool
        True if NAs in data with mean at point in time
    fc_lead: int
        how many leads should the FC - data should have
        (all CS data come already with 6m lag). Returns of date t are aligned
        with FC data of date t-fc_lead
    demean_ret: bool
        if returns should be demeaned in each period
    mkt_bounds: tuple
        containing either lower and upper ranks (if mkt_by='rank')
        or lower and upper percentiles (if mkt_by='size')
    mkt_by: str
        how mkt_cap data should be filter. Either 'size', which selects
        a relative portion of mkt_cap (0,0.5) 50% of mkt cap is included
        or 'rank', with, for example(0,1000), which uses the top 1000 stocks
    size: str
        'large' (top 1000), 'mid' (1001-2000) or 'small'(2001-rest) FF 1996
    sample_balance: int
        how many stocks should be selected each period (balanced panel)
    company_names: bool
        if company names should be added to df returned
    drop_na: bool
        if any NA should be carried on
    exchanges: list
        which stock exchanges should be considered.
        1: NYSE, 2: AMEX, 3: NASDAQ, 10: Boston Stock Ex,
        13: Chicago Stock Ex, 16: Pacific Stock Ex, 17: Philadelphia Stock Ex
        19: Toronto Stock Ex 20: OTC

    Returns
    -------
    pandas.DataFrame
        a stacked DF of the cross-section FC including 'date' and 'PERMNO' id
        and 'ret'
    """
    # TODO sector filter (exclude financials etc)
    dates = pd.date_range(start=start_date, end=end_date, freq='M')
    logger.info('Data from {} until {} will be collected...'.format(
        dates[0], dates[-1]))
    data_list = list()
    num_dates, count_dates = len(dates), 0

    # import here (otherwise circle reference)
    from phdlib.data.monthly import get_crsp_monthly
    # crsp_list = []
    crsp_panel_data = pd.Panel(
        get_crsp_monthly(['shr_code', 'adj_ret', 'mkt_cap']))
    # TODO (see paper shumway 1999, delisting with missing delisting returns)
    # use adjusted (complemented by delisting returns)
    ret = crsp_panel_data['adj_ret']

    # prepare drop columns
    if drop_columns is None:
        drop_columns = ['ret']
    else:
        drop_columns += ['ret']
    logger.info('Dropping columns : {}'.format(drop_columns))

    if size == 'all':
        size = None

    # if columns is used, add PERMNO
    if columns is not None:
        columns = columns + ['PERMNO']
        columns = list(set(columns))

    # df.loc[:,df.drop('b', axis=1).columns] = df.drop('b', axis=1).groupby(level='ID').shift(2)

    # collect data for each desired date (skip first as data needs to be r
    for date in dates[fc_lead:]:
        count_dates += 1
        logger.info('Collected ({0:.3}% of csv files)'.format(
                count_dates / num_dates * 100))
        # get lagged FC data to align with current returns
        current_date = date - fc_lead * pd.tseries.offsets.MonthEnd()
        # determine current file name
        tmp_file = settings.MONTHLY_DATA_PATH.joinpath(
            str(current_date.year)).joinpath(str(current_date.month)).joinpath(
            settings.FC_FILE)

        #  load file for year and month
        logger.info('Loading file: {}...'.format(str(tmp_file)))
        tmp_df = pd.read_csv(str(tmp_file), index_col='PERMNO',
                             dtype=np.float32, engine='c', usecols=columns)
        # drop undesired columns
        if tmp_df.columns.isin(drop_columns).any():
            tmp_df.drop(drop_columns, axis=1, inplace=True)

        # # filter data by sector, shr_code, or market cap
        filtered_permno = permno_filter(crsp_panel_data, shr_codes=share_codes,
                                        date=date, mkt_by=mkt_by,
                                        mkt_bounds=mkt_bounds, size=size)

        logger.info('Length tmp_df prior filtering: {}'.format(len(tmp_df)))
        tmp_df = tmp_df.loc[filtered_permno, :]
        logger.info('Length tmp_df after filtering: {}'.format(len(tmp_df)))

        # TODO green et al page 9 (remove data were mkt cap and book value is not available)
        if drop_na_columns:
            pass

        # see details and then adjust accordingly
        if replace_inf:
            tmp_df.replace([np.inf, -np.inf], 0, inplace=True)

        # winsorize data if wanted
        if winsorize_prc is not None:
            logger.info('Performing winsorizing...')
            tmp_df = tools.winsorize(tmp_df, percentile=winsorize_prc, axis=0)

        # NA fill with means
        if fill_na:
            logger.info('Performing NA mean fills, len NA {}'.format(
                len(tmp_df.dropna())))
            tmp_df.fillna(tmp_df.mean(), inplace=True)
            logger.info('len NA after {}'.format(len(tmp_df.dropna())))

        # reset index after columns are adjusted (permno must not be changed)
        tmp_df.reset_index(inplace=True)

        # get returns from current date + data_lag months away
        returns = ret.loc[date, :].to_frame()
        returns.columns = ['ret']

        # merge returns of current period to last tmp_df
        tmp_df = tmp_df.merge(returns, left_on='PERMNO',
                              right_index=True, how='left')

        # final NA drop
        if drop_na:
            tmp_len = len(tmp_df)
            tmp_df.dropna(inplace=True)
            logger.info('Dropping {0:.03}% of data due to NAs'.format(
                    (1 - len(tmp_df) / tmp_len)))

        # fill up/scale down unbalanced sample
        if sample_balance is not None:
            logger.info('Sampling {} from {} obs..'.format(sample_balance,
                                                         len(tmp_df)))
            # TODO if replace should be considered with replace if len>
            tmp_df = tmp_df.sample(sample_balance, replace=True)

        # z-score a long the variables
        if normalize:
            logger.info('Performing z-score.. len tmp_df {}'.format(len(tmp_df)))
            tmp_cols = tmp_df[['ret', 'PERMNO']]
            tmp_df = tools.z_score(tmp_df, axis=0)
            tmp_df[['ret','PERMNO']] = tmp_cols

        # demean return
        if demean_ret:
            tmp_df['ret'] -= tmp_df['ret'].mean()

        # add return data to tmp_df
        tmp_df['date'] = date
        # Collect data
        data_list.append(tmp_df)

    df = pd.concat(data_list, ignore_index=True, axis=0)
    df.reset_index(inplace=True, drop=True)

    # add company names if desired
    if company_names:
        cnm_df =  get_permno_names()
        df = df.merge(cnm_df, left_on='PERMNO', right_on='PERMNO', how='left')

    return df


def remove_data(start_date='19620101', end_date='20141231', case=None,
                columns=None):
    """Removes data from monthly data base

    Parameters
    ----------
    start_date: str
        date-string YYYYMMDD (default= '19620101') when data should start
    end_date: str
        date-string YYYYMMDD (default= '20141231') when data should end
    case: str
        'PERMNO', deletes permno
    columns: list
        which columns to delete

    Returns
    -------

    """

    dates = pd.date_range(start_date,end_date, freq='m')
    for date in dates:
        year = date.year
        month = date.month
        tmp_file_path = settings.MONTHLY_DATA_PATH.joinpath(
                    str(year)).joinpath(str(month)).joinpath(settings.FC_FILE)
        logger.info('Load file: {}...'.format(str(tmp_file_path)))
        tmp_df = pd.read_csv(str(tmp_file_path), index_col='PERMNO',
                             engine='c')

        if case == 'PERMNO':
            num_rows = len(tmp_df)
            tmp_df = tmp_df[tmp_df.index > 1000]
            logger.info('Writing file: {} \n rows old {}, new {}...'.format(
                str(tmp_file_path), num_rows,
                len(tmp_df)))
            tmp_df.to_csv(str(tmp_file_path))
        if columns is not None:
            if tmp_df.columns.isin(columns).any() is False:
                logger.info(
                    '{} not in column at date {}'.format(columns, date))
                continue
            num_cols = len(tmp_df.columns)
            tmp_df.drop(columns, axis=1, inplace=True)
            logger.info('Writing file: {} \n columns old {}, new {}...'.format(
                str(tmp_file_path), num_cols,
                len(tmp_df.columns)))
            tmp_df.to_csv(str(tmp_file_path))


def add_tmp_back_up(file_list, prefix='tmp', compression='infer',
                    suffix='.csv', chunk_len=4):
    """Adds locally backed up files to main data file system

    Parameters
    ----------
    file_list: list
        list of file to be written to the main system
    prefix: str
        if any prefix is attached to the file names, like 'tmp_' etc.
    compression: str
        if data is compressed specify string, if not 'infer' is used
    suffix: str
        file ending, file type etc.
    chunk_len: int
        how many files should be added per run (splitting reduces RAM usage)

    Returns
    -------

    """
    remaining_files = None
    if len(file_list) > chunk_len:
        remaining_files = file_list[chunk_len:]
        file_list = file_list[:chunk_len]
        logger.info(
            'Splitting files. Currently adding: {}. To be added: {}'.format(
                file_list, remaining_files))
    else:
        logger.info('Currently adding: {}'.format(file_list))

    add_data = {}
    for file_name in file_list:
        tmp_csv_file = settings.TMP_PATH.joinpath(
                ''.join([prefix, file_name, suffix]))
        logger.info('Loading back up: {} ...'.format(tmp_csv_file))
        tmp_df = pd.read_csv(str(tmp_csv_file), index_col='date',
                             compression=compression, engine='c')
        fun = lambda x: x[:-2]
        tmp_df.columns = pd.Index(
                [fun(x) for x in tmp_df.columns.tolist()]).astype(int)
        add_data[file_name] = tmp_df
        add_data[file_name].index = pd.to_datetime(add_data[file_name].index)

    add_monthly_firm_char(add_data, back_up_data=False)

    if remaining_files is not None:
        add_tmp_back_up(remaining_files, prefix=prefix,chunk_len=chunk_len,
                        compression=compression,  suffix=suffix)


def main():

    file_list = ['absacc'] #'absacc'
    add_tmp_back_up(file_list, prefix='', suffix='.csv', chunk_len=2)


if __name__ == '__main__':
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')

    df = get_monthly_data(start_date='19700101', end_date='19751231',
                          size='large', drop_index=False, demean_ret=True,
                          drop_na=True,
                          drop_columns=["cfp_ia", "roic", "pchemp_ia", "ipo"])
