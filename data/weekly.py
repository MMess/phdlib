""" Calculates stats based on weekly return data

"""

import logging
import logging.config
import pandas as pd
logger = logging.getLogger(__name__)
from sqlalchemy import create_engine

from phdlib.data import manager
from phdlib.misc import logger_settings
from phdlib import settings


def get_ff_weekly_data(db_con):
    """The function collects the Fama-French weekly form the page
    NOTE: The data is not used, since weekly FF data contain saturdays during
    times when saturday was still a trading day.
    :return:
    """

    FF = web.DataReader("F-F_Research_Data_Factors_weekly_TXT",
                        "famafrench")[0]
    FF.index = pd.to_datetime(FF.index, format='%Y%m%d')
    FF.rename(columns={"1 b'Mkt-RF'": 'Market', "2 b'SMB'": 'SMB',
                       "3 b'HML'": 'HML', "4 b'RF'": 'RF'},
              inplace=True)
    FF[['Market', 'SMB', 'HML']] = FF[['Market', 'SMB', 'HML']]/100
    FF.to_sql('ff_weekly',db_con, if_exists='append', chunksize=100)


def get_weekly_data(db_con):
    """ Collects the weekly stock returns + weekly FF data

    Parameters
    ----------
    db_con: sqlalchemy.engine
        a valid SQL engine connection

    Returns
    -------
    stock_data: pandas.DataFrame
        containing weekly stock excess returns
    ff_data: pandas.DataFrame
        containing FF_data with weekly market excess returns


    """

    # collect all weekly stock return files
    file_pattern = 'WeeklyCRSP_*'
    raw_dir = files.Directory(str(settings.RAW_DATA_PATH))
    raw_files = raw_dir.find_file(file_pattern)
    stock_data = []
    for file in raw_files:
        logger.info('Read file {}'.format(file,))
        data_tmp = pd.read_csv(str(settings.RAW_DATA_PATH.joinpath(file)),
                               engine='c', low_memory=False,
                               parse_dates=['date'], index_col='date')
        stock_data.append(data_tmp)

    # align the data
    logger.info('Concatenating data...')
    stock_data = pd.concat(stock_data, axis=1)

    # get FF french data
    logger.info('Read weekly FF data from SQL...')
    ff_data = pd.read_sql('ff_weekly', db_con, index_col='index')

    return stock_data, ff_data


def weekly_returns_stats(stock_data, ff_data):
    """ Calculates weekly returns stats (beta, beta^2, idiovola)

    Parameters
    ----------
    stock_data: pandas.DataFrame
        Containing weekly excess returns of the stocks
    ff_data: pandas.DataFrame
        Containing weekly returns of benchmark data (market returns)

    Returns
    -------

    """
    min_obs_ols = 52*1  # 1 year
    window = 52*3
    market_beta, std_err_residuals, no_data_permno = list(), list(), list()

    num_stocks = len(stock_data.columns)

    for col in range(num_stocks):
        logger.info(
            'Calculate stock num {0} of {1} ({2:.3}%)'.format(col, num_stocks,
                                                        col / num_stocks*100))
        y_tmp = stock_data.ix[:, col].dropna()
        current_permno = y_tmp.name
        if len(y_tmp) < min_obs_ols:
            no_data_permno.append(y_tmp.name)
            logger.warning('PERMNO: {} has only {} obs (< min obs:{}),'
                           ' no rolling beta estimated'.format(current_permno,
                                                               len(y_tmp),
                                                               min_obs_ols))
            continue

        logger.debug(
            'y_tmp: {}, ff data: {}'.format(y_tmp, ff_data[['Market', 'RF']]))

        # align market data to current stock
        x_tmp, y_tmp = ff_data[['Market', 'RF']].align(y_tmp, axis=0,
                                                       join='right')

        tmp_res = pd.stats.ols.MovingOLS(y=y_tmp, x=x_tmp['Market'],
                                         window_type='rolling', window=window,
                                         min_periods=min_obs_ols)
        tmp_beta = tmp_res.beta['x']
        tmp_beta.name = current_permno
        market_beta.append(tmp_beta)

        # get idio_vol by, ((rmse^2)*obs/(obs-df))^0.5 (assume E[e]=0)
        mse = (tmp_res.rmse**2)*tmp_res.nobs /(tmp_res.nobs - tmp_res.df)
        mse.name = current_permno
        std_err_residuals.append(mse**0.5)

    market_beta = pd.concat(market_beta, axis=1)
    std_err_residuals = pd.concat(std_err_residuals, axis=1)

    data_export = {'idiovol': std_err_residuals}

    manager.add_monthly_firm_char(data_export)

    # market_beta.to_csv(str(settings.TMP_PATH.joinpath('tmp_beta.csv')))


def main():

    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
    db_connection = create_engine(settings.SQL_URL)
    stock_data, ff_data = get_weekly_data(db_connection)
    weekly_returns_stats(stock_data, ff_data)

if __name__ == '__main__':
    main()
