from unittest import TestCase
from phdlib.data import storage
from phdlib.misc.data_generator import DataGenerator
import unittest
import pandas as pd
import os
from pathlib import Path


class TestStorage(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.path = Path('D:\\Database\\Tests\\fc_storage.h5')
        cls.path2 = Path('D:\\Database\\Tests\\panel_storage.h5')
        # cls.path3 = Path('D:\\Database\\Tests\\panel_storage2.h5')

        groups_map = dict(
            a=dict(columns=['a', 'b', 'c', 'd', 'k', 'm']),
            query=dict(columns=['e', 'f', 'g', 'rank_a']),
        )
        cls.storage = storage.LargeDFStorage(cls.path, groups_map=groups_map)
        cls.storage_pnl = storage.LargeDFStorage(cls.path2,
                                                 groups_map=groups_map,
                                                 start_end_index='date')

        cls.df = DataGenerator.get_df(rows=20009, cols=15)
        df = DataGenerator.get_panel(rows=80, cols=200, items=24)
        df = df.to_frame()
        df['rank_a'] = df[['a']].groupby(level='date').rank()
        cls.df_pnl = df
        cls.storage_data_df(cls.storage, cls.df)
        cls.storage_data_panel(cls.storage_pnl, cls.df_pnl)

        # store = pd.HDFStore(str(cls.path3), mode='a')
        # print(cls.df.head())
        # store.append('test',df, data_columns=None)

    @classmethod
    def tearDownClass(cls):
        for file in [cls.path, cls.path2]:
            if file.exists():
                os.remove(str(file))

    @staticmethod
    def storage_data_df(storage, df):
        storage.add_columns(df[['a', 'b', 'c', 'e', 'f']])

    def test_add(self):
        self.storage.add_columns(self.df[['d', 'g']])
        with self.assertRaises(KeyError):
            self.storage.add_columns(self.df[['h','j']])
        with self.assertRaises(ValueError):
            self.storage.add_columns(self.df[['a', 'b', 'c',]])

    def test_update(self):
        self.storage.update(self.df[['a']]*4)
        with self.assertRaises(ValueError):
            self.storage.update(self.df[['m']])
            self.storage.update(self.df[['k']])

    def test_get(self):
        df = self.storage.get(columns=['a','b', 'f'], where='f>0 & e<0')
        self.assertEqual(type(df), pd.DataFrame)
        with self.assertRaises(ValueError):
            self.storage.get(columns=['ssss', 'a'])

    @staticmethod
    def storage_data_panel(storage_pnl, df_pnl):
        storage_pnl.add_columns(df_pnl[['a', 'b', 'c', 'e', 'f',
                                        'rank_a']])

    def test_add_panel(self):
        self.storage_pnl.add_columns(self.df_pnl[['d', 'g']])
        with self.assertRaises(ValueError):
            self.storage_pnl.add_columns(self.df_pnl[
                                             ['a', 'b', 'c', 'e', 'f', 'g']])
        with self.assertRaises(KeyError):
            self.storage_pnl.add_columns(self.df_pnl[['j']])

    def test_update_panel(self):
        self.storage_pnl.update(self.df_pnl[['a']])
        with self.assertRaises(ValueError):
            self.storage.update(self.df_pnl[['m']])

    def test_get_panel(self):
        df = self.storage_pnl.get(columns=['a', 'b', 'e', 'f'],
                                  where='f>0 & e>0')
        self.assertEqual(type(df), pd.DataFrame)
        df = self.storage_pnl.get(columns=['a', 'b', 'f', 'rank_a'],
                              where='rank_a>10 & rank_a<20', start='19900101')
        self.assertEqual(type(df), pd.DataFrame)
        df = self.storage_pnl.get(columns=['a', 'b', 'f'],
                                  where='rank_a>10 & rank_a<20',
                                  start='19900101',
                                  end='19910101')
        self.assertEqual(type(df), pd.DataFrame)
        df = self.storage_pnl.get(columns=['a', 'b',], start='19900101',
                                  end='19910101')
        self.assertEqual(type(df), pd.DataFrame)
        with self.assertRaises(ValueError):
            self.storage_pnl.get(columns=['ssss', 'a'])
        df = self.storage_pnl.get(columns=['a', 'b', 'c'])


    def test_start_end_str(self):
        where_str = self.storage_pnl._start_end_str(start='20100101',
                                                    end='20100201')
        self.assertEqual(type(where_str), str)
        where_str = self.storage_pnl._start_end_str(start='20100101')
        self.assertEqual(type(where_str), str)
        with self.assertRaises(ValueError):
            self.storage._start_end_str(start='20110101',
                                    end='00000001')


#
# class TestHDF():
#     @classmethod
#     def setUpClass(cls):
#         cls.path_test = Path('D:\\Database\\Tests\\fc_storage_test.h5')
#
#     @classmethod
#         def tearDownClass(cls):
#             for file in [cls.path_test]:
#                 if file.exists():
#                     os.remove(str(file))
    # def test_hdf5(self):
    #     store = pd.HDFStore(str(self.path_test), mode='a', complevel=9,
    #                         complib='blosc')
    #     df = DataGenerator.get_panel(rows=900, cols=1000, items=50)
    #     df = df.to_frame()
    #     tmp =  df
    #     store.append('a', tmp)
    #
    #     store.close()
    #     store = pd.HDFStore(str(self.path_test), mode='a', complevel=5,
    #                         complib='blosc')
    #     tmp = df.reindex(columns=[1, 2, 3], copy=False)
    #     tmp.index.name = 'date'
    #     store.append('b', tmp, data_columns=['1','2'])
    #     store.close()
    #
    # def test_read_store(self):
    #     store  = pd.HDFStore(str(self.path_test), mode='r')
    #
    #     print(storage.LargeDFStorage.stored_cols(store))
    #
    #     for x in store.items():
    #         print(x[0][1:])
    #     a = store.get_node('a')
    #
    #     print(store.select('b', start=0, stop=0).columns.tolist())
    #     store.close()

if __name__ == '__main__':
    unittest.main()