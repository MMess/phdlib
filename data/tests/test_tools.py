import logging
import unittest
import pandas as pd
import numpy as np
from phdlib.data.tools import weighted_corr_mat
logger = logging.getLogger(__name__)


def main():
    pass


class WeightedCorr(unittest.TestCase):

    def setUp(self):

        self.obs = 500
        self.corr = np.array([[1, 0.5],[0.5,1]])

        rand_n = np.random.multivariate_normal([0, 0], cov=self.corr,
                                               size=self.obs)
        self.data =pd.DataFrame(rand_n)
        self.unit_weights = pd.Series(np.ones(self.obs), self.data)


    def test_corr(self):
        test = weighted_corr_mat(X=self.data, weights=self.unit_weights)
        print(test)



if __name__ == '__main__':
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
    main()