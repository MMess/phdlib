import logging
import unittest
import pandas as pd
import numpy as np

from phdlib.data.simulation import ar_process

logger = logging.getLogger(__name__)


class ARSimulation(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.coef = 0.3
        cls.constant = 0.2
        cls.vola = 1

        cls.mean = cls.constant/(1-cls.coef)
        cls.burn_in = 200
        cls.periods = 45000
        cls.tol = 1e-2


    def test_ar_simulation(self):


        ar_1 = ar_process(constant=self.constant, coef=self.coef,
                          volatility=self.vola, periods=self.periods,
                          burn_in_periods=self.burn_in)

        self.assertEqual(type(ar_1), np.ndarray)

        ar_1 = pd.Series(ar_1)
        self.assertTrue(abs(ar_1.mean()-self.mean)<self.tol)
        self.assertTrue(abs(ar_1.corr(ar_1.shift(1))-self.coef) < self.tol)




def main():
    pass


if __name__ == '__main__':
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
    main()