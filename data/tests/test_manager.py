import logging
import unittest
import pandas as pd
import numpy as np
from phdlib.data import manager
import pandas_datareader.data as web

logger = logging.getLogger(__name__)



class TestData(unittest.TestCase):

    def test_permno_names(self):

        names_df = manager.get_permno_names()
        self.assertEqual(type(names_df), pd.DataFrame)
        self.assertTrue('PERMNO' in names_df)
        self.assertTrue('Name' in names_df)

    def test_get_monthly_data(self):
        # for size in [None, 'all', 'large', 'mid', 'small', (0, 20)]:
        #     df = manager._get_monthly_data(
        #         columns=['mom1m', 'mom6m', 'adj_ret'],
        #         start_date='20100101', size=size,
        #         drop_na=True)
        #     self.assertEqual(type(df), pd.DataFrame)
        #
        # df = manager._get_monthly_data(columns=['mom1m', 'mom6m', 'adj_ret'],
        #                                start_date='20100101', size=size,
        #                                drop_na=True, sample_balance=100,
        #                                winsorize_prc=0.1)
        # self.assertEqual(type(df), pd.DataFrame)

        size='all'

        df1 = manager.get_monthly_data(start_date='20120101', size=size,
                                       drop_na=True, sample_balance=None,
                                       winsorize_prc=0.05,
                                       fill_na=True,
                                       demean_ret=True,
                                       )

        df2 = manager.get_monthly_data_csv(start_date='20120101', size=size,
                                           drop_na=True, sample_balance=None,
                                           winsorize_prc=0.05,
                                           fill_na=True,
                                           demean_ret=True,
                                           )

        print(set(df2.columns)- set(df1.columns))
        print(df1)
        print(df2.columns)
        print(df2)



class TestFamaFrench(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.ff =  manager.FamaFrenchFactors()

    def test_find_files(self):
        self.assertEqual(type(self.ff._find_files()), list)
        self.assertTrue(len(self.ff._find_files())>0)

    def test_get(self):
        tmp_df = self.ff.get()
        self.assertEqual(type(tmp_df), pd.DataFrame)
        self.assertTrue(tmp_df.columns.isin(['Mkt-RF','SMB',
                                             'HML', 'MOM', 'RF']).all())

    def test_print(self):
        manager.FamaFrenchFactors.print_data_sets(filter='Portfolios')

    def test_download(self):
        name = 'Portfolios_Formed_on_BE-ME'
        df = web.DataReader(name, "famafrench")
        print(df['DESCR'])
        print(df[0])

class TestFFPortfolio(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.ff =  manager.FamaFrenchPortfolios()

    def test_get(self):
        tmp_df = self.ff.get()
        self.assertEqual(type(tmp_df), pd.DataFrame)
        self.assertTrue(tmp_df.columns.isin(['Lo 30',
                                             'Med 40',
                                             'Hi 30']).all())

        FFP_eq = manager.FamaFrenchPortfolios(which='ME',
                                              weighting='equal',
                                              buckets='decile')
        tmp_df =  FFP_eq.get()
        self.assertEqual(type(tmp_df), pd.DataFrame)
        self.assertTrue(tmp_df.columns.isin(
            FFP_eq._buckets_cols['decile']).all())

        FFP_eq = manager.FamaFrenchPortfolios(which='MOM12',
                                              weighting='mkt_cap',
                                              buckets='mom_style')

        tmp_df =  FFP_eq.get()
        self.assertEqual(type(tmp_df), pd.DataFrame)
        self.assertTrue(tmp_df.columns.isin(
            FFP_eq._buckets_cols['mom_style']).all())

        for key, value in manager.FamaFrenchPortfolios._data_sets.items():
            # randomly select some variables to check all from time to time
            if np.random.randn()<0.5:
                continue
            if key=='MOM12':
                buckets = 'mom_style'
            else:
                buckets = 'decile'
            print(key)
            FFP = manager.FamaFrenchPortfolios(which=key, weighting='equal',
                                               buckets=buckets)
            tmp_df = FFP.get(start_date='20100101')
            self.assertEqual(type(tmp_df), pd.DataFrame)
            self.assertTrue(tmp_df.columns.isin(
                FFP._buckets_cols['decile']).all())


def main():
    pass


if __name__ == '__main__':
    main()
