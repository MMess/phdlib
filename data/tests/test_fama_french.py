import logging
import unittest
import pandas as pd
from phdlib.data.fama_french import Factor, Portfolio, SectorDownload, Sector
logger = logging.getLogger(__name__)


class TestFactorData(unittest.TestCase):

    def test_get(self):
        df = Factor().get(frequency='daily')

        print(df)

class TestPortfolioData(unittest.TestCase):
    def test_get(self):
        variables = ['BE-ME', 'ME', 'INV', 'VAR', 'OP',
                     'CF-P', 'E-P', 'Beta', 'RES_VAR', 'D-P']
        for _ in variables:
            print(Portfolio().get(variable=_,
                                  frequency='monthly',
                                  weighting='mkt_cap'))

class TestSectorData(unittest.TestCase):
    def test_get(self):
        print(Sector().get('Sec10', frequency='daily'))


class DownloadSector(unittest.TestCase):

    def test_download_sector(self):
        self.assertEqual(type(SectorDownload(which='Sec10', freq='d').get()),
                         pd.DataFrame)


def main():
    pass


if __name__ == '__main__':
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
    unittest.main()