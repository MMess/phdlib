__author__ = 'mmess'
"""SPECIFY FILE HERE
"""
import logging
import pickle
import unittest
import datetime
import numpy as np
import pandas as pd
from phdlib.data import results

logger = logging.getLogger(__name__)


def fun_two(alpha=None, beta=None, num_sim=None):
    return alpha + beta*np.random.randn(num_sim)


class TestLocalResults(unittest.TestCase):

    def setUp(self):
        self.test_fun_1 = lambda x: x ** 2
        self.test_fun_2 = fun_two
        self.files = None
        self.data_keys = ['Results', 'HostName', 'Settings', 'StartDT',
                          'RevisionID', 'EndDT', 'FileName', 'Name']

    def tearDown(self):
        #delete files
        pass

    def test_fun1(self):

        for engine in ['pickle', 'HDF5']:
            res_fun1= results.LocalResults(engine=engine)
            self.assertEqual(type(res_fun1), results.LocalResults)
            settings = {'x': 10}
            data = self.test_fun_1(settings['x'])
            if engine == 'HDF5':
                with self.assertRaises(TypeError):
                    res_fun1.save(data,settings, 'test_fun1')

            data = pd.DataFrame({'a':[1,2,3,4]})
            res_fun1.save(data, settings, 'test_fun1', overwrite=True)

            # load saved results
            test_dict = results.load_local_result(file_name=res_fun1.file.name,
                                      engine=engine)

            print(test_dict)

            self.assertTrue(all(k in test_dict for k in self.data_keys))
            res_fun1.delete()

            with self.assertRaises(FileNotFoundError):
                res_fun1.delete()

    def test_fun2(self):


        res_fun2 = results.LocalResults(engine='pickle')
        self.assertEqual(type(res_fun2), results.LocalResults)
        settings = {'alpha': 10, 'beta': 0.5, 'num_sim': 1000}
        data = self.test_fun_2(**settings)
        res_fun2.save(data,settings, 'test_fun2')

        # load saved results
        with open(str(res_fun2.file), 'rb') as fp:
            test_dict = pickle.load(fp)

        self.assertEqual(type(res_fun2.run_time), datetime.timedelta)
        self.assertTrue(all(k in test_dict for k in self.data_keys))
        res_fun2.delete()

        with self.assertRaises(FileNotFoundError):
            res_fun2.delete()


class TestFlattenDict(unittest.TestCase):

    def setUp(self):
        self.flat_dict = {'a': 1, 'v': 2, 'c': 2}
        self.one_level_nested = {'a': {'c': 'ss'}, 'b': [1, 2, 3]}
        self.two_level_nested = {'a': {'c': 'ss', 'd': {'v': 2}},
                                 'b': [1, 2, 3]}

    def test_flat_dict(self):

        self.assertEqual(results.flatten_parameter(self.flat_dict),
                         self.flat_dict)

    def test_nested_dict(self):
        self.assertEqual(results.flatten_parameter(self.one_level_nested),
                         {'c': 'ss', 'b': [1, 2, 3]})

        double_key = {**self.flat_dict,**self.one_level_nested}

        with self.assertRaises(KeyError):
            results.flatten_parameter(double_key)

        self.assertEqual(results.flatten_parameter(self.two_level_nested),
                         {'v': 2, 'b': [1, 2, 3], 'c': 'ss'})


def main():
    pass


if __name__ == '__main__':
    main()
