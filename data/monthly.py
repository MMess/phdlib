""" Calculates FC based on monthly CRSP data files
"""
import logging

import numpy as np
import pandas as pd
import time

from phdlib import settings
from phdlib.data import manager
from phdlib.data import sector_data
from phdlib.misc import logger_settings, functions

logger = logging.getLogger(__name__)


def raw_data_name_filter(data_items):
    """Returns the required raw data which are needed to get all data_items

    Parameters
    ----------
    data_items: list
        a list of characteristics

    Returns
    -------
    raw_data_list: list
        a list of CRSP items/variables names needed to construct the data

    """
    raw_data_list = ['PERMNO', 'date']
    raw_data_dict = {'mkt_cap': ['SHROUT', 'PRC'],
                     'rank_mkt_cap': ['SHROUT', 'PRC', 'SHRCD'],
                     'rank_mkt_cap_lag': ['SHROUT', 'PRC', 'SHRCD'],
                     'mve': ['SHROUT', 'PRC'],
                     'ret': ['RET'],
                     'dl_ret': ['DLRET'],
                     'shr_code': ['SHRCD'],
                     'sector_sic': ['SICCD'],
                     'sector_48': ['SICCD'],
                     'mom1m': ['RET'],
                     'mom6m': ['RET'],
                     'mom12m': ['RET'],
                     'mom36m': ['RET'],
                     'chg_mom6m': ['RET'],
                     'chshrout': ['SHROUT'],
                     'ipo': ['date'],
                     'turn': ['VOL', 'SHROUT'],
                     'dolvol': ['VOL', 'PRC'],
                     'sector_sic2': ['SICCD'],
                     'adj_ret':['RET', 'DLRET']}

    for data_type in data_items:
        raw_data_list += raw_data_dict[data_type]

    # return unique elements only
    return list(set(raw_data_list))


def get_crsp_monthly(data_list=None, engine='csv'):
    """ Calculates all crsp monthly data from RAW CRSP data

    Parameters
    ----------
    data_list: list
        list of str, where each str has to be valid/existing data entry

    Returns
    -------
    data_dict: dict
        a dictionary, with the data_list item as key and  as value the
        specific data as pandas.DataFrame with date row index and permno ID as
        column index

    """
    logger.info('The following list is calculated: {}'.format(data_list))

    raw_data_type_list = raw_data_name_filter(data_list)
    logger.info('Loading columns {} \n from file {} ... '.format(
        raw_data_type_list, settings.CRSP_MONTHLY.name))
    if engine=='csv':
        data_tmp = pd.read_csv(str(settings.CRSP_MONTHLY), engine='c',
                               compression='gzip', low_memory=False,
                               usecols=raw_data_type_list)
        data_tmp['date'] = pd.to_datetime(data_tmp['date'], format='%Y%m%d')
        data_tmp['date'] = pd.Index(data_tmp['date']).to_period(
            'm').to_timestamp('m')
    else:
        dstore = manager.load_data_storage()
        data_tmp = dstore.get(columns=list(set(raw_data_type_list)
                                           -set(['PERMNO', 'date'])),
                              where='(SHRCD=10 | SHRCD=11) & (EXCHCD=1 |'
                                    'EXCHCD=2 | EXCHCD=3)')
        data_tmp = data_tmp.reset_index()

    data_dict = dict()
    # market cap
    if len(set(['mkt_cap','mve', 'rank_mkt_cap', 'rank_mkt_cap_lag'])
                   & set(data_list))>0:
        logger.info('Getting monthly CRSP mkt cap...')
        # negative price indicates bid/ask average for a trading day vs closing
        data_tmp['mkt_cap'] = 1000 * data_tmp['SHROUT'] * abs(data_tmp['PRC'])
        data_tmp.loc[data_tmp['mkt_cap'].isin(['B', 'C']), 'mkt_cap'] = np.nan
        data_tmp['mkt_cap'] = data_tmp['mkt_cap'].astype(float)
        data_dict['mkt_cap'] = data_tmp.pivot(index='date', columns='PERMNO',
                                 values='mkt_cap')

    if len(set(['rank_mkt_cap', 'rank_mkt_cap_lag']) & set(data_list)) > 0:
        logger.info('Getting monthly CRSP rank mkt cap...')
        data_tmp = data_tmp.set_index(['date', 'PERMNO'])
        data_tmp['rank_mkt_cap'] = data_tmp.loc[
            data_tmp['SHRCD'].isin([10, 11]), 'mkt_cap'].groupby(
            level='date').rank(ascending=False)
        data_tmp.reset_index(inplace=True)
        data_dict['rank_mkt_cap'] = data_tmp.pivot(index='date',
                                                   columns='PERMNO',
                                                   values='rank_mkt_cap')

    if 'rank_mkt_cap_lag' in data_list:
        data_dict['rank_mkt_cap_lag'] = data_dict['rank_mkt_cap'].shift()

    # log market cap
    if 'mve' in data_list:
        logger.info('Getting monthly CRSP log mkt cap...')
        data_dict['mve'] = np.log(data_dict['mkt_cap'])

    # return data (if any 'mom' string is in data_list return is needed)
    fun = lambda x: x[:3] == 'mom'
    if 'ret' in data_list or 'adj_ret' in data_list or \
                    True in [fun(x) for x in data_list] :
        logger.info('Getting monthly CRSP returns...')
        returns = data_tmp.pivot(index='date', columns='PERMNO', values='RET')
        returns[returns.isin(['B', 'C'])] = np.nan
        returns.replace([np.inf, -np.inf], np.nan, inplace=True)
        data_dict['ret'] = returns.astype(float)

    # delisting returns
    if 'dl_ret' in data_list or 'adj_ret' in data_list:
        logger.info('Pivoting monthly CRSP delisting returns...')
        dl_returns = data_tmp.pivot(index='date', columns='PERMNO',
                                    values='DLRET')
        dl_returns[dl_returns.isin(['A', 'B', 'C', 'S', 'T', 'P'])] = np.nan
        dl_returns.replace([np.inf, -np.inf], np.nan, inplace=True)
        data_dict['dl_ret'] = dl_returns.astype(float)

    # adjusted returns (returns adjusted by delisting returns)
    if 'adj_ret' in data_list:
        logger.info('Getting monthly adjusted CRSP returns... ')
        adj_ret = data_dict['ret'].fillna(data_dict['dl_ret'])
        ret_null = data_dict['ret'].isnull()
        dl_null = data_dict['dl_ret'].isnull()
        adj_ret.where(ret_null | dl_null,
                      (data_dict['dl_ret'] + 1) * (data_dict['ret'] + 1) - 1,
                      inplace=True)
        data_dict['adj_ret'] = adj_ret

    if 'shr_code' in data_list:
        logger.info('Getting monthly SHRCD... ')
        data_dict['shr_code'] = data_tmp.pivot(index='date', columns='PERMNO',
                                               values='SHRCD')

    # sector data (_sic = data by sic codes, _48 = data by higher code tree)
    if 'sector_sic' in data_list or 'sector_48' in data_list:
        logger.info('Getting monthly CRSP SICCD...')
        data_tmp['SICCD'].where(
            data_tmp['SICCD'].isin(['B', 'C', 'Z']) == False,
            inplace=True)
        data_tmp['SICCD'] = data_tmp['SICCD'].astype(float)

        if 'sector_48' in data_list:
            logger.info('Getting grouped sectors_48...')
            sector_table = sector_data.get_sic_sector_table()
            merged = data_tmp.merge(sector_table, on='SICCD', how='left')
            data_dict['sector_48'] = merged.pivot(index='date',
                                                  columns='PERMNO',
                                                  values='Sector_ID')

    if 'sector_sic' in data_list:
        logger.info('Getting monthly sector_sic...')
        data_dict['sector_sic'] = data_tmp.pivot(index='date',
                                                 columns='PERMNO',
                                                 values='SICCD')

    if 'sector_sic2' in data_list:
        logger.info('Getting monthly sector_sic2...')
        tmp_df = data_tmp.pivot(index='date', columns='PERMNO', values='SICCD')
        data_dict['sector_sic2'] = tmp_df.applymap(
            lambda x: int(str(x).zfill(4)[:2]) if str(x) != 'nan' else x)

    # IPO date first 12 month after first crsp notation
    if 'ipo' in data_list:
        logger.info('Getting IPO info...')
        grouped = data_tmp.groupby('PERMNO')
        tmp_stocks = []
        counter, t0 = 0, time.time()

        for id, stock in grouped:
            logger.info(functions.loop_tracker(counter, len(grouped), t0,
                                               'IPO Calculation', True))
            post_ipo_date_12m = stock['date'].min() + \
                                pd.tseries.offsets.DateOffset(years=1)
            stock['ipo'] = 0
            stock['ipo'] = stock['ipo'].where(
                stock['date'] >= post_ipo_date_12m, 1)
            tmp_stocks.append(stock)
            counter += 1

        tmp_df = pd.concat(tmp_stocks, ignore_index=True)
        data_dict['ipo'] = tmp_df.pivot(index='date', columns='PERMNO',
                                        values='ipo')

    if 'turn' in data_list:
        logger.info('Getting turn data...')
        tmp_vol = data_tmp.pivot(index='date', columns='PERMNO',
                                 values='VOL') * 100
        tmp_shrout = data_tmp.pivot(index='date', columns='PERMNO',
                                    values='SHROUT') * 1000
        data_dict['turn'] = pd.rolling_mean(tmp_vol, window=3) / tmp_shrout

    if 'dolvol' in data_list:
        logger.info('Getting dolvol data...')
        tmp_vol = data_tmp.pivot(index='date', columns='PERMNO',
                                 values='VOL') * 100
        tmp_price = abs(data_tmp.pivot(index='date', columns='PERMNO',
                                       values='PRC'))
        # lagged by one period
        data_dict['dolvol'] = np.log((tmp_vol * tmp_price).shift())

    if 'chshrout' in data_list:
        logger.info('Change in shares-outstanding is calculated...')
        data_dict['chshrout'] = data_tmp.pivot(index='date', columns='PERMNO',
                                               values='SHROUT').pct_change(12)

    if 'mom1m' in data_list:
        logger.info('Momentum 1m measure is calculated...')
        data_dict['mom1m'] = momentum_calc(data_dict['ret'],
                                           waiting_periods=0, end_period=1)
    if 'mom6m' in data_list or 'chg_mom6m' in data_list:
        logger.info('Momentum 6m measure is calculated...')
        data_dict['mom6m'] = momentum_calc(data_dict['ret'],
                                           waiting_periods=1, end_period=6)
    if 'mom12m' in data_list:
        logger.info('Momentum 12m measure is calculated...')
        data_dict['mom12m'] = momentum_calc(data_dict['ret'],
                                            waiting_periods=1, end_period=12)
    if 'mom36m' in data_list:
        logger.info('Momentum 36m measure is calculated...')
        data_dict['mom36m'] = momentum_calc(data_dict['ret'],
                                            waiting_periods=12, end_period=36)
    if 'chg_mom6m' in data_list:
        logger.info('chg_mom6m measure is calculated...')
        data_dict['chg_mom6m'] = data_dict['mom6m'] - data_dict['mom6m'].shift(
            6)

    logger.info('All specified crsp firm characteristics are calculated')
    return data_dict


def create_monthly_stats():
    """Controlling of re- or calculation of monthly crsp data based on rawdata
    :return:
    """

    # get crsp data needed for industry calculation
    monthly_data = get_crsp_monthly(
        ['mkt_cap', 'sector_48', 'shr_code', 'mve'])

    monthly_data = pd.Panel(monthly_data)
    # drop all share codes not 10,11
    monthly_data['sector_48'].where(monthly_data['shr_code'].isin([10, 11]),
                                    inplace=True)

    monthly_data = sector_data.calculate_sector_adj_fc(monthly_data,
                                                       log_diff=True,
                                                       which=['mkt_cap'])
    add_data = {'mve_ia': monthly_data['mkt_cap_ia']}
    manager.add_monthly_firm_char(add_data)


def momentum_calc(return_data=None, waiting_periods=1, end_period=12):
    """ Calculates momentum measures, based on net return data

    Parameters
    ----------
    return_data: pandas.DataFrame
        with a datetime index and return data as values
    waiting_periods: int
        how many periods to be skipped
    end_period: int
        how many return periods considered

    Returns
    -------

    """

    f = lambda x: x.prod()
    window_length = end_period - waiting_periods
    momentum = pd.rolling_apply(return_data + 1, func=f, window=window_length,
                                min_periods=window_length) - 1

    return momentum.shift(periods=waiting_periods)


def main():
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')

    df = get_crsp_monthly(['rank_mkt_cap_lag'])
    # manager.add_monthly_firm_char(add_data)
    return df

if __name__ == '__main__':
    df = main()
