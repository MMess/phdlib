""" Generic result saver, to keep track of version, parameters across

Helps to keep order to results when run on multiple machines and server farms

"""

import datetime
import json
import logging
import os
import pickle
import socket
import shutil

import pandas as pd
from sqlalchemy import create_engine

import phdlib as phd
from phdlib.misc import logger_settings, data_generator, ftp_access, files
from phdlib import settings

logger = logging.getLogger(__name__)


class LocalResults(object):
    """ Storage and information class for running functions/simulations etc

    Keeps track of runtime (start dt, and end dt), the used settings, the
    revision number of the files used to run the function, the host_name etc.

    """
    # TODO design results if it makes sense to pass the function vs data
    # TODO might make sense to have also a load function
    suffix_map = {'HDF5': settings.SUFFIX_RES + '.h5',
                  'pickle': settings.SUFFIX_RES + '.p', }

    def __init__(self, engine='HDF5', path=settings.RESULTS_PATH):
        """
        
        Parameters
        ----------
        engine: str
            which file format to use, either 'pickle' or 'HDF5'.
            Preferred engine is HDF5 (much more robust), however, currently
            only usable with pd.DataFrame as results data and only with
            one table. For multiple table or non pd.DF use pickle engine.
        """
        self.start_dt = datetime.datetime.now()
        self.end_dt = None
        self.file = None
        self.engine = engine
        self.suffix = self.suffix_map[engine]
        self.path = path

    def save(self, data, parameters, results_name=None, overwrite=False):
        """ Saves results locally in python readable bytes (pickle)

        Additionally, it attaches the current git revision (if available), the
        hostname, and the results_name

        Parameters
        ----------
        data: pandas.DataFrame or dict of Frames
            results which are to be saved
        parameters: dict
            settings which were used in the function run
        results_name: str
            name of the result type, which results/project/estimation
        overwrite: bool
            if existing files should be overwritten (if False raise Error)

        Returns
        -------

        """

        self.end_dt = datetime.datetime.now()
        end_dt = self.end_dt.strftime('%Y-%m-%d_%H%M%S')
        self.file = self.path.joinpath(
            '_'.join([end_dt, results_name, self.suffix]))

        # check if file exits already
        if overwrite is False:
            if self.file.exists():
                logger.error('File {} exists already'.format(str(self.file)))
                raise FileExistsError
        # results dict
        results = {'StartDT': self.start_dt, 'EndDT': self.end_dt,
                   'Results': data, 'Settings': parameters,
                   'FileName': self.file.name,
                   'HostName': socket.gethostname(),
                   'RevisionID': phd.revision_id, 'Name': results_name}
        # save results to dict
        self.save_to_disk(results, engine=self.engine, file=self.file)

    @staticmethod
    def save_to_disk(results, file, engine='HDF5'):
        """ Saves results to local disk
        
        Parameters
        ----------
        results: dict
        """

        if engine == 'pickle':
            # write file to local results dir
            with open(str(file), 'wb') as fp:
                pickle.dump(results, fp)
                fp.close()
        elif engine == 'HDF5':
            store = pd.HDFStore(str(file))
            data = results.pop('Results')

            if isinstance(data, dict):
                for key, value in data.items():
                    store['/'.join(['Results', key])] = value
                # save all keys of result tables to settings file
                results['result_tables'] = list(data.keys())
            else:
                if isinstance(data, (pd.DataFrame, pd.Series)) is False:
                    store.close()
                    raise TypeError('Only pandas.DF or Series can be saved '
                                    'with HDF5 table')
                # save just a single table to hd5 store
                store.put('Results', data)
                results['result_tables'] = None

            store['meta'] = pd.Series([1])
            store.get_storer('meta').attrs.metadata = results
            store.close()

        logger.info('The following results file was saved: {}'.format(
            str(file)))

    def delete(self, file=None, raise_error=True):
        """ Deletes file from disk
        
        Parameters
        ----------
        file: path-like
            full file path to file
        raise_error: bool
            if file does not exists raise error (True) 

        Returns
        -------

        """

        if file is None:
            file = self.file

        if file.exists():
            os.remove(str(file))
        else:
            if raise_error:
                raise FileNotFoundError

    @property
    def run_time(self):
        if self.end_dt is None:
            raise RuntimeError('No results yet obtained')
        return self.end_dt - self.start_dt


class LoadLocalResult():
    def __init__(self, file_pattern, conditions=None, since=None,
                 which='latest', engine='pickle',
                 path=settings.RESULTS_PATH):
        """

        Parameters
        ----------
        file_pattern
        conditions: dict
            a dict of parameters, key values need to be specified
        since: date-like
            searches only files sent 
        """
        self._file_pattern = file_pattern
        self._file_names = None
        self._file_name = None
        self._conditions = conditions
        self._since = since
        self.path = path
        self.files_true = None
        if which == 'latest':
            self._latest = 0
        elif which == 'first':
            self._latest = -1
        self.engine = engine

        self._select_file()

    @staticmethod
    def _infer_engine(file_name):
        """
        
        Parameters
        ----------
        file_name: path-like
            file to be loaded

        Returns
        -------
        engine: str
            either "HDF5" or "pickle"
        """
        if str(file_name)[-1] == 'p':
            engine = 'pickle'
        else:
            engine = 'HDF5'

        return engine

    def _select_file(self):
        """ Selects file from local storage

        Returns
        -------

        """
        # TODO clean-up this function
        dir = files.Directory(self.path)
        matched_files = dir.find_file(self._file_pattern, pattern_loc='any')

        if len(matched_files) == 0:
            raise ValueError('No file with pattern {} found'.format(
                self._file_pattern))

        elif len(matched_files) > 0:
            matched_files.sort(reverse=True)

            if self._since is not None:
                sent_since = list()
                for file in matched_files:
                    # load local file
                    engine = self._infer_engine(file)
                    tmp_results = load_local_result(file, engine=engine,
                                                    path=self.path)
                    if tmp_results['StartDT'] >= pd.Timestamp(self._since):
                        sent_since.append(file)

                matched_files = sent_since

            # take the most recent
            if self._conditions is None:
                self.files_true = matched_files

                if len(matched_files) > 1:
                    logger.warning('More than one file found, using latest')
                    self._file_name = matched_files[self._latest]
                elif len(matched_files) == 1:
                    self._file_name = matched_files[0]

            else:
                self.files_true = list()

                for file in matched_files:
                    matching = True
                    # load local file
                    engine = self._infer_engine(file)
                    tmp_results = load_local_result(file, engine=engine,
                                                    path=self.path)
                    # load parameters of file
                    tmp_para = flatten_parameter(tmp_results['Settings'])
                    for key, value in self._conditions.items():
                        try:
                            if tmp_para[key] != value:
                                matching = False
                        except KeyError:
                            matching = False

                    if matching:
                        self.files_true.append(file)

                if len(self.files_true) > 1:
                    # raise NotImplementedError
                    logger.warning('More than one file found, using latest')
                    self._file_name = self.files_true[self._latest]
                elif len(self.files_true) == 1:
                    self._file_name = self.files_true[self._latest]
                else:
                    raise ValueError('No file with pattern {} and '
                                     'matching conditions found'.format(
                        self._file_pattern))

            engine = self._infer_engine(self._file_name)
            self.file = load_local_result(self._file_name, engine=engine,
                                          path=self.path)

    @staticmethod
    def move_results(files, sub_folder, path=settings.RESULTS_PATH):
        """ Moves results to sub_folder

        Parameters
        ----------
        files: list
            list of files to be moved
        sub_folder: str/iterable
            moves file to sub folder of results dir
        """
        if isinstance(sub_folder, str):
            sub_folder = [sub_folder]
        target_dir = path.joinpath(*sub_folder)

        for file in files:
            source = path.joinpath(file)
            target = target_dir.joinpath(file)

            logger.info('Moving file from {} to {}...'.format(target, source))
            shutil.move(str(source), str(target))


    @property
    def parameter(self):
        return flatten_parameter(self.file['Settings'])

    @property
    def data(self):
        return self.file['Results']

    @property
    def end_time(self):
        return self.file['EndDT']

    @property
    def start_time(self):
        return self.file['StartDT']

    @property
    def host(self):
        return self.file['HostName']

    @property
    def revision(self):
        return self.file['RevisionID']

    @staticmethod
    def update_parameters(file_name, parameters, overwrite=True):
        """

        Parameters
        ----------
        file_name: str
            file name of which p
        parameters: dict
            key values of new parameters
        overwrite

        Returns
        -------

        """

        file_path = LocalResults.path.joinpath(file_name)
        results = LoadLocalResult(file_pattern=file_name)

        for key, value in parameters.items():
            results.parameter[key] = value

        LocalResults.save_to_disk(results,file_name)
        print(file_path)
        # check if file exits already
        # if file_path.exists():
        #     logger.error('File {} exists already'.format(str(self.file)))
        #     raise FileExistsError

        # write file to local results dir
        with open(str(file_path), 'wb') as fp:
            pickle.dump(self.file, fp)


class DBResults(object):
    def _get_non_db(self):
        """ returns all local results files which are not yet in database

        Returns
        -------

        """
        result_list = list()
        result_files = list()
        result_dir_list = os.listdir(str(settings.RESULTS_PATH))

        # get all result files of local dir
        for file_name in result_dir_list:
            if file_name.endswith(settings.SUFFIX_RES):
                result_files.append(file_name)

        # get info of current results in database
        result_info = pd.read_sql('results', self.db_con, columns=['FileName'])

        for file in result_files:
            # check if file exists already in results table
            if file in result_info['FileName'].values.tolist():
                logger.info('{} already in archived in results'.format(file))
                continue

            # open pickled results and to list
            logger.info('Add {} to results_list'.format(file))
            with open(str(settings.RESULTS_PATH.joinpath(file)), 'rb') as fp:
                print(fp)
                result_list.append(pickle.load(fp))
                fp.close()

        return result_list

    def save_to_db(self):
        pass

    def get_results(self):
        pass

    def collect_results(self):
        pass


def collect_results(db_con=None):
    """ Collects results from the local results directory
    :return:
    """

    result_list = list()
    result_files = list()
    result_dir_list = os.listdir(str(settings.RESULTS_PATH))

    # get all result files of local dir
    for file_name in result_dir_list:
        if file_name.endswith(settings.SUFFIX_RES):
            result_files.append(file_name)

    # get info of current results in database
    result_info = pd.read_sql('results', db_con, columns=['FileName'])

    for file in result_files:
        # check if file exists already in results table
        if file in result_info['FileName'].values.tolist():
            logger.info('{} already in archived in results'.format(file))
            continue

        # open pickled results and to list
        logger.info('Add {} to results_list'.format(file))
        with open(str(settings.RESULTS_PATH.joinpath(file)), 'rb') as fp:
            result_list.append(pickle.load(fp))
            fp.close()

    return result_list


def save_results_db(result_list, db_con):
    """ Saves result to the final storage

    Storage consists of three parts, SQL (Overview), JSON(Settings) and
    HDF(Results)

    Parameters
    ----------
    result_list: list
        a list of results to be stored
    db_con: sqlalchemy.Engine
        a valid sql session

    Returns
    -------

    """

    table_columns = ('StartDT', 'EndDT', 'FileName', 'HostName',
                     'RevisionID', 'Name')
    store_hd5 = pd.HDFStore(str(settings.RESULT_HDF5))

    for result in result_list:
        # open results and save them to list SQL
        data = dict((k, result[k]) for k in table_columns if k in result)
        pd.DataFrame(data, index=[0]).to_sql('results', db_con,
                                             if_exists='append',
                                             index=False)

        # obtain IDs and file names (get newly assigned ID)
        id_table = pd.read_sql('results', db_con, columns=['ID', 'FileName'])
        logger.debug(
            'Cols id-Table: {}, keys in result: {}'.format(id_table.columns,
                                                           result.keys()))
        result_id = id_table[id_table['FileName'] == result['FileName']]
        if len(result_id) > 1:
            raise ValueError('More than one filename in DB not possible')
        result['ID'] = int(result_id.ID.values[0])

        # save result data to hdf 5
        logger.info('save results with id {} to database'.format(result['ID']))
        id_string = '_'.join(['ID', str(result['ID'])])

        # check if results are dict -> multiple tables
        if isinstance(result['Results'], dict):
            logger.debug('Results are: {}'.format(result['Results']))
            for key, value in result['Results'].items():
                store_hd5['/'.join([id_string, key])] = value
            # save all keys of result tables to settings file
            result['Settings']['result_tables'] = list(
                result['Results'].keys())
        else:
            # save just a single table to hd5 store
            store_hd5['/'.join([id_string, 'result'])] = result['Results']
            result['Settings']['result_tables'] = ['result']

        # save settings data
        new_dict = {result['ID']: result['Settings']}
        if settings.RESULT_META_JS.exists():
            with open(str(settings.RESULT_META_JS)) as fp:
                settings_dict = json.load(fp)
        else:
            settings_dict = dict()
        settings_dict.update(new_dict)
        with open(str(settings.RESULT_META_JS), 'w') as fp:
            json.dump(settings_dict, fp)

    store_hd5.close()


def archive_files(sub_folder, pattern='Result',
                  path=settings.RESULTS_PATH, **conditions):
    """ Moves files with different conditions to "archive"

    Parameters
    ----------
    sub_folder: str or iterable
    size
    pattern

    Returns
    -------

    """

    conditions = {**conditions}
    files = LoadLocalResult(file_pattern=pattern,
                            since='20171101',
                            conditions=conditions,
                            path=path).files_true
    LoadLocalResult.move_results(files, sub_folder, path=path)


def get_results(result_ids):
    """ returns the results stored in the local result storages

    Parameters
    ----------
    result_ids: list
        list of restult ids


    Returns
    -------
    dict
        a dict with result_id as key
    """
    res = {}
    # open databases json for parameters, and hd5 table for result tables
    results_hd5 = pd.HDFStore(str(settings.RESULT_HDF5), mode='r')
    with open(str(settings.RESULT_META_JS), 'r') as fp:
        results_json = json.load(fp)

    # loop through result ids
    for result_id in result_ids:
        # get settings of tables
        settings_dict = results_json[str(result_id)]
        results = dict()
        # load each table in HDF group
        id_string = '_'.join(['ID', str(result_id)])
        for table in settings_dict['result_tables']:
            results[table] = results_hd5['/'.join([id_string, table])]

        tmp_res = {'result_data': results, 'settings': settings_dict}
        res[result_id] = tmp_res

    results_hd5.close()

    return res


def process_results(ftp_update=False):
    """The function process results of all computers
    :return:
    """

    # collect result from server and store results file locally via ftp
    if ftp_update:
        ftp_access.get_result_data()

    # create db-connection to store and get data
    db_con = create_engine(settings.SQL_URL)

    # collect local result files to list
    result_list = collect_results(db_con)

    # save results to long-term storage
    save_results_db(result_list, db_con)


def load_local_result(file_name, engine='pickle', path = settings.RESULTS_PATH):
    """ Loads file from local drive

    Parameters
    ----------
    file_name: str
        name of the file (path is taken from settings)
    engine: str
        either 'pickle' or 'HDF5'

    Returns
    -------

    """

    if engine == 'pickle':
        with open(str(path.joinpath(file_name)), 'rb') as fp:
            local_file = pickle.load(fp)
            fp.close()

    elif engine == 'HDF5':
        store = pd.HDFStore(str(path.joinpath(file_name)),
                            mode='r')
        local_file = dict()
        if '/meta' in store.keys():
            local_file.update(store.get_storer('meta').attrs.metadata)
        else:
            local_file.update(store.get_storer('Results').attrs.metadata)
        # check if tables info is saved (if not older code version)
        if 'result_tables' not in local_file:
            local_file['result_tables'] = None
        if local_file['result_tables'] is None:
            local_file['Results'] = store['Results']
        else:
            data = dict()
            for table in local_file['result_tables']:
                data[table] = store['/'.join(['Results', table])]
            local_file['Results'] = data

        store.close()

    return local_file


def flatten_parameter(nested_dict):
    """ Flattens a nested dicts (up to any level) outer keys are lost

    However, keys need to be unique otherwise KeyError is raised

    Parameters
    ----------
    nested_dict: dict
        with potentially (one level) nested dict

    Returns
    -------
    dict

    """

    flat_dict = dict()
    for key, value in nested_dict.items():
        if isinstance(value, dict):
            for key_nested in value.keys():
                if key_nested in nested_dict.keys():
                    raise KeyError(
                        '"{}" already in dict, cannot be flatten'.format(
                            key_nested))

            flat_dict.update(flatten_parameter(value))
        else:
            flat_dict[key] = value

    return flat_dict


def pickle_to_hdf5_results():
    """ Browses through results folder and transform all files to HDF results
    
    Returns
    -------

    """

    files = LoadLocalResult(file_pattern='.p').files_true

    for file in files:
        hdf5_file_name = settings.RESULTS_PATH.joinpath('HDF5').joinpath(
            file[:-2] + '.h5')

        results = load_local_result(file_name=file, engine='pickle')
        LocalResults.save_to_disk(results, file=hdf5_file_name)


def main():
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')

    size = 'large'

    fw_correction = 'holm'

    res_path = settings.RESULTS_PATH.joinpath('cross_section_fm_pols')
    archive_files(sub_folder='trash', path=res_path, fw_correction=fw_correction)


if __name__ == '__main__':
    main()
