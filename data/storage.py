""" Generic ultra fast local time series data storage based on HDF5 tables

"""

import pandas as pd

def dict_reverse(target_dict):
    """ Maps the keys of a dict to its values by reversing,
    
    NOTE: the values have to be unique!
    
    Parameters
    ----------
    target_dict: dict

    Returns
    -------
    dict
    """
    column_map = dict()
    for g, value in target_dict.items():
        if len(set(column_map.keys()) & set(value)) > 0:
            raise ValueError('Columns have to be unique')
        for col in value:
            column_map[col] = g

    return column_map


class LargeDFStorage:

    # TODO add index features to ensure correct indexes
    # index_names = ('date', 'permno')

    def __init__(self, h5_path, groups_map, start_end_index=None):
        """ Store for times-series (and panel) data

        Build on a HDF5 storage. Allows to save relative large data with
        disk querying, for example, when reading csv file gets into RAM issues.
        Its relatively fast and based on one file. Hence, can easily moved
        and ported to other locations (no need to set-up anything). Problematic
        for multi-user writing operations (not tested).

        Parameters
        ----------
        h5_path: str
            hdf5 storage path
        groups_map: dict
            where keys are group_names and values are dict, with at least key
            'columns' where the value is list of column names.
            A special group_name is reserved for group_name/key "query", which
            can be used as queering and conditioning table when getting data,
            see :meth:`.get`. Size of groups depends on hardware specification,
            bigger groups should be faster, but eventually results in RAM
            problems when data is large.
        start_end_index: str
            name of the index which defines some order for example 'date' if
            the row index is named 'date' and you want to query with respect to
            the date.
        """

        self.path = str(h5_path)
        self.groups_map = groups_map
        self.column_map = self._get_column_map()
        # if desired make part of arguments
        self.complib = 'blosc'
        self.complevel = 9
        self.start_end_index = start_end_index
        self.columns = list(self.column_map.keys())

    def _get_column_map(self):
        """ Calc the inverse of the groups_map/ensures uniqueness of cols

        Returns
        -------
        column_map: dict
            with cols as keys and group_names as values
        """

        column_map = dict()
        for g, value in self.groups_map.items():
            if len(set(column_map.keys()) & set(value['columns'])) > 0:
                raise ValueError('Columns have to be unique')
            for col in value['columns']:
                column_map[col] = g

        return column_map

    @staticmethod
    def group_col_names(store, group_name):
        """ Returns all column names of specific group saved in store

        Parameters
        ----------
        store: pd.HDFStore
        group_name: str

        Returns
        -------
        list:
            of all column names in the group
        """
        if group_name not in store:
            return []


        try:
            # hack to get column names, straightforward way!?
            return store.select(group_name, start=0, stop=0).columns.tolist()
        except AttributeError as msg:
            print(group_name)
            print(msg)
            store.remove(group_name)
            return []

    @staticmethod
    def stored_cols(store):
        """ Collects all columns stored in HDF5 store

        Parameters
        ----------
        store: pd.HDFStore

        Returns
        -------
        list:
            a list of all columns currently in the store
        """
        stored_cols = list()
        for x in store.items():
            group_name = x[0][1:]
            stored_cols += LargeDFStorage.group_col_names(store, group_name)

        return stored_cols

    def _find_groups(self, columns):
        """ Searches all groups required for covering columns

        Parameters
        ----------
        columns: list
            list of valid columns

        Returns
        -------
        list:
            of unique groups
        """
        groups = list()
        for column in columns:
            groups.append(self.column_map[column])

        return list(set(groups))

    def add_columns(self, df, how='outer'):
        """ Adds columns to storage for the first time. If columns should
        be updated use(use :meth:`.update` instead)

        Parameters
        ----------
        df: pandas.DataFrame
            with new columns (not yet stored in any of the tables)
        how: str
            how the new columns should be added in case some data is already
            stored from the specific group, 'left' uses storage index, 'right'
            uses df index,'inner' the intersection of df and the storage index
            'outer' the union of df and storage

        Returns
        -------

        """
        store = pd.HDFStore(self.path, mode='a' , complevel=self.complevel,
                            complib=self.complib)

        # check if any column has been stored already
        if df.columns.isin(self.stored_cols(store)).any():
            store.close()
            raise ValueError('Some cols are already in the store')

        # find all groups needed to store the data
        try:
            groups = self._find_groups(df.columns)
        except KeyError as err:
            store.close()
            raise KeyError(err)

        for group in groups:
            v = self.groups_map[group]

            # select columns of current group in df
            select_cols = df.columns[df.columns.isin(v['columns'])].tolist()
            tmp = df.reindex(columns=select_cols, copy=False)

            # set data column to False only in case of query data
            dc = None

            stored_cols = self.group_col_names(store,group)
            # no columns in group (group does not exists yet)
            if len(stored_cols) == 0:
                if group == 'query':
                    dc = tmp.columns.tolist()
                store.append(group, tmp, data_columns=dc)
            else:
                # load current disk data to memory
                df_grp = store.get(group)
                # add new column(s) to df_disk
                df_grp = df_grp.merge(tmp, left_index=True, right_index=True,
                                      how=how)
                if group == 'query':
                    dc = df_grp.columns.tolist()

                # remove data from disk
                store.remove(group)
                # save old data with new, additional columns
                store.append(group, df_grp, data_columns=dc)

        store.close()

    def _query_table(self, store, columns, where):
        """ Selects data from table 'query' and uses where expression

        Parameters
        ----------
        store: pd.HDFStore
        columns: list
            desired data columns
        where: str
            a valid select expression

        Returns
        -------

        """

        query_cols = self.group_col_names(store, 'query')
        if len(query_cols) == 0:
            store.close()
            raise ValueError('No data to query table')
        get_cols = list(set(query_cols) & set(columns))
        if len(get_cols) == 0:
            # load only one column to minimize memory usage
            df_query = store.select('query', columns=query_cols[0],
                                    where=where)
            add_query = False
        else:
            # load columns which are anyways needed already
            df_query = store.select('query', columns=get_cols, where=where)
            add_query = True

        return df_query, add_query

    def _start_end_str(self, start=None, end=None):
        """ Combines start and end date as string 

        Parameters
        ----------
        start: date-like
             should be compatible with pd.Timestamp
        end: date-like
            should be compatible with pd.Timestamp
            
        Returns
        -------
        str or None
        """
        if start is None and end is None:
            return
        if self.start_end_index is None:
            raise ValueError('Name of start/end index is note defined')

        where_str = list()
        if start is not None:
            start = pd.Timestamp(start).strftime(format='%Y%m%d')
            where_str.append(self.start_end_index + '>=' + start)
        if end is not None:
            end = pd.Timestamp(end).strftime(format='%Y%m%d')
            where_str.append(self.start_end_index + '<=' + end)

        return ' & '.join(where_str)

    def get(self, columns, where=None, start=None, end=None):
        """ Retrieve data from storage

        Parameters
        ----------
        columns: list/str
            list of columns to use, or use 'all' if all columns should be
            retrieved
        where: str
            a valid select statement

        Returns
        -------
        pandas.DataFrame
            with all requested columns and considering where
        """
        start_end_where = self._start_end_str(start, end)
        store = pd.HDFStore(str(self.path), mode='r')
        # get all columns in stored in HDFStorage
        stored_cols = self.stored_cols(store)

        if columns == 'all':
            columns = stored_cols

        # check if all desired columns can be found in storage
        if len(set(columns) - set(stored_cols)) > 0:
            store.close()
            raise ValueError('Column(s): {}. not in storage'.format(
                set(columns)- set(stored_cols)))

        # get all relevant groups (where columns are taken from)
        groups = self._find_groups(columns)

        # if where query is defined retrieve data from storage, eventually
        # only index of df_query might be used
        if where is not None:
            if start_end_where is not None:
                where += ' & ' + start_end_where
            df_query, add_df_query = self._query_table(store, columns, where)
        else:
            df_query, add_df_query = None, False

        # dd collector
        df = list()
        for group in groups:
            # skip in case where was used and columns used from
            if where is not None and group=='query':
                continue
            # all columns which are in group but also requested
            get_cols = list(
                set(self.group_col_names(store, group)) & set(columns))
            tmp_df = store.select(group, columns=get_cols,
                                  where=start_end_where)
            if df_query is None:
                df.append(tmp_df)
            else:
                # align query index with df index from storage
                # TODO check if passing the explicit index is faster while loading
                df_query, tmp_df = df_query.align(tmp_df, join='left', axis=0)
                df.append(tmp_df)

        store.close()

        # if any data of query should be added
        if add_df_query:
            df.append(df_query)

        # combine all columns
        df = pd.concat(df, axis=1)

        return df

    def update(self, df, how='left'):
        """ Updates data in storage, all columns have to be stored already in
        order to be accepted for updating (use :meth:`.add_columns` instead)

        Parameters
        ----------
        df: pd.DataFrame
            with index as in storage, and column as desired
        how: str
            how data should be update in case some data from that group is
            already in the storage. 'left' uses the storage index and ignores


        Returns
        -------
        None
        """

        store = pd.HDFStore(self.path, mode='a' , complevel=self.complevel,
                            complib=self.complib)

        # check if all column have been stored already
        if df.columns.isin(self.stored_cols(store)).all()==False:
            store.close()
            raise ValueError('Some cols have not been stored yet')

        # find all groups needed to store the data
        groups = self._find_groups(df.columns)
        for group in groups:
            dc = None
            # load current disk data to memory
            group_df = store.get(group)
            # update with new data
            group_df, df_adjusted = group_df.align(df, join=how, axis=0)
            group_df.update(df_adjusted, join='left')
            # save updated df back to disk
            if group == 'query':
                dc = group_df.columns.tolist()
            # remove data from disk (only after successful memory merging)
            store.remove(group)
            store.append(group, group_df, data_columns=dc)

        store.close()

        return


def main():
    pass

if __name__ == '__main__':
    main()