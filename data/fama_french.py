"""  This file connects to FamaFrench data lib and builds a local file storage
"""
import logging
import pandas as pd
import pandas_datareader.data as web
from pandas_datareader.famafrench import get_available_datasets

from phdlib.data import storage
from phdlib.settings import FF_DATA_PATH

logger = logging.getLogger(__name__)

DATA_SETS = dict(factor=dict(),
                 portfolio=dict(),
                 sector=dict())

CUTOFF_COLS = {'ff_style': ['Lo 30', 'Med 40', 'Hi 30'],
               'quantile': ['Lo 20', 'Qnt 2', 'Qnt 3', 'Qnt 4', 'Hi 20'],
               'decile': ['Lo 10', 'Dec 2', 'Dec 3', 'Dec 4', 'Dec 5',
                          'Dec 6', 'Dec 7', 'Dec 8', 'Dec 9', 'Hi 10'],
               'mom_style': ['Lo PRIOR', 'PRIOR 2', 'PRIOR 3', 'PRIOR 4',
                             'PRIOR 5', 'PRIOR 6', 'PRIOR 7', 'PRIOR 8',
                             'PRIOR 9', 'Hi PRIOR'],
               'none': None}

class Core:

    _items = None
    _freq_map = {'monthly': 'm', 'daily': 'd', 'yearly': 'y'}
    index_name = 'date'
    path = FF_DATA_PATH
    type_name = None

    def __init__(self, download_missing=True, update=False):
        """ Base wrapper around HDF Storage class for Fama French local store

        Handles data loading from disk and updating FF from the web
        (see subclasses for more classes)

        Parameters
        ----------
        download_missing: bool
            if data should be downloaded from the web if not on local drive
        update: bool
            if data should be downloaded and inserted to local storage
        """
        self.download_missing = download_missing
        self.update = update
        self.freq = None

    def _load_storage(self,  file_path, columns):
        """ Loads the HDF5 storage
        
        Parameters
        ----------
        file_path: str/Path
            dir where the data is stored
        columns: list
            which columns to load

        Returns
        -------
        storage instance
        """
        # only one group all data in one column
        group_map = dict(data=dict(columns=columns), )
        # TODO rename index name to "date"
        store = storage.LargeDFStorage(file_path, group_map,
                                       start_end_index='index')
        return store

    def _load_data(self, store, columns, start, end):
        """ Loads data from storage if available
        
        Parameters
        ----------
        store: LargeDFStorage
        columns: iterable
        start: date like
        end: date like

        Returns
        -------
        pd.DataFrame
        
        """

        try:
            df = store.get(columns=columns, start=start, end=end)
            if self.update:
                df = self.download_details()
                store.update(df)
        except OSError as err:
            if self.download_missing:
                df = self.download_details()
                store.add_columns(df)
                df = store.get(columns=columns, start=start, end=end)
            else:
                raise err

        return df

    def file_name(self, frequency, weighting=None, cutoff=None,
                  variable=None, schema=None):
        """ Defines the file name of the specific data-set
        
        Parameters
        ----------
        frequency: str
        weighting: str
        cutoff: str
        variable:str

        Returns
        -------
        file_name: str
            hdf store file_name
        """

        name_list = [self.type_name]

        for _ in [frequency, weighting, cutoff, variable, schema]:
            if _ is not None:
                name_list.append(_)

        file_name = '_'.join(name_list) + '.h5'

        return file_name


class Factor(Core):
    type_name = 'factor'
    factors = ['Mkt-RF', 'SMB', 'HML', 'MOM', 'RF', 'LTR', 'STR', 'RMW', 'CMA']
    model = {'five-factor': ['Mkt-RF', 'SMB', 'HML', 'RMW', 'CMA'],
             'five+mom-factor': ['Mkt-RF', 'SMB', 'HML', 'RMW', 'CMA', 'MOM'],
             'three-factor': ['Mkt-RF', 'SMB', 'HML', ],
             'carhart': ['Mkt-RF', 'SMB', 'HML', 'MOM'],
             'CAPM': ['Mkt-RF']
             }

    def get(self, factors=None, model=None, frequency='monthly', start=None,
            end=None):
        """ Loads data from local drive to memory

        Parameters
        ----------
        factors: iterable
            specify list of factors to be retrieved
        model: str
            {'five-factor', 'five+mom-factor', 'three-factor' , 'CAPM',
            'carhart'}
            if model is selected, gets associated factors
        frequency: str
            which data frequency
        start: datalike
        end: datelike

        Returns
        -------
        factor_data: pd.DataFrame
            with date as index and factors as columns
        """

        if factors is None and model is None:
            factors = self.factors
        elif factors is not None and model is not None:
            raise ValueError('Only one of "factors" and "model" can be used')
        elif factors is None:
            factors = self.model[model]

        self.freq = frequency
        store_path = self.path.joinpath(self.file_name(frequency=frequency))
        store = self._load_storage(store_path, columns=self.factors)

        return self._load_data(store, columns=factors, start=start, end=end)

    def download_details(self):
        """ Downloads factor data from the FF data lib

        Returns
        -------
        factor_data: pd.DataFrame
        """
        dw_load = FactorDownload(which=self.factors,
                                 freq=self._freq_map[self.freq])
        return dw_load.get()


class Sector(Core):
    type_name = 'sector'
    schema = ['Sec10']
    schema_cols = {'Sec10': ['NoDur', 'Durbl', 'Manuf', 'Enrgy', 'HiTec',
                             'Telcm', 'Shops', 'Hlth', 'Utils', 'Other']}

    def get(self, schema, frequency='monthly', start=None, end=None):

        self._data_dict = dict(frequency=frequency, schema=schema)
        self.freq = frequency
        store_path = self.path.joinpath(self.file_name(**self._data_dict))
        cols = self.schema_cols[schema]
        store = self._load_storage(store_path, columns=cols)

        return self._load_data(store, columns=cols, start=start, end=end)[cols]

    def download_details(self):
        """ Downloads Sector data from the FF data lib

        Returns
        -------
        sector_data: pd.DataFrame
        """
        dw_load = SectorDownload(which=self._data_dict['schema'],
                                 freq=self._freq_map[self.freq], )
        return dw_load.get()


class Portfolio(Core):
    type_name = 'portfolio'
    variables = ['BE-ME', 'ME', 'INV', 'VAR', 'MOM12', 'OP',
                 'CF-P', 'E-P', 'Beta', 'RES_VAR', 'D-P']

    def get(self, variable, frequency='monthly', start=None, end=None,
            cutoff='decile', weighting='mkt_cap'):

        self._data_dict = dict(frequency=frequency,
                               variable=variable,
                               cutoff=cutoff,
                               weighting=weighting)
        self.freq = frequency

        cols = CUTOFF_COLS[cutoff]
        store_path = self.path.joinpath(self.file_name(**self._data_dict))
        store = self._load_storage(store_path, columns=cols)

        return self._load_data(store, columns=cols, start=start, end=end)[cols]

    def download_details(self):
        """ Downloads portfolio data from the FF data lib

        Returns
        -------
        portfolio_data: pd.DataFrame
        """
        dw_load = PortfolioDownload(which=self._data_dict['variable'],
                                    freq=self._freq_map[self.freq],
                                    weighting=self._data_dict['weighting'],
                                    buckets=self._data_dict['cutoff'])
        return dw_load.get()


class FactorDownload:
    # add manually if needed more file names

    _file_factors = {'F-F_Research_Data_5_Factors_2x3':
                         ['Mkt-RF', 'SMB', 'HML', 'RF', 'RMW', 'CMA'],
                     'F-F_Momentum_Factor': ['MOM'],
                     'F-F_LT_Reversal_Factor': ['LTR'],
                     'F-F_ST_Reversal_Factor': ['STR']}

    _factors_file = storage.dict_reverse(_file_factors)
    _rename_cols = {'Mom   ': 'MOM', 'LT_Rev': 'LTR', 'ST_Rev': 'STR',
                    'Hlth ': 'Hlth'}

    def __init__(self, which=('Mkt-RF', 'SMB', 'HML', 'MOM', 'RF'), freq='m'):
        """ Fama French Data Handler (always downloads from page)

        Parameters
        ----------
        which: list
            which factors you want to download
        freq: str
            {'m', 'd', 'y'}
        """
        self.which = which
        self._data = None
        self.freq = freq

    def _find_files(self):
        """ Search files specified by datareader for required data

        Returns
        -------

        """

        to_find = set(self.which)
        file_names = list(set([self._factors_file[x] for x in to_find]))

        if self.freq == 'd':
            file_names = [x + '_daily' for x in file_names]

        download_details = list()
        for _ in file_names:
            download_details.append({'file_name': _, 'dict_key': 0})

        return download_details

    def get(self, start='19630701', end=None):
        """ Downloads all specified data from FF site

        Parameters
        ----------
        start: str/datetime
            when data should stat
        end: str/enddate
            when data should end

        Returns
        -------
        pd.DataFrame
            which timestamps index and which as columns
        """

        files_to_download = self._find_files()

        df = list()
        for file in files_to_download:
            df.append(self._download_data(file['file_name'], start_date=start,
                                          loc=file['dict_key']))
        df = pd.concat(df, axis=1)

        return df

    @staticmethod
    def print_data_sets(filter=None):
        data_sets = get_available_datasets()
        for data_set in data_sets:
            if filter is None:
                print(data_set)
            else:
                if filter in data_set:
                    print(data_set)

    @staticmethod
    def _download_data(name, start_date=None, end_date=None, loc=None,
                       columns=None):
        """ Downloads files via pandas_datareader

        Parameters
        ----------
        name: str
            FF file name
        start_date: str/datetime
            when data should start
        end_date: str/datetime
            when data should start
        loc: str
            key_name of relevant data set

        Returns
        -------
        pd.DataFrame
        """
        # load data from FF website
        df = web.DataReader(name, "famafrench", start=start_date,
                            end=end_date)[loc]

        # if df.columns.isin(FactorDownload._rename_cols.keys()).any():
        df.rename(columns=FactorDownload._rename_cols, inplace=True)

        if columns is not None:
            df = df[columns]

        # change PeriodIndex to timestamps
        try:
            df.index = df.index.to_timestamp(how='end')
        except AttributeError:
            pass

        return df / 100


class PortfolioDownload(FactorDownload):
    _data_sets = {'ME': 'Portfolios_Formed_on_ME',
                  'BE-ME': 'Portfolios_Formed_on_BE-ME',
                  'OP': 'Portfolios_Formed_on_OP',
                  'INV': 'Portfolios_Formed_on_INV',
                  'E-P': 'Portfolios_Formed_on_E-P',
                  'CF-P': 'Portfolios_Formed_on_CF-P',
                  'D-P': 'Portfolios_Formed_on_D-P',
                  'Beta': 'Portfolios_Formed_on_BETA',
                  # 'NI': 'Portfolios_Formed_on_NI,',
                  'VAR': 'Portfolios_Formed_on_VAR',
                  'RES_VAR': 'Portfolios_Formed_on_RESVAR',
                  'MOM12': '10_Portfolios_Prior_12_2',
                  }

    _buckets_cols = CUTOFF_COLS
    _weights_loc = {'mkt_cap': {'m':0,'y':2},'equal': {'m':1,'y':3}}

    def __init__(self, which='ME', freq='m', weighting='mkt_cap',
                 buckets='ff_style'):
        """ Fama French Data Handler (always downloads from page)
        # TODO implement local file_storage

        Parameters
        ----------
        which
        freq
        """
        self.which = which
        self._data = None
        self.freq = freq
        self.weighting =  weighting
        self.buckets = buckets

    def _find_files(self):
        raise NotImplementedError

    def get(self, start_date='19630701', end=None):

        dict_loc = self._weights_loc[self.weighting][self.freq]

        return self._download_data(self._data_sets[self.which], loc=dict_loc,
                                   columns=self._buckets_cols[self.buckets],
                                   start_date=start_date, end_date=end)


class SectorDownload(PortfolioDownload):
    _data_sets = {'Sec10': '10_Industry_Portfolios',
                  'Sec10d': '10_Industry_Portfolios_daily',
                  }

    def __init__(self, which='Sec10', freq='m'):
        if freq == 'd':
            which += 'd'

        super().__init__(which=which, weighting='mkt_cap', buckets='none')


def main():
    pass

if __name__ == '__main__':
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
    main()