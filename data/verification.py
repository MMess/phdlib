""" Data verification exercise by replication FF factor data bottom-up
"""
import logging
import numpy as np
import pandas as pd
from finance import asset_analysis, factor_analysis
from phdlib.data import manager
from phdlib.misc import logger_settings

logger = logging.getLogger(__name__)


def check_univariate_sorts():
    """ Re-constructs FamaFrench univariate FC portfolios and compares them

    Initially 'decile' portfolio are formed according the FF data description.
    The actual FF portfolio data is retrieved and benchmarked to it. The
    quality of the data is measured by its R^2s.
    Fits are very good for 'mom12m', 'size', good for 'ep' and 'bm' and
    reasonable for 'gma' and 'invest' given a slightly
    different implementations

    """

    start_date = '20050630'
    factors = { #factor name: (fama french name, ascending, rebalancing freq)
                'bm': ('BE-ME', False,12),
                'mve': ('ME', False,12),
                'invest': ('INV', False,12),
                'gma': ('OP', False, 12),
                'ep': ('E-P', False, 12),
                'mom12m': ('MOM12', False, 1)
               }
    data = manager.get_monthly_data(columns=list(factors.keys()),
                                    normalize=False,
                                    fc_lead=0,
                                    start_date=start_date)
    alt_data = manager.get_monthly_data(columns=list(factors.keys()),
                                        normalize=False,
                                        fc_lead=0,
                                        exchanges=(1,),
                                        start_date=start_date)
    buckets = 'decile'
    factor_rsqrd = list()

    for factor, val in factors.items():
        ff_var_name = val[0]
        logger.info('Running currently factor {}'.format(factor))
        # get criteria in correct format
        criteria = data.pivot(index='date', columns='PERMNO', values=factor)
        alt_criteria = alt_data.pivot(index='date', columns='PERMNO',
                                      values=factor)
        # calculate univariate factors
        logger.info('Calculating portfolio returns...')
        factor_constructor = factor_analysis.FactorAnalysis(
            criteria, buckets=buckets, ascending=val[1],
            trade_freq=val[2], weighting='mkt_cap', alt_criteria=alt_criteria)

        pf_rets = factor_constructor.portfolio.pf_returns()

        pf_rets.dropna(inplace=True)
        # get fama_french returns on this uniform data
        if factor == 'mom12m':
            tmp_bucket = 'mom_style'
        else:
            tmp_bucket = buckets
        ff_pf = manager.FamaFrenchPortfolios(which=ff_var_name,
                                             weighting='mkt_cap',
                                             buckets=tmp_bucket)
        ff_portfolio = ff_pf.get(start_date=start_date)
        # rename columns to
        tmp_cols = manager.FamaFrenchPortfolios._buckets_cols[buckets]
        logger.info(
            f'Renaming PF rets columns {pf_rets.columns} to {tmp_cols}')
        pf_rets.columns = tmp_cols
        if factor == 'mom12m':
            ff_portfolio.columns = tmp_cols

        reg_res = dict()
        # run regression on factor
        for column in pf_rets.columns:

            tmp_ret_analysis = asset_analysis.ReturnAnalysis(
                pf_rets[column], ff_portfolio[column])
            logger.info('Results of {}'.format(factor))
            ols = tmp_ret_analysis.bm_regression()
            logger.info(ols.summary())
            reg_res[column]= ols.rsquared

        tmp_ser = pd.Series(reg_res)
        tmp_ser.name = factor
        factor_rsqrd.append(tmp_ser)

    factor_rsqrd = pd.concat(factor_rsqrd, axis=1).transpose()
    logger.info(f'The following R^2\'s are measured: \n{factor_rsqrd}')


def calculate_quantiles():
    """ Prints the percentiles as provided by FF data, uses NYSE stocks
    """

    factors = ['mom12m']
    start_date = '20050630'
    data = manager.get_monthly_data(columns=list(factors),
                                    normalize=False,
                                    fc_lead=0,
                                    exchanges=(1,),
                                    start_date=start_date)

    for factor in factors:
        criteria = data.pivot(index='date', columns='PERMNO', values=factor)
        qts = criteria.quantile(np.arange(0,1,0.05), axis=1)
        print(qts)




if __name__ == '__main__':
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('INFO')
    check_univariate_sorts()
