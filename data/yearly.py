""" Yearly FC calculations (mostly all Compustat based factors
"""
import datetime as dt
import logging

import numpy as np
import pandas as pd

from phdlib import settings
from phdlib.data import monthly, sector_data
from phdlib.misc import logger_settings, functions

logger = logging.getLogger(__name__)

FC_CS = ('ep', 'dy', 'bm', 'lev', 'currat', 'pchcurrat', 'quick',
         'quick', 'pchquick', 'salecash', 'salerec', 'saleinv',
         'pchsaleinv', 'cashdebt', 'depr', 'pchdepr', 'sgr', 'sp',
         'pchsale_pchinvt',
         'pchsale_pchrect', 'pchgm_pchsale', 'pchsale_pchxsga',
         'chinv', 'pchemp', 'hire', 'cfp', 'acc', 'rd', 'lgr',
         'egr', 'rd_sale', 'rd_mve', 'agr', 'cashpr', 'gma', 'cash',
         'pctacc', 'absacc', 'roic', 'grcapex', 'tang', 'invest',
         'age', 'divi',
         'divo', 'pchcapx', 'chato', 'chpm')

def get_link_data():
    """ Loads the link-table data from csv file and prepares the raw data

    Returns
    -------
    pd.DataFrame
    """

    link_data = pd.read_csv(settings.LINK_DATA,
                            usecols=['gvkey', 'LPERMNO', 'LPERMCO', 'LINKDT',
                                     'LINKENDDT', 'FYRC'],
                            engine='c',
                            compression='gzip', low_memory=False)
    # format date
    link_data['LINKDT'] = pd.to_datetime(link_data['LINKDT'],
                                         format='%Y%m%d')
    # end default for existing data null
    link_data.loc[link_data['LINKENDDT'] == 'E', ['LINKENDDT']] = np.nan
    link_data['LINKENDDT'] = pd.to_datetime(link_data['LINKENDDT'],
                                            format='%Y%m%d')

    link_data.dropna(how='all', subset=['LPERMNO'], inplace=True)
    link_data.reset_index(inplace=True, drop=True)
    # make an offset of one month to align
    link_data['LINKENDDT'] = link_data['LINKENDDT'].fillna(
            dt.datetime(2014, 12, 31))
    link_data['LINKENDDT'] = pd.DatetimeIndex(link_data['LINKENDDT'])
    link_data['LINKDT'] = pd.DatetimeIndex(link_data['LINKDT'])

    return link_data


def get_compustat_data():
    """ Returns all yearly raw compustat data as given by the WRDS
    :return:
    """
    compustat_df = pd.read_csv(settings.CS_YEARLY, engine='c',
                               compression='gzip', low_memory=False)
    compustat_df['datadate'] = pd.to_datetime(compustat_df['datadate'],
                                              format='%Y%m%d')
    return compustat_df


def clean_compustat_data(compustat, link_data):
    """ Cleans and prepared the CS data

    Parameters
    ----------
    compustat: pd.DataFrame
        compustat accounting data
    link_data: pd.DataFrame
        crsp-compustat link table information
    Returns
    -------
    pd.DataFrame
        cleaned version of compustat variable
    """

    # all entries where no permno entry is available are removed
    logger.info('Num of entries in cs table {}'.format(compustat.shape[0]))
    gv_unique_list = link_data['gvkey'].unique().tolist()
    compustat.drop(
            compustat[compustat.gvkey.isin(gv_unique_list) == False].index,
            inplace=True)
    compustat.drop(compustat[compustat.indfmt == 'FS'].index, inplace=True)
    logger.info('Num of entries in cs table {} after drop non permno entry'
                'gvs'.format(compustat.shape[0]))

    # replace NAs by zero for the following columns (Green et al 2014, page 39)
    zero_columns = ['xrd', 'fatl', 'fatb', 'dm', 'emp', 'intan', 'dcvt', 'lco',
                    'aco', 'dp', 'rect', 'invt', 'ap', 'dvt', 'ao', 'che',
                    'nopi', 'lo', 'at']

    # replace NAs in columns as in the list of Green et al 2014
    for col_name, col in compustat.iteritems():
        if col_name in zero_columns:
            compustat[col_name] = col.fillna(0)
            logger.info('NAs entries of column {} replaced by zero'.format(
                col_name))

    # TODO clean infmt FS vs INDL !?
    # FS--------------------------
    # Financial Services (includes banks, insurance companies, broker/dealers,
    # real estate and other financial services)
    # INDL-------------------------
    # Industrial (includes companies reporting manufacturing, retail,
    # construction and other commercial operations other than financial
    # services)
    # ISSUE-----------------------
    # Issue-level Fundamentals
    # BANK------------------------
    return compustat


def calculate_accounting_stats(acc_df, which=None):
    """Calculates commonly used accounting ratios

    Parameters
    ----------
    acc_df: dict
        a dict of accounting data. key-> cs name, value -> pandas.DataFrame
    which: list
        which accounting data are desired

    Returns
    -------

    """
    # ['emp']

    # set up list
    if which is None:
        which =FC_CS

        """
        Overview over raw compustat abbreviations used to calculate
        act (current assets)
        at (total assets)
        capx (capital expenditure)
        ceq (book value of equity)
        che (cash and cash equivalents)
        cogs (costs of goods sold)
        dlc (Debt in Current Liabilities - Total)
        dltt (Long-Term Debt - Total )
        dp (depreciation and amortization)
        dvt (total dividends)
        ebit (Earnings Before Interest and Taxes)
        emp (employees)
        ib (annual income before extraordinary items)
        invt (inventories)
        lct (current liabilities)
        lt (total liabilities)
        nopi (Non-operating Income/Expense)-
        oancf (Operating Activities Net Cash Flow)
        ppegt (gross Property, Plant and Equipment)
        rect (accounts receivable)
        sale (sales)
        txp (Income Taxes Payable)
        xrd (Research and Development Expense)
        xsga (Selling, General and Administrative Expense)
        """

    # ep: earning-price ratio (earnings/mkt_cap)
    if 'ep' in which:
        acc_df['ep'] = acc_df['ib'] / acc_df['mkt_cap']

    # dy: dividend ratio (total dvd/mkt cap)
    if 'dy' in which:
        acc_df['dy'] = acc_df['dvt'] / acc_df['mkt_cap']

    # bm: book-to-market
    if 'bm' in which:
        acc_df['bm'] = acc_df['ceq'] / acc_df['mkt_cap']

    # lev: leverage ratio (liabilities/mkt_cap)
    if 'lev' in which:
        acc_df['lev'] = acc_df['lt'] / acc_df['mkt_cap']

    # currat: current ratio
    if 'currat' in which or 'pchcurrat' in which:
        acc_df['currat'] = acc_df['act'] / acc_df['lct']

    # pchcurrat: percentage change in current ratio
    if 'pchcurrat' in which:
        acc_df['pchcurrat'] = acc_df['currat'].pct_change()

    # quick: quick ratio (current assets - inventory)/current liabilities
    if 'quick' in which:
        acc_df['quick'] = (acc_df['act'] - acc_df['invt']) / \
                          acc_df['lct']

    # pchquick: percentage change in quick ratio
    if 'pchquick' in which:
        acc_df['pchquick'] = acc_df['quick'].pct_change()

    # salecash:
    if 'salecash' in which:
        acc_df['salecash'] = acc_df['sale'] / acc_df['che']

    # salerec:
    if 'salerec' in which:
        acc_df['salerec'] = acc_df['sale'] / acc_df['rect']

    # saleinv:
    if 'saleinv' in which or 'pchsaleinv' in which:
        acc_df['saleinv'] = acc_df['sale'] / acc_df['invt']

    # pchsaleinv:
    if 'pchsaleinv' in which:
        acc_df['pchsaleinv'] = acc_df['saleinv'].pct_change()

    # cash debt
    if 'cashdebt' in which:
        acc_df['cashdebt'] = (acc_df['ib'] - acc_df['dp']) / \
                             acc_df['lt']

    # depr
    if 'depr' in which:
        acc_df['depr'] = acc_df['dp'] / acc_df['ppegt']

    # pchdepr
    if 'pchdepr' in which:
        acc_df['pchdepr'] = acc_df['depr'].pct_change()

    # sgr (annual sales change)
    if 'sgr' in which:
        acc_df['sgr'] = acc_df['sale'].pct_change()

    # sp (sales to price (mkt cap))
    if 'sp' in which:
        acc_df['sp'] = acc_df['sale'] / acc_df['mkt_cap']

    # pchsale_pchinvt (Difference of pct che in sales and inventory )
    if 'pchsale_pchinvt' in which:
        acc_df['pchsale_pchinvt'] = acc_df['sale'].pct_change() - \
                                    acc_df['invt'].pct_change()

    # pchsale_pchrect (Difference of pct che in sales and receivables )
    if 'pchsale_pchrect' in which:
        acc_df['pchsale_pchrect'] = acc_df['sale'].pct_change() - \
                                    acc_df['rect'].pct_change()

    if 'pchgm_pchsale' in which:
        acc_df['pchgm_pchsale'] = (acc_df['sale'] -
                                   acc_df[
                                                'cogs']).pct_change() - \
                                  acc_df['sale'].pct_change()

    if 'pchsale_pchxsga' in which:
        acc_df['pchsale_pchxsga'] = acc_df['sale'].pct_change() - \
                                    acc_df['xsga'].pct_change()

    if 'chinv' in which:
        acc_df['chinv'] = acc_df['invt'].diff() / acc_df['at']

    if 'pchemp' in which:
        acc_df['pchemp'] = acc_df['emp'].pct_change()

    if 'hire' in which:
        acc_df['hire'] = acc_df['emp'].pct_change()

    if 'cfp' in which:
        tmp_cash_flow = acc_df['oancf']
        # ib - act.diff - che.diff - lct.diff + dlc.diff + (txp-dp).diff
        alt_cash_flow = acc_df['ib'].fillna(0) \
                        - acc_df['act'].diff().fillna(0) \
                        - acc_df['che'].diff().fillna(0) \
                        - acc_df['lct'].diff().fillna(0) \
                        + acc_df['dlc'].diff().fillna(0) \
                        + (acc_df['txp'] - acc_df['dp']).diff().fillna(0)
        # replace missing cash flow data by alternative
        tmp_cash_flow.where(acc_df['oancf'].isnull() == False,alt_cash_flow,
                            inplace=True)
        acc_df['cfp'] = tmp_cash_flow / acc_df['mkt_cap']

    # working capital accruals
    if 'acc' in which or 'absacc' in which:
        tmp_cash_flow = acc_df['oancf']
        # ib - act.diff - che.diff - lct.diff + dlc.diff + (txp-dp).diff
        alt_cash_flow = acc_df['ib'].fillna(0) \
                        - acc_df['act'].diff().fillna(0) \
                        - acc_df['che'].diff().fillna(0) \
                        - acc_df['lct'].diff().fillna(0) \
                        + acc_df['dlc'].diff().fillna(0) \
                        + (acc_df['txp'] - acc_df['dp']).diff().fillna(0)
        # replace missing cash flow data by alternative
        tmp_cash_flow.where(acc_df['oancf'].isnull() == False,alt_cash_flow,
                            inplace=True)
        acc_df['acc'] = (acc_df['ib'] - tmp_cash_flow) / acc_df['at']

    # R&D increase
    if 'rd' in which:
        acc_df['rd'] = 1
        # is 0 if increase relative to total assets below 5%
        acc_df['rd'] = acc_df['rd'].where(
            (acc_df['xrd'] / acc_df['at']).pct_change() > 0.05,0)

    # annual pct change in long term debt
    if 'lgr' in which:
        acc_df['lgr'] = acc_df['lt'].pct_change()

    # annual pct change in book equity
    if 'egr' in which:
        acc_df['egr'] = acc_df['ceq'].pct_change()

    # R%D to sales
    if 'rd_sale' in which:
        acc_df['rd_sale'] = acc_df['xrd']/acc_df['sale']

    # R&D to mkt_cap
    if 'rd_mve' in which:
        acc_df['rd_mve'] = acc_df['xrd']/acc_df['mkt_cap']

    # asset growth
    if 'agr' in which:
        acc_df['agr'] = acc_df['at'].pct_change()

    # cash productivity
    if 'cashpr' in which:
        acc_df['cashpr'] = (acc_df['mkt_cap'] + acc_df['dltt'] \
                            - acc_df['at'])/ acc_df['che']

    # Gross profitability
    if 'gma' in which:
        # divided by lagged total assets
        acc_df['gma'] = (acc_df['sale']-acc_df['cogs'])/acc_df['at'].shift(1)

    # cash-to-assets
    if 'cash' in which:
        acc_df['cash'] = acc_df['che']/acc_df['at']

    # Percent accruals
    if 'pctacc' in which:
        tmp_cash_flow = acc_df['oancf']
        # ib - act.diff - che.diff - lct.diff + dlc.diff + (txp-dp).diff
        alt_cash_flow = acc_df['ib'].fillna(0) \
                        - acc_df['act'].diff().fillna(0) \
                        - acc_df['che'].diff().fillna(0) \
                        - acc_df['lct'].diff().fillna(0) \
                        + acc_df['dlc'].diff().fillna(0) \
                        + (acc_df['txp'] - acc_df['dp']).diff().fillna(0)
        # replace missing cash flow data by alternative
        tmp_cash_flow.where(acc_df['oancf'].isnull() == False, alt_cash_flow,
                            inplace=True)
        tmp_ib = acc_df['ib'].where(acc_df['ib'] != 0, 0.01)
        acc_df['pctacc'] = (tmp_ib- tmp_cash_flow)/tmp_ib.abs()

    # absolute value of acc
    if 'absacc' in which:
        acc_df['absacc'] = acc_df['acc'].abs()

    # Return on invested capital
    if 'roic' in which:
        acc_df['roic'] = (acc_df['ebit'] - acc_df['nopi']) / (acc_df['ceq'] +
                                                              acc_df['lt'] -
                                                              acc_df['che'])

    # Pct change over two year growth in capital expenditures (CAPEX)
    if 'grcapex' in which:
        acc_df['grcapex'] = acc_df['capx'].diff(2)

    # Debt capacity-to-firm tangibility
    if 'tang' in which:
        acc_df['tang'] = (acc_df['che'] + 0.715 * acc_df['rect'] +
                          0.547 * acc_df['invt'] + 0.535 * acc_df['ppegt'])\
                         / acc_df['at']

    # CAPEX in inventory
    if 'invest' in which:
        acc_df['invest'] = (acc_df['ppegt'].diff() + acc_df['invt'].diff()) / \
                           acc_df['at'].shift()

    # age in years (first compustat coverage)
    if 'age' in which:
        days = (acc_df.index - acc_df.index.min()).map(
            lambda x: x.days)
        acc_df['age'] = days/365.25

    # dividend initiation (year t-1 no dividends, year t dividends paid)
    if 'divi' in which:
        acc_df['divi'] = 1
        acc_df['divi'] = acc_df['divi'].where(acc_df['dvt'].shift() == 0, 0)
        acc_df['divi'] = acc_df['divi'].where(acc_df['dvt'] > 0, 0)

    # dividend omission (year t no dividends, year t-1 dividends paid)
    if 'divo' in which:
        acc_df['divo'] = 1
        acc_df['divo'] = acc_df['divo'].where(acc_df['dvt'].shift() > 0, 0)
        acc_df['divo'] = acc_df['divo'].where(acc_df['dvt'] == 0, 0)

    # change in capital expenditures (only for ia usage)
    if 'pchcapx' in which:
        acc_df['pchcapx'] = acc_df['capx'].pct_change()

    # change in asset turnover (only later for ia usage)
    if 'chato' in which:
        acc_df['chato'] = (acc_df['sale']/acc_df['at']).diff()

    if 'chpm' in which:
        acc_df['chpm'] = (acc_df['ib']/acc_df['sale']).diff()

    # TODO nincr
    # TODO indmom
    # TODO ps

    return acc_df


def get_monthly_account_stats(compustat_df, crsp_data, link_data,
                              which=FC_CS, data_lag=6, as_dict=True):
    """ Aligns cs, crsp and link_data and transforms yearly to monthly freq

    Parameters
    ----------
    compustat_df: pd.DataFrame
        containing all available COMPUSTAT accounting data
    crsp_data: pd.DataFrame
        stacked DataFrame of mkt_cap variable
    link_data: pd.DataFrame
        CRSP-COMPUSTAT link table
    which: list
        which variables should be calculated
    data_lag: how long should accounting data lag be. For example, data_lag of
        6 implies that data of fiscal year-end 12/31/t are assigned to 6/30/t+1
    as_dict: bool

    Returns
    -------

    """
    stocks_data = list()
    link_data.drop_duplicates(subset=['gvkey', 'LPERMNO', 'LINKDT'],
                              inplace=True)
    num_rows = len(link_data.index)

    # loop through each entry in link_data in case one permno for many gv-keys
    for index, row in link_data[:].iterrows():
        logging.info('Number of rows companies : {0} of {1} '
                     '({2:.3}%) - Current PERMNO {3}'.format(
                index, num_rows, index / num_rows * 100, int(row['LPERMNO'])))

        tmp_df = compustat_df.loc[compustat_df['gvkey'] == row['gvkey'], :]
        # delete data prior to link start
        tmp_df = tmp_df.loc[tmp_df['datadate'] > row['LINKDT'], :]
        if len(tmp_df.index) < 1:
            logger.warning('No data for PERMNO: {}'.format(row['LPERMNO']))
            continue
        # get current crsp data via PERMNO
        tmp_crsp = crsp_data.loc[crsp_data['PERMNO'] == row['LPERMNO'], :]
        # merge data of CRSP and CS via exact data dates
        tmp_df = tmp_df.merge(tmp_crsp, how='left', left_on='datadate',
                              right_on='date')
        # set index to datetime index
        tmp_df.set_index('datadate', inplace=True)
        # get accounting results
        tmp_df = calculate_accounting_stats(tmp_df, which=which)
        # logger.debug('Index tmp_df {}'.format(tmp_df.index))
        # change frequency from yearly to monthly data
        tmp_df = tmp_df.asfreq('m', method='ffill')
        # TODO WHY needed (not wrong in any case, eventually redundant
        tmp_df = tmp_df.reindex(pd.date_range(
                start=tmp_df.index[0], end=row['LINKENDDT'], freq='m'))
        # data shift for data-lag-periods, to avoid backward looking bias
        tmp_df = tmp_df.shift(data_lag)
        # ensure data is only used for the length of CRSP-CS link dates
        tmp_df = tmp_df.loc[row['LINKDT']:row['LINKENDDT']]
        # assign PERMNO ID to current data
        tmp_df['PERMNO'] = row['LPERMNO']
        tmp_df.dropna(subset=['gvkey'], inplace=True)
        stocks_data.append(tmp_df)

    acc_data = pd.concat(stocks_data)
    acc_data.reset_index(inplace=True)
    acc_data.drop('date', inplace=True, axis=1)
    acc_data.rename(columns={'index': 'date'}, inplace=True)
    acc_data['ones'] = 1

    # drop duplicates GV PERMNO Date
    logger.info('Dropping NAs...')
    acc_data.dropna(subset=['gvkey'], inplace=True)
    logger.info('Dropping duplicates...')
    acc_data.drop_duplicates(subset=['gvkey', 'PERMNO', 'date'], inplace=True)

    # check if duplicates are still present
    grouped = acc_data.groupby(by=['date', 'PERMNO'])
    permno_list = list()
    for name, group in grouped:
        if len(group) > 1:
            logger.warning('The following PERMNO has double entries {}'.format(
                    group['PERMNO'].values[0]))
            permno_list.append(group['PERMNO'].values[0])

    if len(permno_list) > 0:
        permno_set = set(permno_list)
        logger.warning('the following PERMNOs have double occurences: \n {}'.format(
            permno_set))

    acc_data.drop_duplicates(subset=['PERMNO', 'date'], inplace=True)

    logger.debug('Groupby {}'.format((acc_data.groupby(['date', 'PERMNO']).count() > 1).any()['ones']))
    # check if date-permno entries are unique
    if (acc_data.groupby(['date', 'PERMNO']).count() > 1).any()['ones']:
        logger.warning('Non unique date entry for some PERMNO, check data!')

    # create dict of accounting data (format for dumping data)
    if as_dict:
        logger.info('Create dict of pivoted accounting stats')
        acc_dict = dict()
        # add all data to dictionary
        for acc_stat in which:
            acc_dict[acc_stat] = acc_data.pivot(index='date', columns='PERMNO',
                                                values=acc_stat)
        return acc_dict
    else:
        return acc_data


def save_data_tmp(data_dict):
    """The function saves data to temporary folder
    :param data_dict:
    :return:
    """

    for key, value in data_dict.items():
        logger.info(f'Saving {key} to tmp data folder...')
        file_name = '.'.join([key, 'csv'])
        value.to_csv(str(settings.TMP_PATH.joinpath(file_name)))


def calculate_ia_adjusted_country_stats(acc_data, which=None):
    """Calculates industry adjusted accounting measures
    :param acc_data: dict,
        a dict with fc name as key and the value a pandas data frame
    :param which: list (default=None),
        a list of which fc (keys of the dict) should be industry adjusted
    :return: dict,
        returns the original dict, augmented by the industry adjusted data
    """

    # use "sector_48" to for bm, mve, and cfp, for the rest use "sector_sic2"
    sec_item = 'sector_sic2'

    crsp_sectors = monthly.get_crsp_monthly([sec_item, 'shr_code'])

    # replace all sector data with share codes not 10, 11 with NAs
    crsp_sectors[sec_item].where(crsp_sectors['shr_code'].isin([10, 11]),
                                    inplace=True)
    acc_data[sec_item] = crsp_sectors[sec_item]

    logger.info('Prior panel: {}'.format(acc_data))
    acc_data = pd.Panel(acc_data)
    logger.info('After panel: {}'.format(acc_data))
    acc_data = sector_data.calculate_sector_adj_fc(acc_data, which=which,
                                                   sec_item=sec_item)

    return dict(acc_data)


def calc_industry_adjusted():
    """ Calculates sector adjusted data based on tmp saved FC
    :return:
    """

    # which = ['cfp', 'bm', 'pchemp']  # with sector_48
    # which = ['mve'] # with log_diff true (adjust sector fun) and sector_48
    #  the run as ia with sector_sic2
    which = ['chato', 'chpm', 'pchcapx']
    acc_data = dict()

    # load FC of interest
    for fc in which:

        if fc == 'mve':
            acc_data[fc] = monthly.get_crsp_monthly(['mkt_cap'])[
                'mkt_cap']
            continue

        file_name = '.'.join([fc, 'csv', 'gz'])
        logger.info('Loading file {}...'.format(file_name))
        acc_data[fc] = pd.read_csv(str(settings.TMP_PATH.joinpath(file_name)),
                                   index_col='date', engine='c',
                                   parse_dates=True)

        acc_data[fc].columns = [float(x[:-2]) for x in acc_data[fc].columns]
        acc_data[fc].columns.name = 'PERMNO'
    # calculate sector adjusted FC
    acc_data = calculate_ia_adjusted_country_stats(acc_data, which=which)

    # return acc_data
    # save data to tmp path
    for key, value in acc_data.items():
        # save only industry adjusted
        if key[-3:] == '_ia':
            logger.info('Saving {} to tmp data folder...'.format(key))
            file_name = '.'.join([key, 'csv'])
            value.to_csv(str(settings.TMP_PATH.joinpath(file_name)))


def main():
    start_datetime = dt.datetime.now()

    #  get data needed for calculations
    link_data = get_link_data()
    compustat_df = get_compustat_data()
    compustat_df = clean_compustat_data(compustat_df, link_data)
    crsp_data = monthly.get_crsp_monthly(['mkt_cap'])
    crsp_data = crsp_data['mkt_cap'].stack().reset_index().rename(
            columns={0: 'mkt_cap'})

    crsp_data['mkt_cap'] /= 1000000
    # main program run
    acc_data = get_monthly_account_stats(compustat_df, crsp_data, link_data,
                                         which=['roic'])
    # acc_data = calculate_ia_adjusted_country_stats(acc_data, which=['cfp'])

    # calc_industry_adjusted()
    save_data_tmp(acc_data)

    # log run time information
    end_datetime = dt.datetime.now()
    run_time_str = functions.run_time_string(start_datetime, end_datetime)
    logger.info('\n'.join(['year_data has finished', run_time_str]))


if __name__ == '__main__':

    logging.config.dictConfig(logger_settings.log_set_ups)
    logging.root.setLevel('DEBUG')
    main()
