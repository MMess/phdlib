import logging
import logging.config
import numpy as np
import pandas as pd

from phdlib import settings
from phdlib.misc import logger_settings

logger = logging.getLogger(__name__)


def get_sector_mkt_cap():
    """ The function returns the mkt cap of the sectors

    :return:
    """
    from phdlib.data import monthly
    logger.info('Prepare sector data')
    crsp_monthly = monthly.get_crsp_monthly(['mkt_cap', 'mve', 'sector_48'])

    total_mkt_cap = crsp_monthly['mkt_cap'].sum(axis=1)
    siccd_mkt_cap = crsp_monthly['mkt_cap'][
        crsp_monthly['sector_48'].isnull() == False].sum(
            axis=1)

    logger.info('Plot sector mkt cap ratio')
    (siccd_mkt_cap/total_mkt_cap).plot()
    # pd.concat([total_mkt_cap,siccd_mkt_cap]).plot()
    return crsp_monthly


def calculate_sector_adj_fc(panel_data, sec_item='sector_48',
                            suffix='_ia', log_diff=False, which=None):
    """ Returns a sector demeaned characteristics
    :param panel_data: pd.Panel,
        with a time index on the major axis, and the ID on the minor
    :param sec_item:
    :param suffix:
    :param log_diff: Bool, default=False
        if log(x) - log(mean(x)) or x - mean(x), def
    :param which: list(default=None),
        a list of fc which should be industry adjusted
    :return:  panel with an additional item sector de-meaned
    """
    # get FC name as set of different from sector item
    if which is None:
        fc_list = list(set(panel_data.items) -set([sec_item]))
    else:
        fc_list = which

    # loop through each fc which should be industry adjusted
    for fc_name in fc_list:
        logger.info('Calculating ind adjusted fc for {} ...'.format(fc_name))
        # create name with corresponding suffix
        fc_name_ia = ''.join([fc_name, suffix])

        # stack panel to make group by feasible
        stacked_panel = panel_data[[fc_name, sec_item]].to_frame()
        grouped = stacked_panel.groupby(by=sec_item)
        data_list = list()

        # loop through each sector and calculate mean
        for name, sec_df in grouped:
            if log_diff:
                sec_df_ia = np.log(
                    sec_df.astype('float64') / sec_df.mean(level=0).astype(
                        'float64'))
            else:
                sec_df_ia = sec_df - sec_df.mean(level=0)

            logger.debug('Sector Name : {} \n df: \n {}'.format(name, sec_df_ia))
            data_list.append(sec_df_ia)

        fc_ia = pd.concat(data_list)
        fc_ia.rename(columns={fc_name: fc_name_ia}, inplace=True)
        fc_ia = fc_ia.reset_index().pivot(index='date', columns='PERMNO',
                                          values=fc_name_ia)
        panel_data[fc_name_ia] = fc_ia

    return panel_data


def get_sic_sector_table():
    """The function returns a table matching SICCD and 48 Sectors Codes
    :param db_connection:
    :return:
    """
    sector_table = pd.read_csv(str(settings.SECTOR),
                               usecols=['ID','Code', 'Level', 'Parent'])

    # get all three levels of data
    level1 = sector_table.drop(
        sector_table.loc[sector_table['Level'] != 1, :].index)
    level2 = sector_table.drop(
        sector_table.loc[sector_table['Level'] != 2, :].index)
    level3 = sector_table.drop(
        sector_table.loc[sector_table['Level'] != 3, :].index)

    # merge data (dependence like a tree - level by level)
    level = level1.merge(level2, left_on='ID', right_on='Parent',
                         suffixes=('_level1', '_level2'), how='left')
    sic_sector = level3.merge(level, left_on='Parent', right_on='ID_level2',
                              how='left')
    sic_sector = sic_sector[['Code', 'Code_level1']].rename(
        columns={'Code': 'SICCD', 'Code_level1': 'Sector_ID'})
    sic_sector['SICCD'] = sic_sector['SICCD'].astype(float)

    return sic_sector


def parse_sector_data_ff(sector_df):
    """ Parses a text file where information of sectors and data are saved
    :param sector_df:
    :return:
    """

    sector_list = list()
    sic_data = list()
    sector_id = 0

    for index, row in sector_df.iterrows():
        tmp_string =  row[0].split(';')
        if len(tmp_string[0])<3:
            logging.info('Current Industry {0[0]}, {0[1]}'.format(tmp_string))
            sector_id+=1
            parent_id1=sector_id
            # ID, Name, Parent, Scheme, Level, Code
            sector_list.append([sector_id,tmp_string[1], np.nan,'SIC',
                                1,tmp_string[0] ])
        else:
            logging.info('Current SubSIC {0[0]}, {0[1]}'.format(tmp_string))
            sector_id+=1
            parent_id2=sector_id
            sector_list.append([sector_id,tmp_string[1], parent_id1,'SIC', 2,
                                tmp_string[0]])
            start_sic = int(tmp_string[0].split('-')[0])
            end_sic = int(tmp_string[0].split('-')[1])
            for sic in range(start_sic,end_sic+1):
                sector_id+=1
                sic_data.append([sic,sector_id ])
                sector_list.append([sector_id, tmp_string[1], parent_id2,'SIC',
                                    3,sic])

    sector_df = pd.DataFrame(sector_list, columns=['ID', 'Name', 'Parent',
                                                   'Scheme', 'Level', 'Code'])

    return sector_df


def main():
    logging.config.dictConfig(logger_settings.log_set_ups)
    logging.root.setLevel('INFO')

    # db_connection = create_engine(settings.SQL_URL)

    # file = settings.RAW_DATA_PATH.joinpath('Siccodes48.txt')
    # data = pd.read_table(str(file), header=None)

    # sector_df =  parse_sector_data_ff(data)
    #
    # sector_df.to_sql('sectors', db_connection, if_exists='append',
    #                  chunksize=100, index=False )

    # sic_sector = get_sic_sector_table(db_connection)



    return get_sector_mkt_cap()

if __name__ == '__main__':
   main()
