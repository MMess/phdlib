import logging
import pandas as pd
from sqlalchemy import create_engine

logger = logging.getLogger(__name__)

from phdlib.misc import logger_settings
from phdlib import settings
from phdlib.data import monthly



def market_weighted_averages():
    beta = pd.read_csv(str(settings.TMP_PATH.joinpath('tmp_beta.csv')),
                       index_col='date', parse_dates=['date'])
    beta = beta.resample('M').ffill()

    beta = beta.stack().reset_index().rename(
        columns={'level_1': 'PERMNO', 0: 'beta'})
    beta['PERMNO'] =  beta['PERMNO'].astype(int)
    # beta = data_manager.get_monthly_data(columns=['beta', 'PERMNO'],
    #                                       normalize=False)
    mkt_cap = monthly.get_crsp_monthly(['mkt_cap'])['mkt_cap']
    mkt_cap = mkt_cap.stack().reset_index().rename(columns={'level0': 'date',
                                                            'level1': 'PERMNO',
                                                            0: 'mkt_cap'})

    beta = beta.merge(mkt_cap, left_on=['PERMNO', 'date'],
                      right_on=['PERMNO', 'date'], how='left')

    beta_grouped = beta.groupby(by='date')
    mkt_mean = list()
    date_index = list()
    for date, group  in beta_grouped:
        logger.info('Current Date: {}'.format(date))
        tmp_data = group['mkt_cap'].where(group['beta'].isnull()==False)
        weight = tmp_data/ tmp_data.sum()
        mkt_mean.append( (weight*group['beta']).sum())
        date_index.append(date)

    mv_weighted_mean = pd.DataFrame({'mkt_mean': mkt_mean, 'mean':
        beta_grouped['beta'].mean()}, index=date_index)

    return mv_weighted_mean


def check_beta():

    # Get weekly data of stocks
    file_name = 'WeeklyCRSP_0.csv'
    logger.info('Reading weekly stock data...')
    df = pd.read_csv(str(settings.RAW_DATA_PATH.joinpath(file_name)),
                     index_col='date', parse_dates=True)
    # Get weekly market data (FF data)
    db_con = create_engine(settings.SQL_URL)
    logger.info('Reading weekly FF data from SQL...')
    ff_data = pd.read_sql('ff_weekly', db_con, index_col='index')

    # Estimate betas
    permnos = [10006]
    min_obs_ols = 52

    for permno in permnos:
        y_tmp = df.loc[:,str(permno)].dropna()
        x_tmp, y_tmp = ff_data[['Market', 'RF']].align(y_tmp, axis=0,
                                                       join='right')

        # use excess returns
        # y_tmp = y_tmp - x_tmp['RF']
        tmp_res = pd.stats.ols.MovingOLS(y=y_tmp, x=x_tmp['Market'],
                                         window_type='rolling', window=156,
                                         min_periods=min_obs_ols)
        tmp_beta = tmp_res.beta['x']
        tmp_beta.to_clipboard()



def main():
    logging.config.dictConfig(logger_settings.log_set_ups)
    logger.setLevel('DEBUG')
    return market_weighted_averages()


if __name__ == '__main__':

    sr = main()
