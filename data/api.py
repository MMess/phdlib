from phdlib.data.manager import get_monthly_data, fc_years, published_FC_set
from phdlib.data.results import LoadLocalResult, LocalResults
from phdlib.data.monthly import get_crsp_monthly
from phdlib.data.tools import r2_trans, weighted_corr_mat
try:
    from phdlib.data.fama_french import Factor, Portfolio, Sector
except ImportError:
    print('pandas_data_reader not installed - FF factors not available')
