"""
The file centralizes all settings relevant for the research library

"""
from platform import system
from pathlib import Path
from os.path import expanduser
from getpass import getuser

ROOTS = {'Windows': 'D:\\Database\\',
         'Linux': '/'.join([expanduser('~'), 'Database/'])
         }

if system() == 'Windows':

    database = ROOTS['Windows']
    user =  getuser()

    DEEP_PATH = Path('C:\\Users\\' + user + '\\Dropbox\\PhD\\Research\\' \
                    'deep_learning_cross_section\\Latex\\')
    LASSO_PATH = Path('C:\\Users\\' + user + '\\Dropbox\\PhD\\Research\\'
                    'Paper_1_AdaptiveLassoZoo\\Latex\\')
    FM_POLS_PATH = Path('C:\\Users\\' + user + '\\Dropbox\\PhD\\Research\\'
                    'cross_section_fm_pooled_ols\\Latex\\')
    TEX_PATH = DEEP_PATH

    LASSO_PLOT = LASSO_PATH.joinpath('plots')
    DEEP_PLOT = DEEP_PATH.joinpath('plots')
    FM_POLS_PLOT = FM_POLS_PATH.joinpath('plots')
    LASSO_TABLES = LASSO_PATH.joinpath('tables')
    DEEP_TABLES = DEEP_PATH.joinpath('tables')
    FM_POLS_TABLES = FM_POLS_PATH.joinpath('tables')
    PLOT_PATH = TEX_PATH.joinpath('plots')
    TABLE_PATH = TEX_PATH.joinpath('tables')
else:
    database = ROOTS['Linux']
    PLOT_PATH = Path(database).joinpath('plots/')
    TABLE_PATH = Path(database).joinpath('tables/')

SEND_MAILS = False

# Data paths and settings
DB_PATH = Path(database)
MONTHLY_DATA_PATH = DB_PATH.joinpath('MonthlyData')
FF_DATA_PATH = DB_PATH.joinpath('FamaFrench')
RAW_DATA_PATH = DB_PATH.joinpath('RawData')
ID_DATA_PATH = DB_PATH.joinpath('Identifier')
RESULTS_PATH = DB_PATH.joinpath('Results')
SETTINGS_PATH = DB_PATH.joinpath('Settings')
TMP_PATH = DB_PATH.joinpath('tmp')
LOG_PATH = DB_PATH.joinpath('log')

SQL_URL = 'mysql+pymysql://dataUser:dataPassword@192.168.100.123/crsp_compustat'

# File information
GARCH_PATH = MONTHLY_DATA_PATH.joinpath('GarchVolas.csv')
GARCH_PARA = MONTHLY_DATA_PATH.joinpath('GarchParaNormal.csv')
GARCH_PARA_ST = MONTHLY_DATA_PATH.joinpath('GarchParaStudentsT.csv')
DATA_STORE = MONTHLY_DATA_PATH.joinpath('crsp_cs_data_storage.h5')
SIMULATION_SETTINGS = SETTINGS_PATH.joinpath('lasso_simulation.json')
RESULT_META_JS = RESULTS_PATH.joinpath('results_meta_data.json')
RESULT_HDF5 = RESULTS_PATH.joinpath('result_tables.h5')
CRSP_ID = ID_DATA_PATH.joinpath('CRSP_IdentifierInfo_1962-2014.gz')
CRSP_MONTHLY = RAW_DATA_PATH.joinpath('MonthlyCRSP_1962-2015_EXCH.gz')
CS_YEARLY = RAW_DATA_PATH.joinpath('YearlyCOMPUSTAT_1962_201603.gz')
FC_OVERVIEW = RAW_DATA_PATH.joinpath('FCOverview.csv')
SECTOR = RAW_DATA_PATH.joinpath('SectorTable.csv')
LINK_DATA = ID_DATA_PATH.joinpath('CRSP_CS_LinkTable.gz')
FC_FILE = 'firm_characteristics.csv'
EMAIL_CRD = ID_DATA_PATH.joinpath('gmail_codeacc.txt')
FTP_CRD = ID_DATA_PATH.joinpath('kolmi_1.txt')

FILE_NAMES = {'CRSP_ID': 'MonthlyCRSP_1962-2015_EXCH.gz',
              'CRSP_Monthly': 'MonthlyCRSP_1962-2014.gz'}
SUFFIX_RES = 'Result'

# Code paths
UX_CODE_PATH = Path('/mydata/Code/')
UX_DATA_PATH = Path(ROOTS['Linux'])
XX1_CODE_PATH = Path('C:\\Anaconda3\\Lib\\site-packages\\phdlib\\')


